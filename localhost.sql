-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 06, 2020 at 11:10 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aimeos`
--
CREATE DATABASE IF NOT EXISTS `aimeos` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `aimeos`;
--
-- Database: `cosmetics`
--
CREATE DATABASE IF NOT EXISTS `cosmetics` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cosmetics`;

-- --------------------------------------------------------

--
-- Table structure for table `cm_actionscheduler_actions`
--

CREATE TABLE `cm_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_actionscheduler_actions`
--

INSERT INTO `cm_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(12, 'action_scheduler/migration_hook', 'complete', '2020-02-23 07:32:44', '2020-02-23 07:32:44', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1582443164;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1582443164;}', 1, 1, '2020-02-23 07:32:47', '2020-02-23 07:32:47', 0, NULL),
(13, 'wc_admin_unsnooze_admin_notes', 'complete', '2020-02-23 07:32:47', '2020-02-23 07:32:47', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1582443167;s:18:\"\0*\0first_timestamp\";i:1582443167;s:13:\"\0*\0recurrence\";i:3600;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1582443167;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}', 2, 1, '2020-02-23 07:32:47', '2020-02-23 07:32:47', 0, NULL),
(14, 'wc_admin_unsnooze_admin_notes', 'complete', '2020-02-23 08:32:47', '2020-02-23 08:32:47', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1582446767;s:18:\"\0*\0first_timestamp\";i:1582443167;s:13:\"\0*\0recurrence\";i:3600;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1582446767;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}', 2, 1, '2020-02-29 13:25:51', '2020-02-29 13:25:51', 0, NULL),
(15, 'woocommerce_update_marketplace_suggestions', 'complete', '2020-02-23 08:12:16', '2020-02-23 08:12:16', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1582445536;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1582445536;}', 0, 1, '2020-02-23 08:12:51', '2020-02-23 08:12:51', 0, NULL),
(16, 'wc_admin_unsnooze_admin_notes', 'complete', '2020-02-29 14:25:51', '2020-02-29 14:25:51', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1582986351;s:18:\"\0*\0first_timestamp\";i:1582443167;s:13:\"\0*\0recurrence\";i:3600;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1582986351;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}', 2, 1, '2020-02-29 19:25:08', '2020-02-29 19:25:08', 0, NULL),
(17, 'wc_admin_unsnooze_admin_notes', 'complete', '2020-02-29 20:25:08', '2020-02-29 20:25:08', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1583007908;s:18:\"\0*\0first_timestamp\";i:1582443167;s:13:\"\0*\0recurrence\";i:3600;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1583007908;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}', 2, 1, '2020-03-01 06:12:40', '2020-03-01 06:12:40', 0, NULL),
(18, 'wc_admin_unsnooze_admin_notes', 'complete', '2020-03-01 07:12:40', '2020-03-01 07:12:40', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1583046760;s:18:\"\0*\0first_timestamp\";i:1582443167;s:13:\"\0*\0recurrence\";i:3600;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1583046760;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}', 2, 1, '2020-03-01 07:12:44', '2020-03-01 07:12:44', 0, NULL),
(19, 'wc_admin_unsnooze_admin_notes', 'complete', '2020-03-01 08:12:44', '2020-03-01 08:12:44', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1583050364;s:18:\"\0*\0first_timestamp\";i:1582443167;s:13:\"\0*\0recurrence\";i:3600;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1583050364;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}', 2, 1, '2020-03-01 08:17:01', '2020-03-01 08:17:01', 0, NULL),
(20, 'wc_admin_unsnooze_admin_notes', 'complete', '2020-03-01 09:17:01', '2020-03-01 09:17:01', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1583054221;s:18:\"\0*\0first_timestamp\";i:1582443167;s:13:\"\0*\0recurrence\";i:3600;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1583054221;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}', 2, 1, '2020-03-07 07:59:03', '2020-03-07 07:59:03', 0, NULL),
(21, 'wc_admin_unsnooze_admin_notes', 'pending', '2020-03-07 08:59:03', '2020-03-07 08:59:03', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1583571543;s:18:\"\0*\0first_timestamp\";i:1582443167;s:13:\"\0*\0recurrence\";i:3600;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1583571543;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}', 2, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cm_actionscheduler_claims`
--

CREATE TABLE `cm_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_actionscheduler_groups`
--

CREATE TABLE `cm_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_actionscheduler_groups`
--

INSERT INTO `cm_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration'),
(2, 'wc-admin-notes');

-- --------------------------------------------------------

--
-- Table structure for table `cm_actionscheduler_logs`
--

CREATE TABLE `cm_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_actionscheduler_logs`
--

INSERT INTO `cm_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 12, 'action created', '2020-02-23 07:32:44', '2020-02-23 07:32:44'),
(2, 13, 'action created', '2020-02-23 07:32:47', '2020-02-23 07:32:47'),
(3, 12, 'action started via Async Request', '2020-02-23 07:32:47', '2020-02-23 07:32:47'),
(4, 12, 'action complete via Async Request', '2020-02-23 07:32:47', '2020-02-23 07:32:47'),
(5, 13, 'action started via Async Request', '2020-02-23 07:32:47', '2020-02-23 07:32:47'),
(6, 13, 'action complete via Async Request', '2020-02-23 07:32:47', '2020-02-23 07:32:47'),
(7, 14, 'action created', '2020-02-23 07:32:47', '2020-02-23 07:32:47'),
(8, 15, 'action created', '2020-02-23 08:12:16', '2020-02-23 08:12:16'),
(9, 15, 'action started via WP Cron', '2020-02-23 08:12:48', '2020-02-23 08:12:48'),
(10, 15, 'action complete via WP Cron', '2020-02-23 08:12:51', '2020-02-23 08:12:51'),
(11, 14, 'action started via WP Cron', '2020-02-29 13:25:51', '2020-02-29 13:25:51'),
(12, 14, 'action complete via WP Cron', '2020-02-29 13:25:51', '2020-02-29 13:25:51'),
(13, 16, 'action created', '2020-02-29 13:25:51', '2020-02-29 13:25:51'),
(14, 16, 'action started via WP Cron', '2020-02-29 19:25:08', '2020-02-29 19:25:08'),
(15, 16, 'action complete via WP Cron', '2020-02-29 19:25:08', '2020-02-29 19:25:08'),
(16, 17, 'action created', '2020-02-29 19:25:08', '2020-02-29 19:25:08'),
(17, 17, 'action started via Async Request', '2020-03-01 06:12:40', '2020-03-01 06:12:40'),
(18, 17, 'action complete via Async Request', '2020-03-01 06:12:40', '2020-03-01 06:12:40'),
(19, 18, 'action created', '2020-03-01 06:12:40', '2020-03-01 06:12:40'),
(20, 18, 'action started via WP Cron', '2020-03-01 07:12:44', '2020-03-01 07:12:44'),
(21, 18, 'action complete via WP Cron', '2020-03-01 07:12:44', '2020-03-01 07:12:44'),
(22, 19, 'action created', '2020-03-01 07:12:44', '2020-03-01 07:12:44'),
(23, 19, 'action started via WP Cron', '2020-03-01 08:17:01', '2020-03-01 08:17:01'),
(24, 19, 'action complete via WP Cron', '2020-03-01 08:17:01', '2020-03-01 08:17:01'),
(25, 20, 'action created', '2020-03-01 08:17:01', '2020-03-01 08:17:01'),
(26, 20, 'action started via WP Cron', '2020-03-07 07:59:02', '2020-03-07 07:59:02'),
(27, 20, 'action complete via WP Cron', '2020-03-07 07:59:03', '2020-03-07 07:59:03'),
(28, 21, 'action created', '2020-03-07 07:59:03', '2020-03-07 07:59:03');

-- --------------------------------------------------------

--
-- Table structure for table `cm_commentmeta`
--

CREATE TABLE `cm_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_comments`
--

CREATE TABLE `cm_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_links`
--

CREATE TABLE `cm_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_mailchimp_carts`
--

CREATE TABLE `cm_mailchimp_carts` (
  `id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cart` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_mailchimp_jobs`
--

CREATE TABLE `cm_mailchimp_jobs` (
  `id` bigint(20) NOT NULL,
  `obj_id` text COLLATE utf8mb4_unicode_520_ci,
  `job` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_options`
--

CREATE TABLE `cm_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_options`
--

INSERT INTO `cm_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://cosmetic.test', 'yes'),
(2, 'home', 'http://cosmetic.test', 'yes'),
(3, 'blogname', 'JM Cosmetics', 'yes'),
(4, 'blogdescription', 'Branded Cosmetic Products', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'ryandingle09@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:158:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=34&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:62:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$\";s:99:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]\";s:62:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:73:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:12:{i:0;s:45:\"access-demo-importer/access-demo-importer.php\";i:1;s:53:\"facebook-for-woocommerce/facebook-for-woocommerce.php\";i:2;s:19:\"jetpack/jetpack.php\";i:3;s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";i:4;s:32:\"white-label-cms/wlcms-plugin.php\";i:5;s:39:\"woocommerce-admin/woocommerce-admin.php\";i:6;s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";i:7;s:45:\"woocommerce-services/woocommerce-services.php\";i:8;s:27:\"woocommerce/woocommerce.php\";i:9;s:33:\"yith-woocommerce-compare/init.php\";i:10;s:36:\"yith-woocommerce-quick-view/init.php\";i:11;s:34:\"yith-woocommerce-wishlist/init.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:2:{i:0;s:83:\"C:\\Users\\Ryan Dingle\\workspace\\cosmetic/wp-content/themes/zigcy-cosmetics/style.css\";i:1;s:0:\"\";}', 'no'),
(40, 'template', 'zigcy-lite', 'yes'),
(41, 'stylesheet', 'zigcy-cosmetics', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '45805', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:3:{i:1;a:0:{}i:2;a:4:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:1053:\"<b id=\"m_3235503856037811046yMail_cursorElementTracker_1582417221352\"><span id=\"m_3235503856037811046yMail_cursorElementTracker_1582417225242\">JM Cosmetics are licensed.and approved in the Philippines by </span><a id=\"m_3235503856037811046yMail_cursorElementTracker_1582417290052\" title=\"Food and Drug Administration\" href=\"https://en.m.wikipedia.org/wiki/Food_and_Drug_Administration\" target=\"_blank\" rel=\"noopener\" data-saferedirecturl=\"https://www.google.com/url?q=https://en.m.wikipedia.org/wiki/Food_and_Drug_Administration&amp;source=gmail&amp;ust=1583069041356000&amp;usg=AFQjCNEDmA2Mp1kuEIubEiEzBsWyiZ9_4g\">Food and Drug Administration</a><span id=\"m_3235503856037811046yMail_cursorElementTracker_1582417223975\"> (FDA), which regulates cosmetics, they defined our product as \"intended to be applied to the human body for cleansing, beautifying, promoting attractiveness, or altering the appearance without affecting the body\'s structure or functions\".It includes material intended for use as an ingredients of a cosmetic products </span></b>\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:45:\"woocommerce-services/woocommerce-services.php\";a:2:{i:0;s:17:\"WC_Connect_Loader\";i:1;s:16:\"plugin_uninstall\";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '21', 'yes'),
(84, 'page_on_front', '34', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1597994935', 'yes'),
(94, 'initial_db_version', '45805', 'yes'),
(95, 'cm_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:114:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:9:{s:19:\"wp_inactive_widgets\";a:1:{i:0;s:6:\"meta-2\";}s:23:\"zigcy-cosmetics-sidebar\";a:0:{}s:9:\"sidebar-1\";a:8:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:28:\"woocommerce_product_search-1\";i:6;s:22:\"woocommerce_products-1\";i:7;s:32:\"woocommerce_product_categories-1\";}s:9:\"sidebar-2\";a:0:{}s:8:\"footer-1\";a:2:{i:0;s:13:\"media_image-1\";i:1;s:6:\"text-2\";}s:8:\"footer-2\";a:1:{i:0;s:10:\"nav_menu-1\";}s:8:\"footer-3\";a:1:{i:0;s:10:\"nav_menu-2\";}s:8:\"footer-4\";a:1:{i:0;s:10:\"nav_menu-3\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'cron', 'a:18:{i:1583568523;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1583569736;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1583569886;a:1:{s:20:\"jetpack_clean_nonces\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1583569963;a:1:{s:33:\"wc_admin_process_orders_milestone\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1583571543;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1583576998;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1583587798;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1583609336;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1583625600;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1583652535;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1583652547;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1583652548;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1583652598;a:1:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1583652608;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1583652762;a:1:{s:14:\"wc_admin_daily\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1583653507;a:1:{s:34:\"yith_wcwl_delete_expired_wishlists\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1583739058;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:11:\"fifteendays\";s:4:\"args\";a:0:{}s:8:\"interval\";i:1296000;}}}s:7:\"version\";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:2:{i:1;a:15:{s:13:\"attachment_id\";i:399;s:3:\"url\";s:56:\"http://cosmetic.test/wp-content/uploads/2020/02/logo.jpg\";s:5:\"title\";s:0:\"\";s:4:\"size\";s:4:\"full\";s:5:\"width\";i:1125;s:6:\"height\";i:1103;s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:9:\"link_type\";s:6:\"custom\";s:8:\"link_url\";s:0:\"\";s:13:\"image_classes\";s:0:\"\";s:12:\"link_classes\";s:0:\"\";s:8:\"link_rel\";s:0:\"\";s:17:\"link_target_blank\";b:0;s:11:\"image_title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:4:{i:1;a:2:{s:5:\"title\";s:10:\"My Account\";s:8:\"nav_menu\";i:27;}i:2;a:2:{s:5:\"title\";s:5:\"Extra\";s:8:\"nav_menu\";i:28;}i:3;a:2:{s:5:\"title\";s:11:\"Information\";s:8:\"nav_menu\";i:29;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'theme_mods_twentytwenty', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"background_color\";s:3:\"fff\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1582443150;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(116, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.3.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.3.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.3.2\";s:7:\"version\";s:5:\"5.3.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1583567944;s:15:\"version_checked\";s:5:\"5.3.2\";s:12:\"translations\";a:0:{}}', 'no'),
(139, 'can_compress_scripts', '1', 'no'),
(142, 'recently_activated', 'a:0:{}', 'yes'),
(149, 'woocommerce_store_address', 'Quezon City Philippines', 'yes'),
(150, 'woocommerce_store_address_2', 'Quezon City Philippines', 'yes'),
(151, 'woocommerce_store_city', 'Quezon', 'yes'),
(152, 'woocommerce_default_country', 'PH:00', 'yes'),
(153, 'woocommerce_store_postcode', '456', 'yes'),
(154, 'woocommerce_allowed_countries', 'all', 'yes'),
(155, 'woocommerce_all_except_countries', '', 'yes'),
(156, 'woocommerce_specific_allowed_countries', '', 'yes'),
(157, 'woocommerce_ship_to_countries', '', 'yes'),
(158, 'woocommerce_specific_ship_to_countries', '', 'yes'),
(159, 'woocommerce_default_customer_address', 'base', 'yes'),
(160, 'woocommerce_calc_taxes', 'no', 'yes'),
(161, 'woocommerce_enable_coupons', 'yes', 'yes'),
(162, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(163, 'woocommerce_currency', 'PHP', 'yes'),
(164, 'woocommerce_currency_pos', 'left', 'yes'),
(165, 'woocommerce_price_thousand_sep', ',', 'yes'),
(166, 'woocommerce_price_decimal_sep', '.', 'yes'),
(167, 'woocommerce_price_num_decimals', '2', 'yes'),
(168, 'woocommerce_shop_page_id', '5', 'yes'),
(169, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(170, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(171, 'woocommerce_placeholder_image', '6', 'yes'),
(172, 'woocommerce_weight_unit', 'kg', 'yes'),
(173, 'woocommerce_dimension_unit', 'cm', 'yes'),
(174, 'woocommerce_enable_reviews', 'yes', 'yes'),
(175, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(176, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(177, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(178, 'woocommerce_review_rating_required', 'yes', 'no'),
(179, 'woocommerce_manage_stock', 'yes', 'yes'),
(180, 'woocommerce_hold_stock_minutes', '60', 'no'),
(181, 'woocommerce_notify_low_stock', 'yes', 'no'),
(182, 'woocommerce_notify_no_stock', 'yes', 'no'),
(183, 'woocommerce_stock_email_recipient', 'ryandingle09@gmail.com', 'no'),
(184, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(185, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(186, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(187, 'woocommerce_stock_format', '', 'yes'),
(188, 'woocommerce_file_download_method', 'force', 'no'),
(189, 'woocommerce_downloads_require_login', 'no', 'no'),
(190, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(191, 'woocommerce_prices_include_tax', 'no', 'yes'),
(192, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(193, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(194, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(195, 'woocommerce_tax_classes', '', 'yes'),
(196, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(197, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(198, 'woocommerce_price_display_suffix', '', 'yes'),
(199, 'woocommerce_tax_total_display', 'itemized', 'no'),
(200, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(201, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(202, 'woocommerce_ship_to_destination', 'billing', 'no'),
(203, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(204, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(205, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(206, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(207, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(208, 'woocommerce_registration_generate_username', 'yes', 'no'),
(209, 'woocommerce_registration_generate_password', 'yes', 'no'),
(210, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(211, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(212, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(213, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(214, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(215, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(216, 'woocommerce_trash_pending_orders', '', 'no'),
(217, 'woocommerce_trash_failed_orders', '', 'no'),
(218, 'woocommerce_trash_cancelled_orders', '', 'no'),
(219, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(220, 'woocommerce_email_from_name', 'JM Cosmetics', 'no'),
(221, 'woocommerce_email_from_address', 'ryandingle09@gmail.com', 'no'),
(222, 'woocommerce_email_header_image', '', 'no'),
(223, 'woocommerce_email_footer_text', '{site_title} &mdash; Built with {WooCommerce}', 'no'),
(224, 'woocommerce_email_base_color', '#96588a', 'no'),
(225, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(226, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(227, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(228, 'woocommerce_cart_page_id', '117', 'no'),
(229, 'woocommerce_checkout_page_id', '119', 'no'),
(230, 'woocommerce_myaccount_page_id', '121', 'no'),
(231, 'woocommerce_terms_page_id', '', 'no'),
(232, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(233, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(234, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(235, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(236, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(237, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(238, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(239, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(240, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(241, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(242, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(243, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(244, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(245, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(246, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(247, 'woocommerce_api_enabled', 'no', 'yes'),
(248, 'woocommerce_allow_tracking', 'yes', 'no'),
(249, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(250, 'woocommerce_single_image_width', '', 'yes'),
(251, 'woocommerce_thumbnail_image_width', '', 'yes'),
(252, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(253, 'woocommerce_demo_store', 'no', 'no'),
(254, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(255, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(256, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(257, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(259, 'default_product_cat', '15', 'yes'),
(262, 'woocommerce_version', '3.9.2', 'yes'),
(263, 'woocommerce_db_version', '3.9.2', 'yes'),
(264, 'woocommerce_admin_notices', 'a:3:{i:0;s:20:\"no_secure_connection\";i:1;s:8:\"wc_admin\";i:2;s:14:\"template_files\";}', 'yes'),
(265, 'woocommerce_maxmind_geolocation_settings', 'a:1:{s:15:\"database_prefix\";s:32:\"9azP9ArSMewAt2IIn33X3gUrptbsdfXv\";}', 'yes'),
(266, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(267, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(268, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(269, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(270, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(271, 'widget_woocommerce_product_categories', 'a:2:{i:1;a:8:{s:5:\"title\";s:18:\"Product categories\";s:7:\"orderby\";s:4:\"name\";s:8:\"dropdown\";i:0;s:5:\"count\";i:0;s:12:\"hierarchical\";i:1;s:18:\"show_children_only\";i:0;s:10:\"hide_empty\";i:0;s:9:\"max_depth\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(272, 'widget_woocommerce_product_search', 'a:2:{i:1;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(273, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(274, 'widget_woocommerce_products', 'a:2:{i:1;a:7:{s:5:\"title\";s:8:\"Products\";s:6:\"number\";i:4;s:4:\"show\";s:0:\"\";s:7:\"orderby\";s:4:\"date\";s:5:\"order\";s:4:\"desc\";s:9:\"hide_free\";i:0;s:11:\"show_hidden\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(275, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(276, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(277, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(278, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(282, '_transient_as_comment_count', 'O:8:\"stdClass\":7:{s:8:\"approved\";s:1:\"1\";s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(283, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(287, 'woocommerce_setup_ab_wc_admin_onboarding', 'a', 'yes'),
(289, 'woocommerce_obw_last_completed_step', 'activate', 'yes'),
(292, 'woocommerce_product_type', 'both', 'yes'),
(293, 'woocommerce_sell_in_person', '1', 'yes'),
(295, 'woocommerce_tracker_last_send', '1583047932', 'yes'),
(300, 'woocommerce_ppec_paypal_settings', 'a:3:{s:7:\"enabled\";s:3:\"yes\";s:16:\"reroute_requests\";s:3:\"yes\";s:5:\"email\";s:22:\"ryandingle09@gmail.com\";}', 'yes'),
(301, 'woocommerce_cheque_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(302, 'woocommerce_bacs_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(303, 'woocommerce_cod_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(304, 'jetpack_activated', '1', 'yes'),
(307, 'jetpack_activation_source', 'a:2:{i:0;s:7:\"unknown\";i:1;N;}', 'yes'),
(308, 'jetpack_options', 'a:4:{s:7:\"version\";s:16:\"8.2.3:1582443080\";s:11:\"old_version\";s:16:\"8.2.3:1582443080\";s:28:\"fallback_no_verify_ssl_certs\";i:0;s:9:\"time_diff\";i:0;}', 'yes'),
(309, 'jetpack_sync_settings_disable', '0', 'yes'),
(312, 'wc_ppec_version', '1.6.20', 'yes'),
(319, 'do_activate', '0', 'yes'),
(325, '_transient_shipping-transient-version', '1582443131', 'yes'),
(326, '_transient_timeout_jetpack_file_data_8.2.3', '1584948820', 'no');
INSERT INTO `cm_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(327, '_transient_jetpack_file_data_8.2.3', 'a:51:{s:32:\"212a162108f1dc20cc6c768d5b47d4f2\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"d3576702faeb399eb47ad20f586c3804\";a:14:{s:4:\"name\";s:8:\"Carousel\";s:11:\"description\";s:75:\"Display images and galleries in a gorgeous, full-screen browsing experience\";s:4:\"sort\";s:2:\"22\";s:20:\"recommendation_order\";s:2:\"12\";s:10:\"introduced\";s:3:\"1.5\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:17:\"Photos and Videos\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:80:\"gallery, carousel, diaporama, slideshow, images, lightbox, exif, metadata, image\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"55409a5f8388b8d33e2350ef80de3ea3\";a:14:{s:4:\"name\";s:13:\"Comment Likes\";s:11:\"description\";s:64:\"Increase visitor engagement by adding a Like button to comments.\";s:4:\"sort\";s:2:\"39\";s:20:\"recommendation_order\";s:2:\"17\";s:10:\"introduced\";s:3:\"5.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:37:\"like widget, like button, like, likes\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"e914e6d31cb61f5a9ef86e1b9573430e\";a:14:{s:4:\"name\";s:8:\"Comments\";s:11:\"description\";s:81:\"Let visitors use a WordPress.com, Twitter, Facebook, or Google account to comment\";s:4:\"sort\";s:2:\"20\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:53:\"comments, comment, facebook, twitter, google+, social\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"f1b8c61705fb18eb8c8584c9f9cdffd9\";a:14:{s:4:\"name\";s:12:\"Contact Form\";s:11:\"description\";s:81:\"Add a customizable contact form to any post or page using the Jetpack Form Block.\";s:4:\"sort\";s:2:\"15\";s:20:\"recommendation_order\";s:2:\"14\";s:10:\"introduced\";s:3:\"1.3\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:5:\"Other\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:214:\"contact, form, grunion, feedback, submission, contact form, email, feedback, contact form plugin, custom form, custom form plugin, form builder, forms, form maker, survey, contact by jetpack, contact us, forms free\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"4fca6eb23a793155d69fdb119a094926\";a:14:{s:4:\"name\";s:9:\"Copy Post\";s:11:\"description\";s:77:\"Enable the option to copy entire posts and pages, including tags and settings\";s:4:\"sort\";s:2:\"15\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"7.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:15:\"copy, duplicate\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"cfdac01e3c3c529f93a8f49edef1f5db\";a:14:{s:4:\"name\";s:20:\"Custom content types\";s:11:\"description\";s:74:\"Display different types of content on your site with custom content types.\";s:4:\"sort\";s:2:\"34\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"3.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:72:\"cpt, custom post types, portfolio, portfolios, testimonial, testimonials\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"4b9137ecf507290743735fb1f94535df\";a:14:{s:4:\"name\";s:10:\"Custom CSS\";s:11:\"description\";s:88:\"Adds options for CSS preprocessor use, disabling the theme\'s CSS, or custom image width.\";s:4:\"sort\";s:1:\"2\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.7\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:10:\"Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:108:\"css, customize, custom, style, editor, less, sass, preprocessor, font, mobile, appearance, theme, stylesheet\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"95d75b38d76d2ee1b5b537026eadb8ff\";a:14:{s:4:\"name\";s:21:\"Enhanced Distribution\";s:11:\"description\";s:27:\"Increase reach and traffic.\";s:4:\"sort\";s:1:\"5\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:6:\"Public\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:54:\"google, seo, firehose, search, broadcast, broadcasting\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"f1bb571a95c5de1e6adaf9db8567c039\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"822f9ef1281dace3fb7cc420c77d24e0\";a:14:{s:4:\"name\";s:16:\"Google Analytics\";s:11:\"description\";s:56:\"Set up Google Analytics without touching a line of code.\";s:4:\"sort\";s:2:\"37\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"4.5\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:37:\"webmaster, google, analytics, console\";s:12:\"plan_classes\";s:17:\"business, premium\";}s:32:\"c167275f926ef0eefaec9a679bd88d34\";a:14:{s:4:\"name\";s:19:\"Gravatar Hovercards\";s:11:\"description\";s:58:\"Enable pop-up business cards over commenters’ Gravatars.\";s:4:\"sort\";s:2:\"11\";s:20:\"recommendation_order\";s:2:\"13\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:18:\"Social, Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:20:\"gravatar, hovercards\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"58cbd4585a74829a1c88aa9c295f3993\";a:14:{s:4:\"name\";s:15:\"Infinite Scroll\";s:11:\"description\";s:53:\"Automatically load new content when a visitor scrolls\";s:4:\"sort\";s:2:\"26\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:10:\"Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:33:\"scroll, infinite, infinite scroll\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"d4a35eabc948caefad71a0d3303b95c8\";a:14:{s:4:\"name\";s:8:\"JSON API\";s:11:\"description\";s:51:\"Allow applications to securely access your content.\";s:4:\"sort\";s:2:\"19\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.9\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:6:\"Public\";s:11:\"module_tags\";s:19:\"Writing, Developers\";s:7:\"feature\";s:7:\"General\";s:25:\"additional_search_queries\";s:50:\"api, rest, develop, developers, json, klout, oauth\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"7b0c670bc3f8209dc83abb8610e23a89\";a:14:{s:4:\"name\";s:14:\"Beautiful Math\";s:11:\"description\";s:74:\"Use the LaTeX markup language to write mathematical equations and formulas\";s:4:\"sort\";s:2:\"12\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:47:\"latex, math, equation, equations, formula, code\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"b00e4e6c109ce6f77b5c83fbaaaead4c\";a:14:{s:4:\"name\";s:11:\"Lazy Images\";s:11:\"description\";s:137:\"Speed up your site and create a smoother viewing experience by loading images as visitors scroll down the screen, instead of all at once.\";s:4:\"sort\";s:2:\"24\";s:20:\"recommendation_order\";s:2:\"14\";s:10:\"introduced\";s:5:\"5.6.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:23:\"Appearance, Recommended\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:150:\"mobile, theme, fast images, fast image, image, lazy, lazy load, lazyload, images, lazy images, thumbnail, image lazy load, lazy loading, load, loading\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"8e46c72906c928eca634ac2c8b1bc84f\";a:14:{s:4:\"name\";s:5:\"Likes\";s:11:\"description\";s:63:\"Give visitors an easy way to show they appreciate your content.\";s:4:\"sort\";s:2:\"23\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:26:\"like, likes, wordpress.com\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"2df2264a07aff77e0556121e33349dce\";a:14:{s:4:\"name\";s:8:\"Markdown\";s:11:\"description\";s:50:\"Write posts or pages in plain-text Markdown syntax\";s:4:\"sort\";s:2:\"31\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.8\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:12:\"md, markdown\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"0337eacae47d30c946cb9fc4e5ece649\";a:14:{s:4:\"name\";s:21:\"WordPress.com Toolbar\";s:11:\"description\";s:91:\"Replaces the admin bar with a useful toolbar to quickly manage your site via WordPress.com.\";s:4:\"sort\";s:2:\"38\";s:20:\"recommendation_order\";s:2:\"16\";s:10:\"introduced\";s:3:\"4.8\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"General\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:19:\"adminbar, masterbar\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"cb5d81445061b89d19cb9c7754697a39\";a:14:{s:4:\"name\";s:12:\"Mobile Theme\";s:11:\"description\";s:31:\"Enable the Jetpack Mobile theme\";s:4:\"sort\";s:2:\"21\";s:20:\"recommendation_order\";s:2:\"11\";s:10:\"introduced\";s:3:\"1.8\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:31:\"Appearance, Mobile, Recommended\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:24:\"mobile, theme, minileven\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"ea0fbbd64080c81a90a784924603588c\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"5c53fdb3633ba3232f60180116900273\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"40b97d9ce396339d3e8e46b833a045b5\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"0739df64747f2d02c140f23ce6c19cd8\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"c54bb0a65b39f1316da8632197a88a4e\";a:14:{s:4:\"name\";s:7:\"Monitor\";s:11:\"description\";s:118:\"Jetpack’s downtime monitoring will continuously watch your site, and alert you the moment that downtime is detected.\";s:4:\"sort\";s:2:\"28\";s:20:\"recommendation_order\";s:2:\"10\";s:10:\"introduced\";s:3:\"2.6\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:11:\"Recommended\";s:7:\"feature\";s:8:\"Security\";s:25:\"additional_search_queries\";s:123:\"monitor, uptime, downtime, monitoring, maintenance, maintenance mode, offline, site is down, site down, down, repair, error\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"cc013f4c5480c7bdc1e7edb2f410bf3c\";a:14:{s:4:\"name\";s:13:\"Notifications\";s:11:\"description\";s:57:\"Receive instant notifications of site comments and likes.\";s:4:\"sort\";s:2:\"13\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.9\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:5:\"Other\";s:7:\"feature\";s:7:\"General\";s:25:\"additional_search_queries\";s:62:\"notification, notifications, toolbar, adminbar, push, comments\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"b3b34928b1e549bb52f866accc0450c5\";a:14:{s:4:\"name\";s:9:\"Asset CDN\";s:11:\"description\";s:154:\"Jetpack’s Site Accelerator loads your site faster by optimizing your images and serving your images and static files from our global network of servers.\";s:4:\"sort\";s:2:\"26\";s:20:\"recommendation_order\";s:1:\"1\";s:10:\"introduced\";s:3:\"6.6\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:42:\"Photos and Videos, Appearance, Recommended\";s:7:\"feature\";s:23:\"Recommended, Appearance\";s:25:\"additional_search_queries\";s:160:\"site accelerator, accelerate, static, assets, javascript, css, files, performance, cdn, bandwidth, content delivery network, pagespeed, combine js, optimize css\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"714284944f56d6936a40f3309900bc8e\";a:14:{s:4:\"name\";s:9:\"Image CDN\";s:11:\"description\";s:141:\"Mirrors and serves your images from our free and fast image CDN, improving your site’s performance with no additional load on your servers.\";s:4:\"sort\";s:2:\"25\";s:20:\"recommendation_order\";s:1:\"1\";s:10:\"introduced\";s:3:\"2.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:42:\"Photos and Videos, Appearance, Recommended\";s:7:\"feature\";s:23:\"Recommended, Appearance\";s:25:\"additional_search_queries\";s:171:\"photon, photo cdn, image cdn, speed, compression, resize, responsive images, responsive, content distribution network, optimize, page speed, image optimize, photon jetpack\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"348754bc914ee02c72d9af445627784c\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"041704e207c4c59eea93e0499c908bff\";a:14:{s:4:\"name\";s:13:\"Post by email\";s:11:\"description\";s:33:\"Publish posts by sending an email\";s:4:\"sort\";s:2:\"14\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:20:\"post by email, email\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"26e6cb3e08a6cfd0811c17e7c633c72c\";a:14:{s:4:\"name\";s:7:\"Protect\";s:11:\"description\";s:151:\"Enabling brute force protection will prevent bots and hackers from attempting to log in to your website with common username and password combinations.\";s:4:\"sort\";s:1:\"1\";s:20:\"recommendation_order\";s:1:\"4\";s:10:\"introduced\";s:3:\"3.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:11:\"Recommended\";s:7:\"feature\";s:8:\"Security\";s:25:\"additional_search_queries\";s:173:\"security, jetpack protect, secure, protection, botnet, brute force, protect, login, bot, password, passwords, strong passwords, strong password, wp-login.php,  protect admin\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"915a504082f797395713fd01e0e2e713\";a:14:{s:4:\"name\";s:9:\"Publicize\";s:11:\"description\";s:128:\"Publicize makes it easy to share your site’s posts on several social media networks automatically when you publish a new post.\";s:4:\"sort\";s:2:\"10\";s:20:\"recommendation_order\";s:1:\"7\";s:10:\"introduced\";s:3:\"2.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:19:\"Social, Recommended\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:220:\"facebook, jetpack publicize, twitter, tumblr, linkedin, social, tweet, connections, sharing, social media, automated, automated sharing, auto publish, auto tweet and like, auto tweet, facebook auto post, facebook posting\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"a7b21cc562ee9ffa357bba19701fe45b\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"9243c1a718566213f4eaf3b44cf14b07\";a:14:{s:4:\"name\";s:13:\"Related posts\";s:11:\"description\";s:113:\"Keep visitors engaged on your blog by highlighting relevant and new content at the bottom of each published post.\";s:4:\"sort\";s:2:\"29\";s:20:\"recommendation_order\";s:1:\"9\";s:10:\"introduced\";s:3:\"2.9\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:11:\"Recommended\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:360:\"related, jetpack related posts, related posts for wordpress, related posts, popular posts, popular, related content, related post, contextual, context, contextual related posts, related articles, similar posts, easy related posts, related page, simple related posts, free related posts, related thumbnails, similar, engagement, yet another related posts plugin\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"583e4cda5596ee1b28a19cde33f438be\";a:14:{s:4:\"name\";s:6:\"Search\";s:11:\"description\";s:87:\"Enhanced search, powered by Elasticsearch, a powerful replacement for WordPress search.\";s:4:\"sort\";s:2:\"34\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"5.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:5:\"false\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:6:\"Search\";s:25:\"additional_search_queries\";s:110:\"search, elastic, elastic search, elasticsearch, fast search, search results, search performance, google search\";s:12:\"plan_classes\";s:8:\"business\";}s:32:\"15346c1f7f2a5f29d34378774ecfa830\";a:14:{s:4:\"name\";s:9:\"SEO Tools\";s:11:\"description\";s:50:\"Better results on search engines and social media.\";s:4:\"sort\";s:2:\"35\";s:20:\"recommendation_order\";s:2:\"15\";s:10:\"introduced\";s:3:\"4.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:18:\"Social, Appearance\";s:7:\"feature\";s:7:\"Traffic\";s:25:\"additional_search_queries\";s:81:\"search engine optimization, social preview, meta description, custom title format\";s:12:\"plan_classes\";s:17:\"business, premium\";}s:32:\"72a0ff4cfae86074a7cdd2dcd432ef11\";a:14:{s:4:\"name\";s:7:\"Sharing\";s:11:\"description\";s:120:\"Add Twitter, Facebook and Google+ buttons at the bottom of each post, making it easy for visitors to share your content.\";s:4:\"sort\";s:1:\"7\";s:20:\"recommendation_order\";s:1:\"6\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:3:\"1.2\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:19:\"Social, Recommended\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:229:\"share, sharing, sharedaddy, social buttons, buttons, share facebook, share twitter, social media sharing, social media share, social share, icons, email, facebook, twitter, linkedin, pinterest, pocket, social widget, social media\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"bb8c6c190aaec212a7ab6e940165af4d\";a:14:{s:4:\"name\";s:16:\"Shortcode Embeds\";s:11:\"description\";s:177:\"Shortcodes are WordPress-specific markup that let you add media from popular sites. This feature is no longer necessary as the editor now handles media embeds rather gracefully.\";s:4:\"sort\";s:1:\"3\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:3:\"1.2\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:46:\"Photos and Videos, Social, Writing, Appearance\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:236:\"shortcodes, shortcode, embeds, media, bandcamp, dailymotion, facebook, flickr, google calendars, google maps, google+, polldaddy, recipe, recipes, scribd, slideshare, slideshow, slideshows, soundcloud, ted, twitter, vimeo, vine, youtube\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"1abd31fe07ae4fb0f8bb57dc24592219\";a:14:{s:4:\"name\";s:16:\"WP.me Shortlinks\";s:11:\"description\";s:82:\"Generates shorter links so you can have more space to write on social media sites.\";s:4:\"sort\";s:1:\"8\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:17:\"shortlinks, wp.me\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"cae5f097f8d658e0b0ae50733d7c6476\";a:14:{s:4:\"name\";s:8:\"Sitemaps\";s:11:\"description\";s:50:\"Make it easy for search engines to find your site.\";s:4:\"sort\";s:2:\"13\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"3.9\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:20:\"Recommended, Traffic\";s:7:\"feature\";s:11:\"Recommended\";s:25:\"additional_search_queries\";s:39:\"sitemap, traffic, search, site map, seo\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"e9b8318133b2f95e7906cedb3557a87d\";a:14:{s:4:\"name\";s:14:\"Secure Sign On\";s:11:\"description\";s:63:\"Allow users to log in to this site using WordPress.com accounts\";s:4:\"sort\";s:2:\"30\";s:20:\"recommendation_order\";s:1:\"5\";s:10:\"introduced\";s:3:\"2.6\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:10:\"Developers\";s:7:\"feature\";s:8:\"Security\";s:25:\"additional_search_queries\";s:51:\"sso, single sign on, login, log in, 2fa, two-factor\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"17e66a12031ccf11d8d45ceee0955f05\";a:14:{s:4:\"name\";s:10:\"Site Stats\";s:11:\"description\";s:44:\"Collect valuable traffic stats and insights.\";s:4:\"sort\";s:1:\"1\";s:20:\"recommendation_order\";s:1:\"2\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:23:\"Site Stats, Recommended\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:54:\"statistics, tracking, analytics, views, traffic, stats\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"346cf9756e7c1252acecb9a8ca81a21c\";a:14:{s:4:\"name\";s:13:\"Subscriptions\";s:11:\"description\";s:58:\"Let visitors subscribe to new posts and comments via email\";s:4:\"sort\";s:1:\"9\";s:20:\"recommendation_order\";s:1:\"8\";s:10:\"introduced\";s:3:\"1.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:74:\"subscriptions, subscription, email, follow, followers, subscribers, signup\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"4f84d218792a6efa06ed6feae09c4dd5\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"ca086af79d0d9dccacc934ccff5b4fd7\";a:14:{s:4:\"name\";s:15:\"Tiled Galleries\";s:11:\"description\";s:61:\"Display image galleries in a variety of elegant arrangements.\";s:4:\"sort\";s:2:\"24\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:17:\"Photos and Videos\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:43:\"gallery, tiles, tiled, grid, mosaic, images\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"43c24feb7c541c376af93e0251c1a261\";a:14:{s:4:\"name\";s:20:\"Backups and Scanning\";s:11:\"description\";s:100:\"Protect your site with daily or real-time backups and automated virus scanning and threat detection.\";s:4:\"sort\";s:2:\"32\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:5:\"0:1.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:5:\"false\";s:4:\"free\";s:5:\"false\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:16:\"Security, Health\";s:25:\"additional_search_queries\";s:386:\"backup, cloud backup, database backup, restore, wordpress backup, backup plugin, wordpress backup plugin, back up, backup wordpress, backwpup, vaultpress, backups, off-site backups, offsite backup, offsite, off-site, antivirus, malware scanner, security, virus, viruses, prevent viruses, scan, anti-virus, antimalware, protection, safe browsing, malware, wp security, wordpress security\";s:12:\"plan_classes\";s:27:\"personal, business, premium\";}s:32:\"b9396d8038fc29140b499098d2294d79\";a:14:{s:4:\"name\";s:17:\"Site verification\";s:11:\"description\";s:58:\"Establish your site\'s authenticity with external services.\";s:4:\"sort\";s:2:\"33\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"3.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:56:\"webmaster, seo, google, bing, pinterest, search, console\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"afe184082e106c1bdfe1ee844f98aef3\";a:14:{s:4:\"name\";s:10:\"VideoPress\";s:11:\"description\";s:101:\"Save on hosting storage and bandwidth costs by streaming fast, ad-free video from our global network.\";s:4:\"sort\";s:2:\"27\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.5\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:5:\"false\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:17:\"Photos and Videos\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:118:\"video, videos, videopress, video gallery, video player, videoplayer, mobile video, vimeo, youtube, html5 video, stream\";s:12:\"plan_classes\";s:17:\"business, premium\";}s:32:\"44637d43460370af9a1b31ce3ccec0cd\";a:14:{s:4:\"name\";s:17:\"Widget Visibility\";s:11:\"description\";s:42:\"Control where widgets appear on your site.\";s:4:\"sort\";s:2:\"17\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:10:\"Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:54:\"widget visibility, logic, conditional, widgets, widget\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"694c105a5c3b659acfcddad220048d08\";a:14:{s:4:\"name\";s:21:\"Extra Sidebar Widgets\";s:11:\"description\";s:49:\"Provides additional widgets for use on your site.\";s:4:\"sort\";s:1:\"4\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:18:\"Social, Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:65:\"widget, widgets, facebook, gallery, twitter, gravatar, image, rss\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"ae15da72c5802d72f320640bad669561\";a:14:{s:4:\"name\";s:3:\"Ads\";s:11:\"description\";s:60:\"Earn income by allowing Jetpack to display high quality ads.\";s:4:\"sort\";s:1:\"1\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:5:\"4.5.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:19:\"Traffic, Appearance\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:26:\"advertising, ad codes, ads\";s:12:\"plan_classes\";s:17:\"premium, business\";}}', 'no'),
(328, 'jetpack_available_modules', 'a:1:{s:5:\"8.2.3\";a:42:{s:8:\"carousel\";s:3:\"1.5\";s:13:\"comment-likes\";s:3:\"5.1\";s:8:\"comments\";s:3:\"1.4\";s:12:\"contact-form\";s:3:\"1.3\";s:9:\"copy-post\";s:3:\"7.0\";s:20:\"custom-content-types\";s:3:\"3.1\";s:10:\"custom-css\";s:3:\"1.7\";s:21:\"enhanced-distribution\";s:3:\"1.2\";s:16:\"google-analytics\";s:3:\"4.5\";s:19:\"gravatar-hovercards\";s:3:\"1.1\";s:15:\"infinite-scroll\";s:3:\"2.0\";s:8:\"json-api\";s:3:\"1.9\";s:5:\"latex\";s:3:\"1.1\";s:11:\"lazy-images\";s:5:\"5.6.0\";s:5:\"likes\";s:3:\"2.2\";s:8:\"markdown\";s:3:\"2.8\";s:9:\"masterbar\";s:3:\"4.8\";s:9:\"minileven\";s:3:\"1.8\";s:7:\"monitor\";s:3:\"2.6\";s:5:\"notes\";s:3:\"1.9\";s:10:\"photon-cdn\";s:3:\"6.6\";s:6:\"photon\";s:3:\"2.0\";s:13:\"post-by-email\";s:3:\"2.0\";s:7:\"protect\";s:3:\"3.4\";s:9:\"publicize\";s:3:\"2.0\";s:13:\"related-posts\";s:3:\"2.9\";s:6:\"search\";s:3:\"5.0\";s:9:\"seo-tools\";s:3:\"4.4\";s:10:\"sharedaddy\";s:3:\"1.1\";s:10:\"shortcodes\";s:3:\"1.1\";s:10:\"shortlinks\";s:3:\"1.1\";s:8:\"sitemaps\";s:3:\"3.9\";s:3:\"sso\";s:3:\"2.6\";s:5:\"stats\";s:3:\"1.1\";s:13:\"subscriptions\";s:3:\"1.2\";s:13:\"tiled-gallery\";s:3:\"2.1\";s:10:\"vaultpress\";s:5:\"0:1.2\";s:18:\"verification-tools\";s:3:\"3.0\";s:10:\"videopress\";s:3:\"2.5\";s:17:\"widget-visibility\";s:3:\"2.4\";s:7:\"widgets\";s:3:\"1.2\";s:7:\"wordads\";s:5:\"4.5.0\";}}', 'yes'),
(329, 'woocommerce_flat_rate_1_settings', 'a:3:{s:5:\"title\";s:9:\"Flat rate\";s:10:\"tax_status\";s:7:\"taxable\";s:4:\"cost\";s:3:\"200\";}', 'yes'),
(330, 'woocommerce_flat_rate_2_settings', 'a:3:{s:5:\"title\";s:9:\"Flat rate\";s:10:\"tax_status\";s:7:\"taxable\";s:4:\"cost\";s:3:\"500\";}', 'yes'),
(331, 'mailchimp_woocommerce_plugin_do_activation_redirect', '', 'yes'),
(336, 'current_theme', 'Zigcy Cosmetics', 'yes'),
(337, 'theme_mods_storefront', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1582443369;s:4:\"data\";a:7:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:8:\"header-1\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:8:\"footer-4\";a:0:{}}}}', 'yes'),
(338, 'theme_switched', '', 'yes'),
(339, 'mailchimp_woocommerce_version', '2.3.2', 'no'),
(340, 'mailchimp-woocommerce', 'a:2:{s:33:\"woocommerce_settings_save_general\";b:1;s:19:\"store_currency_code\";s:3:\"PHP\";}', 'yes'),
(342, 'mailchimp-woocommerce-store_id', '5e522a91de0a2', 'yes'),
(343, 'action_scheduler_hybrid_store_demarkation', '11', 'yes'),
(344, 'schema-ActionScheduler_StoreSchema', '3.0.1582443163', 'yes'),
(345, 'schema-ActionScheduler_LoggerSchema', '2.0.1582443163', 'yes'),
(346, 'woocommerce_onboarding_opt_in', 'no', 'yes'),
(350, 'woocommerce_catalog_rows', '4', 'yes'),
(351, 'woocommerce_catalog_columns', '3', 'yes'),
(352, 'woocommerce_maybe_regenerate_images_hash', 'ea5fbada23a74c15c9dc2808416d614b', 'yes'),
(354, 'storefront_nux_fresh_site', '0', 'yes'),
(357, 'woocommerce_admin_version', '0.25.1', 'yes'),
(358, 'woocommerce_admin_install_timestamp', '1582443164', 'yes'),
(360, 'woocommerce_onboarding_profile', 'a:0:{}', 'yes'),
(361, 'woocommerce_admin_last_orders_milestone', '0', 'yes'),
(367, 'mailchimp_woocommerce_db_mailchimp_carts', '1', 'no'),
(368, 'mailchimp-woocommerce_cart_table_add_index_update', '1', 'yes'),
(369, 'action_scheduler_lock_async-request-runner', '1583568015', 'yes'),
(370, 'action_scheduler_migration_status', 'complete', 'yes'),
(374, 'jetpack_tos_agreed', '1', 'yes'),
(375, 'jetpack_secrets', 'a:1:{s:18:\"jetpack_register_1\";a:3:{s:8:\"secret_1\";s:32:\"Yyaj8UhISkn6GNWaBtFXRm2t5PzwEI6w\";s:8:\"secret_2\";s:32:\"eJz4e14PTjEKfp6tqk1JDjuDKjs9ZKvC\";s:3:\"exp\";i:1582443775;}}', 'no'),
(376, '_transient_jetpack_assumed_site_creation_date', '2020-02-23 07:28:55', 'yes'),
(377, 'wc_connect_options', 'a:1:{s:12:\"tos_accepted\";b:1;}', 'yes'),
(379, 'jetpack_ab_connect_banner_green_bar', 'a', 'yes'),
(388, 'jetpack_log', 'a:2:{i:0;a:4:{s:4:\"time\";i:1582443211;s:7:\"user_id\";i:1;s:7:\"blog_id\";b:0;s:4:\"code\";s:8:\"register\";}i:1;a:4:{s:4:\"time\";i:1582443218;s:7:\"user_id\";i:1;s:7:\"blog_id\";b:0;s:4:\"code\";s:8:\"register\";}}', 'no'),
(389, 'sharing-options', 'a:1:{s:6:\"global\";a:5:{s:12:\"button_style\";s:9:\"icon-text\";s:13:\"sharing_label\";s:11:\"Share this:\";s:10:\"open_links\";s:4:\"same\";s:4:\"show\";a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}s:6:\"custom\";a:0:{}}}', 'yes'),
(390, 'stats_options', 'a:7:{s:9:\"admin_bar\";b:1;s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:11:\"count_roles\";a:0:{}s:7:\"blog_id\";b:0;s:12:\"do_not_track\";b:1;s:10:\"hide_smile\";b:1;s:7:\"version\";s:1:\"9\";}', 'yes'),
(392, '_transient_timeout_wc_shipping_method_count_legacy', '1585035232', 'no'),
(393, '_transient_wc_shipping_method_count_legacy', 'a:2:{s:7:\"version\";s:10:\"1582443131\";s:5:\"value\";i:2;}', 'no'),
(401, 'theme_mods_zigcy-cosmetics', 'a:82:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1582443713;s:4:\"data\";a:7:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:8:\"header-1\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:8:\"footer-4\";a:0:{}}}s:18:\"nav_menu_locations\";a:2:{s:6:\"menu-1\";s:6:\"Menu 1\";s:11:\"footer_menu\";i:30;}s:8:\"template\";s:10:\"zigcy-lite\";s:4:\"mods\";a:69:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:25;}s:18:\"custom_css_post_id\";i:-1;s:26:\"zigcy_lite_top_header_text\";s:38:\"Get 50% Discount on all summer arrival\";s:21:\"zigcy_lite_contact_no\";s:0:\"\";s:30:\"zigcy_lite_social_icons_enable\";s:2:\"on\";s:25:\"zigcy_lite_social_new_tab\";i:1;s:23:\"zigcy_lite_facebook_url\";s:1:\"#\";s:22:\"zigcy_lite_twitter_url\";s:1:\"#\";s:22:\"zigcy_lite_youtube_url\";s:1:\"#\";s:24:\"zigcy_lite_pinterest_url\";s:1:\"#\";s:24:\"zigcy_lite_instagram_url\";s:1:\"#\";s:16:\"header_textcolor\";s:5:\"blank\";s:24:\"zigcy_lite_slider_enable\";s:2:\"on\";s:26:\"zigcy_lite_slider_category\";s:6:\"slider\";s:23:\"zigcy_lite_promo_enable\";s:2:\"on\";s:25:\"zigcy_lite_area_one_image\";s:103:\"http://demo.accesspressthemes.com/zigcy-lite/demo-one/wp-content/uploads/sites/2/2019/01/2019-01-17.png\";s:25:\"zigcy_lite_area_two_image\";s:103:\"http://demo.accesspressthemes.com/zigcy-lite/demo-one/wp-content/uploads/sites/2/2018/11/2019-03-22.jpg\";s:25:\"zigcy_lite_area_one_title\";s:15:\"Mid Season Sale\";s:28:\"zigcy_lite_area_one_subtitle\";s:19:\"SALES UP TO 25% OFF\";s:31:\"zigcy_lite_area_one_price_title\";s:15:\"Starting At $20\";s:25:\"zigcy_lite_area_two_title\";s:14:\"Autumn Fashion\";s:28:\"zigcy_lite_area_two_subtitle\";s:21:\"NEW ARRIVAL FALL 2018\";s:31:\"zigcy_lite_area_two_button_text\";s:8:\"Shop Now\";s:31:\"zigcy_lite_area_two_button_link\";s:1:\"#\";s:25:\"zigcy_lite_pro_cat_enable\";s:2:\"on\";s:33:\"zigcy_lite_product_categories_one\";i:31;s:33:\"zigcy_lite_product_categories_two\";i:30;s:35:\"zigcy_lite_product_categories_three\";i:32;s:31:\"zigcy_lite_feat_prod_cat_enable\";s:2:\"on\";s:32:\"zigcy_lite_feature_product_title\";s:11:\"MENS STYLES\";s:35:\"zigcy_lite_feature_product_subtitle\";s:34:\"GET SALE ALERTS ON STYLES YOU LOVE\";s:37:\"zigcy_lite_feature_product_categories\";i:19;s:32:\"zigcy_lite_sml_feat_pro_per_page\";i:8;s:35:\"zigcy_lite_sml_feat_pro_show_rating\";i:1;s:21:\"zigcy_lite_cta_enable\";s:2:\"on\";s:23:\"zigcy_lite_cta_bg_image\";s:94:\"http://demo.accesspressthemes.com/zigcy-lite/wp-content/uploads/2018/11/home1-large-banner.jpg\";s:20:\"zigcy_lite_cta_title\";s:21:\"Seasonal Sale 70% Off\";s:23:\"zigcy_lite_cta_subtitle\";s:29:\"GRAB YOUR FASHION TREND TODAY\";s:22:\"zigcy_lite_price_title\";s:15:\"$45.00 - $90.00\";s:32:\"zigcy_lite_latest_cat_prod_title\";s:16:\"Mens Accessories\";s:35:\"zigcy_lite_latest_cat_prod_subtitle\";s:21:\"In Store Availability\";s:36:\"zigcy_lite_latest_product_categories\";i:20;s:34:\"zigcy_lite_sml_lat_pro_show_rating\";i:1;s:33:\"zigcy_lite_latest_cat_prod_enable\";s:2:\"on\";s:31:\"zigcy_lite_sml_lat_pro_per_page\";i:9;s:23:\"zigcy_lite_footer_image\";s:81:\"http://demo.accesspressthemes.com/zigcy-lite/wp-content/uploads/2018/11/aaa-1.png\";s:27:\"zigcy_lite_footer_copyright\";s:7:\"© 2018\";s:31:\"sml_single_page_layout_sidebars\";s:10:\"no-sidebar\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1548749563;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:8:\"footer-1\";a:2:{i:0;s:13:\"media_image-2\";i:1;s:6:\"text-2\";}s:8:\"footer-2\";a:1:{i:0;s:10:\"nav_menu-3\";}s:8:\"footer-3\";a:1:{i:0;s:10:\"nav_menu-2\";}}}s:22:\"zigcy_lite_slider_type\";s:3:\"cat\";s:29:\"zigcy_lite_frontpage_sections\";a:8:{i:0;s:26:\"zigcy_lite_pro_cat_setting\";i:1;s:34:\"zigcy_lite_prod_cat_slider_setting\";i:2;s:32:\"zigcy_lite_feat_prod_cat_setting\";i:3;s:22:\"zigcy_lite_cta_setting\";i:4;s:31:\"zigcy_lite_lat_prod_cat_setting\";i:5;s:31:\"zigcy_lite_prod_tab_cat_setting\";i:6;s:23:\"zigcy_lite_blog_setting\";i:7;s:25:\"zigcy_lite_client_setting\";}s:24:\"zigcy_lite_client_enable\";s:2:\"on\";s:28:\"zigcy_lite_client_categories\";i:17;s:32:\"zigcy_lite_pro_cat_slider_enable\";s:3:\"off\";s:36:\"zigcy_lite_pro_cat_slider_categories\";i:19;s:31:\"zigcy_lite_pro_cat_slider_title\";s:14:\"Latest Fashion\";s:34:\"zigcy_lite_pro_cat_slider_subtitle\";s:12:\"What we need\";s:26:\"zigcy_lite_pro_cat_sl_prod\";i:6;s:22:\"zigcy_lite_blog_enable\";s:2:\"on\";s:21:\"zigcy_lite_blog_title\";s:12:\"Fashion Blog\";s:24:\"zigcy_lite_blog_subtitle\";s:11:\"what we see\";s:26:\"zigcy_lite_blog_categories\";i:16;s:30:\"zigcy_lite_post_excerpt_length\";i:98;s:21:\"zigcy_lite_call_title\";s:0:\"\";s:11:\"custom_logo\";i:344;s:22:\"zigcy_lite_exclude_cat\";s:8:\"26,17,1,\";s:25:\"sml_archive_post_excerpts\";i:300;s:22:\"zigcy_lite_cart_enable\";s:2:\"on\";}s:7:\"options\";a:20:{s:22:\"woocommerce_demo_store\";s:2:\"no\";s:29:\"woocommerce_demo_store_notice\";s:79:\"This is a demo store for testing purposes &mdash; no orders shall be fulfilled.\";s:29:\"woocommerce_shop_page_display\";s:0:\"\";s:36:\"woocommerce_category_archive_display\";s:0:\"\";s:35:\"woocommerce_default_catalog_orderby\";s:10:\"menu_order\";s:30:\"woocommerce_single_image_width\";s:3:\"600\";s:33:\"woocommerce_thumbnail_image_width\";s:3:\"300\";s:30:\"woocommerce_thumbnail_cropping\";s:3:\"1:1\";s:43:\"woocommerce_thumbnail_cropping_custom_width\";s:1:\"4\";s:44:\"woocommerce_thumbnail_cropping_custom_height\";s:1:\"3\";s:34:\"woocommerce_checkout_company_field\";s:8:\"optional\";s:36:\"woocommerce_checkout_address_2_field\";s:8:\"optional\";s:32:\"woocommerce_checkout_phone_field\";s:8:\"required\";s:46:\"woocommerce_checkout_highlight_required_fields\";s:3:\"yes\";s:55:\"woocommerce_checkout_terms_and_conditions_checkbox_text\";s:44:\"I have read and agree to the website [terms]\";s:40:\"woocommerce_checkout_privacy_policy_text\";s:161:\"Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].\";s:26:\"wp_page_for_privacy_policy\";s:1:\"3\";s:25:\"woocommerce_terms_page_id\";s:0:\"\";s:9:\"site_icon\";s:1:\"0\";s:23:\"nav_menus_created_posts\";a:0:{}}s:6:\"wp_css\";s:0:\"\";s:24:\"zigcy_lite_slider_enable\";s:2:\"on\";s:26:\"zigcy_lite_slider_category\";s:6:\"slider\";s:25:\"zigcy_lite_pro_cat_enable\";s:3:\"off\";s:33:\"zigcy_lite_product_categories_one\";i:31;s:33:\"zigcy_lite_product_categories_two\";i:30;s:35:\"zigcy_lite_product_categories_three\";i:32;s:29:\"zigcy_lite_frontpage_sections\";a:8:{i:0;s:26:\"zigcy_lite_pro_cat_setting\";i:1;s:34:\"zigcy_lite_prod_cat_slider_setting\";i:2;s:32:\"zigcy_lite_feat_prod_cat_setting\";i:3;s:22:\"zigcy_lite_cta_setting\";i:4;s:31:\"zigcy_lite_lat_prod_cat_setting\";i:5;s:31:\"zigcy_lite_prod_tab_cat_setting\";i:6;s:23:\"zigcy_lite_blog_setting\";i:7;s:25:\"zigcy_lite_client_setting\";}i:0;b:0;s:26:\"zigcy_lite_top_header_text\";s:38:\"Get 50% Discount on all summer arrival\";s:21:\"zigcy_lite_contact_no\";s:0:\"\";s:30:\"zigcy_lite_social_icons_enable\";s:2:\"on\";s:25:\"zigcy_lite_social_new_tab\";i:1;s:23:\"zigcy_lite_facebook_url\";s:1:\"#\";s:22:\"zigcy_lite_twitter_url\";s:1:\"#\";s:22:\"zigcy_lite_youtube_url\";s:1:\"#\";s:24:\"zigcy_lite_pinterest_url\";s:1:\"#\";s:24:\"zigcy_lite_instagram_url\";s:1:\"#\";s:16:\"header_textcolor\";s:5:\"blank\";s:23:\"zigcy_lite_promo_enable\";s:2:\"on\";s:25:\"zigcy_lite_area_one_image\";s:54:\"http://cosmetic.test/wp-content/uploads/2020/03/h4.jpg\";s:25:\"zigcy_lite_area_two_image\";s:54:\"http://cosmetic.test/wp-content/uploads/2020/03/h3.jpg\";s:25:\"zigcy_lite_area_one_title\";s:15:\"Mid Season Sale\";s:28:\"zigcy_lite_area_one_subtitle\";s:19:\"SALES UP TO 25% OFF\";s:31:\"zigcy_lite_area_one_price_title\";s:15:\"Starting At $20\";s:25:\"zigcy_lite_area_two_title\";s:14:\"Autumn Fashion\";s:28:\"zigcy_lite_area_two_subtitle\";s:21:\"NEW ARRIVAL FALL 2019\";s:31:\"zigcy_lite_area_two_button_text\";s:8:\"Shop Now\";s:31:\"zigcy_lite_area_two_button_link\";s:1:\"#\";s:31:\"zigcy_lite_feat_prod_cat_enable\";s:2:\"on\";s:32:\"zigcy_lite_feature_product_title\";s:9:\"Hot Items\";s:35:\"zigcy_lite_feature_product_subtitle\";s:31:\"GET SALE ALERTS ON KIT YOU LOVE\";s:37:\"zigcy_lite_feature_product_categories\";i:19;s:32:\"zigcy_lite_sml_feat_pro_per_page\";i:8;s:35:\"zigcy_lite_sml_feat_pro_show_rating\";i:1;s:21:\"zigcy_lite_cta_enable\";s:2:\"on\";s:23:\"zigcy_lite_cta_bg_image\";s:58:\"http://cosmetic.test/wp-content/uploads/2020/02/front1.jpg\";s:20:\"zigcy_lite_cta_title\";s:21:\"Seasonal Sale 70% Off\";s:23:\"zigcy_lite_cta_subtitle\";s:31:\"GRAB YOUR COSMETICS TREND TODAY\";s:22:\"zigcy_lite_price_title\";s:15:\"$45.00 - $90.00\";s:32:\"zigcy_lite_latest_cat_prod_title\";s:19:\"OTHER ITEMS ON SALE\";s:35:\"zigcy_lite_latest_cat_prod_subtitle\";s:21:\"In Store Availability\";s:36:\"zigcy_lite_latest_product_categories\";i:20;s:34:\"zigcy_lite_sml_lat_pro_show_rating\";i:1;s:33:\"zigcy_lite_latest_cat_prod_enable\";s:2:\"on\";s:31:\"zigcy_lite_sml_lat_pro_per_page\";i:9;s:23:\"zigcy_lite_footer_image\";s:81:\"http://demo.accesspressthemes.com/zigcy-lite/wp-content/uploads/2018/11/aaa-1.png\";s:27:\"zigcy_lite_footer_copyright\";s:7:\"© 2018\";s:31:\"sml_single_page_layout_sidebars\";s:10:\"no-sidebar\";s:22:\"zigcy_lite_slider_type\";s:3:\"cat\";s:24:\"zigcy_lite_client_enable\";s:2:\"on\";s:28:\"zigcy_lite_client_categories\";i:17;s:32:\"zigcy_lite_pro_cat_slider_enable\";s:3:\"off\";s:36:\"zigcy_lite_pro_cat_slider_categories\";i:19;s:31:\"zigcy_lite_pro_cat_slider_title\";s:16:\"Latest Cosmetics\";s:34:\"zigcy_lite_pro_cat_slider_subtitle\";s:12:\"What we need\";s:26:\"zigcy_lite_pro_cat_sl_prod\";i:6;s:22:\"zigcy_lite_blog_enable\";s:2:\"on\";s:21:\"zigcy_lite_blog_title\";s:7:\"MY Blog\";s:24:\"zigcy_lite_blog_subtitle\";s:11:\"what we see\";s:26:\"zigcy_lite_blog_categories\";i:16;s:30:\"zigcy_lite_post_excerpt_length\";i:98;s:21:\"zigcy_lite_call_title\";s:0:\"\";s:11:\"custom_logo\";i:401;s:22:\"zigcy_lite_exclude_cat\";s:8:\"26,17,1,\";s:25:\"sml_archive_post_excerpts\";i:300;s:22:\"zigcy_lite_cart_enable\";s:2:\"on\";s:12:\"header_image\";s:13:\"remove-header\";s:16:\"background_image\";s:0:\"\";s:17:\"background_preset\";s:4:\"fill\";s:21:\"background_position_x\";s:4:\"left\";s:21:\"background_position_y\";s:3:\"top\";s:15:\"background_size\";s:5:\"cover\";s:17:\"background_repeat\";s:9:\"no-repeat\";s:21:\"background_attachment\";s:5:\"fixed\";s:22:\"zigcy_lite_header_type\";s:7:\"layout1\";}', 'yes'),
(405, 'theme_switched_via_customizer', '', 'yes'),
(406, 'customize_stashed_theme_mods', 'a:0:{}', 'no'),
(423, 'widget_yith-woocompare-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `cm_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(424, 'yith_system_info', 'a:2:{s:11:\"system_info\";a:13:{s:14:\"min_wp_version\";a:1:{s:5:\"value\";s:5:\"5.3.2\";}s:14:\"min_wc_version\";a:1:{s:5:\"value\";s:5:\"3.9.2\";}s:15:\"wp_memory_limit\";a:1:{s:5:\"value\";i:536870912;}s:15:\"min_php_version\";a:1:{s:5:\"value\";s:6:\"7.2.19\";}s:15:\"min_tls_version\";a:1:{s:5:\"value\";s:3:\"1.3\";}s:15:\"imagick_version\";a:1:{s:5:\"value\";s:3:\"n/a\";}s:15:\"wp_cron_enabled\";a:1:{s:5:\"value\";b:1;}s:16:\"mbstring_enabled\";a:1:{s:5:\"value\";b:1;}s:17:\"simplexml_enabled\";a:1:{s:5:\"value\";b:1;}s:10:\"gd_enabled\";a:1:{s:5:\"value\";b:1;}s:13:\"iconv_enabled\";a:1:{s:5:\"value\";b:1;}s:15:\"opcache_enabled\";a:1:{s:5:\"value\";b:0;}s:17:\"url_fopen_enabled\";a:1:{s:5:\"value\";s:1:\"1\";}}s:6:\"errors\";b:0;}', 'yes'),
(427, '_transient_product_query-transient-version', '1583046786', 'yes'),
(429, 'category_children', 'a:0:{}', 'yes'),
(434, 'woocommerce_thumbnail_cropping', 'custom', 'yes'),
(435, 'woocommerce_thumbnail_cropping_custom_width', '', 'yes'),
(436, 'woocommerce_thumbnail_cropping_custom_height', '', 'yes'),
(437, 'elementor_disable_color_schemes', 'yes', 'yes'),
(438, 'elementor_disable_typography_schemes', 'yes', 'yes'),
(439, 'yith-wcwl-page-id', '129', 'yes'),
(440, 'yith_wcwl_version', '3.0.6', 'yes'),
(441, 'yith_wcwl_db_version', '3.0.0', 'yes'),
(442, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:0;s:3:\"all\";i:0;s:9:\"moderated\";i:0;s:8:\"approved\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(446, 'yith_woocompare_is_button', 'button', 'yes'),
(447, 'yith_woocompare_button_text', 'Compare', 'yes'),
(448, 'yith_woocompare_compare_button_in_product_page', 'yes', 'yes'),
(449, 'yith_woocompare_compare_button_in_products_list', 'no', 'yes'),
(450, 'yith_woocompare_auto_open', 'yes', 'yes'),
(451, 'yith_woocompare_table_text', 'Compare products', 'yes'),
(452, 'yith_woocompare_fields_attrs', 'all', 'yes'),
(453, 'yith_woocompare_price_end', 'yes', 'yes'),
(454, 'yith_woocompare_add_to_cart_end', 'no', 'yes'),
(455, 'yith_woocompare_image_size', 'a:3:{s:5:\"width\";i:220;s:6:\"height\";i:154;s:4:\"crop\";i:1;}', 'yes'),
(456, 'yit_plugin_fw_panel_wc_default_options_set', 'a:3:{s:21:\"yith_woocompare_panel\";b:1;s:15:\"yith_wcqv_panel\";b:1;s:15:\"yith_wcwl_panel\";b:1;}', 'yes'),
(457, 'yith-wcqv-enable', 'yes', 'yes'),
(458, 'yith-wcqv-enable-mobile', 'yes', 'yes'),
(459, 'yith-wcqv-button-label', 'Quick View', 'yes'),
(460, 'yith-wcqv-enable-lightbox', 'yes', 'yes'),
(461, 'yith-wcqv-background-modal', '#ffffff', 'yes'),
(462, 'yith-wcqv-close-color', '#cdcdcd', 'yes'),
(463, 'yith-wcqv-close-color-hover', '#ff0000', 'yes'),
(464, 'yith_wcwl_ajax_enable', 'no', 'yes'),
(465, 'yith_wfbt_enable_integration', 'yes', 'yes'),
(466, 'yith_wcwl_after_add_to_wishlist_behaviour', 'view', 'yes'),
(467, 'yith_wcwl_show_on_loop', 'no', 'yes'),
(468, 'yith_wcwl_loop_position', 'after_add_to_cart', 'yes'),
(469, 'yith_wcwl_button_position', 'after_add_to_cart', 'yes'),
(470, 'yith_wcwl_add_to_wishlist_text', 'Add to wishlist', 'yes'),
(471, 'yith_wcwl_product_added_text', 'Product added!', 'yes'),
(472, 'yith_wcwl_browse_wishlist_text', 'Browse wishlist', 'yes'),
(473, 'yith_wcwl_already_in_wishlist_text', 'The product is already in your wishlist!', 'yes'),
(474, 'yith_wcwl_add_to_wishlist_style', 'link', 'yes'),
(475, 'yith_wcwl_rounded_corners_radius', '16', 'yes'),
(476, 'yith_wcwl_add_to_wishlist_icon', 'fa-heart-o', 'yes'),
(477, 'yith_wcwl_add_to_wishlist_custom_icon', '', 'yes'),
(478, 'yith_wcwl_added_to_wishlist_icon', 'fa-heart', 'yes'),
(479, 'yith_wcwl_added_to_wishlist_custom_icon', '', 'yes'),
(480, 'yith_wcwl_custom_css', '', 'yes'),
(481, 'yith_wcwl_wishlist_page_id', '', 'yes'),
(482, 'yith_wcwl_variation_show', '', 'yes'),
(483, 'yith_wcwl_price_show', '', 'yes'),
(484, 'yith_wcwl_stock_show', '', 'yes'),
(485, 'yith_wcwl_show_dateadded', '', 'yes'),
(486, 'yith_wcwl_add_to_cart_show', '', 'yes'),
(487, 'yith_wcwl_show_remove', 'yes', 'yes'),
(488, 'yith_wcwl_repeat_remove_button', '', 'yes'),
(489, 'yith_wcwl_redirect_cart', 'no', 'yes'),
(490, 'yith_wcwl_remove_after_add_to_cart', 'yes', 'yes'),
(491, 'yith_wcwl_enable_share', 'yes', 'yes'),
(492, 'yith_wcwl_share_fb', 'yes', 'yes'),
(493, 'yith_wcwl_share_twitter', 'yes', 'yes'),
(494, 'yith_wcwl_share_pinterest', 'yes', 'yes'),
(495, 'yith_wcwl_share_email', 'yes', 'yes'),
(496, 'yith_wcwl_share_whatsapp', 'yes', 'yes'),
(497, 'yith_wcwl_share_url', 'no', 'yes'),
(498, 'yith_wcwl_socials_title', 'My wishlist on JM Cosmetics', 'yes'),
(499, 'yith_wcwl_socials_text', '', 'yes'),
(500, 'yith_wcwl_socials_image_url', '', 'yes'),
(501, 'yith_wcwl_wishlist_title', 'My wishlist on JM Cosmetics', 'yes'),
(502, 'yith_wcwl_add_to_cart_text', 'Add to cart', 'yes'),
(503, 'yith_wcwl_add_to_cart_style', 'link', 'yes'),
(504, 'yith_wcwl_add_to_cart_rounded_corners_radius', '16', 'yes'),
(505, 'yith_wcwl_add_to_cart_icon', 'fa-shopping-cart', 'yes'),
(506, 'yith_wcwl_add_to_cart_custom_icon', '', 'yes'),
(507, 'yith_wcwl_color_headers_background', '#F4F4F4', 'yes'),
(508, 'yith_wcwl_fb_button_icon', 'fa-facebook', 'yes'),
(509, 'yith_wcwl_fb_button_custom_icon', '', 'yes'),
(510, 'yith_wcwl_tw_button_icon', 'fa-twitter', 'yes'),
(511, 'yith_wcwl_tw_button_custom_icon', '', 'yes'),
(512, 'yith_wcwl_pr_button_icon', 'fa-pinterest', 'yes'),
(513, 'yith_wcwl_pr_button_custom_icon', '', 'yes'),
(514, 'yith_wcwl_em_button_icon', 'fa-envelope-o', 'yes'),
(515, 'yith_wcwl_em_button_custom_icon', '', 'yes'),
(516, 'yith_wcwl_wa_button_icon', 'fa-whatsapp', 'yes'),
(517, 'yith_wcwl_wa_button_custom_icon', '', 'yes'),
(518, 'yith_plugin_fw_promo_2019_bis', '1', 'yes'),
(534, 'woocommerce_marketplace_suggestions', 'a:2:{s:11:\"suggestions\";a:28:{i:0;a:4:{s:4:\"slug\";s:28:\"product-edit-meta-tab-header\";s:7:\"context\";s:28:\"product-edit-meta-tab-header\";s:5:\"title\";s:22:\"Recommended extensions\";s:13:\"allow-dismiss\";b:0;}i:1;a:6:{s:4:\"slug\";s:39:\"product-edit-meta-tab-footer-browse-all\";s:7:\"context\";s:28:\"product-edit-meta-tab-footer\";s:9:\"link-text\";s:21:\"Browse all extensions\";s:3:\"url\";s:64:\"https://woocommerce.com/product-category/woocommerce-extensions/\";s:8:\"promoted\";s:31:\"category-woocommerce-extensions\";s:13:\"allow-dismiss\";b:0;}i:2;a:9:{s:4:\"slug\";s:46:\"product-edit-mailchimp-woocommerce-memberships\";s:7:\"product\";s:33:\"woocommerce-memberships-mailchimp\";s:14:\"show-if-active\";a:1:{i:0;s:23:\"woocommerce-memberships\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:117:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/mailchimp-for-memberships.svg\";s:5:\"title\";s:25:\"Mailchimp for Memberships\";s:4:\"copy\";s:79:\"Completely automate your email lists by syncing membership changes to Mailchimp\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:67:\"https://woocommerce.com/products/mailchimp-woocommerce-memberships/\";}i:3;a:9:{s:4:\"slug\";s:19:\"product-edit-addons\";s:7:\"product\";s:26:\"woocommerce-product-addons\";s:14:\"show-if-active\";a:2:{i:0;s:25:\"woocommerce-subscriptions\";i:1;s:20:\"woocommerce-bookings\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:107:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/product-add-ons.svg\";s:5:\"title\";s:15:\"Product Add-Ons\";s:4:\"copy\";s:93:\"Offer add-ons like gift wrapping, special messages or other special options for your products\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:49:\"https://woocommerce.com/products/product-add-ons/\";}i:4;a:9:{s:4:\"slug\";s:46:\"product-edit-woocommerce-subscriptions-gifting\";s:7:\"product\";s:33:\"woocommerce-subscriptions-gifting\";s:14:\"show-if-active\";a:1:{i:0;s:25:\"woocommerce-subscriptions\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:117:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/gifting-for-subscriptions.svg\";s:5:\"title\";s:25:\"Gifting for Subscriptions\";s:4:\"copy\";s:70:\"Let customers buy subscriptions for others - they\'re the ultimate gift\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:67:\"https://woocommerce.com/products/woocommerce-subscriptions-gifting/\";}i:5;a:9:{s:4:\"slug\";s:42:\"product-edit-teams-woocommerce-memberships\";s:7:\"product\";s:33:\"woocommerce-memberships-for-teams\";s:14:\"show-if-active\";a:1:{i:0;s:23:\"woocommerce-memberships\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:113:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/teams-for-memberships.svg\";s:5:\"title\";s:21:\"Teams for Memberships\";s:4:\"copy\";s:123:\"Adds B2B functionality to WooCommerce Memberships, allowing sites to sell team, group, corporate, or family member accounts\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:63:\"https://woocommerce.com/products/teams-woocommerce-memberships/\";}i:6;a:8:{s:4:\"slug\";s:29:\"product-edit-variation-images\";s:7:\"product\";s:39:\"woocommerce-additional-variation-images\";s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:119:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/additional-variation-images.svg\";s:5:\"title\";s:27:\"Additional Variation Images\";s:4:\"copy\";s:72:\"Showcase your products in the best light with a image for each variation\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:73:\"https://woocommerce.com/products/woocommerce-additional-variation-images/\";}i:7;a:9:{s:4:\"slug\";s:47:\"product-edit-woocommerce-subscription-downloads\";s:7:\"product\";s:34:\"woocommerce-subscription-downloads\";s:14:\"show-if-active\";a:1:{i:0;s:25:\"woocommerce-subscriptions\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:114:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/subscription-downloads.svg\";s:5:\"title\";s:22:\"Subscription Downloads\";s:4:\"copy\";s:57:\"Give customers special downloads with their subscriptions\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:68:\"https://woocommerce.com/products/woocommerce-subscription-downloads/\";}i:8;a:8:{s:4:\"slug\";s:31:\"product-edit-min-max-quantities\";s:7:\"product\";s:30:\"woocommerce-min-max-quantities\";s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:110:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/min-max-quantities.svg\";s:5:\"title\";s:18:\"Min/Max Quantities\";s:4:\"copy\";s:81:\"Specify minimum and maximum allowed product quantities for orders to be completed\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:52:\"https://woocommerce.com/products/min-max-quantities/\";}i:9;a:8:{s:4:\"slug\";s:28:\"product-edit-name-your-price\";s:7:\"product\";s:27:\"woocommerce-name-your-price\";s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:107:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/name-your-price.svg\";s:5:\"title\";s:15:\"Name Your Price\";s:4:\"copy\";s:70:\"Let customers pay what they want - useful for donations, tips and more\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:49:\"https://woocommerce.com/products/name-your-price/\";}i:10;a:8:{s:4:\"slug\";s:42:\"product-edit-woocommerce-one-page-checkout\";s:7:\"product\";s:29:\"woocommerce-one-page-checkout\";s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:109:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/one-page-checkout.svg\";s:5:\"title\";s:17:\"One Page Checkout\";s:4:\"copy\";s:92:\"Don\'t make customers click around - let them choose products, checkout & pay all on one page\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:63:\"https://woocommerce.com/products/woocommerce-one-page-checkout/\";}i:11;a:4:{s:4:\"slug\";s:19:\"orders-empty-header\";s:7:\"context\";s:24:\"orders-list-empty-header\";s:5:\"title\";s:20:\"Tools for your store\";s:13:\"allow-dismiss\";b:0;}i:12;a:6:{s:4:\"slug\";s:30:\"orders-empty-footer-browse-all\";s:7:\"context\";s:24:\"orders-list-empty-footer\";s:9:\"link-text\";s:21:\"Browse all extensions\";s:3:\"url\";s:64:\"https://woocommerce.com/product-category/woocommerce-extensions/\";s:8:\"promoted\";s:31:\"category-woocommerce-extensions\";s:13:\"allow-dismiss\";b:0;}i:13;a:8:{s:4:\"slug\";s:19:\"orders-empty-zapier\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:18:\"woocommerce-zapier\";s:4:\"icon\";s:98:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/zapier.svg\";s:5:\"title\";s:6:\"Zapier\";s:4:\"copy\";s:88:\"Save time and increase productivity by connecting your store to more than 1000+ services\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:52:\"https://woocommerce.com/products/woocommerce-zapier/\";}i:14;a:8:{s:4:\"slug\";s:30:\"orders-empty-shipment-tracking\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:29:\"woocommerce-shipment-tracking\";s:4:\"icon\";s:109:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/shipment-tracking.svg\";s:5:\"title\";s:17:\"Shipment Tracking\";s:4:\"copy\";s:86:\"Let customers know when their orders will arrive by adding shipment tracking to emails\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:51:\"https://woocommerce.com/products/shipment-tracking/\";}i:15;a:8:{s:4:\"slug\";s:32:\"orders-empty-table-rate-shipping\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:31:\"woocommerce-table-rate-shipping\";s:4:\"icon\";s:111:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/table-rate-shipping.svg\";s:5:\"title\";s:19:\"Table Rate Shipping\";s:4:\"copy\";s:122:\"Advanced, flexible shipping. Define multiple shipping rates based on location, price, weight, shipping class or item count\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:53:\"https://woocommerce.com/products/table-rate-shipping/\";}i:16;a:8:{s:4:\"slug\";s:40:\"orders-empty-shipping-carrier-extensions\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:4:\"icon\";s:119:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/shipping-carrier-extensions.svg\";s:5:\"title\";s:27:\"Shipping Carrier Extensions\";s:4:\"copy\";s:116:\"Show live rates from FedEx, UPS, USPS and more directly on your store - never under or overcharge for shipping again\";s:11:\"button-text\";s:13:\"Find Carriers\";s:8:\"promoted\";s:26:\"category-shipping-carriers\";s:3:\"url\";s:99:\"https://woocommerce.com/product-category/woocommerce-extensions/shipping-methods/shipping-carriers/\";}i:17;a:8:{s:4:\"slug\";s:32:\"orders-empty-google-product-feed\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:25:\"woocommerce-product-feeds\";s:4:\"icon\";s:111:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/google-product-feed.svg\";s:5:\"title\";s:19:\"Google Product Feed\";s:4:\"copy\";s:76:\"Increase sales by letting customers find you when they\'re shopping on Google\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:53:\"https://woocommerce.com/products/google-product-feed/\";}i:18;a:8:{s:4:\"slug\";s:27:\"orders-empty-stripe-payment\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:26:\"woocommerce-gateway-stripe\";s:4:\"icon\";s:106:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/stripe-payment.svg\";s:5:\"title\";s:6:\"Stripe\";s:4:\"copy\";s:132:\"The complete payments platform engineered for growth. Millions around the globe use Stripe to start, run and scale their businesses.\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:40:\"https://woocommerce.com/products/stripe/\";}i:19;a:4:{s:4:\"slug\";s:35:\"products-empty-header-product-types\";s:7:\"context\";s:26:\"products-list-empty-header\";s:5:\"title\";s:23:\"Other types of products\";s:13:\"allow-dismiss\";b:0;}i:20;a:6:{s:4:\"slug\";s:32:\"products-empty-footer-browse-all\";s:7:\"context\";s:26:\"products-list-empty-footer\";s:9:\"link-text\";s:21:\"Browse all extensions\";s:3:\"url\";s:64:\"https://woocommerce.com/product-category/woocommerce-extensions/\";s:8:\"promoted\";s:31:\"category-woocommerce-extensions\";s:13:\"allow-dismiss\";b:0;}i:21;a:8:{s:4:\"slug\";s:30:\"products-empty-product-vendors\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:27:\"woocommerce-product-vendors\";s:4:\"icon\";s:107:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/product-vendors.svg\";s:5:\"title\";s:15:\"Product Vendors\";s:4:\"copy\";s:47:\"Turn your store into a multi-vendor marketplace\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:49:\"https://woocommerce.com/products/product-vendors/\";}i:22;a:8:{s:4:\"slug\";s:26:\"products-empty-memberships\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:23:\"woocommerce-memberships\";s:4:\"icon\";s:103:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/memberships.svg\";s:5:\"title\";s:11:\"Memberships\";s:4:\"copy\";s:76:\"Give members access to restricted content or products, for a fee or for free\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:57:\"https://woocommerce.com/products/woocommerce-memberships/\";}i:23;a:9:{s:4:\"slug\";s:35:\"products-empty-woocommerce-deposits\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:20:\"woocommerce-deposits\";s:14:\"show-if-active\";a:1:{i:0;s:20:\"woocommerce-bookings\";}s:4:\"icon\";s:100:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/deposits.svg\";s:5:\"title\";s:8:\"Deposits\";s:4:\"copy\";s:75:\"Make it easier for customers to pay by offering a deposit or a payment plan\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:54:\"https://woocommerce.com/products/woocommerce-deposits/\";}i:24;a:8:{s:4:\"slug\";s:40:\"products-empty-woocommerce-subscriptions\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:25:\"woocommerce-subscriptions\";s:4:\"icon\";s:105:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/subscriptions.svg\";s:5:\"title\";s:13:\"Subscriptions\";s:4:\"copy\";s:97:\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:59:\"https://woocommerce.com/products/woocommerce-subscriptions/\";}i:25;a:8:{s:4:\"slug\";s:35:\"products-empty-woocommerce-bookings\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:20:\"woocommerce-bookings\";s:4:\"icon\";s:100:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/bookings.svg\";s:5:\"title\";s:8:\"Bookings\";s:4:\"copy\";s:99:\"Allow customers to book appointments, make reservations or rent equipment without leaving your site\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:54:\"https://woocommerce.com/products/woocommerce-bookings/\";}i:26;a:8:{s:4:\"slug\";s:30:\"products-empty-product-bundles\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:27:\"woocommerce-product-bundles\";s:4:\"icon\";s:107:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/product-bundles.svg\";s:5:\"title\";s:15:\"Product Bundles\";s:4:\"copy\";s:49:\"Offer customizable bundles and assembled products\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:49:\"https://woocommerce.com/products/product-bundles/\";}i:27;a:8:{s:4:\"slug\";s:29:\"products-empty-stripe-payment\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:26:\"woocommerce-gateway-stripe\";s:4:\"icon\";s:106:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/stripe-payment.svg\";s:5:\"title\";s:6:\"Stripe\";s:4:\"copy\";s:132:\"The complete payments platform engineered for growth. Millions around the globe use Stripe to start, run and scale their businesses.\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:40:\"https://woocommerce.com/products/stripe/\";}}s:7:\"updated\";i:1582445568;}', 'no'),
(777, '_transient_woocommerce_reports-transient-version', '1582983596', 'yes'),
(778, '_transient_timeout_wc_report_orders_stats_f0e955fb4cb65d8663d5e0032be55582', '1583588397', 'no'),
(779, '_transient_wc_report_orders_stats_f0e955fb4cb65d8663d5e0032be55582', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":12:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:5:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-09\";s:10:\"date_start\";s:19:\"2020-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-29 21:39:00\";s:12:\"date_end_gmt\";s:19:\"2020-02-29 21:39:00\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-08\";s:10:\"date_start\";s:19:\"2020-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-07\";s:10:\"date_start\";s:19:\"2020-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:7:\"2020-06\";s:10:\"date_start\";s:19:\"2020-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:7:\"2020-05\";s:10:\"date_start\";s:19:\"2020-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:5;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(780, '_transient_timeout_wc_report_orders_stats_820edeac26debeba5dc9b69649c0670d', '1583588397', 'no'),
(781, '_transient_wc_report_orders_stats_820edeac26debeba5dc9b69649c0670d', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":11:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:5:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-09\";s:10:\"date_start\";s:19:\"2020-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-29 21:39:00\";s:12:\"date_end_gmt\";s:19:\"2020-02-29 21:39:00\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-08\";s:10:\"date_start\";s:19:\"2020-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-07\";s:10:\"date_start\";s:19:\"2020-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:7:\"2020-06\";s:10:\"date_start\";s:19:\"2020-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:7:\"2020-05\";s:10:\"date_start\";s:19:\"2020-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:5;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(782, '_transient_timeout_wc_report_orders_stats_8ba867e3123130fdba7a69a6087f797c', '1583588398', 'no'),
(783, '_transient_wc_report_orders_stats_8ba867e3123130fdba7a69a6087f797c', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":12:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:29:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-02-01\";s:10:\"date_start\";s:19:\"2020-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2020-02-02\";s:10:\"date_start\";s:19:\"2020-02-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-02 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2020-02-03\";s:10:\"date_start\";s:19:\"2020-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2020-02-04\";s:10:\"date_start\";s:19:\"2020-02-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-04 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2020-02-05\";s:10:\"date_start\";s:19:\"2020-02-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-05 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2020-02-06\";s:10:\"date_start\";s:19:\"2020-02-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-06 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2020-02-07\";s:10:\"date_start\";s:19:\"2020-02-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2020-02-08\";s:10:\"date_start\";s:19:\"2020-02-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-08 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2020-02-09\";s:10:\"date_start\";s:19:\"2020-02-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-09 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2020-02-10\";s:10:\"date_start\";s:19:\"2020-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2020-02-11\";s:10:\"date_start\";s:19:\"2020-02-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-11 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2020-02-12\";s:10:\"date_start\";s:19:\"2020-02-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-12 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2020-02-13\";s:10:\"date_start\";s:19:\"2020-02-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-13 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2020-02-14\";s:10:\"date_start\";s:19:\"2020-02-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:14;a:6:{s:8:\"interval\";s:10:\"2020-02-15\";s:10:\"date_start\";s:19:\"2020-02-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-15 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:15;a:6:{s:8:\"interval\";s:10:\"2020-02-16\";s:10:\"date_start\";s:19:\"2020-02-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-16 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:16;a:6:{s:8:\"interval\";s:10:\"2020-02-17\";s:10:\"date_start\";s:19:\"2020-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-17 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-17 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:17;a:6:{s:8:\"interval\";s:10:\"2020-02-18\";s:10:\"date_start\";s:19:\"2020-02-18 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-18 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-18 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-18 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:18;a:6:{s:8:\"interval\";s:10:\"2020-02-19\";s:10:\"date_start\";s:19:\"2020-02-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-19 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-19 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-19 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:19;a:6:{s:8:\"interval\";s:10:\"2020-02-20\";s:10:\"date_start\";s:19:\"2020-02-20 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-20 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-20 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-20 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:20;a:6:{s:8:\"interval\";s:10:\"2020-02-21\";s:10:\"date_start\";s:19:\"2020-02-21 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-21 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-21 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-21 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:21;a:6:{s:8:\"interval\";s:10:\"2020-02-22\";s:10:\"date_start\";s:19:\"2020-02-22 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-22 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-22 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-22 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:22;a:6:{s:8:\"interval\";s:10:\"2020-02-23\";s:10:\"date_start\";s:19:\"2020-02-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-23 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:23;a:6:{s:8:\"interval\";s:10:\"2020-02-24\";s:10:\"date_start\";s:19:\"2020-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-24 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-24 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:24;a:6:{s:8:\"interval\";s:10:\"2020-02-25\";s:10:\"date_start\";s:19:\"2020-02-25 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-25 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-25 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-25 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:25;a:6:{s:8:\"interval\";s:10:\"2020-02-26\";s:10:\"date_start\";s:19:\"2020-02-26 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-26 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-26 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-26 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:26;a:6:{s:8:\"interval\";s:10:\"2020-02-27\";s:10:\"date_start\";s:19:\"2020-02-27 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-27 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-27 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-27 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:27;a:6:{s:8:\"interval\";s:10:\"2020-02-28\";s:10:\"date_start\";s:19:\"2020-02-28 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-28 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-28 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-28 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:28;a:6:{s:8:\"interval\";s:10:\"2020-02-29\";s:10:\"date_start\";s:19:\"2020-02-29 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-29 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-29 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-29 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:29;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(784, '_transient_timeout_wc_report_orders_stats_2646e86dfd621d46ff16152db8726b12', '1583588398', 'no'),
(785, '_transient_wc_report_orders_stats_2646e86dfd621d46ff16152db8726b12', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":12:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:5:{i:0;a:6:{s:8:\"interval\";s:7:\"2019-09\";s:10:\"date_start\";s:19:\"2019-02-25 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-25 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2019-08\";s:10:\"date_start\";s:19:\"2019-02-18 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-18 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-24 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-24 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2019-07\";s:10:\"date_start\";s:19:\"2019-02-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-11 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-17 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-17 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:7:\"2019-06\";s:10:\"date_start\";s:19:\"2019-02-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-04 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:7:\"2019-05\";s:10:\"date_start\";s:19:\"2019-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:5;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(786, '_transient_timeout_wc_report_orders_stats_678d12838f4c3a755384e5f51a72b595', '1583588398', 'no');
INSERT INTO `cm_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(787, '_transient_wc_report_orders_stats_678d12838f4c3a755384e5f51a72b595', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":11:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:5:{i:0;a:6:{s:8:\"interval\";s:7:\"2019-09\";s:10:\"date_start\";s:19:\"2019-02-25 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-25 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2019-08\";s:10:\"date_start\";s:19:\"2019-02-18 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-18 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-24 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-24 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2019-07\";s:10:\"date_start\";s:19:\"2019-02-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-11 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-17 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-17 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:7:\"2019-06\";s:10:\"date_start\";s:19:\"2019-02-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-04 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:7:\"2019-05\";s:10:\"date_start\";s:19:\"2019-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:5;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(788, '_transient_timeout_wc_report_orders_stats_c4ca9a13241a8b517b4fdf64e1850684', '1583588398', 'no'),
(789, '_transient_wc_report_orders_stats_c4ca9a13241a8b517b4fdf64e1850684', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":11:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:29:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-02-01\";s:10:\"date_start\";s:19:\"2020-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2020-02-02\";s:10:\"date_start\";s:19:\"2020-02-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-02 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2020-02-03\";s:10:\"date_start\";s:19:\"2020-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2020-02-04\";s:10:\"date_start\";s:19:\"2020-02-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-04 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2020-02-05\";s:10:\"date_start\";s:19:\"2020-02-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-05 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2020-02-06\";s:10:\"date_start\";s:19:\"2020-02-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-06 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2020-02-07\";s:10:\"date_start\";s:19:\"2020-02-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2020-02-08\";s:10:\"date_start\";s:19:\"2020-02-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-08 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2020-02-09\";s:10:\"date_start\";s:19:\"2020-02-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-09 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2020-02-10\";s:10:\"date_start\";s:19:\"2020-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2020-02-11\";s:10:\"date_start\";s:19:\"2020-02-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-11 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2020-02-12\";s:10:\"date_start\";s:19:\"2020-02-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-12 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2020-02-13\";s:10:\"date_start\";s:19:\"2020-02-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-13 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2020-02-14\";s:10:\"date_start\";s:19:\"2020-02-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:14;a:6:{s:8:\"interval\";s:10:\"2020-02-15\";s:10:\"date_start\";s:19:\"2020-02-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-15 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:15;a:6:{s:8:\"interval\";s:10:\"2020-02-16\";s:10:\"date_start\";s:19:\"2020-02-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-16 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:16;a:6:{s:8:\"interval\";s:10:\"2020-02-17\";s:10:\"date_start\";s:19:\"2020-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-17 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-17 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:17;a:6:{s:8:\"interval\";s:10:\"2020-02-18\";s:10:\"date_start\";s:19:\"2020-02-18 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-18 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-18 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-18 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:18;a:6:{s:8:\"interval\";s:10:\"2020-02-19\";s:10:\"date_start\";s:19:\"2020-02-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-19 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-19 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-19 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:19;a:6:{s:8:\"interval\";s:10:\"2020-02-20\";s:10:\"date_start\";s:19:\"2020-02-20 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-20 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-20 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-20 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:20;a:6:{s:8:\"interval\";s:10:\"2020-02-21\";s:10:\"date_start\";s:19:\"2020-02-21 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-21 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-21 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-21 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:21;a:6:{s:8:\"interval\";s:10:\"2020-02-22\";s:10:\"date_start\";s:19:\"2020-02-22 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-22 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-22 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-22 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:22;a:6:{s:8:\"interval\";s:10:\"2020-02-23\";s:10:\"date_start\";s:19:\"2020-02-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-23 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:23;a:6:{s:8:\"interval\";s:10:\"2020-02-24\";s:10:\"date_start\";s:19:\"2020-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-24 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-24 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:24;a:6:{s:8:\"interval\";s:10:\"2020-02-25\";s:10:\"date_start\";s:19:\"2020-02-25 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-25 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-25 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-25 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:25;a:6:{s:8:\"interval\";s:10:\"2020-02-26\";s:10:\"date_start\";s:19:\"2020-02-26 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-26 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-26 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-26 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:26;a:6:{s:8:\"interval\";s:10:\"2020-02-27\";s:10:\"date_start\";s:19:\"2020-02-27 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-27 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-27 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-27 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:27;a:6:{s:8:\"interval\";s:10:\"2020-02-28\";s:10:\"date_start\";s:19:\"2020-02-28 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-28 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-28 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-28 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:28;a:6:{s:8:\"interval\";s:10:\"2020-02-29\";s:10:\"date_start\";s:19:\"2020-02-29 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-29 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-29 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-29 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:29;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(790, '_transient_timeout_wc_report_orders_stats_f5a051f810449d4c4e3ff7bae309695e', '1583588399', 'no'),
(791, '_transient_wc_report_orders_stats_f5a051f810449d4c4e3ff7bae309695e', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":12:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:29:{i:0;a:6:{s:8:\"interval\";s:10:\"2019-02-01\";s:10:\"date_start\";s:19:\"2019-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2019-02-02\";s:10:\"date_start\";s:19:\"2019-02-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-02 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2019-02-03\";s:10:\"date_start\";s:19:\"2019-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2019-02-04\";s:10:\"date_start\";s:19:\"2019-02-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-04 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2019-02-05\";s:10:\"date_start\";s:19:\"2019-02-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-05 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2019-02-06\";s:10:\"date_start\";s:19:\"2019-02-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-06 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2019-02-07\";s:10:\"date_start\";s:19:\"2019-02-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-07 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2019-02-08\";s:10:\"date_start\";s:19:\"2019-02-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-08 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2019-02-09\";s:10:\"date_start\";s:19:\"2019-02-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-09 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2019-02-10\";s:10:\"date_start\";s:19:\"2019-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2019-02-11\";s:10:\"date_start\";s:19:\"2019-02-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-11 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2019-02-12\";s:10:\"date_start\";s:19:\"2019-02-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-12 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2019-02-13\";s:10:\"date_start\";s:19:\"2019-02-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-13 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2019-02-14\";s:10:\"date_start\";s:19:\"2019-02-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-14 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:14;a:6:{s:8:\"interval\";s:10:\"2019-02-15\";s:10:\"date_start\";s:19:\"2019-02-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-15 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:15;a:6:{s:8:\"interval\";s:10:\"2019-02-16\";s:10:\"date_start\";s:19:\"2019-02-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-16 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:16;a:6:{s:8:\"interval\";s:10:\"2019-02-17\";s:10:\"date_start\";s:19:\"2019-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-17 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-17 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:17;a:6:{s:8:\"interval\";s:10:\"2019-02-18\";s:10:\"date_start\";s:19:\"2019-02-18 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-18 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-18 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-18 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:18;a:6:{s:8:\"interval\";s:10:\"2019-02-19\";s:10:\"date_start\";s:19:\"2019-02-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-19 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-19 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-19 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:19;a:6:{s:8:\"interval\";s:10:\"2019-02-20\";s:10:\"date_start\";s:19:\"2019-02-20 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-20 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-20 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-20 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:20;a:6:{s:8:\"interval\";s:10:\"2019-02-21\";s:10:\"date_start\";s:19:\"2019-02-21 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-21 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-21 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-21 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:21;a:6:{s:8:\"interval\";s:10:\"2019-02-22\";s:10:\"date_start\";s:19:\"2019-02-22 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-22 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-22 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-22 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:22;a:6:{s:8:\"interval\";s:10:\"2019-02-23\";s:10:\"date_start\";s:19:\"2019-02-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-23 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:23;a:6:{s:8:\"interval\";s:10:\"2019-02-24\";s:10:\"date_start\";s:19:\"2019-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-24 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-24 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:24;a:6:{s:8:\"interval\";s:10:\"2019-02-25\";s:10:\"date_start\";s:19:\"2019-02-25 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-25 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-25 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-25 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:25;a:6:{s:8:\"interval\";s:10:\"2019-02-26\";s:10:\"date_start\";s:19:\"2019-02-26 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-26 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-26 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-26 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:26;a:6:{s:8:\"interval\";s:10:\"2019-02-27\";s:10:\"date_start\";s:19:\"2019-02-27 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-27 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-27 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-27 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:27;a:6:{s:8:\"interval\";s:10:\"2019-02-28\";s:10:\"date_start\";s:19:\"2019-02-28 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-28 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-28 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-28 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:28;a:6:{s:8:\"interval\";s:10:\"2019-03-01\";s:10:\"date_start\";s:19:\"2019-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:29;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(792, '_transient_timeout_wc_report_orders_stats_94c15cabc15c7b88cff25822e4e7a998', '1583588400', 'no');
INSERT INTO `cm_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(793, '_transient_wc_report_orders_stats_94c15cabc15c7b88cff25822e4e7a998', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":11:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:29:{i:0;a:6:{s:8:\"interval\";s:10:\"2019-02-01\";s:10:\"date_start\";s:19:\"2019-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2019-02-02\";s:10:\"date_start\";s:19:\"2019-02-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-02 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2019-02-03\";s:10:\"date_start\";s:19:\"2019-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2019-02-04\";s:10:\"date_start\";s:19:\"2019-02-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-04 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2019-02-05\";s:10:\"date_start\";s:19:\"2019-02-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-05 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2019-02-06\";s:10:\"date_start\";s:19:\"2019-02-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-06 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2019-02-07\";s:10:\"date_start\";s:19:\"2019-02-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-07 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2019-02-08\";s:10:\"date_start\";s:19:\"2019-02-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-08 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2019-02-09\";s:10:\"date_start\";s:19:\"2019-02-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-09 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2019-02-10\";s:10:\"date_start\";s:19:\"2019-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2019-02-11\";s:10:\"date_start\";s:19:\"2019-02-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-11 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2019-02-12\";s:10:\"date_start\";s:19:\"2019-02-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-12 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2019-02-13\";s:10:\"date_start\";s:19:\"2019-02-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-13 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2019-02-14\";s:10:\"date_start\";s:19:\"2019-02-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-14 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:14;a:6:{s:8:\"interval\";s:10:\"2019-02-15\";s:10:\"date_start\";s:19:\"2019-02-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-15 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:15;a:6:{s:8:\"interval\";s:10:\"2019-02-16\";s:10:\"date_start\";s:19:\"2019-02-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-16 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:16;a:6:{s:8:\"interval\";s:10:\"2019-02-17\";s:10:\"date_start\";s:19:\"2019-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-17 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-17 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:17;a:6:{s:8:\"interval\";s:10:\"2019-02-18\";s:10:\"date_start\";s:19:\"2019-02-18 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-18 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-18 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-18 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:18;a:6:{s:8:\"interval\";s:10:\"2019-02-19\";s:10:\"date_start\";s:19:\"2019-02-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-19 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-19 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-19 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:19;a:6:{s:8:\"interval\";s:10:\"2019-02-20\";s:10:\"date_start\";s:19:\"2019-02-20 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-20 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-20 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-20 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:20;a:6:{s:8:\"interval\";s:10:\"2019-02-21\";s:10:\"date_start\";s:19:\"2019-02-21 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-21 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-21 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-21 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:21;a:6:{s:8:\"interval\";s:10:\"2019-02-22\";s:10:\"date_start\";s:19:\"2019-02-22 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-22 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-22 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-22 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:22;a:6:{s:8:\"interval\";s:10:\"2019-02-23\";s:10:\"date_start\";s:19:\"2019-02-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-23 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:23;a:6:{s:8:\"interval\";s:10:\"2019-02-24\";s:10:\"date_start\";s:19:\"2019-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-24 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-24 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:24;a:6:{s:8:\"interval\";s:10:\"2019-02-25\";s:10:\"date_start\";s:19:\"2019-02-25 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-25 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-25 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-25 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:25;a:6:{s:8:\"interval\";s:10:\"2019-02-26\";s:10:\"date_start\";s:19:\"2019-02-26 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-26 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-26 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-26 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:26;a:6:{s:8:\"interval\";s:10:\"2019-02-27\";s:10:\"date_start\";s:19:\"2019-02-27 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-27 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-27 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-27 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:27;a:6:{s:8:\"interval\";s:10:\"2019-02-28\";s:10:\"date_start\";s:19:\"2019-02-28 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-28 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-28 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-28 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:28;a:6:{s:8:\"interval\";s:10:\"2019-03-01\";s:10:\"date_start\";s:19:\"2019-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:29;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(794, '_transient_timeout_wc_report_coupons_stats_827a886d8ac9486b8bf4efc2da86133c', '1583588400', 'no'),
(795, '_transient_wc_report_coupons_stats_827a886d8ac9486b8bf4efc2da86133c', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:29:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-02-01\";s:10:\"date_start\";s:19:\"2020-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2020-02-02\";s:10:\"date_start\";s:19:\"2020-02-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-02 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2020-02-03\";s:10:\"date_start\";s:19:\"2020-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2020-02-04\";s:10:\"date_start\";s:19:\"2020-02-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-04 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2020-02-05\";s:10:\"date_start\";s:19:\"2020-02-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-05 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2020-02-06\";s:10:\"date_start\";s:19:\"2020-02-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-06 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2020-02-07\";s:10:\"date_start\";s:19:\"2020-02-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2020-02-08\";s:10:\"date_start\";s:19:\"2020-02-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-08 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2020-02-09\";s:10:\"date_start\";s:19:\"2020-02-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-09 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2020-02-10\";s:10:\"date_start\";s:19:\"2020-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2020-02-11\";s:10:\"date_start\";s:19:\"2020-02-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-11 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2020-02-12\";s:10:\"date_start\";s:19:\"2020-02-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-12 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2020-02-13\";s:10:\"date_start\";s:19:\"2020-02-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-13 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2020-02-14\";s:10:\"date_start\";s:19:\"2020-02-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:14;a:6:{s:8:\"interval\";s:10:\"2020-02-15\";s:10:\"date_start\";s:19:\"2020-02-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-15 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:15;a:6:{s:8:\"interval\";s:10:\"2020-02-16\";s:10:\"date_start\";s:19:\"2020-02-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-16 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:16;a:6:{s:8:\"interval\";s:10:\"2020-02-17\";s:10:\"date_start\";s:19:\"2020-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-17 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-17 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:17;a:6:{s:8:\"interval\";s:10:\"2020-02-18\";s:10:\"date_start\";s:19:\"2020-02-18 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-18 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-18 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-18 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:18;a:6:{s:8:\"interval\";s:10:\"2020-02-19\";s:10:\"date_start\";s:19:\"2020-02-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-19 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-19 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-19 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:19;a:6:{s:8:\"interval\";s:10:\"2020-02-20\";s:10:\"date_start\";s:19:\"2020-02-20 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-20 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-20 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-20 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:20;a:6:{s:8:\"interval\";s:10:\"2020-02-21\";s:10:\"date_start\";s:19:\"2020-02-21 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-21 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-21 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-21 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:21;a:6:{s:8:\"interval\";s:10:\"2020-02-22\";s:10:\"date_start\";s:19:\"2020-02-22 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-22 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-22 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-22 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:22;a:6:{s:8:\"interval\";s:10:\"2020-02-23\";s:10:\"date_start\";s:19:\"2020-02-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-23 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:23;a:6:{s:8:\"interval\";s:10:\"2020-02-24\";s:10:\"date_start\";s:19:\"2020-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-24 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-24 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:24;a:6:{s:8:\"interval\";s:10:\"2020-02-25\";s:10:\"date_start\";s:19:\"2020-02-25 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-25 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-25 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-25 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:25;a:6:{s:8:\"interval\";s:10:\"2020-02-26\";s:10:\"date_start\";s:19:\"2020-02-26 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-26 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-26 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-26 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:26;a:6:{s:8:\"interval\";s:10:\"2020-02-27\";s:10:\"date_start\";s:19:\"2020-02-27 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-27 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-27 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-27 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:27;a:6:{s:8:\"interval\";s:10:\"2020-02-28\";s:10:\"date_start\";s:19:\"2020-02-28 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-28 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-28 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-28 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:28;a:6:{s:8:\"interval\";s:10:\"2020-02-29\";s:10:\"date_start\";s:19:\"2020-02-29 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-29 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-29 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-29 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:29;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(796, '_transient_timeout_wc_report_coupons_stats_ff89de42de95d7707745e7d2cfa444f7', '1583588401', 'no'),
(797, '_transient_wc_report_coupons_stats_ff89de42de95d7707745e7d2cfa444f7', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:29:{i:0;a:6:{s:8:\"interval\";s:10:\"2019-02-01\";s:10:\"date_start\";s:19:\"2019-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2019-02-02\";s:10:\"date_start\";s:19:\"2019-02-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-02 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2019-02-03\";s:10:\"date_start\";s:19:\"2019-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2019-02-04\";s:10:\"date_start\";s:19:\"2019-02-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-04 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2019-02-05\";s:10:\"date_start\";s:19:\"2019-02-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-05 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2019-02-06\";s:10:\"date_start\";s:19:\"2019-02-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-06 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2019-02-07\";s:10:\"date_start\";s:19:\"2019-02-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-07 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2019-02-08\";s:10:\"date_start\";s:19:\"2019-02-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-08 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2019-02-09\";s:10:\"date_start\";s:19:\"2019-02-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-09 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2019-02-10\";s:10:\"date_start\";s:19:\"2019-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2019-02-11\";s:10:\"date_start\";s:19:\"2019-02-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-11 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2019-02-12\";s:10:\"date_start\";s:19:\"2019-02-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-12 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2019-02-13\";s:10:\"date_start\";s:19:\"2019-02-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-13 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2019-02-14\";s:10:\"date_start\";s:19:\"2019-02-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-14 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:14;a:6:{s:8:\"interval\";s:10:\"2019-02-15\";s:10:\"date_start\";s:19:\"2019-02-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-15 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:15;a:6:{s:8:\"interval\";s:10:\"2019-02-16\";s:10:\"date_start\";s:19:\"2019-02-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-16 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:16;a:6:{s:8:\"interval\";s:10:\"2019-02-17\";s:10:\"date_start\";s:19:\"2019-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-17 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-17 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:17;a:6:{s:8:\"interval\";s:10:\"2019-02-18\";s:10:\"date_start\";s:19:\"2019-02-18 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-18 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-18 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-18 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:18;a:6:{s:8:\"interval\";s:10:\"2019-02-19\";s:10:\"date_start\";s:19:\"2019-02-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-19 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-19 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-19 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:19;a:6:{s:8:\"interval\";s:10:\"2019-02-20\";s:10:\"date_start\";s:19:\"2019-02-20 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-20 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-20 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-20 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:20;a:6:{s:8:\"interval\";s:10:\"2019-02-21\";s:10:\"date_start\";s:19:\"2019-02-21 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-21 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-21 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-21 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:21;a:6:{s:8:\"interval\";s:10:\"2019-02-22\";s:10:\"date_start\";s:19:\"2019-02-22 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-22 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-22 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-22 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:22;a:6:{s:8:\"interval\";s:10:\"2019-02-23\";s:10:\"date_start\";s:19:\"2019-02-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-23 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:23;a:6:{s:8:\"interval\";s:10:\"2019-02-24\";s:10:\"date_start\";s:19:\"2019-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-24 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-24 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:24;a:6:{s:8:\"interval\";s:10:\"2019-02-25\";s:10:\"date_start\";s:19:\"2019-02-25 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-25 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-25 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-25 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:25;a:6:{s:8:\"interval\";s:10:\"2019-02-26\";s:10:\"date_start\";s:19:\"2019-02-26 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-26 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-26 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-26 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:26;a:6:{s:8:\"interval\";s:10:\"2019-02-27\";s:10:\"date_start\";s:19:\"2019-02-27 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-27 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-27 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-27 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:27;a:6:{s:8:\"interval\";s:10:\"2019-02-28\";s:10:\"date_start\";s:19:\"2019-02-28 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-28 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-28 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-28 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:28;a:6:{s:8:\"interval\";s:10:\"2019-03-01\";s:10:\"date_start\";s:19:\"2019-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:29;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(798, '_transient_timeout_wc_report_taxes_stats_0d3c16ec8558a5952af679ff32b0ce28', '1583588401', 'no');
INSERT INTO `cm_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(799, '_transient_wc_report_taxes_stats_0d3c16ec8558a5952af679ff32b0ce28', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:29:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-02-01\";s:10:\"date_start\";s:19:\"2020-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2020-02-02\";s:10:\"date_start\";s:19:\"2020-02-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-02 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2020-02-03\";s:10:\"date_start\";s:19:\"2020-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2020-02-04\";s:10:\"date_start\";s:19:\"2020-02-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-04 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2020-02-05\";s:10:\"date_start\";s:19:\"2020-02-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-05 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2020-02-06\";s:10:\"date_start\";s:19:\"2020-02-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-06 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2020-02-07\";s:10:\"date_start\";s:19:\"2020-02-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2020-02-08\";s:10:\"date_start\";s:19:\"2020-02-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-08 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2020-02-09\";s:10:\"date_start\";s:19:\"2020-02-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-09 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2020-02-10\";s:10:\"date_start\";s:19:\"2020-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2020-02-11\";s:10:\"date_start\";s:19:\"2020-02-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-11 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2020-02-12\";s:10:\"date_start\";s:19:\"2020-02-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-12 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2020-02-13\";s:10:\"date_start\";s:19:\"2020-02-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-13 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2020-02-14\";s:10:\"date_start\";s:19:\"2020-02-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:14;a:6:{s:8:\"interval\";s:10:\"2020-02-15\";s:10:\"date_start\";s:19:\"2020-02-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-15 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:15;a:6:{s:8:\"interval\";s:10:\"2020-02-16\";s:10:\"date_start\";s:19:\"2020-02-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-16 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:16;a:6:{s:8:\"interval\";s:10:\"2020-02-17\";s:10:\"date_start\";s:19:\"2020-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-17 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-17 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:17;a:6:{s:8:\"interval\";s:10:\"2020-02-18\";s:10:\"date_start\";s:19:\"2020-02-18 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-18 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-18 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-18 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:18;a:6:{s:8:\"interval\";s:10:\"2020-02-19\";s:10:\"date_start\";s:19:\"2020-02-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-19 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-19 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-19 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:19;a:6:{s:8:\"interval\";s:10:\"2020-02-20\";s:10:\"date_start\";s:19:\"2020-02-20 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-20 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-20 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-20 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:20;a:6:{s:8:\"interval\";s:10:\"2020-02-21\";s:10:\"date_start\";s:19:\"2020-02-21 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-21 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-21 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-21 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:21;a:6:{s:8:\"interval\";s:10:\"2020-02-22\";s:10:\"date_start\";s:19:\"2020-02-22 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-22 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-22 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-22 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:22;a:6:{s:8:\"interval\";s:10:\"2020-02-23\";s:10:\"date_start\";s:19:\"2020-02-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-23 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:23;a:6:{s:8:\"interval\";s:10:\"2020-02-24\";s:10:\"date_start\";s:19:\"2020-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-24 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-24 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:24;a:6:{s:8:\"interval\";s:10:\"2020-02-25\";s:10:\"date_start\";s:19:\"2020-02-25 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-25 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-25 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-25 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:25;a:6:{s:8:\"interval\";s:10:\"2020-02-26\";s:10:\"date_start\";s:19:\"2020-02-26 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-26 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-26 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-26 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:26;a:6:{s:8:\"interval\";s:10:\"2020-02-27\";s:10:\"date_start\";s:19:\"2020-02-27 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-27 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-27 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-27 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:27;a:6:{s:8:\"interval\";s:10:\"2020-02-28\";s:10:\"date_start\";s:19:\"2020-02-28 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-28 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-28 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-28 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:28;a:6:{s:8:\"interval\";s:10:\"2020-02-29\";s:10:\"date_start\";s:19:\"2020-02-29 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-29 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-29 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-29 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:29;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(800, '_transient_timeout_wc_report_taxes_stats_620f35bd3db7064b858b939c054caa6c', '1583588402', 'no'),
(801, '_transient_wc_report_taxes_stats_620f35bd3db7064b858b939c054caa6c', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:29:{i:0;a:6:{s:8:\"interval\";s:10:\"2019-02-01\";s:10:\"date_start\";s:19:\"2019-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2019-02-02\";s:10:\"date_start\";s:19:\"2019-02-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-02 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2019-02-03\";s:10:\"date_start\";s:19:\"2019-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2019-02-04\";s:10:\"date_start\";s:19:\"2019-02-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-04 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2019-02-05\";s:10:\"date_start\";s:19:\"2019-02-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-05 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2019-02-06\";s:10:\"date_start\";s:19:\"2019-02-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-06 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2019-02-07\";s:10:\"date_start\";s:19:\"2019-02-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-07 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2019-02-08\";s:10:\"date_start\";s:19:\"2019-02-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-08 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2019-02-09\";s:10:\"date_start\";s:19:\"2019-02-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-09 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2019-02-10\";s:10:\"date_start\";s:19:\"2019-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2019-02-11\";s:10:\"date_start\";s:19:\"2019-02-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-11 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2019-02-12\";s:10:\"date_start\";s:19:\"2019-02-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-12 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2019-02-13\";s:10:\"date_start\";s:19:\"2019-02-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-13 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2019-02-14\";s:10:\"date_start\";s:19:\"2019-02-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-14 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:14;a:6:{s:8:\"interval\";s:10:\"2019-02-15\";s:10:\"date_start\";s:19:\"2019-02-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-15 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:15;a:6:{s:8:\"interval\";s:10:\"2019-02-16\";s:10:\"date_start\";s:19:\"2019-02-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-16 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:16;a:6:{s:8:\"interval\";s:10:\"2019-02-17\";s:10:\"date_start\";s:19:\"2019-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-17 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-17 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:17;a:6:{s:8:\"interval\";s:10:\"2019-02-18\";s:10:\"date_start\";s:19:\"2019-02-18 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-18 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-18 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-18 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:18;a:6:{s:8:\"interval\";s:10:\"2019-02-19\";s:10:\"date_start\";s:19:\"2019-02-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-19 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-19 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-19 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:19;a:6:{s:8:\"interval\";s:10:\"2019-02-20\";s:10:\"date_start\";s:19:\"2019-02-20 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-20 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-20 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-20 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:20;a:6:{s:8:\"interval\";s:10:\"2019-02-21\";s:10:\"date_start\";s:19:\"2019-02-21 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-21 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-21 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-21 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:21;a:6:{s:8:\"interval\";s:10:\"2019-02-22\";s:10:\"date_start\";s:19:\"2019-02-22 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-22 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-22 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-22 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:22;a:6:{s:8:\"interval\";s:10:\"2019-02-23\";s:10:\"date_start\";s:19:\"2019-02-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-23 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:23;a:6:{s:8:\"interval\";s:10:\"2019-02-24\";s:10:\"date_start\";s:19:\"2019-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-24 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-24 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:24;a:6:{s:8:\"interval\";s:10:\"2019-02-25\";s:10:\"date_start\";s:19:\"2019-02-25 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-25 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-25 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-25 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:25;a:6:{s:8:\"interval\";s:10:\"2019-02-26\";s:10:\"date_start\";s:19:\"2019-02-26 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-26 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-26 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-26 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:26;a:6:{s:8:\"interval\";s:10:\"2019-02-27\";s:10:\"date_start\";s:19:\"2019-02-27 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-27 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-27 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-27 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:27;a:6:{s:8:\"interval\";s:10:\"2019-02-28\";s:10:\"date_start\";s:19:\"2019-02-28 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-28 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-28 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-28 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}i:28;a:6:{s:8:\"interval\";s:10:\"2019-03-01\";s:10:\"date_start\";s:19:\"2019-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:29;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(802, '_transient_timeout_wc_report_downloads_stats_756077ee7d4256b81f04242f1f537d1a', '1583588402', 'no'),
(803, '_transient_wc_report_downloads_stats_756077ee7d4256b81f04242f1f537d1a', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}s:9:\"intervals\";a:29:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-02-01\";s:10:\"date_start\";s:19:\"2020-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:1;a:6:{s:8:\"interval\";s:10:\"2020-02-02\";s:10:\"date_start\";s:19:\"2020-02-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-02 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:2;a:6:{s:8:\"interval\";s:10:\"2020-02-03\";s:10:\"date_start\";s:19:\"2020-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:3;a:6:{s:8:\"interval\";s:10:\"2020-02-04\";s:10:\"date_start\";s:19:\"2020-02-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-04 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:4;a:6:{s:8:\"interval\";s:10:\"2020-02-05\";s:10:\"date_start\";s:19:\"2020-02-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-05 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:5;a:6:{s:8:\"interval\";s:10:\"2020-02-06\";s:10:\"date_start\";s:19:\"2020-02-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-06 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:6;a:6:{s:8:\"interval\";s:10:\"2020-02-07\";s:10:\"date_start\";s:19:\"2020-02-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:7;a:6:{s:8:\"interval\";s:10:\"2020-02-08\";s:10:\"date_start\";s:19:\"2020-02-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-08 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:8;a:6:{s:8:\"interval\";s:10:\"2020-02-09\";s:10:\"date_start\";s:19:\"2020-02-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-09 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:9;a:6:{s:8:\"interval\";s:10:\"2020-02-10\";s:10:\"date_start\";s:19:\"2020-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:10;a:6:{s:8:\"interval\";s:10:\"2020-02-11\";s:10:\"date_start\";s:19:\"2020-02-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-11 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:11;a:6:{s:8:\"interval\";s:10:\"2020-02-12\";s:10:\"date_start\";s:19:\"2020-02-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-12 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:12;a:6:{s:8:\"interval\";s:10:\"2020-02-13\";s:10:\"date_start\";s:19:\"2020-02-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-13 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:13;a:6:{s:8:\"interval\";s:10:\"2020-02-14\";s:10:\"date_start\";s:19:\"2020-02-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:14;a:6:{s:8:\"interval\";s:10:\"2020-02-15\";s:10:\"date_start\";s:19:\"2020-02-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-15 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:15;a:6:{s:8:\"interval\";s:10:\"2020-02-16\";s:10:\"date_start\";s:19:\"2020-02-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-16 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:16;a:6:{s:8:\"interval\";s:10:\"2020-02-17\";s:10:\"date_start\";s:19:\"2020-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-17 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-17 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:17;a:6:{s:8:\"interval\";s:10:\"2020-02-18\";s:10:\"date_start\";s:19:\"2020-02-18 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-18 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-18 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-18 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:18;a:6:{s:8:\"interval\";s:10:\"2020-02-19\";s:10:\"date_start\";s:19:\"2020-02-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-19 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-19 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-19 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:19;a:6:{s:8:\"interval\";s:10:\"2020-02-20\";s:10:\"date_start\";s:19:\"2020-02-20 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-20 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-20 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-20 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:20;a:6:{s:8:\"interval\";s:10:\"2020-02-21\";s:10:\"date_start\";s:19:\"2020-02-21 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-21 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-21 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-21 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:21;a:6:{s:8:\"interval\";s:10:\"2020-02-22\";s:10:\"date_start\";s:19:\"2020-02-22 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-22 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-22 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-22 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:22;a:6:{s:8:\"interval\";s:10:\"2020-02-23\";s:10:\"date_start\";s:19:\"2020-02-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-23 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:23;a:6:{s:8:\"interval\";s:10:\"2020-02-24\";s:10:\"date_start\";s:19:\"2020-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-24 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-24 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:24;a:6:{s:8:\"interval\";s:10:\"2020-02-25\";s:10:\"date_start\";s:19:\"2020-02-25 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-25 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-25 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-25 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:25;a:6:{s:8:\"interval\";s:10:\"2020-02-26\";s:10:\"date_start\";s:19:\"2020-02-26 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-26 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-26 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-26 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:26;a:6:{s:8:\"interval\";s:10:\"2020-02-27\";s:10:\"date_start\";s:19:\"2020-02-27 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-27 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-27 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-27 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:27;a:6:{s:8:\"interval\";s:10:\"2020-02-28\";s:10:\"date_start\";s:19:\"2020-02-28 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-28 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-28 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-28 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:28;a:6:{s:8:\"interval\";s:10:\"2020-02-29\";s:10:\"date_start\";s:19:\"2020-02-29 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-29 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-29 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-29 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}}s:5:\"total\";i:29;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(804, '_transient_timeout_wc_report_downloads_stats_8ec1cfeb2798f3859a4d06217bad3415', '1583588402', 'no'),
(805, '_transient_wc_report_downloads_stats_8ec1cfeb2798f3859a4d06217bad3415', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}s:9:\"intervals\";a:29:{i:0;a:6:{s:8:\"interval\";s:10:\"2019-02-01\";s:10:\"date_start\";s:19:\"2019-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:1;a:6:{s:8:\"interval\";s:10:\"2019-02-02\";s:10:\"date_start\";s:19:\"2019-02-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-02 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:2;a:6:{s:8:\"interval\";s:10:\"2019-02-03\";s:10:\"date_start\";s:19:\"2019-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:3;a:6:{s:8:\"interval\";s:10:\"2019-02-04\";s:10:\"date_start\";s:19:\"2019-02-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-04 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:4;a:6:{s:8:\"interval\";s:10:\"2019-02-05\";s:10:\"date_start\";s:19:\"2019-02-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-05 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:5;a:6:{s:8:\"interval\";s:10:\"2019-02-06\";s:10:\"date_start\";s:19:\"2019-02-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-06 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:6;a:6:{s:8:\"interval\";s:10:\"2019-02-07\";s:10:\"date_start\";s:19:\"2019-02-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-07 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:7;a:6:{s:8:\"interval\";s:10:\"2019-02-08\";s:10:\"date_start\";s:19:\"2019-02-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-08 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:8;a:6:{s:8:\"interval\";s:10:\"2019-02-09\";s:10:\"date_start\";s:19:\"2019-02-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-09 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:9;a:6:{s:8:\"interval\";s:10:\"2019-02-10\";s:10:\"date_start\";s:19:\"2019-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:10;a:6:{s:8:\"interval\";s:10:\"2019-02-11\";s:10:\"date_start\";s:19:\"2019-02-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-11 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:11;a:6:{s:8:\"interval\";s:10:\"2019-02-12\";s:10:\"date_start\";s:19:\"2019-02-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-12 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:12;a:6:{s:8:\"interval\";s:10:\"2019-02-13\";s:10:\"date_start\";s:19:\"2019-02-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-13 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:13;a:6:{s:8:\"interval\";s:10:\"2019-02-14\";s:10:\"date_start\";s:19:\"2019-02-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-14 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:14;a:6:{s:8:\"interval\";s:10:\"2019-02-15\";s:10:\"date_start\";s:19:\"2019-02-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-15 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:15;a:6:{s:8:\"interval\";s:10:\"2019-02-16\";s:10:\"date_start\";s:19:\"2019-02-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-16 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:16;a:6:{s:8:\"interval\";s:10:\"2019-02-17\";s:10:\"date_start\";s:19:\"2019-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-17 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-17 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:17;a:6:{s:8:\"interval\";s:10:\"2019-02-18\";s:10:\"date_start\";s:19:\"2019-02-18 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-18 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-18 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-18 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:18;a:6:{s:8:\"interval\";s:10:\"2019-02-19\";s:10:\"date_start\";s:19:\"2019-02-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-19 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-19 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-19 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:19;a:6:{s:8:\"interval\";s:10:\"2019-02-20\";s:10:\"date_start\";s:19:\"2019-02-20 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-20 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-20 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-20 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:20;a:6:{s:8:\"interval\";s:10:\"2019-02-21\";s:10:\"date_start\";s:19:\"2019-02-21 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-21 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-21 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-21 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:21;a:6:{s:8:\"interval\";s:10:\"2019-02-22\";s:10:\"date_start\";s:19:\"2019-02-22 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-22 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-22 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-22 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:22;a:6:{s:8:\"interval\";s:10:\"2019-02-23\";s:10:\"date_start\";s:19:\"2019-02-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-23 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:23;a:6:{s:8:\"interval\";s:10:\"2019-02-24\";s:10:\"date_start\";s:19:\"2019-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-24 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-24 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:24;a:6:{s:8:\"interval\";s:10:\"2019-02-25\";s:10:\"date_start\";s:19:\"2019-02-25 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-25 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-25 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-25 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:25;a:6:{s:8:\"interval\";s:10:\"2019-02-26\";s:10:\"date_start\";s:19:\"2019-02-26 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-26 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-26 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-26 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:26;a:6:{s:8:\"interval\";s:10:\"2019-02-27\";s:10:\"date_start\";s:19:\"2019-02-27 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-27 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-27 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-27 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:27;a:6:{s:8:\"interval\";s:10:\"2019-02-28\";s:10:\"date_start\";s:19:\"2019-02-28 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-02-28 00:00:00\";s:8:\"date_end\";s:19:\"2019-02-28 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-02-28 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}i:28;a:6:{s:8:\"interval\";s:10:\"2019-03-01\";s:10:\"date_start\";s:19:\"2019-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":1:{s:14:\"download_count\";i:0;}}}s:5:\"total\";i:29;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(806, '_transient_timeout_wc_report_orders_stats_e3e883b2e6f3118e16daf8d45f10ce0b', '1583588403', 'no'),
(807, '_transient_wc_report_orders_stats_e3e883b2e6f3118e16daf8d45f10ce0b', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":12:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:5:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-09\";s:10:\"date_start\";s:19:\"2020-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-29 21:40:00\";s:12:\"date_end_gmt\";s:19:\"2020-02-29 21:40:00\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-08\";s:10:\"date_start\";s:19:\"2020-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-07\";s:10:\"date_start\";s:19:\"2020-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:7:\"2020-06\";s:10:\"date_start\";s:19:\"2020-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:7:\"2020-05\";s:10:\"date_start\";s:19:\"2020-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:5;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(808, '_transient_timeout_wc_report_orders_stats_429e0f31cc1f3996b57219babfc45e3d', '1583588403', 'no');
INSERT INTO `cm_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(809, '_transient_wc_report_orders_stats_429e0f31cc1f3996b57219babfc45e3d', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":11:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:5:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-09\";s:10:\"date_start\";s:19:\"2020-02-24 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-24 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-29 21:40:00\";s:12:\"date_end_gmt\";s:19:\"2020-02-29 21:40:00\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-08\";s:10:\"date_start\";s:19:\"2020-02-17 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-17 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-23 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-23 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-07\";s:10:\"date_start\";s:19:\"2020-02-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-10 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:7:\"2020-06\";s:10:\"date_start\";s:19:\"2020-02-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-03 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:7:\"2020-05\";s:10:\"date_start\";s:19:\"2020-02-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-02-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-02-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-02-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:5;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(879, 'product_cat_children', 'a:0:{}', 'yes'),
(882, '_transient_timeout_wc_report_orders_stats_6774d99d566ce6370ca2f14a1939295c', '1583650136', 'no'),
(883, '_transient_wc_report_orders_stats_6774d99d566ce6370ca2f14a1939295c', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":12:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-09\";s:10:\"date_start\";s:19:\"2020-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-03-01 14:48:00\";s:12:\"date_end_gmt\";s:19:\"2020-03-01 14:48:00\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(884, '_transient_timeout_wc_report_orders_stats_a191cc458bc1348a696403f67feaf103', '1583650136', 'no'),
(885, '_transient_wc_report_orders_stats_a191cc458bc1348a696403f67feaf103', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":11:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-09\";s:10:\"date_start\";s:19:\"2020-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-03-01 14:48:00\";s:12:\"date_end_gmt\";s:19:\"2020-03-01 14:48:00\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(886, '_transient_timeout_wc_report_orders_stats_b01f050fba6948f0e581ccc1916ddc54', '1583650137', 'no'),
(887, '_transient_wc_report_orders_stats_b01f050fba6948f0e581ccc1916ddc54', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":12:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2019-09\";s:10:\"date_start\";s:19:\"2019-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(888, '_transient_timeout_wc_report_orders_stats_4baf39e6d451a31e6d423512de00089a', '1583650137', 'no'),
(889, '_transient_wc_report_orders_stats_4baf39e6d451a31e6d423512de00089a', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":11:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2019-09\";s:10:\"date_start\";s:19:\"2019-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(890, '_transient_timeout_wc_report_orders_stats_6c6ab375797bc22463c4fbbc518bf3dc', '1583650137', 'no'),
(891, '_transient_wc_report_orders_stats_6c6ab375797bc22463c4fbbc518bf3dc', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":12:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-03-01\";s:10:\"date_start\";s:19:\"2020-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(892, '_transient_timeout_wc_report_orders_stats_569f3798207b4fb0e851d222cce956f1', '1583650137', 'no'),
(893, '_transient_wc_report_orders_stats_569f3798207b4fb0e851d222cce956f1', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":12:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:10:\"2019-03-01\";s:10:\"date_start\";s:19:\"2019-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":11:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:11:\"gross_sales\";d:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(894, '_transient_timeout_wc_report_orders_stats_e9a316c0ec7d1bfc53b337a68ea98098', '1583650138', 'no'),
(895, '_transient_wc_report_orders_stats_e9a316c0ec7d1bfc53b337a68ea98098', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":11:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-03-01\";s:10:\"date_start\";s:19:\"2020-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(896, '_transient_timeout_wc_report_orders_stats_3dd44cc151097eae7453b7ba4b3cc28e', '1583650138', 'no'),
(897, '_transient_wc_report_orders_stats_3dd44cc151097eae7453b7ba4b3cc28e', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":11:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:10:\"2019-03-01\";s:10:\"date_start\";s:19:\"2019-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":10:{s:11:\"net_revenue\";d:0;s:15:\"avg_order_value\";d:0;s:12:\"orders_count\";i:0;s:19:\"avg_items_per_order\";d:0;s:14:\"num_items_sold\";i:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(898, '_transient_timeout_wc_report_coupons_stats_ea5e9dc9b9acc60f217f4b63f6d41930', '1583650139', 'no'),
(899, '_transient_wc_report_coupons_stats_ea5e9dc9b9acc60f217f4b63f6d41930', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-03-01\";s:10:\"date_start\";s:19:\"2020-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(900, '_transient_timeout_wc_report_coupons_stats_a19b88fb8f525d52742b4ae4868bcc6e', '1583650140', 'no'),
(901, '_transient_wc_report_coupons_stats_a19b88fb8f525d52742b4ae4868bcc6e', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:10:\"2019-03-01\";s:10:\"date_start\";s:19:\"2019-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:6:\"amount\";d:0;s:13:\"coupons_count\";i:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(902, '_transient_timeout_wc_report_taxes_stats_d67291db64ee1fdabe176315ab7898a2', '1583650140', 'no'),
(903, '_transient_wc_report_taxes_stats_d67291db64ee1fdabe176315ab7898a2', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-03-01\";s:10:\"date_start\";s:19:\"2020-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(904, '_transient_timeout_wc_report_taxes_stats_ccc5002e1296112e317ff392d58aaa1c', '1583650141', 'no'),
(905, '_transient_wc_report_taxes_stats_ccc5002e1296112e317ff392d58aaa1c', 'a:2:{s:7:\"version\";s:10:\"1582983596\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:10:\"2019-03-01\";s:10:\"date_start\";s:19:\"2019-03-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-03-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-03-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-03-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:9:\"tax_codes\";i:0;s:9:\"total_tax\";d:0;s:9:\"order_tax\";d:0;s:12:\"shipping_tax\";d:0;s:12:\"orders_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(915, '_transient_product-transient-version', '1583046786', 'yes'),
(943, '_transient_timeout_wc_term_counts', '1586160450', 'no'),
(944, '_transient_wc_term_counts', 'a:11:{i:19;s:1:\"8\";i:20;s:2:\"11\";i:25;s:1:\"4\";i:26;s:1:\"4\";i:27;s:1:\"4\";i:28;s:0:\"\";i:21;s:0:\"\";i:22;s:0:\"\";i:23;s:0:\"\";i:24;s:0:\"\";i:15;s:0:\"\";}', 'no'),
(946, 'woocommerce_tracker_ua', 'a:1:{i:0;s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36\";}', 'yes'),
(947, '_site_transient_timeout_browser_37a36c2f4dcdaeece8d58b455ac2abba', '1583651671', 'no'),
(948, '_site_transient_browser_37a36c2f4dcdaeece8d58b455ac2abba', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"80.0.3987.122\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(957, '_transient_timeout_wc_low_stock_count', '1585638873', 'no'),
(958, '_transient_wc_low_stock_count', '0', 'no'),
(959, '_transient_timeout_wc_outofstock_count', '1585638873', 'no'),
(960, '_transient_wc_outofstock_count', '0', 'no'),
(986, 'yit_recently_activated', 'a:0:{}', 'yes'),
(987, '_site_transient_timeout_yith_promo_message', '3166180492', 'no'),
(988, '_site_transient_yith_promo_message', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- Default border color: #acc327 -->\n<!-- Default background color: #ecf7ed -->\n\n<promotions>\n    <expiry_date>2019-12-10</expiry_date>\n    <promo>\n        <promo_id>yithblackfriday2019</promo_id>\n        <title><![CDATA[<strong>YITH Black Friday</strong>]]></title>\n        <description><![CDATA[\n            Don\'t miss our <strong>30% discount</strong> on all our products! No coupon needed in cart. Valid from <strong>28th November</strong> to <strong>2nd December</strong>.\n        ]]></description>\n        <link>\n            <label>Get your deals now!</label>\n            <url><![CDATA[https://yithemes.com]]></url>\n        </link>\n        <style>\n            <image_bg_color>#272121</image_bg_color>\n            <border_color>#272121</border_color>\n            <background_color>#ffffff</background_color>\n        </style>\n        <start_date>2019-11-27 23:59:59</start_date>\n        <end_date>2019-12-03 08:00:00</end_date>\n    </promo>\n</promotions>', 'no'),
(995, 'wlcms_admin_bar_menus', 'a:27:{s:12:\"user-actions\";O:8:\"stdClass\":6:{s:2:\"id\";s:12:\"user-actions\";s:5:\"title\";b:0;s:6:\"parent\";s:10:\"my-account\";s:4:\"href\";b:0;s:5:\"group\";b:1;s:4:\"meta\";a:0:{}}s:9:\"user-info\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"user-info\";s:5:\"title\";s:303:\"<img alt=\'\' src=\'http://0.gravatar.com/avatar/0f80b05ce35acc6474b7f16a6835996b?s=64&#038;d=mm&#038;r=g\' srcset=\'http://0.gravatar.com/avatar/0f80b05ce35acc6474b7f16a6835996b?s=128&#038;d=mm&#038;r=g 2x\' class=\'avatar avatar-64 photo\' height=\'64\' width=\'64\' /><span class=\'display-name\'>superadmin</span>\";s:6:\"parent\";s:12:\"user-actions\";s:4:\"href\";s:41:\"http://cosmetic.test/wp-admin/profile.php\";s:5:\"group\";b:0;s:4:\"meta\";a:1:{s:8:\"tabindex\";i:-1;}}s:12:\"edit-profile\";O:8:\"stdClass\":6:{s:2:\"id\";s:12:\"edit-profile\";s:5:\"title\";s:15:\"Edit My Profile\";s:6:\"parent\";s:12:\"user-actions\";s:4:\"href\";s:41:\"http://cosmetic.test/wp-admin/profile.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:6:\"logout\";O:8:\"stdClass\":6:{s:2:\"id\";s:6:\"logout\";s:5:\"title\";s:7:\"Log Out\";s:6:\"parent\";s:12:\"user-actions\";s:4:\"href\";s:71:\"http://cosmetic.test/wp-login.php?action=logout&amp;_wpnonce=1ac6c5f5a2\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:11:\"menu-toggle\";O:8:\"stdClass\":6:{s:2:\"id\";s:11:\"menu-toggle\";s:5:\"title\";s:73:\"<span class=\"ab-icon\"></span><span class=\"screen-reader-text\">Menu</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:1:\"#\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:10:\"my-account\";O:8:\"stdClass\":6:{s:2:\"id\";s:10:\"my-account\";s:5:\"title\";s:309:\"Howdy, <span class=\"display-name\">superadmin</span><img alt=\'\' src=\'http://0.gravatar.com/avatar/0f80b05ce35acc6474b7f16a6835996b?s=26&#038;d=mm&#038;r=g\' srcset=\'http://0.gravatar.com/avatar/0f80b05ce35acc6474b7f16a6835996b?s=52&#038;d=mm&#038;r=g 2x\' class=\'avatar avatar-26 photo\' height=\'26\' width=\'26\' />\";s:6:\"parent\";s:13:\"top-secondary\";s:4:\"href\";s:41:\"http://cosmetic.test/wp-admin/profile.php\";s:5:\"group\";b:0;s:4:\"meta\";a:1:{s:5:\"class\";s:11:\"with-avatar\";}}s:7:\"wp-logo\";O:8:\"stdClass\":6:{s:2:\"id\";s:7:\"wp-logo\";s:5:\"title\";s:84:\"<span class=\"ab-icon\"></span><span class=\"screen-reader-text\">About WordPress</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:39:\"http://cosmetic.test/wp-admin/about.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:5:\"about\";O:8:\"stdClass\":6:{s:2:\"id\";s:5:\"about\";s:5:\"title\";s:15:\"About WordPress\";s:6:\"parent\";s:7:\"wp-logo\";s:4:\"href\";s:39:\"http://cosmetic.test/wp-admin/about.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:5:\"wporg\";O:8:\"stdClass\":6:{s:2:\"id\";s:5:\"wporg\";s:5:\"title\";s:13:\"WordPress.org\";s:6:\"parent\";s:16:\"wp-logo-external\";s:4:\"href\";s:22:\"https://wordpress.org/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:13:\"documentation\";O:8:\"stdClass\":6:{s:2:\"id\";s:13:\"documentation\";s:5:\"title\";s:13:\"Documentation\";s:6:\"parent\";s:16:\"wp-logo-external\";s:4:\"href\";s:28:\"https://codex.wordpress.org/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:14:\"support-forums\";O:8:\"stdClass\":6:{s:2:\"id\";s:14:\"support-forums\";s:5:\"title\";s:7:\"Support\";s:6:\"parent\";s:16:\"wp-logo-external\";s:4:\"href\";s:30:\"https://wordpress.org/support/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:8:\"feedback\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"feedback\";s:5:\"title\";s:8:\"Feedback\";s:6:\"parent\";s:16:\"wp-logo-external\";s:4:\"href\";s:57:\"https://wordpress.org/support/forum/requests-and-feedback\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:9:\"site-name\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"site-name\";s:5:\"title\";s:12:\"JM Cosmetics\";s:6:\"parent\";b:0;s:4:\"href\";s:21:\"http://cosmetic.test/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:9:\"view-site\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"view-site\";s:5:\"title\";s:10:\"Visit Site\";s:6:\"parent\";s:9:\"site-name\";s:4:\"href\";s:21:\"http://cosmetic.test/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:10:\"view-store\";O:8:\"stdClass\":6:{s:2:\"id\";s:10:\"view-store\";s:5:\"title\";s:11:\"Visit Store\";s:6:\"parent\";s:9:\"site-name\";s:4:\"href\";s:28:\"http://cosmetic.test/shop-2/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:7:\"updates\";O:8:\"stdClass\":6:{s:2:\"id\";s:7:\"updates\";s:5:\"title\";s:131:\"<span class=\"ab-icon\"></span><span class=\"ab-label\">2</span><span class=\"screen-reader-text\">1 Plugin Update, 1 Theme Update</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:45:\"http://cosmetic.test/wp-admin/update-core.php\";s:5:\"group\";b:0;s:4:\"meta\";a:1:{s:5:\"title\";s:31:\"1 Plugin Update, 1 Theme Update\";}}s:8:\"comments\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"comments\";s:5:\"title\";s:206:\"<span class=\"ab-icon\"></span><span class=\"ab-label awaiting-mod pending-count count-0\" aria-hidden=\"true\">0</span><span class=\"screen-reader-text comments-in-moderation-text\">0 Comments in moderation</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:47:\"http://cosmetic.test/wp-admin/edit-comments.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:11:\"new-content\";O:8:\"stdClass\":6:{s:2:\"id\";s:11:\"new-content\";s:5:\"title\";s:62:\"<span class=\"ab-icon\"></span><span class=\"ab-label\">New</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:42:\"http://cosmetic.test/wp-admin/post-new.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:8:\"new-post\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"new-post\";s:5:\"title\";s:4:\"Post\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:42:\"http://cosmetic.test/wp-admin/post-new.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:9:\"new-media\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"new-media\";s:5:\"title\";s:5:\"Media\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:43:\"http://cosmetic.test/wp-admin/media-new.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:8:\"new-page\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"new-page\";s:5:\"title\";s:4:\"Page\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:57:\"http://cosmetic.test/wp-admin/post-new.php?post_type=page\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:11:\"new-product\";O:8:\"stdClass\":6:{s:2:\"id\";s:11:\"new-product\";s:5:\"title\";s:7:\"Product\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:60:\"http://cosmetic.test/wp-admin/post-new.php?post_type=product\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:14:\"new-shop_order\";O:8:\"stdClass\":6:{s:2:\"id\";s:14:\"new-shop_order\";s:5:\"title\";s:5:\"Order\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:63:\"http://cosmetic.test/wp-admin/post-new.php?post_type=shop_order\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:15:\"new-shop_coupon\";O:8:\"stdClass\":6:{s:2:\"id\";s:15:\"new-shop_coupon\";s:5:\"title\";s:6:\"Coupon\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:64:\"http://cosmetic.test/wp-admin/post-new.php?post_type=shop_coupon\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:8:\"new-user\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"new-user\";s:5:\"title\";s:4:\"User\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:42:\"http://cosmetic.test/wp-admin/user-new.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:13:\"top-secondary\";O:8:\"stdClass\":6:{s:2:\"id\";s:13:\"top-secondary\";s:5:\"title\";b:0;s:6:\"parent\";b:0;s:4:\"href\";b:0;s:5:\"group\";b:1;s:4:\"meta\";a:1:{s:5:\"class\";s:16:\"ab-top-secondary\";}}s:16:\"wp-logo-external\";O:8:\"stdClass\":6:{s:2:\"id\";s:16:\"wp-logo-external\";s:5:\"title\";b:0;s:6:\"parent\";s:7:\"wp-logo\";s:4:\"href\";b:0;s:5:\"group\";b:1;s:4:\"meta\";a:1:{s:5:\"class\";s:16:\"ab-sub-secondary\";}}}', 'no'),
(997, 'wlcms_options', 'a:77:{s:7:\"version\";s:5:\"2.1.2\";s:14:\"developer_icon\";s:0:\"\";s:25:\"use_developer_icon_footer\";s:0:\"\";s:25:\"developer_icon_footer_url\";s:0:\"\";s:25:\"developer_side_menu_image\";s:0:\"\";s:24:\"developer_icon_admin_bar\";s:0:\"\";s:25:\"developer_branding_footer\";s:0:\"\";s:29:\"use_developer_side_menu_image\";s:0:\"\";s:29:\"hide_wordpress_logo_and_links\";s:1:\"1\";s:15:\"hide_wp_version\";s:1:\"1\";s:14:\"admin_bar_logo\";s:0:\"\";s:20:\"admin_bar_logo_width\";s:0:\"\";s:18:\"admin_bar_alt_text\";s:11:\"Ryan Dingle\";s:20:\"admin_bar_howdy_text\";s:0:\"\";s:13:\"admin_bar_url\";s:25:\"https://rdingle.com/about\";s:15:\"side_menu_image\";s:0:\"\";s:25:\"collapsed_side_menu_image\";s:0:\"\";s:18:\"side_menu_link_url\";s:25:\"https://rdingle.com/about\";s:18:\"side_menu_alt_text\";s:11:\"Ryan Dingle\";s:12:\"footer_image\";s:0:\"\";s:10:\"footer_url\";s:25:\"https://rdingle.com/about\";s:11:\"footer_html\";s:0:\"\";s:14:\"dashboard_icon\";s:0:\"\";s:15:\"dashboard_title\";s:12:\"JM Cosmetics\";s:19:\"dashboard_role_stat\";s:0:\"\";s:34:\"dashboard_widgets_visibility_roles\";s:0:\"\";s:25:\"hide_all_dashboard_panels\";s:1:\"1\";s:16:\"hide_at_a_glance\";s:1:\"1\";s:15:\"hide_activities\";s:1:\"1\";s:20:\"hide_recent_comments\";s:1:\"1\";s:16:\"hide_quick_press\";s:1:\"1\";s:20:\"hide_news_and_events\";s:1:\"1\";s:23:\"remove_empty_dash_panel\";s:1:\"1\";s:13:\"welcome_panel\";a:2:{i:0;a:4:{s:9:\"is_active\";b:0;s:10:\"show_title\";b:0;s:13:\"template_type\";s:4:\"html\";s:10:\"visible_to\";a:5:{i:0;s:13:\"administrator\";i:1;s:6:\"editor\";i:2;s:6:\"author\";i:3;s:11:\"contributor\";i:4;s:10:\"subscriber\";}}i:1;a:4:{s:9:\"is_active\";b:0;s:10:\"show_title\";b:0;s:13:\"template_type\";s:4:\"html\";s:10:\"visible_to\";a:5:{i:0;s:13:\"administrator\";i:1;s:6:\"editor\";i:2;s:6:\"author\";i:3;s:11:\"contributor\";i:4;s:10:\"subscriber\";}}}s:17:\"add_own_rss_panel\";s:0:\"\";s:23:\"rss_feed_number_of_item\";s:0:\"\";s:17:\"show_post_content\";s:0:\"\";s:16:\"rss_introduction\";s:0:\"\";s:8:\"rss_logo\";s:0:\"\";s:9:\"rss_title\";s:11:\"Ryan Dingle\";s:11:\"wlcms_admin\";s:0:\"\";s:18:\"enable_wlcms_admin\";s:0:\"\";s:18:\"hide_admin_bar_all\";s:0:\"\";s:13:\"hide_help_box\";s:0:\"\";s:19:\"hide_screen_options\";s:0:\"\";s:17:\"hide_nag_messages\";s:0:\"\";s:25:\"settings_custom_css_admin\";s:0:\"\";s:23:\"settings_custom_css_url\";s:0:\"\";s:14:\"developer_name\";s:11:\"Ryan Dingle\";s:11:\"footer_text\";s:22:\"Created by Ryan Dingle\";s:16:\"rss_feed_address\";s:0:\"\";s:17:\"custom_page_title\";s:12:\"JM Cosmetics\";s:18:\"logo_bottom_margin\";s:0:\"\";s:10:\"logo_width\";i:1125;s:11:\"logo_height\";i:1103;s:16:\"background_color\";s:0:\"\";s:16:\"background_image\";s:0:\"\";s:28:\"full_screen_background_image\";s:0:\"\";s:20:\"background_positions\";s:0:\"\";s:17:\"background_repeat\";s:0:\"\";s:27:\"hide_register_lost_password\";s:0:\"\";s:17:\"hide_back_to_link\";s:0:\"\";s:21:\"form_background_color\";s:0:\"\";s:16:\"form_label_color\";s:0:\"\";s:17:\"form_button_color\";s:0:\"\";s:22:\"form_button_text_color\";s:0:\"\";s:23:\"form_button_hover_color\";s:0:\"\";s:28:\"form_button_text_hover_color\";s:0:\"\";s:27:\"back_to_register_link_color\";s:0:\"\";s:33:\"back_to_register_link_hover_color\";s:0:\"\";s:25:\"privacy_policy_link_color\";s:0:\"\";s:31:\"privacy_policy_link_hover_color\";s:0:\"\";s:16:\"login_custom_css\";s:0:\"\";s:15:\"login_custom_js\";s:0:\"\";s:10:\"login_logo\";s:56:\"http://cosmetic.test/wp-content/uploads/2020/02/logo.jpg\";s:15:\"add_retina_logo\";s:0:\"\";s:17:\"retina_login_logo\";s:0:\"\";}', 'yes'),
(998, '_wlcms_messages', 'a:1:{s:7:\"success\";a:1:{i:0;s:15:\"Settings saved.\";}}', 'yes'),
(1012, '_site_transient_timeout_php_check_73ecd64509db505b6046b20394d377da', '1583653866', 'no'),
(1013, '_site_transient_php_check_73ecd64509db505b6046b20394d377da', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(1023, '_transient_timeout_jetpack_idc_allowed', '1583571537', 'no'),
(1024, '_transient_jetpack_idc_allowed', '1', 'no'),
(1027, '_transient_timeout__woocommerce_helper_subscriptions', '1583568845', 'no'),
(1028, '_transient__woocommerce_helper_subscriptions', 'a:0:{}', 'no'),
(1029, '_site_transient_timeout_theme_roots', '1583569745', 'no'),
(1030, '_site_transient_theme_roots', 'a:7:{s:10:\"storefront\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";s:15:\"zigcy-cosmetics\";s:7:\"/themes\";s:10:\"zigcy-lite\";s:7:\"/themes\";}', 'no'),
(1031, '_transient_timeout__woocommerce_helper_updates', '1583611145', 'no'),
(1032, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"358c40eaa335ac0d027f3e9faa3c726e\";s:7:\"updated\";i:1583567945;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(1033, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1583567949;s:7:\"checked\";a:7:{s:10:\"storefront\";s:5:\"2.5.4\";s:14:\"twentynineteen\";s:3:\"1.4\";s:15:\"twentyseventeen\";s:3:\"2.2\";s:13:\"twentysixteen\";s:3:\"2.0\";s:12:\"twentytwenty\";s:3:\"1.1\";s:15:\"zigcy-cosmetics\";s:5:\"1.0.4\";s:10:\"zigcy-lite\";s:5:\"1.2.4\";}s:8:\"response\";a:1:{s:10:\"storefront\";a:6:{s:5:\"theme\";s:10:\"storefront\";s:11:\"new_version\";s:5:\"2.5.5\";s:3:\"url\";s:40:\"https://wordpress.org/themes/storefront/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/storefront.2.5.5.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";b:0;}}s:12:\"translations\";a:0:{}}', 'no'),
(1034, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1583567950;s:7:\"checked\";a:14:{s:45:\"access-demo-importer/access-demo-importer.php\";s:5:\"1.0.2\";s:19:\"akismet/akismet.php\";s:5:\"4.1.3\";s:53:\"facebook-for-woocommerce/facebook-for-woocommerce.php\";s:6:\"1.9.15\";s:9:\"hello.php\";s:5:\"1.7.2\";s:19:\"jetpack/jetpack.php\";s:5:\"8.2.3\";s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";s:5:\"2.3.3\";s:32:\"white-label-cms/wlcms-plugin.php\";s:5:\"2.1.2\";s:27:\"woocommerce/woocommerce.php\";s:5:\"3.9.2\";s:39:\"woocommerce-admin/woocommerce-admin.php\";s:6:\"0.25.1\";s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";s:6:\"1.6.20\";s:45:\"woocommerce-services/woocommerce-services.php\";s:6:\"1.22.3\";s:33:\"yith-woocommerce-compare/init.php\";s:6:\"2.3.18\";s:36:\"yith-woocommerce-quick-view/init.php\";s:6:\"1.3.18\";s:34:\"yith-woocommerce-wishlist/init.php\";s:5:\"3.0.6\";}s:8:\"response\";a:8:{s:53:\"facebook-for-woocommerce/facebook-for-woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:38:\"w.org/plugins/facebook-for-woocommerce\";s:4:\"slug\";s:24:\"facebook-for-woocommerce\";s:6:\"plugin\";s:53:\"facebook-for-woocommerce/facebook-for-woocommerce.php\";s:11:\"new_version\";s:6:\"1.10.0\";s:3:\"url\";s:55:\"https://wordpress.org/plugins/facebook-for-woocommerce/\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/plugin/facebook-for-woocommerce.1.10.0.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:77:\"https://ps.w.org/facebook-for-woocommerce/assets/icon-256x256.png?rev=2040223\";s:2:\"1x\";s:69:\"https://ps.w.org/facebook-for-woocommerce/assets/icon.svg?rev=2040223\";s:3:\"svg\";s:69:\"https://ps.w.org/facebook-for-woocommerce/assets/icon.svg?rev=2040223\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.5\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:19:\"jetpack/jetpack.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/jetpack\";s:4:\"slug\";s:7:\"jetpack\";s:6:\"plugin\";s:19:\"jetpack/jetpack.php\";s:11:\"new_version\";s:3:\"8.3\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/jetpack/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/jetpack.8.3.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:60:\"https://ps.w.org/jetpack/assets/icon-256x256.png?rev=1791404\";s:2:\"1x\";s:52:\"https://ps.w.org/jetpack/assets/icon.svg?rev=1791404\";s:3:\"svg\";s:52:\"https://ps.w.org/jetpack/assets/icon.svg?rev=1791404\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/jetpack/assets/banner-1544x500.png?rev=1791404\";s:2:\"1x\";s:62:\"https://ps.w.org/jetpack/assets/banner-772x250.png?rev=1791404\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.3.2\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:39:\"w.org/plugins/mailchimp-for-woocommerce\";s:4:\"slug\";s:25:\"mailchimp-for-woocommerce\";s:6:\"plugin\";s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";s:11:\"new_version\";s:5:\"2.3.4\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/mailchimp-for-woocommerce/\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/plugin/mailchimp-for-woocommerce.2.3.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/mailchimp-for-woocommerce/assets/icon-256x256.png?rev=1509501\";s:2:\"1x\";s:78:\"https://ps.w.org/mailchimp-for-woocommerce/assets/icon-256x256.png?rev=1509501\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/mailchimp-for-woocommerce/assets/banner-1544x500.png?rev=1950415\";s:2:\"1x\";s:80:\"https://ps.w.org/mailchimp-for-woocommerce/assets/banner-772x250.jpg?rev=1950415\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.3.2\";s:12:\"requires_php\";s:3:\"7.0\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.9.3\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.9.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2075035\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2075035\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2075035\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2075035\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.3.2\";s:12:\"requires_php\";s:3:\"7.0\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:39:\"woocommerce-admin/woocommerce-admin.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:31:\"w.org/plugins/woocommerce-admin\";s:4:\"slug\";s:17:\"woocommerce-admin\";s:6:\"plugin\";s:39:\"woocommerce-admin/woocommerce-admin.php\";s:11:\"new_version\";s:5:\"1.0.0\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/woocommerce-admin/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce-admin.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/woocommerce-admin/assets/icon-256x256.jpg?rev=2057866\";s:2:\"1x\";s:70:\"https://ps.w.org/woocommerce-admin/assets/icon-128x128.jpg?rev=2057866\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/woocommerce-admin/assets/banner-1544x500.jpg?rev=2057866\";s:2:\"1x\";s:72:\"https://ps.w.org/woocommerce-admin/assets/banner-772x250.jpg?rev=2057866\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.3.2\";s:12:\"requires_php\";s:6:\"5.6.20\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:45:\"woocommerce-services/woocommerce-services.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:34:\"w.org/plugins/woocommerce-services\";s:4:\"slug\";s:20:\"woocommerce-services\";s:6:\"plugin\";s:45:\"woocommerce-services/woocommerce-services.php\";s:11:\"new_version\";s:6:\"1.22.4\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/woocommerce-services/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/woocommerce-services.1.22.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/woocommerce-services/assets/icon-256x256.png?rev=1910075\";s:2:\"1x\";s:73:\"https://ps.w.org/woocommerce-services/assets/icon-128x128.png?rev=1910075\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/woocommerce-services/assets/banner-1544x500.png?rev=1962920\";s:2:\"1x\";s:75:\"https://ps.w.org/woocommerce-services/assets/banner-772x250.png?rev=1962920\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.3.2\";s:12:\"requires_php\";s:3:\"5.3\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:36:\"yith-woocommerce-quick-view/init.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:41:\"w.org/plugins/yith-woocommerce-quick-view\";s:4:\"slug\";s:27:\"yith-woocommerce-quick-view\";s:6:\"plugin\";s:36:\"yith-woocommerce-quick-view/init.php\";s:11:\"new_version\";s:5:\"1.4.0\";s:3:\"url\";s:58:\"https://wordpress.org/plugins/yith-woocommerce-quick-view/\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/plugin/yith-woocommerce-quick-view.1.4.0.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:80:\"https://ps.w.org/yith-woocommerce-quick-view/assets/icon-128x128.jpg?rev=1460911\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:83:\"https://ps.w.org/yith-woocommerce-quick-view/assets/banner-1544x500.jpg?rev=1460911\";s:2:\"1x\";s:82:\"https://ps.w.org/yith-woocommerce-quick-view/assets/banner-772x250.jpg?rev=1460911\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.4\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:34:\"yith-woocommerce-wishlist/init.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:39:\"w.org/plugins/yith-woocommerce-wishlist\";s:4:\"slug\";s:25:\"yith-woocommerce-wishlist\";s:6:\"plugin\";s:34:\"yith-woocommerce-wishlist/init.php\";s:11:\"new_version\";s:5:\"3.0.8\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/yith-woocommerce-wishlist/\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/plugin/yith-woocommerce-wishlist.3.0.8.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:78:\"https://ps.w.org/yith-woocommerce-wishlist/assets/icon-128x128.jpg?rev=2215573\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/yith-woocommerce-wishlist/assets/banner-1544x500.jpg?rev=2209192\";s:2:\"1x\";s:80:\"https://ps.w.org/yith-woocommerce-wishlist/assets/banner-772x250.jpg?rev=2209192\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.4\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:6:{s:45:\"access-demo-importer/access-demo-importer.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:34:\"w.org/plugins/access-demo-importer\";s:4:\"slug\";s:20:\"access-demo-importer\";s:6:\"plugin\";s:45:\"access-demo-importer/access-demo-importer.php\";s:11:\"new_version\";s:5:\"1.0.2\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/access-demo-importer/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/access-demo-importer.1.0.2.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:64:\"https://s.w.org/plugins/geopattern-icon/access-demo-importer.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.3\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:32:\"white-label-cms/wlcms-plugin.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/white-label-cms\";s:4:\"slug\";s:15:\"white-label-cms\";s:6:\"plugin\";s:32:\"white-label-cms/wlcms-plugin.php\";s:11:\"new_version\";s:5:\"2.1.2\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/white-label-cms/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/white-label-cms.2.1.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/white-label-cms/assets/icon-256x256.png?rev=1977768\";s:2:\"1x\";s:68:\"https://ps.w.org/white-label-cms/assets/icon-128x128.png?rev=1977768\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/white-label-cms/assets/banner-1544x500.png?rev=2010322\";s:2:\"1x\";s:70:\"https://ps.w.org/white-label-cms/assets/banner-772x250.png?rev=2010322\";}s:11:\"banners_rtl\";a:0:{}}s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:57:\"w.org/plugins/woocommerce-gateway-paypal-express-checkout\";s:4:\"slug\";s:43:\"woocommerce-gateway-paypal-express-checkout\";s:6:\"plugin\";s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";s:11:\"new_version\";s:6:\"1.6.20\";s:3:\"url\";s:74:\"https://wordpress.org/plugins/woocommerce-gateway-paypal-express-checkout/\";s:7:\"package\";s:93:\"https://downloads.wordpress.org/plugin/woocommerce-gateway-paypal-express-checkout.1.6.20.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:96:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/icon-256x256.png?rev=1900204\";s:2:\"1x\";s:96:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/icon-128x128.png?rev=1900204\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:99:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/banner-1544x500.png?rev=1948167\";s:2:\"1x\";s:98:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/banner-772x250.png?rev=1948167\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"yith-woocommerce-compare/init.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:38:\"w.org/plugins/yith-woocommerce-compare\";s:4:\"slug\";s:24:\"yith-woocommerce-compare\";s:6:\"plugin\";s:33:\"yith-woocommerce-compare/init.php\";s:11:\"new_version\";s:6:\"2.3.18\";s:3:\"url\";s:55:\"https://wordpress.org/plugins/yith-woocommerce-compare/\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/plugin/yith-woocommerce-compare.2.3.18.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:77:\"https://ps.w.org/yith-woocommerce-compare/assets/icon-128x128.jpg?rev=1460909\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:80:\"https://ps.w.org/yith-woocommerce-compare/assets/banner-1544x500.jpg?rev=1460909\";s:2:\"1x\";s:79:\"https://ps.w.org/yith-woocommerce-compare/assets/banner-772x250.jpg?rev=1460909\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(1039, '_transient_timeout_wc_admin_unsnooze_admin_notes_checked', '1583571555', 'no'),
(1040, '_transient_wc_admin_unsnooze_admin_notes_checked', 'yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `cm_postmeta`
--

CREATE TABLE `cm_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_postmeta`
--

INSERT INTO `cm_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2, 3, '_wp_page_template', 'default'),
(5, 6, '_wp_attached_file', 'woocommerce-placeholder.png'),
(6, 6, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(11, 13, '_wp_trash_meta_status', 'publish'),
(12, 13, '_wp_trash_meta_time', '1582443371'),
(13, 48, '_wp_attached_file', '2018/11/siblings-817369_1920.jpg'),
(14, 48, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1339;s:4:\"file\";s:32:\"2018/11/siblings-817369_1920.jpg\";s:5:\"sizes\";a:13:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"siblings-817369_1920-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"siblings-817369_1920-300x209.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"siblings-817369_1920-1024x714.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:714;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"siblings-817369_1920-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"siblings-817369_1920-768x536.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:536;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:34:\"siblings-817369_1920-1536x1071.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1071;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:32:\"siblings-817369_1920-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:32:\"siblings-817369_1920-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"siblings-817369_1920-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"siblings-817369_1920-600x418.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:418;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:32:\"siblings-817369_1920-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"siblings-817369_1920-600x418.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:418;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"siblings-817369_1920-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(15, 49, '_wp_attached_file', '2018/11/gadgets-336635_1920.jpg'),
(16, 49, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1263;s:4:\"file\";s:31:\"2018/11/gadgets-336635_1920.jpg\";s:5:\"sizes\";a:13:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:31:\"gadgets-336635_1920-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"gadgets-336635_1920-300x197.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:197;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"gadgets-336635_1920-1024x674.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:674;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"gadgets-336635_1920-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"gadgets-336635_1920-768x505.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:505;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:33:\"gadgets-336635_1920-1536x1010.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1010;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:31:\"gadgets-336635_1920-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:31:\"gadgets-336635_1920-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:31:\"gadgets-336635_1920-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:31:\"gadgets-336635_1920-600x395.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:395;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:31:\"gadgets-336635_1920-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:31:\"gadgets-336635_1920-600x395.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:395;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:31:\"gadgets-336635_1920-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(17, 50, '_wp_attached_file', '2018/11/smartwatch-828786_960_720.jpg'),
(18, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:37:\"2018/11/smartwatch-828786_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:37:\"smartwatch-828786_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"smartwatch-828786_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"smartwatch-828786_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"smartwatch-828786_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:37:\"smartwatch-828786_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:37:\"smartwatch-828786_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:37:\"smartwatch-828786_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:37:\"smartwatch-828786_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:37:\"smartwatch-828786_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:37:\"smartwatch-828786_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:37:\"smartwatch-828786_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(19, 54, '_wp_attached_file', '2018/11/bag-15841_960_720.jpg'),
(20, 54, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:29:\"2018/11/bag-15841_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:29:\"bag-15841_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"bag-15841_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"bag-15841_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"bag-15841_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:29:\"bag-15841_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:29:\"bag-15841_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:29:\"bag-15841_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:29:\"bag-15841_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:29:\"bag-15841_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:29:\"bag-15841_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:29:\"bag-15841_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(21, 56, '_wp_attached_file', '2018/11/white-926838_960_720.jpg'),
(22, 56, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:32:\"2018/11/white-926838_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"white-926838_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"white-926838_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"white-926838_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"white-926838_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:32:\"white-926838_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:32:\"white-926838_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"white-926838_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"white-926838_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:32:\"white-926838_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"white-926838_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"white-926838_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(23, 58, '_wp_attached_file', '2018/11/waiting-410328_960_720.jpg'),
(24, 58, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:34:\"2018/11/waiting-410328_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:34:\"waiting-410328_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"waiting-410328_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"waiting-410328_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"waiting-410328_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:34:\"waiting-410328_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:34:\"waiting-410328_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:34:\"waiting-410328_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:34:\"waiting-410328_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:34:\"waiting-410328_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:34:\"waiting-410328_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:34:\"waiting-410328_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(25, 60, '_wp_attached_file', '2018/11/business-suit-690048_960_720.jpg'),
(26, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:360;s:4:\"file\";s:40:\"2018/11/business-suit-690048_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:40:\"business-suit-690048_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:40:\"business-suit-690048_960_720-300x113.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:113;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:40:\"business-suit-690048_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:40:\"business-suit-690048_960_720-768x288.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:288;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:40:\"business-suit-690048_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:40:\"business-suit-690048_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:40:\"business-suit-690048_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:40:\"business-suit-690048_960_720-600x225.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:40:\"business-suit-690048_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:40:\"business-suit-690048_960_720-600x225.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:40:\"business-suit-690048_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(27, 62, '_wp_attached_file', '2018/11/business-lady-1584655_960_720.jpg'),
(28, 62, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:540;s:4:\"file\";s:41:\"2018/11/business-lady-1584655_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:41:\"business-lady-1584655_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:41:\"business-lady-1584655_960_720-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:41:\"business-lady-1584655_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:41:\"business-lady-1584655_960_720-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:41:\"business-lady-1584655_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:41:\"business-lady-1584655_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:41:\"business-lady-1584655_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:41:\"business-lady-1584655_960_720-600x338.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:338;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:41:\"business-lady-1584655_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:41:\"business-lady-1584655_960_720-600x338.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:338;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:41:\"business-lady-1584655_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(29, 64, '_wp_attached_file', '2018/11/adult-1869621_960_720.jpg'),
(30, 64, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:33:\"2018/11/adult-1869621_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"adult-1869621_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"adult-1869621_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"adult-1869621_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"adult-1869621_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:33:\"adult-1869621_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:33:\"adult-1869621_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:33:\"adult-1869621_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"adult-1869621_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:33:\"adult-1869621_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"adult-1869621_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"adult-1869621_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(31, 66, '_wp_attached_file', '2018/11/adult-1868988_960_720.jpg'),
(32, 66, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:33:\"2018/11/adult-1868988_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"adult-1868988_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"adult-1868988_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"adult-1868988_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"adult-1868988_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:33:\"adult-1868988_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:33:\"adult-1868988_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:33:\"adult-1868988_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"adult-1868988_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:33:\"adult-1868988_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"adult-1868988_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"adult-1868988_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(33, 70, '_wp_attached_file', '2018/11/1255257800_2_1_1.jpg'),
(34, 70, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:1500;s:4:\"file\";s:28:\"2018/11/1255257800_2_1_1.jpg\";s:5:\"sizes\";a:12:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:28:\"1255257800_2_1_1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"1255257800_2_1_1-200x300.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"1255257800_2_1_1-683x1024.jpg\";s:5:\"width\";i:683;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"1255257800_2_1_1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"1255257800_2_1_1-768x1152.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1152;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:28:\"1255257800_2_1_1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:28:\"1255257800_2_1_1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:28:\"1255257800_2_1_1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:28:\"1255257800_2_1_1-600x900.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:900;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:28:\"1255257800_2_1_1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:28:\"1255257800_2_1_1-600x900.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:900;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:28:\"1255257800_2_1_1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(35, 77, '_wp_attached_file', '2018/11/belt-139757_960_720.jpg'),
(36, 77, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:31:\"2018/11/belt-139757_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:31:\"belt-139757_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"belt-139757_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"belt-139757_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"belt-139757_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:31:\"belt-139757_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:31:\"belt-139757_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:31:\"belt-139757_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:31:\"belt-139757_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:31:\"belt-139757_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:31:\"belt-139757_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:31:\"belt-139757_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(37, 79, '_wp_attached_file', '2018/11/jeans-226422_960_720.jpg'),
(38, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:32:\"2018/11/jeans-226422_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"jeans-226422_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"jeans-226422_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"jeans-226422_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"jeans-226422_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:32:\"jeans-226422_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:32:\"jeans-226422_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"jeans-226422_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"jeans-226422_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:32:\"jeans-226422_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"jeans-226422_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"jeans-226422_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(39, 81, '_wp_attached_file', '2018/11/sunglasses-2632259_960_720.jpg'),
(40, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:38:\"2018/11/sunglasses-2632259_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:38:\"sunglasses-2632259_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"sunglasses-2632259_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"sunglasses-2632259_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:38:\"sunglasses-2632259_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:38:\"sunglasses-2632259_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:38:\"sunglasses-2632259_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:38:\"sunglasses-2632259_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:38:\"sunglasses-2632259_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:38:\"sunglasses-2632259_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:38:\"sunglasses-2632259_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:38:\"sunglasses-2632259_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(41, 83, '_wp_attached_file', '2018/11/bicycle-1209682_960_720.jpg'),
(42, 83, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:35:\"2018/11/bicycle-1209682_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"bicycle-1209682_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"bicycle-1209682_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"bicycle-1209682_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"bicycle-1209682_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:35:\"bicycle-1209682_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:35:\"bicycle-1209682_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:35:\"bicycle-1209682_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:35:\"bicycle-1209682_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:35:\"bicycle-1209682_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:35:\"bicycle-1209682_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:35:\"bicycle-1209682_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(43, 85, '_wp_attached_file', '2018/11/iphone-410324_960_720.jpg'),
(44, 85, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:639;s:4:\"file\";s:33:\"2018/11/iphone-410324_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"iphone-410324_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"iphone-410324_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"iphone-410324_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"iphone-410324_960_720-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:33:\"iphone-410324_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:33:\"iphone-410324_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:33:\"iphone-410324_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"iphone-410324_960_720-600x399.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:399;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:33:\"iphone-410324_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"iphone-410324_960_720-600x399.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:399;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"iphone-410324_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(45, 87, '_wp_attached_file', '2018/11/photo-431119_960_720.jpg'),
(46, 87, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:32:\"2018/11/photo-431119_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"photo-431119_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"photo-431119_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"photo-431119_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"photo-431119_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:32:\"photo-431119_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:32:\"photo-431119_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"photo-431119_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"photo-431119_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:32:\"photo-431119_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"photo-431119_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"photo-431119_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(47, 89, '_wp_attached_file', '2018/11/brown-shoes-1150071_960_720-1.jpg'),
(48, 89, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:41:\"2018/11/brown-shoes-1150071_960_720-1.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:41:\"brown-shoes-1150071_960_720-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:41:\"brown-shoes-1150071_960_720-1-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:41:\"brown-shoes-1150071_960_720-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:41:\"brown-shoes-1150071_960_720-1-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:41:\"brown-shoes-1150071_960_720-1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:41:\"brown-shoes-1150071_960_720-1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:41:\"brown-shoes-1150071_960_720-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:41:\"brown-shoes-1150071_960_720-1-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:41:\"brown-shoes-1150071_960_720-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:41:\"brown-shoes-1150071_960_720-1-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:41:\"brown-shoes-1150071_960_720-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(49, 91, '_wp_attached_file', '2018/11/purse-494169_960_720.jpg'),
(50, 91, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:32:\"2018/11/purse-494169_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"purse-494169_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"purse-494169_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"purse-494169_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"purse-494169_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:32:\"purse-494169_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:32:\"purse-494169_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"purse-494169_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"purse-494169_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:32:\"purse-494169_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"purse-494169_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"purse-494169_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(51, 93, '_wp_attached_file', '2018/11/watch-1245791_960_720.jpg');
INSERT INTO `cm_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(52, 93, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:33:\"2018/11/watch-1245791_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"watch-1245791_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"watch-1245791_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"watch-1245791_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"watch-1245791_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:33:\"watch-1245791_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:33:\"watch-1245791_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:33:\"watch-1245791_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"watch-1245791_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:33:\"watch-1245791_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"watch-1245791_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"watch-1245791_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(53, 95, '_wp_attached_file', '2018/11/menswear-952836_960_720.jpg'),
(54, 95, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:637;s:4:\"file\";s:35:\"2018/11/menswear-952836_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"menswear-952836_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"menswear-952836_960_720-300x199.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"menswear-952836_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"menswear-952836_960_720-768x510.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:510;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:35:\"menswear-952836_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:35:\"menswear-952836_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:35:\"menswear-952836_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:35:\"menswear-952836_960_720-600x398.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:398;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:35:\"menswear-952836_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:35:\"menswear-952836_960_720-600x398.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:398;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:35:\"menswear-952836_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(55, 127, '_wp_attached_file', '2018/11/payment.png'),
(56, 127, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:413;s:6:\"height\";i:27;s:4:\"file\";s:19:\"2018/11/payment.png\";s:5:\"sizes\";a:7:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"payment-100x27.png\";s:5:\"width\";i:100;s:6:\"height\";i:27;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"payment-300x20.png\";s:5:\"width\";i:300;s:6:\"height\";i:20;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"payment-150x27.png\";s:5:\"width\";i:150;s:6:\"height\";i:27;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:18:\"payment-390x27.png\";s:5:\"width\";i:390;s:6:\"height\";i:27;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"payment-300x27.png\";s:5:\"width\";i:300;s:6:\"height\";i:27;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:18:\"payment-300x27.png\";s:5:\"width\";i:300;s:6:\"height\";i:27;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"payment-100x27.png\";s:5:\"width\";i:100;s:6:\"height\";i:27;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(57, 131, '_wp_attached_file', '2018/11/s1.jpg'),
(58, 131, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:650;s:4:\"file\";s:14:\"2018/11/s1.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"s1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s1-300x203.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"s1-768x520.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:520;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"s1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"s1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"s1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:14:\"s1-600x406.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:14:\"s1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:14:\"s1-600x406.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"s1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(59, 132, '_wp_attached_file', '2018/11/s2.jpg'),
(60, 132, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:650;s:4:\"file\";s:14:\"2018/11/s2.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"s2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s2-300x203.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"s2-768x520.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:520;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"s2-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"s2-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"s2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:14:\"s2-600x406.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:14:\"s2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:14:\"s2-600x406.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"s2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(61, 133, '_wp_attached_file', '2018/11/s3.jpg'),
(62, 133, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:650;s:4:\"file\";s:14:\"2018/11/s3.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"s3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s3-300x203.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"s3-768x520.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:520;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"s3-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"s3-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"s3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:14:\"s3-600x406.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:14:\"s3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:14:\"s3-600x406.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"s3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(63, 141, '_wp_attached_file', '2018/11/t1-1.jpg'),
(64, 141, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:325;s:4:\"file\";s:16:\"2018/11/t1-1.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"t1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"t1-1-300x102.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:102;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"t1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"t1-1-768x260.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:260;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:16:\"t1-1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:16:\"t1-1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"t1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:16:\"t1-1-600x203.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:16:\"t1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:16:\"t1-1-600x203.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"t1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(65, 143, '_wp_attached_file', '2018/11/s4.jpg'),
(66, 143, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:650;s:4:\"file\";s:14:\"2018/11/s4.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"s4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s4-300x203.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"s4-768x520.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:520;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"s4-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"s4-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"s4-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:14:\"s4-600x406.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:14:\"s4-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:14:\"s4-600x406.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"s4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(67, 144, '_wp_attached_file', '2018/11/t4.jpg'),
(68, 144, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:325;s:4:\"file\";s:14:\"2018/11/t4.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"t4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"t4-300x102.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:102;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"t4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"t4-768x260.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:260;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"t4-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"t4-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"t4-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:14:\"t4-600x203.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:14:\"t4-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:14:\"t4-600x203.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"t4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(69, 146, '_wp_attached_file', '2018/11/t6.jpg'),
(70, 146, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:325;s:4:\"file\";s:14:\"2018/11/t6.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"t6-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"t6-300x102.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:102;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"t6-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"t6-768x260.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:260;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"t6-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"t6-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"t6-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:14:\"t6-600x203.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:14:\"t6-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:14:\"t6-600x203.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"t6-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(71, 149, '_wp_attached_file', '2018/11/t7.jpg'),
(72, 149, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:325;s:4:\"file\";s:14:\"2018/11/t7.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"t7-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"t7-300x102.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:102;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"t7-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"t7-768x260.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:260;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"t7-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"t7-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"t7-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:14:\"t7-600x203.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:14:\"t7-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:14:\"t7-600x203.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"t7-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(73, 150, '_wp_attached_file', '2018/11/s1-1.jpg'),
(74, 150, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:650;s:4:\"file\";s:16:\"2018/11/s1-1.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"s1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"s1-1-300x203.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"s1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"s1-1-768x520.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:520;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:16:\"s1-1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:16:\"s1-1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"s1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:16:\"s1-1-600x406.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:16:\"s1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:16:\"s1-1-600x406.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"s1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(75, 151, '_wp_attached_file', '2018/11/2018-11-27.png'),
(76, 151, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:259;s:6:\"height\";i:79;s:4:\"file\";s:22:\"2018/11/2018-11-27.png\";s:5:\"sizes\";a:3:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"2018-11-27-100x79.png\";s:5:\"width\";i:100;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"2018-11-27-150x79.png\";s:5:\"width\";i:150;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"2018-11-27-100x79.png\";s:5:\"width\";i:100;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(77, 153, '_wp_attached_file', '2018/11/home1-large-banner.jpg'),
(78, 153, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1740;s:6:\"height\";i:600;s:4:\"file\";s:30:\"2018/11/home1-large-banner.jpg\";s:5:\"sizes\";a:13:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"home1-large-banner-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"home1-large-banner-300x103.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:103;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"home1-large-banner-1024x353.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:353;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"home1-large-banner-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"home1-large-banner-768x265.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:265;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:31:\"home1-large-banner-1536x530.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:530;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:30:\"home1-large-banner-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:30:\"home1-large-banner-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"home1-large-banner-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"home1-large-banner-600x207.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:207;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:30:\"home1-large-banner-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:30:\"home1-large-banner-600x207.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:207;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"home1-large-banner-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(79, 157, '_wp_attached_file', '2018/11/1.jpg'),
(80, 157, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:13:\"2018/11/1.jpg\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:13:\"1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:13:\"1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:13:\"1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:13:\"1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:13:\"1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:13:\"1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(81, 158, '_wp_attached_file', '2018/11/2.jpg'),
(82, 158, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:13:\"2018/11/2.jpg\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:13:\"2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:13:\"2-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:13:\"2-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:13:\"2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:13:\"2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:13:\"2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(83, 159, '_wp_attached_file', '2018/11/4.jpg'),
(84, 159, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:13:\"2018/11/4.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(85, 160, '_wp_attached_file', '2018/11/9.jpg'),
(86, 160, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:13:\"2018/11/9.jpg\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:13:\"9-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"9-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"9-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:13:\"9-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:13:\"9-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:13:\"9-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:13:\"9-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:13:\"9-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(87, 161, '_wp_attached_file', '2018/11/10.jpg'),
(88, 161, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:14:\"2018/11/10.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(89, 162, '_wp_attached_file', '2018/11/11.jpg'),
(90, 162, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:14:\"2018/11/11.jpg\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"11-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"11-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"11-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"11-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"11-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"11-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:14:\"11-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"11-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(91, 164, '_wp_attached_file', '2018/11/19.jpg'),
(92, 164, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:850;s:6:\"height\";i:1055;s:4:\"file\";s:14:\"2018/11/19.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(93, 166, '_wp_attached_file', '2018/11/23.jpg'),
(94, 166, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:850;s:6:\"height\";i:1055;s:4:\"file\";s:14:\"2018/11/23.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(95, 167, '_wp_attached_file', '2018/11/27-27-600x600-1.jpg'),
(96, 167, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:27:\"2018/11/27-27-600x600-1.jpg\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"27-27-600x600-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"27-27-600x600-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"27-27-600x600-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:27:\"27-27-600x600-1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:27:\"27-27-600x600-1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"27-27-600x600-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:27:\"27-27-600x600-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"27-27-600x600-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(97, 168, '_wp_attached_file', '2018/11/27.jpg'),
(98, 168, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:14:\"2018/11/27.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(99, 170, '_wp_attached_file', '2018/11/25.jpg'),
(100, 170, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:14:\"2018/11/25.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(101, 171, '_wp_attached_file', '2018/11/prd3.jpg'),
(102, 171, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:16:\"2018/11/prd3.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(103, 172, '_wp_attached_file', '2018/11/26.jpg'),
(104, 172, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:14:\"2018/11/26.jpg\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"26-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"26-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"26-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"26-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"26-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"26-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:14:\"26-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"26-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(105, 174, '_wp_attached_file', '2018/11/prd1.jpg'),
(106, 174, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:16:\"2018/11/prd1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(107, 175, '_wp_attached_file', '2018/11/21.jpg'),
(108, 175, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:850;s:6:\"height\";i:1055;s:4:\"file\";s:14:\"2018/11/21.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(109, 176, '_wp_attached_file', '2018/11/81.jpg'),
(110, 176, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:850;s:6:\"height\";i:1055;s:4:\"file\";s:14:\"2018/11/81.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(111, 177, '_wp_attached_file', '2018/11/1-1.jpg'),
(112, 177, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:850;s:6:\"height\";i:1055;s:4:\"file\";s:15:\"2018/11/1-1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(113, 178, '_wp_attached_file', '2018/11/15.jpg'),
(114, 178, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:850;s:6:\"height\";i:1055;s:4:\"file\";s:14:\"2018/11/15.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(115, 179, '_wp_attached_file', '2018/11/15h.jpg'),
(116, 179, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:850;s:6:\"height\";i:1055;s:4:\"file\";s:15:\"2018/11/15h.jpg\";s:5:\"sizes\";a:12:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"15h-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"15h-242x300.jpg\";s:5:\"width\";i:242;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"15h-825x1024.jpg\";s:5:\"width\";i:825;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"15h-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"15h-768x953.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:953;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:15:\"15h-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:15:\"15h-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"15h-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:15:\"15h-600x745.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:745;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:15:\"15h-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:15:\"15h-600x745.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:745;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"15h-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(117, 180, '_wp_attached_file', '2018/11/29.jpg');
INSERT INTO `cm_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(118, 180, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:850;s:6:\"height\";i:1055;s:4:\"file\";s:14:\"2018/11/29.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(119, 181, '_wp_attached_file', '2018/11/29h-700x869-1.jpg'),
(120, 181, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:700;s:6:\"height\";i:869;s:4:\"file\";s:25:\"2018/11/29h-700x869-1.jpg\";s:5:\"sizes\";a:10:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"29h-700x869-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"29h-700x869-1-242x300.jpg\";s:5:\"width\";i:242;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"29h-700x869-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:25:\"29h-700x869-1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:25:\"29h-700x869-1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"29h-700x869-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:25:\"29h-700x869-1-600x745.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:745;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:25:\"29h-700x869-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:25:\"29h-700x869-1-600x745.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:745;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"29h-700x869-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(121, 182, '_wp_attached_file', '2018/11/18-1.jpg'),
(122, 182, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:850;s:6:\"height\";i:1055;s:4:\"file\";s:16:\"2018/11/18-1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(123, 183, '_wp_attached_file', '2018/11/18h-700x869-1.jpg'),
(124, 183, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:700;s:6:\"height\";i:869;s:4:\"file\";s:25:\"2018/11/18h-700x869-1.jpg\";s:5:\"sizes\";a:10:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"18h-700x869-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"18h-700x869-1-242x300.jpg\";s:5:\"width\";i:242;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"18h-700x869-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:25:\"18h-700x869-1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:25:\"18h-700x869-1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"18h-700x869-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:25:\"18h-700x869-1-600x745.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:745;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:25:\"18h-700x869-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:25:\"18h-700x869-1-600x745.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:745;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"18h-700x869-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(125, 184, '_wp_attached_file', '2018/11/pebbled-backpack1.jpg'),
(126, 184, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1366;s:6:\"height\";i:1328;s:4:\"file\";s:29:\"2018/11/pebbled-backpack1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(127, 185, '_wp_attached_file', '2018/11/pebbled-backpack3-800x778-1.jpg'),
(128, 185, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:778;s:4:\"file\";s:39:\"2018/11/pebbled-backpack3-800x778-1.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:39:\"pebbled-backpack3-800x778-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:39:\"pebbled-backpack3-800x778-1-300x292.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:39:\"pebbled-backpack3-800x778-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:39:\"pebbled-backpack3-800x778-1-768x747.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:747;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:39:\"pebbled-backpack3-800x778-1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:39:\"pebbled-backpack3-800x778-1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:39:\"pebbled-backpack3-800x778-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:39:\"pebbled-backpack3-800x778-1-600x584.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:584;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:39:\"pebbled-backpack3-800x778-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:39:\"pebbled-backpack3-800x778-1-600x584.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:584;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:39:\"pebbled-backpack3-800x778-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(129, 186, '_wp_attached_file', '2018/11/asymm1.jpg'),
(130, 186, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1366;s:6:\"height\";i:1328;s:4:\"file\";s:18:\"2018/11/asymm1.jpg\";s:5:\"sizes\";a:12:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"asymm1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"asymm1-300x292.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"asymm1-1024x996.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:996;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"asymm1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"asymm1-768x747.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:747;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:18:\"asymm1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:18:\"asymm1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"asymm1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:18:\"asymm1-600x583.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:583;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:18:\"asymm1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:18:\"asymm1-600x583.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:583;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"asymm1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(131, 187, '_wp_attached_file', '2018/11/asymm2-800x778-1.jpg'),
(132, 187, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:778;s:4:\"file\";s:28:\"2018/11/asymm2-800x778-1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(133, 189, '_wp_attached_file', '2018/11/3-600x600-1.jpg'),
(134, 189, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:23:\"2018/11/3-600x600-1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(135, 190, '_wp_attached_file', '2018/11/leatherbelt1.jpg'),
(136, 190, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1366;s:6:\"height\";i:1328;s:4:\"file\";s:24:\"2018/11/leatherbelt1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(137, 191, '_wp_attached_file', '2018/11/leatherbelt2-800x778-1.jpg'),
(138, 191, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:778;s:4:\"file\";s:34:\"2018/11/leatherbelt2-800x778-1.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:34:\"leatherbelt2-800x778-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"leatherbelt2-800x778-1-300x292.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"leatherbelt2-800x778-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"leatherbelt2-800x778-1-768x747.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:747;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:34:\"leatherbelt2-800x778-1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:34:\"leatherbelt2-800x778-1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:34:\"leatherbelt2-800x778-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:34:\"leatherbelt2-800x778-1-600x584.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:584;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:34:\"leatherbelt2-800x778-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:34:\"leatherbelt2-800x778-1-600x584.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:584;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:34:\"leatherbelt2-800x778-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(139, 193, '_wp_attached_file', '2018/11/2018-11-27-1.png'),
(140, 193, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:259;s:6:\"height\";i:79;s:4:\"file\";s:24:\"2018/11/2018-11-27-1.png\";s:5:\"sizes\";a:3:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"2018-11-27-1-100x79.png\";s:5:\"width\";i:100;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"2018-11-27-1-150x79.png\";s:5:\"width\";i:150;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"2018-11-27-1-100x79.png\";s:5:\"width\";i:100;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(141, 204, '_wp_attached_file', '2018/11/mid1.png'),
(142, 204, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:451;s:6:\"height\";i:600;s:4:\"file\";s:16:\"2018/11/mid1.png\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"mid1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"mid1-226x300.png\";s:5:\"width\";i:226;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"mid1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:16:\"mid1-451x300.png\";s:5:\"width\";i:451;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:16:\"mid1-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"mid1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:16:\"mid1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"mid1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(143, 205, '_wp_attached_file', '2018/11/mid2.png'),
(144, 205, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:451;s:6:\"height\";i:600;s:4:\"file\";s:16:\"2018/11/mid2.png\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"mid2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"mid2-226x300.png\";s:5:\"width\";i:226;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"mid2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:16:\"mid2-451x300.png\";s:5:\"width\";i:451;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:16:\"mid2-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"mid2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:16:\"mid2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"mid2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(145, 206, '_wp_attached_file', '2018/11/mid3.png'),
(146, 206, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:451;s:6:\"height\";i:600;s:4:\"file\";s:16:\"2018/11/mid3.png\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"mid3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"mid3-226x300.png\";s:5:\"width\";i:226;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"mid3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:16:\"mid3-451x300.png\";s:5:\"width\";i:451;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:16:\"mid3-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"mid3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:16:\"mid3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"mid3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(147, 207, '_wp_attached_file', '2018/11/mid4.png'),
(148, 207, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:451;s:6:\"height\";i:600;s:4:\"file\";s:16:\"2018/11/mid4.png\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"mid4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"mid4-226x300.png\";s:5:\"width\";i:226;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"mid4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:16:\"mid4-451x300.png\";s:5:\"width\";i:451;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:16:\"mid4-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"mid4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:16:\"mid4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"mid4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(149, 208, '_wp_attached_file', '2018/11/aaa-1.png'),
(150, 208, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:413;s:6:\"height\";i:27;s:4:\"file\";s:17:\"2018/11/aaa-1.png\";s:5:\"sizes\";a:7:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"aaa-1-100x27.png\";s:5:\"width\";i:100;s:6:\"height\";i:27;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"aaa-1-300x20.png\";s:5:\"width\";i:300;s:6:\"height\";i:20;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"aaa-1-150x27.png\";s:5:\"width\";i:150;s:6:\"height\";i:27;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:16:\"aaa-1-390x27.png\";s:5:\"width\";i:390;s:6:\"height\";i:27;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"aaa-1-300x27.png\";s:5:\"width\";i:300;s:6:\"height\";i:27;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:16:\"aaa-1-300x27.png\";s:5:\"width\";i:300;s:6:\"height\";i:27;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"aaa-1-100x27.png\";s:5:\"width\";i:100;s:6:\"height\";i:27;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(151, 210, '_wp_attached_file', '2018/11/slider223.png'),
(152, 210, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:650;s:4:\"file\";s:21:\"2018/11/slider223.png\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"slider223-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"slider223-300x203.png\";s:5:\"width\";i:300;s:6:\"height\";i:203;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"slider223-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"slider223-768x520.png\";s:5:\"width\";i:768;s:6:\"height\";i:520;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:21:\"slider223-600x300.png\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:21:\"slider223-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"slider223-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"slider223-600x406.png\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:21:\"slider223-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"slider223-600x406.png\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"slider223-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(153, 223, '_wp_attached_file', '2018/11/portrait-1130391_1920.jpg'),
(154, 223, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1280;s:4:\"file\";s:33:\"2018/11/portrait-1130391_1920.jpg\";s:5:\"sizes\";a:13:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"portrait-1130391_1920-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"portrait-1130391_1920-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"portrait-1130391_1920-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"portrait-1130391_1920-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"portrait-1130391_1920-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:35:\"portrait-1130391_1920-1536x1024.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:33:\"portrait-1130391_1920-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:33:\"portrait-1130391_1920-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:33:\"portrait-1130391_1920-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"portrait-1130391_1920-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:33:\"portrait-1130391_1920-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"portrait-1130391_1920-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"portrait-1130391_1920-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(155, 226, '_wp_attached_file', '2018/11/slider1-1.png'),
(156, 226, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:650;s:4:\"file\";s:21:\"2018/11/slider1-1.png\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"slider1-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"slider1-1-300x203.png\";s:5:\"width\";i:300;s:6:\"height\";i:203;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"slider1-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"slider1-1-768x520.png\";s:5:\"width\";i:768;s:6:\"height\";i:520;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:21:\"slider1-1-600x300.png\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:21:\"slider1-1-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"slider1-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"slider1-1-600x406.png\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:21:\"slider1-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"slider1-1-600x406.png\";s:5:\"width\";i:600;s:6:\"height\";i:406;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"slider1-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(157, 231, '_wp_attached_file', '2018/11/cropped-watch-1245791_960_720.jpg'),
(158, 231, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:371;s:4:\"file\";s:41:\"2018/11/cropped-watch-1245791_960_720.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:41:\"cropped-watch-1245791_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:41:\"cropped-watch-1245791_960_720-300x111.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:111;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:41:\"cropped-watch-1245791_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:41:\"cropped-watch-1245791_960_720-768x285.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:41:\"cropped-watch-1245791_960_720-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:41:\"cropped-watch-1245791_960_720-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:41:\"cropped-watch-1245791_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:41:\"cropped-watch-1245791_960_720-600x223.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:223;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:41:\"cropped-watch-1245791_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:41:\"cropped-watch-1245791_960_720-600x223.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:223;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:41:\"cropped-watch-1245791_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(159, 231, '_wp_attachment_context', 'custom-header'),
(160, 231, '_wp_attachment_custom_header_last_used_store-mart-lite', '1543833773'),
(161, 231, '_wp_attachment_is_custom_header', 'store-mart-lite'),
(162, 233, '_wp_attached_file', '2019/01/2019-01-17.png'),
(163, 233, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:325;s:4:\"file\";s:22:\"2019/01/2019-01-17.png\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"2019-01-17-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"2019-01-17-300x102.png\";s:5:\"width\";i:300;s:6:\"height\";i:102;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"2019-01-17-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"2019-01-17-768x260.png\";s:5:\"width\";i:768;s:6:\"height\";i:260;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:22:\"2019-01-17-600x300.png\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:22:\"2019-01-17-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"2019-01-17-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"2019-01-17-600x203.png\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:22:\"2019-01-17-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:22:\"2019-01-17-600x203.png\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"2019-01-17-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(164, 235, '_wp_attached_file', '2018/11/2019-01-17-1.png'),
(165, 235, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:650;s:4:\"file\";s:24:\"2018/11/2019-01-17-1.png\";s:5:\"sizes\";a:1:{s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-01-17-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(166, 237, '_wp_attached_file', '2019/01/2019-01-17-2.png'),
(167, 237, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:451;s:6:\"height\";i:600;s:4:\"file\";s:24:\"2019/01/2019-01-17-2.png\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-01-17-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"2019-01-17-2-226x300.png\";s:5:\"width\";i:226;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-01-17-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:24:\"2019-01-17-2-451x300.png\";s:5:\"width\";i:451;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:24:\"2019-01-17-2-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"2019-01-17-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:24:\"2019-01-17-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-01-17-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(168, 238, '_wp_attached_file', '2019/01/2019-01-17-3.png'),
(169, 238, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:451;s:6:\"height\";i:600;s:4:\"file\";s:24:\"2019/01/2019-01-17-3.png\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-01-17-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"2019-01-17-3-226x300.png\";s:5:\"width\";i:226;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-01-17-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:24:\"2019-01-17-3-451x300.png\";s:5:\"width\";i:451;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:24:\"2019-01-17-3-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"2019-01-17-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:24:\"2019-01-17-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-01-17-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(170, 239, '_wp_attached_file', '2019/01/2019-01-17-4.png'),
(171, 239, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:451;s:6:\"height\";i:600;s:4:\"file\";s:24:\"2019/01/2019-01-17-4.png\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-01-17-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"2019-01-17-4-226x300.png\";s:5:\"width\";i:226;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-01-17-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:24:\"2019-01-17-4-451x300.png\";s:5:\"width\";i:451;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:24:\"2019-01-17-4-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"2019-01-17-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:24:\"2019-01-17-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-01-17-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(172, 273, '_wp_attached_file', '2019/03/clients-logo-1.png'),
(173, 273, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:245;s:6:\"height\";i:100;s:4:\"file\";s:26:\"2019/03/clients-logo-1.png\";s:5:\"sizes\";a:3:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-1-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(174, 274, '_wp_attached_file', '2019/03/clients-logo-2.png'),
(175, 274, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:245;s:6:\"height\";i:100;s:4:\"file\";s:26:\"2019/03/clients-logo-2.png\";s:5:\"sizes\";a:3:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-2-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(176, 275, '_wp_attached_file', '2019/03/clients-logo-3.png'),
(177, 275, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:245;s:6:\"height\";i:100;s:4:\"file\";s:26:\"2019/03/clients-logo-3.png\";s:5:\"sizes\";a:3:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-3-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(178, 276, '_wp_attached_file', '2019/03/clients-logo-4.png'),
(179, 276, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:245;s:6:\"height\";i:100;s:4:\"file\";s:26:\"2019/03/clients-logo-4.png\";s:5:\"sizes\";a:3:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-4-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(180, 277, '_wp_attached_file', '2019/03/clients-logo-5.png'),
(181, 277, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:245;s:6:\"height\";i:100;s:4:\"file\";s:26:\"2019/03/clients-logo-5.png\";s:5:\"sizes\";a:3:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-5-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(182, 278, '_wp_attached_file', '2019/03/clients-logo-6.png'),
(183, 278, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:245;s:6:\"height\";i:100;s:4:\"file\";s:26:\"2019/03/clients-logo-6.png\";s:5:\"sizes\";a:3:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-6-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(184, 279, '_wp_attached_file', '2019/03/clients-logo-7.png');
INSERT INTO `cm_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(185, 279, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:245;s:6:\"height\";i:100;s:4:\"file\";s:26:\"2019/03/clients-logo-7.png\";s:5:\"sizes\";a:3:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-7-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"clients-logo-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(186, 303, '_wp_attached_file', '2019/03/amanda-vick-1404811-unsplash-1-scaled.jpg'),
(187, 303, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2560;s:6:\"height\";i:1707;s:4:\"file\";s:49:\"2019/03/amanda-vick-1404811-unsplash-1-scaled.jpg\";s:5:\"sizes\";a:14:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:49:\"amanda-vick-1404811-unsplash-1-scaled-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:42:\"amanda-vick-1404811-unsplash-1-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:43:\"amanda-vick-1404811-unsplash-1-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:42:\"amanda-vick-1404811-unsplash-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:42:\"amanda-vick-1404811-unsplash-1-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:44:\"amanda-vick-1404811-unsplash-1-1536x1024.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:44:\"amanda-vick-1404811-unsplash-1-2048x1365.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1365;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:42:\"amanda-vick-1404811-unsplash-1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:42:\"amanda-vick-1404811-unsplash-1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:42:\"amanda-vick-1404811-unsplash-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:42:\"amanda-vick-1404811-unsplash-1-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:42:\"amanda-vick-1404811-unsplash-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:42:\"amanda-vick-1404811-unsplash-1-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:49:\"amanda-vick-1404811-unsplash-1-scaled-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(188, 328, '_wp_attached_file', '2019/03/gajaltae-kte.png'),
(189, 328, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:460;s:6:\"height\";i:510;s:4:\"file\";s:24:\"2019/03/gajaltae-kte.png\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"gajaltae-kte-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"gajaltae-kte-271x300.png\";s:5:\"width\";i:271;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"gajaltae-kte-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:24:\"gajaltae-kte-460x300.png\";s:5:\"width\";i:460;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:24:\"gajaltae-kte-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"gajaltae-kte-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:24:\"gajaltae-kte-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"gajaltae-kte-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(190, 329, '_wp_attached_file', '2019/03/abag.png'),
(191, 329, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:460;s:6:\"height\";i:510;s:4:\"file\";s:16:\"2019/03/abag.png\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"abag-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"abag-271x300.png\";s:5:\"width\";i:271;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"abag-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:16:\"abag-460x300.png\";s:5:\"width\";i:460;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:16:\"abag-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"abag-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:16:\"abag-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"abag-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(192, 330, '_wp_attached_file', '2019/03/goraman.png'),
(193, 330, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:460;s:6:\"height\";i:510;s:4:\"file\";s:19:\"2019/03/goraman.png\";s:5:\"sizes\";a:8:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"goraman-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"goraman-271x300.png\";s:5:\"width\";i:271;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"goraman-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:19:\"goraman-460x300.png\";s:5:\"width\";i:460;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:19:\"goraman-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"goraman-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:19:\"goraman-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"goraman-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(194, 337, '_wp_attached_file', '2019/03/2019-03-11-2.jpg'),
(195, 337, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:815;s:6:\"height\";i:571;s:4:\"file\";s:24:\"2019/03/2019-03-11-2.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-03-11-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"2019-03-11-2-300x210.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:210;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-03-11-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"2019-03-11-2-768x538.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:538;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:24:\"2019-03-11-2-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:24:\"2019-03-11-2-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"2019-03-11-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:24:\"2019-03-11-2-600x420.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:420;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:24:\"2019-03-11-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:24:\"2019-03-11-2-600x420.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:420;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-03-11-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(196, 338, '_wp_attached_file', '2019/03/2019-03-11-1.jpg'),
(197, 338, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:815;s:6:\"height\";i:571;s:4:\"file\";s:24:\"2019/03/2019-03-11-1.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-03-11-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"2019-03-11-1-300x210.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:210;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-03-11-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"2019-03-11-1-768x538.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:538;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:24:\"2019-03-11-1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:24:\"2019-03-11-1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"2019-03-11-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:24:\"2019-03-11-1-600x420.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:420;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:24:\"2019-03-11-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:24:\"2019-03-11-1-600x420.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:420;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-03-11-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(198, 339, '_wp_attached_file', '2019/03/2019-03-11.jpg'),
(199, 339, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:815;s:6:\"height\";i:571;s:4:\"file\";s:22:\"2019/03/2019-03-11.jpg\";s:5:\"sizes\";a:1:{s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"2019-03-11-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(200, 344, '_wp_attached_file', '2019/03/2019-03-12-2.png'),
(201, 344, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:181;s:6:\"height\";i:79;s:4:\"file\";s:24:\"2019/03/2019-03-12-2.png\";s:5:\"sizes\";a:3:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"2019-03-12-2-100x79.png\";s:5:\"width\";i:100;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"2019-03-12-2-150x79.png\";s:5:\"width\";i:150;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"2019-03-12-2-100x79.png\";s:5:\"width\";i:100;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(202, 350, '_wp_attached_file', '2018/11/2019-03-22.jpg'),
(203, 350, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:325;s:4:\"file\";s:22:\"2018/11/2019-03-22.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"2019-03-22-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"2019-03-22-300x102.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:102;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"2019-03-22-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"2019-03-22-768x260.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:260;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:22:\"2019-03-22-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:22:\"2019-03-22-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"2019-03-22-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"2019-03-22-600x203.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:22:\"2019-03-22-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:22:\"2019-03-22-600x203.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"2019-03-22-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(204, 351, '_wp_attached_file', '2018/11/2019-03-22-1.jpg'),
(205, 351, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:325;s:4:\"file\";s:24:\"2018/11/2019-03-22-1.jpg\";s:5:\"sizes\";a:11:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-03-22-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"2019-03-22-1-300x102.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:102;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-03-22-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"2019-03-22-1-768x260.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:260;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:24:\"2019-03-22-1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:24:\"2019-03-22-1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"2019-03-22-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:24:\"2019-03-22-1-600x203.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:24:\"2019-03-22-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:24:\"2019-03-22-1-600x203.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:203;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"2019-03-22-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(206, 366, '_wp_attached_file', '2019/05/woocommerce-placeholder.png'),
(207, 366, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:35:\"2019/05/woocommerce-placeholder.png\";s:5:\"sizes\";a:12:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-600x300.png\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-390x290.png\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-600x600.png\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-600x600.png\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(208, 244, '_wp_page_template', 'default'),
(209, 245, '_wp_page_template', 'default'),
(210, 246, '_wp_page_template', 'default'),
(211, 367, '_menu_item_type', 'custom'),
(212, 367, '_menu_item_menu_item_parent', '379'),
(213, 367, '_menu_item_object_id', '367'),
(214, 367, '_menu_item_object', 'custom'),
(215, 367, '_menu_item_target', ''),
(216, 367, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(217, 367, '_menu_item_xfn', ''),
(218, 367, '_menu_item_url', 'https://demo.accesspressthemes.com/zigcy-lite/demo-two/'),
(219, 368, '_menu_item_type', 'custom'),
(220, 368, '_menu_item_menu_item_parent', '379'),
(221, 368, '_menu_item_object_id', '368'),
(222, 368, '_menu_item_object', 'custom'),
(223, 368, '_menu_item_target', ''),
(224, 368, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(225, 368, '_menu_item_xfn', ''),
(226, 368, '_menu_item_url', 'https://demo.accesspressthemes.com/zigcy-lite/demo-three/'),
(227, 369, '_menu_item_type', 'custom'),
(228, 369, '_menu_item_menu_item_parent', '0'),
(229, 369, '_menu_item_object_id', '369'),
(230, 369, '_menu_item_object', 'custom'),
(231, 369, '_menu_item_target', ''),
(232, 369, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(233, 369, '_menu_item_xfn', ''),
(234, 369, '_menu_item_url', '#'),
(235, 370, '_menu_item_type', 'custom'),
(236, 370, '_menu_item_menu_item_parent', '0'),
(237, 370, '_menu_item_object_id', '370'),
(238, 370, '_menu_item_object', 'custom'),
(239, 370, '_menu_item_target', ''),
(240, 370, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(241, 370, '_menu_item_xfn', ''),
(242, 370, '_menu_item_url', '#'),
(243, 371, '_menu_item_type', 'custom'),
(244, 371, '_menu_item_menu_item_parent', '0'),
(245, 371, '_menu_item_object_id', '371'),
(246, 371, '_menu_item_object', 'custom'),
(247, 371, '_menu_item_target', ''),
(248, 371, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(249, 371, '_menu_item_xfn', ''),
(250, 371, '_menu_item_url', '#'),
(251, 372, '_menu_item_type', 'custom'),
(252, 372, '_menu_item_menu_item_parent', '0'),
(253, 372, '_menu_item_object_id', '372'),
(254, 372, '_menu_item_object', 'custom'),
(255, 372, '_menu_item_target', ''),
(256, 372, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(257, 372, '_menu_item_xfn', ''),
(258, 372, '_menu_item_url', '#'),
(259, 2, '_wp_page_template', 'default'),
(262, 376, '_thumbnail_id', '418'),
(265, 377, '_thumbnail_id', '422'),
(266, 21, '_wp_page_template', 'default'),
(267, 34, '_wp_page_template', 'tpl-home.php'),
(268, 117, '_wp_page_template', 'default'),
(269, 119, '_wp_page_template', 'default'),
(270, 121, '_wp_page_template', 'default'),
(271, 129, '_wp_page_template', 'default'),
(272, 211, '_wp_page_template', 'default'),
(273, 211, 'panels_data', 'a:3:{s:7:\"widgets\";a:3:{i:0;a:12:{s:10:\"map_center\";s:0:\"\";s:15:\"api_key_section\";a:2:{s:7:\"api_key\";s:39:\"AIzaSyDdNxiZdrOWmRpKu7vrCO9j1rSgEUqSnZk\";s:24:\"so_field_container_state\";s:4:\"open\";}s:8:\"settings\";a:13:{s:8:\"map_type\";s:11:\"interactive\";s:5:\"width\";s:3:\"640\";s:6:\"height\";s:3:\"480\";s:15:\"destination_url\";s:0:\"\";s:4:\"zoom\";i:12;s:11:\"scroll_zoom\";b:1;s:9:\"draggable\";b:1;s:14:\"fallback_image\";i:0;s:19:\"fallback_image_size\";s:4:\"full\";s:24:\"so_field_container_state\";s:4:\"open\";s:10:\"new_window\";b:0;s:18:\"disable_default_ui\";b:0;s:13:\"keep_centered\";b:0;}s:7:\"markers\";a:7:{s:16:\"marker_at_center\";b:1;s:11:\"marker_icon\";i:0;s:12:\"info_display\";s:5:\"click\";s:13:\"info_multiple\";b:1;s:24:\"so_field_container_state\";s:6:\"closed\";s:17:\"markers_draggable\";b:0;s:16:\"marker_positions\";a:0:{}}s:6:\"styles\";a:5:{s:12:\"style_method\";s:6:\"normal\";s:15:\"styled_map_name\";s:0:\"\";s:19:\"raw_json_map_styles\";s:0:\"\";s:24:\"so_field_container_state\";s:6:\"closed\";s:17:\"custom_map_styles\";a:0:{}}s:10:\"directions\";a:9:{s:6:\"origin\";s:0:\"\";s:11:\"destination\";s:0:\"\";s:11:\"travel_mode\";s:7:\"driving\";s:24:\"so_field_container_state\";s:6:\"closed\";s:14:\"avoid_highways\";b:0;s:11:\"avoid_tolls\";b:0;s:17:\"preserve_viewport\";b:0;s:9:\"waypoints\";a:0:{}s:18:\"optimize_waypoints\";b:0;}s:12:\"_sow_form_id\";s:32:\"19854621835c00d7eb4ace0582551268\";s:19:\"_sow_form_timestamp\";s:13:\"1543559243011\";s:2:\"id\";s:23:\"sow-google-map-21110000\";s:11:\"option_name\";s:21:\"widget_sow-google-map\";s:22:\"so_sidebar_emulator_id\";s:23:\"sow-google-map-21110000\";s:11:\"panels_info\";a:7:{s:5:\"class\";s:34:\"SiteOrigin_Widget_GoogleMap_Widget\";s:3:\"raw\";b:0;s:4:\"grid\";i:0;s:4:\"cell\";i:0;s:2:\"id\";i:0;s:9:\"widget_id\";s:36:\"59d24fa7-64cf-42d6-ba09-449c6ed095cc\";s:5:\"style\";a:2:{s:27:\"background_image_attachment\";b:0;s:18:\"background_display\";s:4:\"tile\";}}}i:1;a:10:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:473:\"<h2 class=\"sml-ct-get-in-touch\">GET IN TOUCH !</h2>\n<p>Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis, hinc partem ei est.</p>\n<h2 class=\"sml-ct-details\">DETAILS</h2>\n<p>3 Wakehurst Street New York, NY 10002<br />\n+1-202-555-0133<br />\nsupport@fluent.com</p>\n<h2 class=\"sml-ct-op-hours\">OPENING HOURS</h2>\n<p>Monday – Friday 09:00 – 23:00<br />\nSaturday 09:00 – 22:00<br />\nSunday 12:00 – 18:00</p>\n\";s:20:\"text_selected_editor\";s:4:\"tmce\";s:5:\"autop\";b:1;s:12:\"_sow_form_id\";s:32:\"15899474125c00d81ec3588873433210\";s:19:\"_sow_form_timestamp\";s:13:\"1543559271991\";s:2:\"id\";s:19:\"sow-editor-21110001\";s:11:\"option_name\";s:17:\"widget_sow-editor\";s:22:\"so_sidebar_emulator_id\";s:19:\"sow-editor-21110001\";s:11:\"panels_info\";a:7:{s:5:\"class\";s:31:\"SiteOrigin_Widget_Editor_Widget\";s:3:\"raw\";b:0;s:4:\"grid\";i:1;s:4:\"cell\";i:0;s:2:\"id\";i:1;s:9:\"widget_id\";s:36:\"4a8aef62-e95b-4bc5-9378-6931abeaae3d\";s:5:\"style\";a:2:{s:27:\"background_image_attachment\";b:0;s:18:\"background_display\";s:4:\"tile\";}}}i:2;a:6:{s:5:\"title\";s:0:\"\";s:7:\"content\";s:46:\"[contact-form-7 id=\"267\" title=\"Contact form\"]\";s:2:\"id\";s:20:\"custom_html-21110002\";s:11:\"option_name\";s:18:\"widget_custom_html\";s:22:\"so_sidebar_emulator_id\";s:20:\"custom_html-21110002\";s:11:\"panels_info\";a:6:{s:5:\"class\";s:21:\"WP_Widget_Custom_HTML\";s:4:\"grid\";i:1;s:4:\"cell\";i:1;s:2:\"id\";i:2;s:9:\"widget_id\";s:36:\"e4e8aad7-4641-40cc-bdf7-a44c618dcc54\";s:5:\"style\";a:2:{s:27:\"background_image_attachment\";b:0;s:18:\"background_display\";s:4:\"tile\";}}}}s:5:\"grids\";a:2:{i:0;a:4:{s:5:\"cells\";i:1;s:5:\"style\";a:4:{s:27:\"background_image_attachment\";b:0;s:18:\"background_display\";s:4:\"tile\";s:11:\"row_stretch\";s:14:\"full-stretched\";s:14:\"cell_alignment\";s:10:\"flex-start\";}s:5:\"ratio\";i:1;s:15:\"ratio_direction\";s:5:\"right\";}i:1;a:2:{s:5:\"cells\";i:2;s:5:\"style\";a:3:{s:27:\"background_image_attachment\";b:0;s:18:\"background_display\";s:4:\"tile\";s:14:\"cell_alignment\";s:10:\"flex-start\";}}}s:10:\"grid_cells\";a:3:{i:0;a:4:{s:4:\"grid\";i:0;s:5:\"index\";i:0;s:6:\"weight\";i:1;s:5:\"style\";a:0:{}}i:1;a:4:{s:4:\"grid\";i:1;s:5:\"index\";i:0;s:6:\"weight\";d:0.5;s:5:\"style\";a:0:{}}i:2;a:4:{s:4:\"grid\";i:1;s:5:\"index\";i:1;s:6:\"weight\";d:0.5;s:5:\"style\";a:0:{}}}}'),
(274, 243, '_wp_page_template', 'default'),
(277, 313, '_wp_page_template', 'default'),
(278, 315, '_wp_page_template', 'default'),
(279, 317, '_wp_page_template', 'default'),
(280, 378, '_wp_page_template', 'default'),
(283, 55, '_wc_review_count', '0'),
(284, 55, '_wc_rating_count', 'a:0:{}'),
(285, 55, '_wc_average_rating', '0'),
(286, 55, '_thumbnail_id', '448'),
(287, 55, '_sku', ''),
(288, 55, '_regular_price', '40'),
(289, 55, '_sale_price', ''),
(290, 55, '_sale_price_dates_from', ''),
(291, 55, '_sale_price_dates_to', ''),
(292, 55, 'total_sales', '0'),
(293, 55, '_tax_status', 'taxable'),
(294, 55, '_tax_class', ''),
(295, 55, '_manage_stock', 'no'),
(296, 55, '_backorders', 'no'),
(297, 55, '_low_stock_amount', ''),
(298, 55, '_sold_individually', 'no'),
(299, 55, '_weight', ''),
(300, 55, '_length', ''),
(301, 55, '_width', ''),
(302, 55, '_height', ''),
(303, 55, '_upsell_ids', 'a:0:{}'),
(304, 55, '_crosssell_ids', 'a:0:{}'),
(305, 55, '_purchase_note', ''),
(306, 55, '_default_attributes', 'a:0:{}'),
(307, 55, '_virtual', 'no'),
(308, 55, '_downloadable', 'no'),
(309, 55, '_product_image_gallery', ''),
(310, 55, '_download_limit', '-1'),
(311, 55, '_download_expiry', '-1'),
(312, 55, '_stock', NULL),
(313, 55, '_stock_status', 'instock'),
(314, 55, '_product_version', '3.9.2'),
(315, 55, '_price', '40'),
(316, 57, '_wc_review_count', '0'),
(317, 57, '_wc_rating_count', 'a:0:{}'),
(318, 57, '_wc_average_rating', '0'),
(319, 57, '_thumbnail_id', '448'),
(320, 57, '_sku', ''),
(321, 57, '_regular_price', '45'),
(322, 57, '_sale_price', ''),
(323, 57, '_sale_price_dates_from', ''),
(324, 57, '_sale_price_dates_to', ''),
(325, 57, 'total_sales', '0'),
(326, 57, '_tax_status', 'taxable'),
(327, 57, '_tax_class', ''),
(328, 57, '_manage_stock', 'no'),
(329, 57, '_backorders', 'no'),
(330, 57, '_low_stock_amount', ''),
(331, 57, '_sold_individually', 'no'),
(332, 57, '_weight', ''),
(333, 57, '_length', ''),
(334, 57, '_width', ''),
(335, 57, '_height', ''),
(336, 57, '_upsell_ids', 'a:0:{}'),
(337, 57, '_crosssell_ids', 'a:0:{}'),
(338, 57, '_purchase_note', ''),
(339, 57, '_default_attributes', 'a:0:{}'),
(340, 57, '_virtual', 'no'),
(341, 57, '_downloadable', 'no'),
(342, 57, '_product_image_gallery', ''),
(343, 57, '_download_limit', '-1'),
(344, 57, '_download_expiry', '-1'),
(345, 57, '_stock', NULL),
(346, 57, '_stock_status', 'instock'),
(347, 57, '_product_version', '3.9.2'),
(348, 57, '_price', '45'),
(349, 59, '_wc_review_count', '0'),
(350, 59, '_wc_rating_count', 'a:0:{}'),
(351, 59, '_wc_average_rating', '0'),
(352, 59, '_thumbnail_id', '407'),
(353, 59, '_sku', ''),
(354, 59, '_regular_price', '65'),
(355, 59, '_sale_price', '55'),
(356, 59, '_sale_price_dates_from', ''),
(357, 59, '_sale_price_dates_to', ''),
(358, 59, 'total_sales', '0'),
(359, 59, '_tax_status', 'taxable'),
(360, 59, '_tax_class', ''),
(361, 59, '_manage_stock', 'no'),
(362, 59, '_backorders', 'no'),
(363, 59, '_low_stock_amount', ''),
(364, 59, '_sold_individually', 'no'),
(365, 59, '_weight', ''),
(366, 59, '_length', ''),
(367, 59, '_width', ''),
(368, 59, '_height', ''),
(369, 59, '_upsell_ids', 'a:0:{}'),
(370, 59, '_crosssell_ids', 'a:0:{}'),
(371, 59, '_purchase_note', ''),
(372, 59, '_default_attributes', 'a:0:{}'),
(373, 59, '_virtual', 'no'),
(374, 59, '_downloadable', 'no'),
(375, 59, '_product_image_gallery', ''),
(376, 59, '_download_limit', '-1'),
(377, 59, '_download_expiry', '-1'),
(378, 59, '_stock', NULL),
(379, 59, '_stock_status', 'instock'),
(380, 59, '_product_version', '3.9.2'),
(381, 59, '_price', '55'),
(382, 61, '_wc_review_count', '0'),
(383, 61, '_wc_rating_count', 'a:0:{}'),
(384, 61, '_wc_average_rating', '0'),
(385, 61, '_thumbnail_id', '434'),
(386, 61, '_sku', ''),
(387, 61, '_regular_price', '90'),
(388, 61, '_sale_price', ''),
(389, 61, '_sale_price_dates_from', ''),
(390, 61, '_sale_price_dates_to', ''),
(391, 61, 'total_sales', '0'),
(392, 61, '_tax_status', 'taxable'),
(393, 61, '_tax_class', ''),
(394, 61, '_manage_stock', 'no'),
(395, 61, '_backorders', 'no'),
(396, 61, '_low_stock_amount', ''),
(397, 61, '_sold_individually', 'no'),
(398, 61, '_weight', ''),
(399, 61, '_length', ''),
(400, 61, '_width', ''),
(401, 61, '_height', ''),
(402, 61, '_upsell_ids', 'a:0:{}'),
(403, 61, '_crosssell_ids', 'a:0:{}'),
(404, 61, '_purchase_note', ''),
(405, 61, '_default_attributes', 'a:0:{}'),
(406, 61, '_virtual', 'no'),
(407, 61, '_downloadable', 'no'),
(408, 61, '_product_image_gallery', '172'),
(409, 61, '_download_limit', '-1'),
(410, 61, '_download_expiry', '-1'),
(411, 61, '_stock', NULL),
(412, 61, '_stock_status', 'instock'),
(413, 61, '_product_version', '3.9.2'),
(414, 61, '_price', '90'),
(415, 63, '_wc_review_count', '0'),
(416, 63, '_wc_rating_count', 'a:0:{}'),
(417, 63, '_wc_average_rating', '0'),
(418, 63, '_thumbnail_id', '415'),
(419, 63, '_sku', ''),
(420, 63, '_regular_price', '85'),
(421, 63, '_sale_price', ''),
(422, 63, '_sale_price_dates_from', ''),
(423, 63, '_sale_price_dates_to', ''),
(424, 63, 'total_sales', '0'),
(425, 63, '_tax_status', 'taxable'),
(426, 63, '_tax_class', ''),
(427, 63, '_manage_stock', 'no'),
(428, 63, '_backorders', 'no'),
(429, 63, '_low_stock_amount', ''),
(430, 63, '_sold_individually', 'no'),
(431, 63, '_weight', ''),
(432, 63, '_length', ''),
(433, 63, '_width', ''),
(434, 63, '_height', ''),
(435, 63, '_upsell_ids', 'a:0:{}'),
(436, 63, '_crosssell_ids', 'a:0:{}'),
(437, 63, '_purchase_note', ''),
(438, 63, '_default_attributes', 'a:0:{}'),
(439, 63, '_virtual', 'no'),
(440, 63, '_downloadable', 'no'),
(441, 63, '_product_image_gallery', '167,166'),
(442, 63, '_download_limit', '-1'),
(443, 63, '_download_expiry', '-1'),
(444, 63, '_stock', NULL),
(445, 63, '_stock_status', 'instock'),
(446, 63, '_product_version', '3.9.2'),
(447, 63, '_price', '85'),
(448, 65, '_wc_review_count', '0'),
(449, 65, '_wc_rating_count', 'a:0:{}'),
(450, 65, '_wc_average_rating', '0'),
(451, 65, '_thumbnail_id', '428'),
(452, 65, '_sku', ''),
(453, 65, '_regular_price', '90'),
(454, 65, '_sale_price', ''),
(455, 65, '_sale_price_dates_from', ''),
(456, 65, '_sale_price_dates_to', ''),
(457, 65, 'total_sales', '0'),
(458, 65, '_tax_status', 'taxable'),
(459, 65, '_tax_class', ''),
(460, 65, '_manage_stock', 'no'),
(461, 65, '_backorders', 'no'),
(462, 65, '_low_stock_amount', ''),
(463, 65, '_sold_individually', 'no'),
(464, 65, '_weight', ''),
(465, 65, '_length', ''),
(466, 65, '_width', ''),
(467, 65, '_height', ''),
(468, 65, '_upsell_ids', 'a:0:{}'),
(469, 65, '_crosssell_ids', 'a:0:{}'),
(470, 65, '_purchase_note', ''),
(471, 65, '_default_attributes', 'a:0:{}'),
(472, 65, '_virtual', 'no'),
(473, 65, '_downloadable', 'no'),
(474, 65, '_product_image_gallery', ''),
(475, 65, '_download_limit', '-1'),
(476, 65, '_download_expiry', '-1'),
(477, 65, '_stock', NULL),
(478, 65, '_stock_status', 'instock'),
(479, 65, '_product_version', '3.9.2'),
(480, 65, '_price', '90'),
(481, 67, '_wc_review_count', '0'),
(482, 67, '_wc_rating_count', 'a:0:{}'),
(483, 67, '_wc_average_rating', '0'),
(484, 67, '_thumbnail_id', '434'),
(485, 67, '_sku', ''),
(486, 67, '_regular_price', '155'),
(487, 67, '_sale_price', '125'),
(488, 67, '_sale_price_dates_from', ''),
(489, 67, '_sale_price_dates_to', ''),
(490, 67, 'total_sales', '0'),
(491, 67, '_tax_status', 'taxable'),
(492, 67, '_tax_class', ''),
(493, 67, '_manage_stock', 'no'),
(494, 67, '_backorders', 'no'),
(495, 67, '_low_stock_amount', ''),
(496, 67, '_sold_individually', 'no'),
(497, 67, '_weight', ''),
(498, 67, '_length', ''),
(499, 67, '_width', ''),
(500, 67, '_height', ''),
(501, 67, '_upsell_ids', 'a:0:{}'),
(502, 67, '_crosssell_ids', 'a:0:{}'),
(503, 67, '_purchase_note', ''),
(504, 67, '_default_attributes', 'a:0:{}'),
(505, 67, '_virtual', 'no'),
(506, 67, '_downloadable', 'no'),
(507, 67, '_product_image_gallery', ''),
(508, 67, '_download_limit', '-1'),
(509, 67, '_download_expiry', '-1'),
(510, 67, '_stock', NULL),
(511, 67, '_stock_status', 'instock'),
(512, 67, '_product_version', '3.9.2'),
(513, 67, '_price', '125'),
(514, 71, '_wc_review_count', '0'),
(515, 71, '_wc_rating_count', 'a:0:{}'),
(516, 71, '_wc_average_rating', '0'),
(517, 71, '_thumbnail_id', '418'),
(518, 71, '_sku', ''),
(519, 71, '_regular_price', '95'),
(520, 71, '_sale_price', ''),
(521, 71, '_sale_price_dates_from', ''),
(522, 71, '_sale_price_dates_to', ''),
(523, 71, 'total_sales', '0'),
(524, 71, '_tax_status', 'taxable'),
(525, 71, '_tax_class', ''),
(526, 71, '_manage_stock', 'no'),
(527, 71, '_backorders', 'no'),
(528, 71, '_low_stock_amount', ''),
(529, 71, '_sold_individually', 'no'),
(530, 71, '_weight', ''),
(531, 71, '_length', ''),
(532, 71, '_width', ''),
(533, 71, '_height', ''),
(534, 71, '_upsell_ids', 'a:0:{}'),
(535, 71, '_crosssell_ids', 'a:0:{}'),
(536, 71, '_purchase_note', ''),
(537, 71, '_default_attributes', 'a:0:{}'),
(538, 71, '_virtual', 'no'),
(539, 71, '_downloadable', 'no'),
(540, 71, '_product_image_gallery', ''),
(541, 71, '_download_limit', '-1'),
(542, 71, '_download_expiry', '-1'),
(543, 71, '_stock', NULL),
(544, 71, '_stock_status', 'instock'),
(545, 71, '_product_version', '3.9.2'),
(546, 71, '_price', '95'),
(549, 222, '_thumbnail_id', '339'),
(552, 225, '_thumbnail_id', '338'),
(557, 272, '_thumbnail_id', '273'),
(560, 281, '_thumbnail_id', '274'),
(563, 284, '_thumbnail_id', '275'),
(566, 287, '_thumbnail_id', '276'),
(569, 289, '_thumbnail_id', '277'),
(572, 290, '_thumbnail_id', '278'),
(575, 293, '_thumbnail_id', '279'),
(578, 302, '_thumbnail_id', '337'),
(579, 76, '_wc_review_count', '0'),
(580, 76, '_wc_rating_count', 'a:0:{}'),
(581, 76, '_wc_average_rating', '0'),
(582, 76, '_thumbnail_id', '416'),
(583, 76, '_sku', ''),
(584, 76, '_regular_price', '25'),
(585, 76, '_sale_price', '20'),
(586, 76, '_sale_price_dates_from', ''),
(587, 76, '_sale_price_dates_to', ''),
(588, 76, 'total_sales', '0'),
(589, 76, '_tax_status', 'taxable'),
(590, 76, '_tax_class', ''),
(591, 76, '_manage_stock', 'no'),
(592, 76, '_backorders', 'no'),
(593, 76, '_low_stock_amount', ''),
(594, 76, '_sold_individually', 'no'),
(595, 76, '_weight', ''),
(596, 76, '_length', ''),
(597, 76, '_width', ''),
(598, 76, '_height', ''),
(599, 76, '_upsell_ids', 'a:0:{}'),
(600, 76, '_crosssell_ids', 'a:0:{}'),
(601, 76, '_purchase_note', ''),
(602, 76, '_default_attributes', 'a:0:{}'),
(603, 76, '_virtual', 'no'),
(604, 76, '_downloadable', 'no'),
(605, 76, '_product_image_gallery', '179'),
(606, 76, '_download_limit', '-1'),
(607, 76, '_download_expiry', '-1'),
(608, 76, '_stock', NULL),
(609, 76, '_stock_status', 'instock'),
(610, 76, '_product_version', '3.9.2'),
(611, 76, '_price', '20'),
(612, 78, '_wc_review_count', '0'),
(613, 78, '_wc_rating_count', 'a:0:{}'),
(614, 78, '_wc_average_rating', '0'),
(615, 78, '_thumbnail_id', '450'),
(616, 78, '_sku', ''),
(617, 78, '_regular_price', '115'),
(618, 78, '_sale_price', ''),
(619, 78, '_sale_price_dates_from', ''),
(620, 78, '_sale_price_dates_to', ''),
(621, 78, 'total_sales', '0'),
(622, 78, '_tax_status', 'taxable'),
(623, 78, '_tax_class', ''),
(624, 78, '_manage_stock', 'no'),
(625, 78, '_backorders', 'no'),
(626, 78, '_low_stock_amount', ''),
(627, 78, '_sold_individually', 'no'),
(628, 78, '_weight', ''),
(629, 78, '_length', ''),
(630, 78, '_width', ''),
(631, 78, '_height', ''),
(632, 78, '_upsell_ids', 'a:0:{}'),
(633, 78, '_crosssell_ids', 'a:0:{}'),
(634, 78, '_purchase_note', ''),
(635, 78, '_default_attributes', 'a:0:{}'),
(636, 78, '_virtual', 'no'),
(637, 78, '_downloadable', 'no'),
(638, 78, '_product_image_gallery', '181'),
(639, 78, '_download_limit', '-1'),
(640, 78, '_download_expiry', '-1'),
(641, 78, '_stock', NULL),
(642, 78, '_stock_status', 'instock'),
(643, 78, '_product_version', '3.9.2'),
(644, 78, '_price', '115'),
(645, 80, '_wc_review_count', '0'),
(646, 80, '_wc_rating_count', 'a:0:{}'),
(647, 80, '_wc_average_rating', '0'),
(648, 80, '_thumbnail_id', '422'),
(649, 80, '_sku', ''),
(650, 80, '_regular_price', '70'),
(651, 80, '_sale_price', ''),
(652, 80, '_sale_price_dates_from', ''),
(653, 80, '_sale_price_dates_to', ''),
(654, 80, 'total_sales', '0'),
(655, 80, '_tax_status', 'taxable'),
(656, 80, '_tax_class', ''),
(657, 80, '_manage_stock', 'no'),
(658, 80, '_backorders', 'no'),
(659, 80, '_low_stock_amount', ''),
(660, 80, '_sold_individually', 'no'),
(661, 80, '_weight', ''),
(662, 80, '_length', ''),
(663, 80, '_width', ''),
(664, 80, '_height', ''),
(665, 80, '_upsell_ids', 'a:0:{}'),
(666, 80, '_crosssell_ids', 'a:0:{}'),
(667, 80, '_purchase_note', ''),
(668, 80, '_default_attributes', 'a:0:{}'),
(669, 80, '_virtual', 'no'),
(670, 80, '_downloadable', 'no'),
(671, 80, '_product_image_gallery', '183'),
(672, 80, '_download_limit', '-1'),
(673, 80, '_download_expiry', '-1'),
(674, 80, '_stock', NULL),
(675, 80, '_stock_status', 'instock'),
(676, 80, '_product_version', '3.9.2'),
(677, 80, '_price', '70'),
(678, 82, '_wc_review_count', '0'),
(679, 82, '_wc_rating_count', 'a:0:{}'),
(680, 82, '_wc_average_rating', '0'),
(681, 82, '_thumbnail_id', '432'),
(682, 82, '_sku', ''),
(683, 82, '_regular_price', '215'),
(684, 82, '_sale_price', '155'),
(685, 82, '_sale_price_dates_from', ''),
(686, 82, '_sale_price_dates_to', ''),
(687, 82, 'total_sales', '0'),
(688, 82, '_tax_status', 'taxable'),
(689, 82, '_tax_class', ''),
(690, 82, '_manage_stock', 'no'),
(691, 82, '_backorders', 'no'),
(692, 82, '_low_stock_amount', ''),
(693, 82, '_sold_individually', 'no'),
(694, 82, '_weight', ''),
(695, 82, '_length', ''),
(696, 82, '_width', ''),
(697, 82, '_height', ''),
(698, 82, '_upsell_ids', 'a:0:{}'),
(699, 82, '_crosssell_ids', 'a:0:{}'),
(700, 82, '_purchase_note', ''),
(701, 82, '_default_attributes', 'a:0:{}'),
(702, 82, '_virtual', 'no'),
(703, 82, '_downloadable', 'no'),
(704, 82, '_product_image_gallery', '185'),
(705, 82, '_download_limit', '-1'),
(706, 82, '_download_expiry', '-1'),
(707, 82, '_stock', NULL),
(708, 82, '_stock_status', 'instock'),
(709, 82, '_product_version', '3.9.2'),
(710, 82, '_price', '155'),
(711, 84, '_wc_review_count', '0'),
(712, 84, '_wc_rating_count', 'a:0:{}'),
(713, 84, '_wc_average_rating', '0'),
(714, 84, '_thumbnail_id', '428'),
(715, 84, '_sku', ''),
(716, 84, '_regular_price', '45'),
(717, 84, '_sale_price', ''),
(718, 84, '_sale_price_dates_from', ''),
(719, 84, '_sale_price_dates_to', ''),
(720, 84, 'total_sales', '0'),
(721, 84, '_tax_status', 'taxable'),
(722, 84, '_tax_class', ''),
(723, 84, '_manage_stock', 'no'),
(724, 84, '_backorders', 'no'),
(725, 84, '_low_stock_amount', ''),
(726, 84, '_sold_individually', 'no'),
(727, 84, '_weight', ''),
(728, 84, '_length', ''),
(729, 84, '_width', ''),
(730, 84, '_height', ''),
(731, 84, '_upsell_ids', 'a:0:{}'),
(732, 84, '_crosssell_ids', 'a:0:{}'),
(733, 84, '_purchase_note', ''),
(734, 84, '_default_attributes', 'a:0:{}'),
(735, 84, '_virtual', 'no'),
(736, 84, '_downloadable', 'no'),
(737, 84, '_product_image_gallery', '186'),
(738, 84, '_download_limit', '-1'),
(739, 84, '_download_expiry', '-1'),
(740, 84, '_stock', NULL),
(741, 84, '_stock_status', 'instock'),
(742, 84, '_product_version', '3.9.2'),
(743, 84, '_price', '45'),
(744, 86, '_wc_review_count', '0'),
(745, 86, '_wc_rating_count', 'a:0:{}'),
(746, 86, '_wc_average_rating', '0'),
(747, 86, '_thumbnail_id', '435'),
(748, 86, '_sku', ''),
(749, 86, '_regular_price', '950'),
(750, 86, '_sale_price', '700'),
(751, 86, '_sale_price_dates_from', ''),
(752, 86, '_sale_price_dates_to', ''),
(753, 86, 'total_sales', '0'),
(754, 86, '_tax_status', 'taxable'),
(755, 86, '_tax_class', ''),
(756, 86, '_manage_stock', 'no'),
(757, 86, '_backorders', 'no'),
(758, 86, '_low_stock_amount', ''),
(759, 86, '_sold_individually', 'no'),
(760, 86, '_weight', ''),
(761, 86, '_length', ''),
(762, 86, '_width', ''),
(763, 86, '_height', ''),
(764, 86, '_upsell_ids', 'a:0:{}'),
(765, 86, '_crosssell_ids', 'a:0:{}'),
(766, 86, '_purchase_note', ''),
(767, 86, '_default_attributes', 'a:0:{}'),
(768, 86, '_virtual', 'no'),
(769, 86, '_downloadable', 'no'),
(770, 86, '_product_image_gallery', ''),
(771, 86, '_download_limit', '-1'),
(772, 86, '_download_expiry', '-1'),
(773, 86, '_stock', NULL),
(774, 86, '_stock_status', 'instock'),
(775, 86, '_product_version', '3.9.2'),
(776, 86, '_price', '700'),
(777, 88, '_wc_review_count', '0'),
(778, 88, '_wc_rating_count', 'a:0:{}'),
(779, 88, '_wc_average_rating', '0'),
(780, 88, '_thumbnail_id', '439'),
(781, 88, '_sku', ''),
(782, 88, '_regular_price', '125'),
(783, 88, '_sale_price', ''),
(784, 88, '_sale_price_dates_from', ''),
(785, 88, '_sale_price_dates_to', ''),
(786, 88, 'total_sales', '0'),
(787, 88, '_tax_status', 'taxable'),
(788, 88, '_tax_class', ''),
(789, 88, '_manage_stock', 'no'),
(790, 88, '_backorders', 'no'),
(791, 88, '_low_stock_amount', ''),
(792, 88, '_sold_individually', 'no'),
(793, 88, '_weight', ''),
(794, 88, '_length', ''),
(795, 88, '_width', ''),
(796, 88, '_height', ''),
(797, 88, '_upsell_ids', 'a:0:{}'),
(798, 88, '_crosssell_ids', 'a:0:{}'),
(799, 88, '_purchase_note', ''),
(800, 88, '_default_attributes', 'a:0:{}'),
(801, 88, '_virtual', 'no'),
(802, 88, '_downloadable', 'no'),
(803, 88, '_product_image_gallery', '191'),
(804, 88, '_download_limit', '-1'),
(805, 88, '_download_expiry', '-1'),
(806, 88, '_stock', NULL),
(807, 88, '_stock_status', 'instock'),
(808, 88, '_product_version', '3.9.2'),
(809, 88, '_price', '125'),
(810, 90, '_wc_review_count', '0'),
(811, 90, '_wc_rating_count', 'a:0:{}'),
(812, 90, '_wc_average_rating', '0'),
(813, 90, '_thumbnail_id', '437'),
(814, 90, '_sku', ''),
(815, 90, '_regular_price', '220'),
(816, 90, '_sale_price', '100'),
(817, 90, '_sale_price_dates_from', ''),
(818, 90, '_sale_price_dates_to', ''),
(819, 90, 'total_sales', '0'),
(820, 90, '_tax_status', 'taxable'),
(821, 90, '_tax_class', ''),
(822, 90, '_manage_stock', 'no'),
(823, 90, '_backorders', 'no'),
(824, 90, '_low_stock_amount', ''),
(825, 90, '_sold_individually', 'no'),
(826, 90, '_weight', ''),
(827, 90, '_length', ''),
(828, 90, '_width', ''),
(829, 90, '_height', ''),
(830, 90, '_upsell_ids', 'a:0:{}'),
(831, 90, '_crosssell_ids', 'a:0:{}'),
(832, 90, '_purchase_note', ''),
(833, 90, '_default_attributes', 'a:0:{}'),
(834, 90, '_virtual', 'no'),
(835, 90, '_downloadable', 'no'),
(836, 90, '_product_image_gallery', ''),
(837, 90, '_download_limit', '-1'),
(838, 90, '_download_expiry', '-1'),
(839, 90, '_stock', NULL),
(840, 90, '_stock_status', 'instock'),
(841, 90, '_product_version', '3.9.2'),
(842, 90, '_price', '100'),
(843, 92, '_wc_review_count', '0'),
(844, 92, '_wc_rating_count', 'a:0:{}'),
(845, 92, '_wc_average_rating', '0'),
(846, 92, '_thumbnail_id', '449'),
(847, 92, '_sku', ''),
(848, 92, '_regular_price', '90'),
(849, 92, '_sale_price', ''),
(850, 92, '_sale_price_dates_from', ''),
(851, 92, '_sale_price_dates_to', ''),
(852, 92, 'total_sales', '0'),
(853, 92, '_tax_status', 'taxable'),
(854, 92, '_tax_class', ''),
(855, 92, '_manage_stock', 'no'),
(856, 92, '_backorders', 'no'),
(857, 92, '_low_stock_amount', ''),
(858, 92, '_sold_individually', 'no'),
(859, 92, '_weight', ''),
(860, 92, '_length', ''),
(861, 92, '_width', ''),
(862, 92, '_height', ''),
(863, 92, '_upsell_ids', 'a:0:{}'),
(864, 92, '_crosssell_ids', 'a:0:{}'),
(865, 92, '_purchase_note', ''),
(866, 92, '_default_attributes', 'a:0:{}'),
(867, 92, '_virtual', 'no'),
(868, 92, '_downloadable', 'no'),
(869, 92, '_product_image_gallery', '160,162'),
(870, 92, '_download_limit', '-1'),
(871, 92, '_download_expiry', '-1'),
(872, 92, '_stock', NULL),
(873, 92, '_stock_status', 'instock'),
(874, 92, '_product_version', '3.9.2'),
(875, 92, '_price', '90'),
(876, 94, '_wc_review_count', '0'),
(877, 94, '_wc_rating_count', 'a:0:{}'),
(878, 94, '_wc_average_rating', '0'),
(879, 94, '_thumbnail_id', '448'),
(880, 94, '_sku', ''),
(881, 94, '_regular_price', '150'),
(882, 94, '_sale_price', ''),
(883, 94, '_sale_price_dates_from', ''),
(884, 94, '_sale_price_dates_to', ''),
(885, 94, 'total_sales', '0'),
(886, 94, '_tax_status', 'taxable'),
(887, 94, '_tax_class', ''),
(888, 94, '_manage_stock', 'no'),
(889, 94, '_backorders', 'no'),
(890, 94, '_low_stock_amount', ''),
(891, 94, '_sold_individually', 'no'),
(892, 94, '_weight', ''),
(893, 94, '_length', ''),
(894, 94, '_width', ''),
(895, 94, '_height', ''),
(896, 94, '_upsell_ids', 'a:0:{}'),
(897, 94, '_crosssell_ids', 'a:0:{}'),
(898, 94, '_purchase_note', ''),
(899, 94, '_default_attributes', 'a:0:{}'),
(900, 94, '_virtual', 'no'),
(901, 94, '_downloadable', 'no'),
(902, 94, '_product_image_gallery', '158,157,448'),
(903, 94, '_download_limit', '-1'),
(904, 94, '_download_expiry', '-1'),
(905, 94, '_stock', NULL),
(906, 94, '_stock_status', 'instock'),
(907, 94, '_product_version', '3.9.2'),
(908, 94, '_price', '150');
INSERT INTO `cm_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(909, 379, '_menu_item_type', 'post_type'),
(910, 379, '_menu_item_menu_item_parent', '0'),
(911, 379, '_menu_item_object_id', '34'),
(912, 379, '_menu_item_object', 'page'),
(913, 379, '_menu_item_target', ''),
(914, 379, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(915, 379, '_menu_item_xfn', ''),
(916, 379, '_menu_item_url', ''),
(917, 380, '_menu_item_type', 'post_type'),
(918, 380, '_menu_item_menu_item_parent', '382'),
(919, 380, '_menu_item_object_id', '117'),
(920, 380, '_menu_item_object', 'page'),
(921, 380, '_menu_item_target', ''),
(922, 380, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(923, 380, '_menu_item_xfn', ''),
(924, 380, '_menu_item_url', ''),
(925, 381, '_menu_item_type', 'post_type'),
(926, 381, '_menu_item_menu_item_parent', '382'),
(927, 381, '_menu_item_object_id', '119'),
(928, 381, '_menu_item_object', 'page'),
(929, 381, '_menu_item_target', ''),
(930, 381, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(931, 381, '_menu_item_xfn', ''),
(932, 381, '_menu_item_url', ''),
(933, 382, '_menu_item_type', 'post_type'),
(934, 382, '_menu_item_menu_item_parent', '0'),
(935, 382, '_menu_item_object_id', '121'),
(936, 382, '_menu_item_object', 'page'),
(937, 382, '_menu_item_target', ''),
(938, 382, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(939, 382, '_menu_item_xfn', ''),
(940, 382, '_menu_item_url', ''),
(941, 383, '_menu_item_type', 'post_type'),
(942, 383, '_menu_item_menu_item_parent', '0'),
(943, 383, '_menu_item_object_id', '211'),
(944, 383, '_menu_item_object', 'page'),
(945, 383, '_menu_item_target', ''),
(946, 383, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(947, 383, '_menu_item_xfn', ''),
(948, 383, '_menu_item_url', ''),
(957, 385, '_menu_item_type', 'post_type'),
(958, 385, '_menu_item_menu_item_parent', '0'),
(959, 385, '_menu_item_object_id', '21'),
(960, 385, '_menu_item_object', 'page'),
(961, 385, '_menu_item_target', ''),
(962, 385, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(963, 385, '_menu_item_xfn', ''),
(964, 385, '_menu_item_url', ''),
(965, 386, '_menu_item_type', 'post_type'),
(966, 386, '_menu_item_menu_item_parent', '0'),
(967, 386, '_menu_item_object_id', '375'),
(968, 386, '_menu_item_object', 'page'),
(969, 386, '_menu_item_target', ''),
(970, 386, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(971, 386, '_menu_item_xfn', ''),
(972, 386, '_menu_item_url', ''),
(973, 387, '_menu_item_type', 'post_type'),
(974, 387, '_menu_item_menu_item_parent', '0'),
(975, 387, '_menu_item_object_id', '374'),
(976, 387, '_menu_item_object', 'page'),
(977, 387, '_menu_item_target', ''),
(978, 387, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(979, 387, '_menu_item_xfn', ''),
(980, 387, '_menu_item_url', ''),
(981, 388, '_menu_item_type', 'post_type'),
(982, 388, '_menu_item_menu_item_parent', '0'),
(983, 388, '_menu_item_object_id', '373'),
(984, 388, '_menu_item_object', 'page'),
(985, 388, '_menu_item_target', ''),
(986, 388, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(987, 388, '_menu_item_xfn', ''),
(988, 388, '_menu_item_url', ''),
(989, 389, '_menu_item_type', 'post_type'),
(990, 389, '_menu_item_menu_item_parent', '0'),
(991, 389, '_menu_item_object_id', '5'),
(992, 389, '_menu_item_object', 'page'),
(993, 389, '_menu_item_target', ''),
(994, 389, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(995, 389, '_menu_item_xfn', ''),
(996, 389, '_menu_item_url', ''),
(997, 390, '_menu_item_type', 'post_type'),
(998, 390, '_menu_item_menu_item_parent', '0'),
(999, 390, '_menu_item_object_id', '211'),
(1000, 390, '_menu_item_object', 'page'),
(1001, 390, '_menu_item_target', ''),
(1002, 390, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1003, 390, '_menu_item_xfn', ''),
(1004, 390, '_menu_item_url', ''),
(1005, 391, '_menu_item_type', 'post_type'),
(1006, 391, '_menu_item_menu_item_parent', '0'),
(1007, 391, '_menu_item_object_id', '315'),
(1008, 391, '_menu_item_object', 'page'),
(1009, 391, '_menu_item_target', ''),
(1010, 391, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1011, 391, '_menu_item_xfn', ''),
(1012, 391, '_menu_item_url', ''),
(1013, 392, '_menu_item_type', 'post_type'),
(1014, 392, '_menu_item_menu_item_parent', '0'),
(1015, 392, '_menu_item_object_id', '21'),
(1016, 392, '_menu_item_object', 'page'),
(1017, 392, '_menu_item_target', ''),
(1018, 392, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1019, 392, '_menu_item_xfn', ''),
(1020, 392, '_menu_item_url', ''),
(1021, 393, '_menu_item_type', 'post_type'),
(1022, 393, '_menu_item_menu_item_parent', '0'),
(1023, 393, '_menu_item_object_id', '34'),
(1024, 393, '_menu_item_object', 'page'),
(1025, 393, '_menu_item_target', ''),
(1026, 393, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1027, 393, '_menu_item_xfn', ''),
(1028, 393, '_menu_item_url', ''),
(1029, 1, '_wp_trash_meta_status', 'publish'),
(1030, 1, '_wp_trash_meta_time', '1582444374'),
(1031, 1, '_wp_desired_post_slug', 'hello-world'),
(1032, 253, '_wp_trash_meta_status', 'publish'),
(1033, 253, '_wp_trash_meta_time', '1582444374'),
(1034, 253, '_wp_desired_post_slug', 'hello-world-2'),
(1035, 254, '_wp_trash_meta_status', 'publish'),
(1036, 254, '_wp_trash_meta_time', '1582444381'),
(1037, 254, '_wp_desired_post_slug', 'hello-world-2-2'),
(1038, 211, '_edit_lock', '1582445545:1'),
(1039, 398, '_edit_lock', '1582983187:1'),
(1040, 398, '_wp_trash_meta_status', 'publish'),
(1041, 398, '_wp_trash_meta_time', '1582983195'),
(1042, 399, '_wp_attached_file', '2020/02/logo.jpg'),
(1043, 399, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1125;s:6:\"height\";i:1103;s:4:\"file\";s:16:\"2020/02/logo.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"logo-300x294.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:294;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"logo-1024x1004.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1004;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"logo-768x753.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:753;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:16:\"logo-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:16:\"logo-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:16:\"logo-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1044, 400, '_edit_lock', '1582983349:1'),
(1045, 399, '_edit_lock', '1582983277:1'),
(1046, 401, '_wp_attached_file', '2020/02/cropped-logo.jpg'),
(1047, 401, '_wp_attachment_context', 'custom-logo'),
(1048, 401, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:914;s:6:\"height\";i:921;s:4:\"file\";s:24:\"2020/02/cropped-logo.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"cropped-logo-298x300.jpg\";s:5:\"width\";i:298;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"cropped-logo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"cropped-logo-768x774.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:774;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:24:\"cropped-logo-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:24:\"cropped-logo-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:24:\"cropped-logo-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"cropped-logo-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"cropped-logo-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1049, 400, '_wp_trash_meta_status', 'publish'),
(1050, 400, '_wp_trash_meta_time', '1582983375'),
(1051, 313, '_edit_lock', '1582983327:1'),
(1052, 403, '_edit_lock', '1582983652:1'),
(1053, 403, '_wp_trash_meta_status', 'publish'),
(1054, 403, '_wp_trash_meta_time', '1582983679'),
(1055, 405, '_menu_item_type', 'post_type'),
(1056, 405, '_menu_item_menu_item_parent', '0'),
(1057, 405, '_menu_item_object_id', '7'),
(1058, 405, '_menu_item_object', 'page'),
(1059, 405, '_menu_item_target', ''),
(1060, 405, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1061, 405, '_menu_item_xfn', ''),
(1062, 405, '_menu_item_url', ''),
(1063, 404, '_wp_trash_meta_status', 'publish'),
(1064, 404, '_wp_trash_meta_time', '1582983736'),
(1065, 406, '_wp_trash_meta_status', 'publish'),
(1066, 406, '_wp_trash_meta_time', '1582983873'),
(1067, 407, '_wp_attached_file', '2020/02/p1.jpg'),
(1068, 407, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1125;s:6:\"height\";i:1380;s:4:\"file\";s:14:\"2020/02/p1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1071, 409, '_wp_attached_file', '2020/02/front1.jpg'),
(1072, 409, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:18:\"2020/02/front1.jpg\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"front1-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"front1-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"front1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"front1-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:19:\"front1-1536x864.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:864;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:18:\"front1-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:18:\"front1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:18:\"front1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"front1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"front1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1073, 409, '_wp_attachment_is_custom_background', 'zigcy-cosmetics'),
(1074, 410, '_wp_attached_file', '2020/02/cropped-front1.jpg'),
(1075, 410, '_wp_attachment_context', 'custom-header'),
(1076, 410, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1000;s:6:\"height\";i:562;s:4:\"file\";s:26:\"2020/02/cropped-front1.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"cropped-front1-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"cropped-front1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"cropped-front1-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:26:\"cropped-front1-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:26:\"cropped-front1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:26:\"cropped-front1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"cropped-front1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"cropped-front1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:17:\"attachment_parent\";i:409;}'),
(1077, 410, '_wp_attachment_custom_header_last_used_zigcy-cosmetics', '1582984279'),
(1078, 410, '_wp_attachment_is_custom_header', 'zigcy-cosmetics'),
(1079, 411, '_edit_lock', '1582984539:1'),
(1080, 411, '_wp_trash_meta_status', 'publish'),
(1081, 411, '_wp_trash_meta_time', '1582984552'),
(1083, 413, '_wp_attached_file', '2020/02/h2.jpg'),
(1084, 413, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:672;s:4:\"file\";s:14:\"2020/02/h2.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"h2-300x168.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"h2-1024x573.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:573;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"h2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"h2-768x430.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:430;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:14:\"h2-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"h2-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"h2-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"h2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"h2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1086, 414, '_edit_lock', '1583044087:1'),
(1087, 415, '_wp_attached_file', '2020/03/h3.jpg'),
(1088, 415, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:600;s:4:\"file\";s:14:\"2020/03/h3.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1089, 416, '_wp_attached_file', '2020/03/h4.jpg'),
(1090, 416, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:240;s:4:\"file\";s:14:\"2020/03/h4.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1091, 414, '_wp_trash_meta_status', 'publish'),
(1092, 414, '_wp_trash_meta_time', '1583044108'),
(1093, 376, '_edit_lock', '1583044258:1'),
(1094, 418, '_wp_attached_file', '2018/11/s1-2.jpg'),
(1095, 418, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:768;s:6:\"height\";i:768;s:4:\"file\";s:16:\"2018/11/s1-2.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1098, 377, '_edit_lock', '1583044889:1'),
(1099, 420, '_wp_attached_file', '2018/11/s2.jpeg'),
(1100, 420, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:800;s:4:\"file\";s:15:\"2018/11/s2.jpeg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"s2-225x300.jpeg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"s2-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:15:\"s2-220x154.jpeg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:15:\"s2-600x300.jpeg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:15:\"s2-390x290.jpeg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"s2-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"s2-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1103, 422, '_wp_attached_file', '2018/11/s3-1.jpg'),
(1104, 422, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:735;s:6:\"height\";i:490;s:4:\"file\";s:16:\"2018/11/s3-1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1107, 423, '_wp_trash_meta_status', 'publish'),
(1108, 423, '_wp_trash_meta_time', '1583044740'),
(1109, 424, '_wp_trash_meta_status', 'publish'),
(1110, 424, '_wp_trash_meta_time', '1583044788'),
(1111, 425, '_wp_trash_meta_status', 'publish'),
(1112, 425, '_wp_trash_meta_time', '1583044878'),
(1113, 426, '_edit_lock', '1583045004:1'),
(1114, 426, '_wp_trash_meta_status', 'publish'),
(1115, 426, '_wp_trash_meta_time', '1583045006'),
(1116, 34, '_edit_lock', '1583044915:1'),
(1117, 94, '_edit_lock', '1583045563:1'),
(1118, 427, '_wp_attached_file', '2018/11/front1.jpg'),
(1119, 427, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:18:\"2018/11/front1.jpg\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"front1-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"front1-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"front1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"front1-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:19:\"front1-1536x864.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:864;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:18:\"front1-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:18:\"front1-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:18:\"front1-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"front1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"front1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1120, 428, '_wp_attached_file', '2018/11/h2.jpg'),
(1121, 428, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:672;s:4:\"file\";s:14:\"2018/11/h2.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1122, 429, '_wp_attached_file', '2018/11/h3.jpg'),
(1123, 429, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:600;s:4:\"file\";s:14:\"2018/11/h3.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"h3-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"h3-1024x512.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"h3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"h3-768x384.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:384;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:14:\"h3-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"h3-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"h3-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"h3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"h3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1124, 430, '_wp_attached_file', '2018/11/h4.jpg'),
(1125, 430, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:240;s:4:\"file\";s:14:\"2018/11/h4.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"h4-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"h4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:14:\"h4-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"h4-390x240.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"h4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"h4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1126, 431, '_wp_attached_file', '2018/11/logo.jpg'),
(1127, 431, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1125;s:6:\"height\";i:1103;s:4:\"file\";s:16:\"2018/11/logo.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"logo-300x294.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:294;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"logo-1024x1004.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1004;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"logo-768x753.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:753;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:16:\"logo-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:16:\"logo-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:16:\"logo-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1128, 432, '_wp_attached_file', '2018/11/p1.jpg'),
(1129, 432, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1125;s:6:\"height\";i:1380;s:4:\"file\";s:14:\"2018/11/p1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1130, 433, '_wp_attached_file', '2018/11/p2.jpg'),
(1131, 433, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1125;s:6:\"height\";i:2436;s:4:\"file\";s:14:\"2018/11/p2.jpg\";s:5:\"sizes\";a:11:{s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"p2-139x300.jpg\";s:5:\"width\";i:139;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"p2-473x1024.jpg\";s:5:\"width\";i:473;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"p2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"p2-768x1663.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1663;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:15:\"p2-709x1536.jpg\";s:5:\"width\";i:709;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:15:\"p2-946x2048.jpg\";s:5:\"width\";i:946;s:6:\"height\";i:2048;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:14:\"p2-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"p2-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"p2-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"p2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"p2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1132, 434, '_wp_attached_file', '2018/11/p3.jpg'),
(1133, 434, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1125;s:6:\"height\";i:1333;s:4:\"file\";s:14:\"2018/11/p3.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1134, 435, '_wp_attached_file', '2018/11/p4.jpg'),
(1135, 435, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:970;s:6:\"height\";i:853;s:4:\"file\";s:14:\"2018/11/p4.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1136, 436, '_wp_attached_file', '2018/11/p5.jpg'),
(1137, 436, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:1626;s:4:\"file\";s:14:\"2018/11/p5.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1138, 437, '_wp_attached_file', '2018/11/p6.jpg'),
(1139, 437, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1012;s:6:\"height\";i:809;s:4:\"file\";s:14:\"2018/11/p6.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1140, 438, '_wp_attached_file', '2018/11/p7.jpg'),
(1141, 438, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:618;s:6:\"height\";i:1550;s:4:\"file\";s:14:\"2018/11/p7.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"p7-120x300.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"p7-408x1024.jpg\";s:5:\"width\";i:408;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"p7-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:15:\"p7-612x1536.jpg\";s:5:\"width\";i:612;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:14:\"p7-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:14:\"p7-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:14:\"p7-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"p7-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"p7-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1142, 439, '_wp_attached_file', '2018/11/p8.jpg'),
(1143, 439, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:980;s:6:\"height\";i:811;s:4:\"file\";s:14:\"2018/11/p8.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1144, 440, '_wp_attached_file', '2018/11/p9.jpg'),
(1145, 440, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:788;s:6:\"height\";i:1443;s:4:\"file\";s:14:\"2018/11/p9.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1146, 441, '_wp_attached_file', '2018/11/p10.jpg'),
(1147, 441, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:712;s:6:\"height\";i:1631;s:4:\"file\";s:15:\"2018/11/p10.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"p10-131x300.jpg\";s:5:\"width\";i:131;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"p10-447x1024.jpg\";s:5:\"width\";i:447;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"p10-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:16:\"p10-671x1536.jpg\";s:5:\"width\";i:671;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:15:\"p10-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:15:\"p10-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:15:\"p10-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"p10-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"p10-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1148, 442, '_wp_attached_file', '2018/11/p11.jpg'),
(1149, 442, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:448;s:6:\"height\";i:1426;s:4:\"file\";s:15:\"2018/11/p11.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"p11-94x300.jpg\";s:5:\"width\";i:94;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"p11-322x1024.jpg\";s:5:\"width\";i:322;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"p11-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:15:\"p11-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:15:\"p11-448x300.jpg\";s:5:\"width\";i:448;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:15:\"p11-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"p11-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"p11-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1150, 443, '_wp_attached_file', '2018/11/p12.jpg'),
(1151, 443, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:470;s:6:\"height\";i:1491;s:4:\"file\";s:15:\"2018/11/p12.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"p12-95x300.jpg\";s:5:\"width\";i:95;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"p12-323x1024.jpg\";s:5:\"width\";i:323;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"p12-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:15:\"p12-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:15:\"p12-470x300.jpg\";s:5:\"width\";i:470;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:15:\"p12-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"p12-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"p12-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1152, 444, '_wp_attached_file', '2018/11/p13.jpg'),
(1153, 444, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:565;s:6:\"height\";i:1368;s:4:\"file\";s:15:\"2018/11/p13.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1154, 445, '_wp_attached_file', '2018/11/p14.jpg'),
(1155, 445, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:444;s:6:\"height\";i:1414;s:4:\"file\";s:15:\"2018/11/p14.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"p14-94x300.jpg\";s:5:\"width\";i:94;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"p14-322x1024.jpg\";s:5:\"width\";i:322;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"p14-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:15:\"p14-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:15:\"p14-444x300.jpg\";s:5:\"width\";i:444;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:15:\"p14-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"p14-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"p14-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1156, 446, '_wp_attached_file', '2018/11/p15.jpg'),
(1157, 446, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:431;s:6:\"height\";i:876;s:4:\"file\";s:15:\"2018/11/p15.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"p15-148x300.jpg\";s:5:\"width\";i:148;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"p15-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:15:\"p15-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:15:\"p15-431x300.jpg\";s:5:\"width\";i:431;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:15:\"p15-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"p15-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"p15-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1158, 447, '_wp_attached_file', '2018/11/p16.jpg'),
(1159, 447, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:432;s:6:\"height\";i:1432;s:4:\"file\";s:15:\"2018/11/p16.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1160, 448, '_wp_attached_file', '2018/11/p17.jpg'),
(1161, 448, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1124;s:6:\"height\";i:1319;s:4:\"file\";s:15:\"2018/11/p17.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1162, 449, '_wp_attached_file', '2018/11/p18.jpg'),
(1163, 449, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1125;s:6:\"height\";i:1413;s:4:\"file\";s:15:\"2018/11/p18.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1164, 450, '_wp_attached_file', '2018/11/s1-3.jpg'),
(1165, 450, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:768;s:6:\"height\";i:768;s:4:\"file\";s:16:\"2018/11/s1-3.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1166, 451, '_wp_attached_file', '2018/11/s2-1.jpeg'),
(1167, 451, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:800;s:4:\"file\";s:17:\"2018/11/s2-1.jpeg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"s2-1-225x300.jpeg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"s2-1-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:17:\"s2-1-220x154.jpeg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:17:\"s2-1-600x300.jpeg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:17:\"s2-1-390x290.jpeg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"s2-1-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"s2-1-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1168, 452, '_wp_attached_file', '2018/11/s3-2.jpg');
INSERT INTO `cm_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1169, 452, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:735;s:6:\"height\";i:490;s:4:\"file\";s:16:\"2018/11/s3-2.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"s3-2-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"s3-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"yith-woocompare-image\";a:4:{s:4:\"file\";s:16:\"s3-2-220x154.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:20:\"zigcy-cosmetics-wide\";a:4:{s:4:\"file\";s:16:\"s3-2-600x300.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"sm-blog-img\";a:4:{s:4:\"file\";s:16:\"s3-2-390x290.jpg\";s:5:\"width\";i:390;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"s3-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"s3-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1170, 94, '_edit_last', '1'),
(1171, 92, '_edit_lock', '1583045599:1'),
(1172, 92, '_edit_last', '1'),
(1173, 90, '_edit_lock', '1583046674:1'),
(1174, 90, '_edit_last', '1'),
(1175, 88, '_edit_lock', '1583045722:1'),
(1176, 88, '_edit_last', '1'),
(1177, 86, '_edit_lock', '1583045781:1'),
(1178, 86, '_edit_last', '1'),
(1179, 84, '_edit_lock', '1583046585:1'),
(1180, 84, '_edit_last', '1'),
(1181, 82, '_edit_lock', '1583045848:1'),
(1182, 82, '_edit_last', '1'),
(1183, 80, '_edit_lock', '1583046619:1'),
(1184, 80, '_edit_last', '1'),
(1185, 78, '_edit_lock', '1583045965:1'),
(1186, 78, '_edit_last', '1'),
(1187, 76, '_edit_lock', '1583045999:1'),
(1188, 76, '_edit_last', '1'),
(1189, 71, '_edit_lock', '1583046905:1'),
(1190, 71, '_edit_last', '1'),
(1191, 67, '_edit_lock', '1583046129:1'),
(1192, 67, '_edit_last', '1'),
(1193, 65, '_edit_lock', '1583046338:1'),
(1194, 65, '_edit_last', '1'),
(1195, 63, '_edit_lock', '1583046372:1'),
(1196, 63, '_edit_last', '1'),
(1197, 61, '_edit_lock', '1583046406:1'),
(1198, 61, '_edit_last', '1'),
(1199, 59, '_edit_lock', '1583046454:1'),
(1200, 59, '_edit_last', '1'),
(1201, 57, '_edit_lock', '1583046489:1'),
(1202, 57, '_edit_last', '1'),
(1203, 55, '_edit_lock', '1583046512:1'),
(1204, 55, '_edit_last', '1');

-- --------------------------------------------------------

--
-- Table structure for table `cm_posts`
--

CREATE TABLE `cm_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_posts`
--

INSERT INTO `cm_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-03-08 07:49:42', '2019-03-08 07:49:42', 'Welcome to <a href=\"http://demo.accesspressthemes.com/zigcy-lite/\">Zigcy Lite </a>. This is your first post. Edit or delete it, then start blogging!', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world__trashed', '', '', '2020-02-23 07:52:54', '2020-02-23 07:52:54', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?p=1', 0, 'post', '', 0),
(2, 1, '2019-03-08 07:49:42', '2019-03-08 07:49:42', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"https://demo.accesspressthemes.com/zigcy-lite/demo-one/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2019-03-08 07:49:42', '2019-03-08 07:49:42', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?page_id=2', 0, 'page', '', 0),
(3, 1, '2020-02-23 07:28:55', '2020-02-23 07:28:55', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://cosmetic.test.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2020-02-23 07:28:55', '2020-02-23 07:28:55', '', 0, 'http://cosmetic.test/?page_id=3', 0, 'page', '', 0),
(5, 1, '2019-01-29 05:12:46', '2019-01-29 05:12:46', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop-2', '', '', '2019-01-29 05:12:46', '2019-01-29 05:12:46', '', 0, 'http://demo.accesspressthemes.com/zigcy-lite/shop/', 0, 'page', '', 0),
(6, 1, '2020-02-23 07:29:58', '2020-02-23 07:29:58', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2020-02-23 07:29:58', '2020-02-23 07:29:58', '', 0, 'http://cosmetic.test/wp-content/uploads/2020/02/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(7, 1, '2020-02-23 07:30:56', '2020-02-23 07:30:56', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2020-02-23 07:30:56', '2020-02-23 07:30:56', '', 0, 'http://cosmetic.test/shop/', 0, 'page', '', 0),
(8, 1, '2020-02-23 07:30:56', '2020-02-23 07:30:56', '<!-- wp:shortcode -->[woocommerce_cart]<!-- /wp:shortcode -->', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2020-02-23 07:30:56', '2020-02-23 07:30:56', '', 0, 'http://cosmetic.test/cart/', 0, 'page', '', 0),
(9, 1, '2020-02-23 07:30:56', '2020-02-23 07:30:56', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2020-02-23 07:30:56', '2020-02-23 07:30:56', '', 0, 'http://cosmetic.test/checkout/', 0, 'page', '', 0),
(10, 1, '2020-02-23 07:30:56', '2020-02-23 07:30:56', '<!-- wp:shortcode -->[woocommerce_my_account]<!-- /wp:shortcode -->', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2020-02-23 07:30:56', '2020-02-23 07:30:56', '', 0, 'http://cosmetic.test/my-account/', 0, 'page', '', 0),
(13, 1, '2020-02-23 07:36:10', '2020-02-23 07:36:10', '{\n    \"old_sidebars_widgets_data\": {\n        \"value\": {\n            \"wp_inactive_widgets\": [],\n            \"sidebar-1\": [\n                \"search-2\",\n                \"recent-posts-2\",\n                \"recent-comments-2\",\n                \"archives-2\",\n                \"categories-2\",\n                \"meta-2\"\n            ],\n            \"header-1\": [],\n            \"footer-1\": [],\n            \"footer-2\": [],\n            \"footer-3\": [],\n            \"footer-4\": []\n        },\n        \"type\": \"global_variable\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-23 07:36:09\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'e133b7cc-65d4-4378-858d-9aa0de84c768', '', '', '2020-02-23 07:36:10', '2020-02-23 07:36:10', '', 0, 'http://cosmetic.test/2020/02/23/e133b7cc-65d4-4378-858d-9aa0de84c768/', 0, 'customize_changeset', '', 0),
(21, 1, '2018-11-27 07:28:50', '2018-11-27 07:28:50', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2018-11-27 07:28:50', '2018-11-27 07:28:50', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?page_id=21', 0, 'page', '', 0),
(34, 1, '2018-11-27 07:46:38', '2018-11-27 07:46:38', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-11-27 07:46:38', '2018-11-27 07:46:38', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?page_id=34', 0, 'page', '', 0),
(48, 1, '2018-11-27 08:03:53', '2018-11-27 08:03:53', '', 'siblings-817369_1920', '', 'inherit', 'open', 'closed', '', 'siblings-817369_1920', '', '', '2018-11-27 08:03:53', '2018-11-27 08:03:53', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/siblings-817369_1920.jpg', 0, 'attachment', 'image/jpeg', 0),
(49, 1, '2018-11-27 08:11:24', '2018-11-27 08:11:24', '', 'gadgets-336635_1920', '', 'inherit', 'open', 'closed', '', 'gadgets-336635_1920', '', '', '2018-11-27 08:11:24', '2018-11-27 08:11:24', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/gadgets-336635_1920.jpg', 0, 'attachment', 'image/jpeg', 0),
(50, 1, '2018-11-27 08:13:20', '2018-11-27 08:13:20', '', 'smartwatch-828786_960_720', '', 'inherit', 'open', 'closed', '', 'smartwatch-828786_960_720', '', '', '2018-11-27 08:13:20', '2018-11-27 08:13:20', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/smartwatch-828786_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(54, 1, '2018-11-27 09:22:15', '2018-11-27 09:22:15', '', 'bag-15841_960_720', '', 'inherit', 'open', 'closed', '', 'bag-15841_960_720', '', '', '2018-11-27 09:22:15', '2018-11-27 09:22:15', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/bag-15841_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(55, 1, '2018-11-27 09:30:45', '2018-11-27 09:30:45', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve.\r\n\r\n</div>\r\n</div>', 'Item 16', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul class=\"supro-list\">\r\n 	<li>Fully padded back panel, web hauded handle</li>\r\n 	<li>Internal padded sleeve fits 15″ laptop &amp; unique fabric application</li>\r\n 	<li>Internal tricot lined tablet sleeve</li>\r\n 	<li>One large main compartment and zippered</li>\r\n 	<li>Premium cotton canvas fabric</li>\r\n</ul>\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'mens-cotton-tshirts', '', '', '2020-03-01 07:10:52', '2020-03-01 07:10:52', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=55', 0, 'product', '', 0),
(56, 1, '2018-11-27 09:30:00', '2018-11-27 09:30:00', '', 'white-926838_960_720', '', 'inherit', 'open', 'closed', '', 'white-926838_960_720', '', '', '2018-11-27 09:30:00', '2018-11-27 09:30:00', '', 55, 'http://cosmetic.test/wp-content/uploads/2018/11/white-926838_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(57, 1, '2018-11-27 09:32:50', '2018-11-27 09:32:50', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>\r\n<div class=\"supro-empty-space \"></div>\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul class=\"supro-list\">\r\n 	<li>Fully padded back panel, web hauded handle</li>\r\n 	<li>Internal padded sleeve fits 15″ laptop &amp; unique fabric application</li>\r\n 	<li>Internal tricot lined tablet sleeve</li>\r\n 	<li>One large main compartment and zippered</li>\r\n 	<li>Premium cotton canvas fabric</li>\r\n</ul>\r\n</div>\r\n</div>', 'Item 15', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'new-shirts', '', '', '2020-03-01 07:10:29', '2020-03-01 07:10:29', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=57', 0, 'product', '', 0),
(58, 1, '2018-11-27 09:31:58', '2018-11-27 09:31:58', '', 'waiting-410328_960_720', '', 'inherit', 'open', 'closed', '', 'waiting-410328_960_720', '', '', '2018-11-27 09:31:58', '2018-11-27 09:31:58', '', 57, 'http://cosmetic.test/wp-content/uploads/2018/11/waiting-410328_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(59, 1, '2018-11-27 09:34:03', '2018-11-27 09:34:03', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Item 14', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'slim-fit-coat', '', '', '2020-03-01 07:09:50', '2020-03-01 07:09:50', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=59', 0, 'product', '', 0),
(60, 1, '2018-11-27 09:33:26', '2018-11-27 09:33:26', '', 'business-suit-690048_960_720', '', 'inherit', 'open', 'closed', '', 'business-suit-690048_960_720', '', '', '2018-11-27 09:33:26', '2018-11-27 09:33:26', '', 59, 'http://cosmetic.test/wp-content/uploads/2018/11/business-suit-690048_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(61, 1, '2018-11-27 09:34:52', '2018-11-27 09:34:52', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Item 13', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'black-lady-coat', '', '', '2020-03-01 07:09:04', '2020-03-01 07:09:04', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=61', 0, 'product', '', 0),
(62, 1, '2018-11-27 09:34:28', '2018-11-27 09:34:28', '', 'business-lady-1584655_960_720', '', 'inherit', 'open', 'closed', '', 'business-lady-1584655_960_720', '', '', '2018-11-27 09:34:28', '2018-11-27 09:34:28', '', 61, 'http://cosmetic.test/wp-content/uploads/2018/11/business-lady-1584655_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(63, 1, '2018-11-27 09:35:46', '2018-11-27 09:35:46', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Item 12', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'denim-jeans-pants', '', '', '2020-03-01 07:08:32', '2020-03-01 07:08:32', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=63', 0, 'product', '', 0),
(64, 1, '2018-11-27 09:35:21', '2018-11-27 09:35:21', '', 'adult-1869621_960_720', '', 'inherit', 'open', 'closed', '', 'adult-1869621_960_720', '', '', '2018-11-27 09:35:21', '2018-11-27 09:35:21', '', 63, 'http://cosmetic.test/wp-content/uploads/2018/11/adult-1869621_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(65, 1, '2018-11-27 09:37:07', '2018-11-27 09:37:07', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Item 11', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'lvd-jeans', '', '', '2020-03-01 07:05:02', '2020-03-01 07:05:02', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=65', 0, 'product', '', 0),
(66, 1, '2018-11-27 09:36:15', '2018-11-27 09:36:15', '', 'adult-1868988_960_720', '', 'inherit', 'open', 'closed', '', 'adult-1868988_960_720', '', '', '2018-11-27 09:36:15', '2018-11-27 09:36:15', '', 65, 'http://cosmetic.test/wp-content/uploads/2018/11/adult-1868988_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(67, 1, '2018-11-27 09:44:42', '2018-11-27 09:44:42', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Item 10', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'super-slim-fit-suit-blazer', '', '', '2020-03-01 07:03:43', '2020-03-01 07:03:43', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=67', 0, 'product', '', 0),
(70, 1, '2018-11-27 09:44:08', '2018-11-27 09:44:08', '', '1255257800_2_1_1', '', 'inherit', 'open', 'closed', '', '1255257800_2_1_1', '', '', '2018-11-27 09:44:08', '2018-11-27 09:44:08', '', 67, 'http://cosmetic.test/wp-content/uploads/2018/11/1255257800_2_1_1.jpg', 0, 'attachment', 'image/jpeg', 0),
(71, 1, '2018-11-27 09:46:20', '2018-11-27 09:46:20', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Item 9', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'textured-slim-fit-blazer', '', '', '2020-03-01 07:13:06', '2020-03-01 07:13:06', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=71', 0, 'product', '', 0),
(76, 1, '2018-11-27 09:55:15', '2018-11-27 09:55:15', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Item 8', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'leather-belt', '', '', '2020-03-01 07:02:18', '2020-03-01 07:02:18', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=76', 0, 'product', '', 0),
(77, 1, '2018-11-27 09:54:52', '2018-11-27 09:54:52', '', 'belt-139757_960_720', '', 'inherit', 'open', 'closed', '', 'belt-139757_960_720', '', '', '2018-11-27 09:54:52', '2018-11-27 09:54:52', '', 76, 'http://cosmetic.test/wp-content/uploads/2018/11/belt-139757_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(78, 1, '2018-11-27 09:56:37', '2018-11-27 09:56:37', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Item 7', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'denim-jeans-jacket', '', '', '2020-03-01 07:01:23', '2020-03-01 07:01:23', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=78', 0, 'product', '', 0),
(79, 1, '2018-11-27 09:56:28', '2018-11-27 09:56:28', '', 'jeans-226422_960_720', '', 'inherit', 'open', 'closed', '', 'jeans-226422_960_720', '', '', '2018-11-27 09:56:28', '2018-11-27 09:56:28', '', 78, 'http://cosmetic.test/wp-content/uploads/2018/11/jeans-226422_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(80, 1, '2018-11-27 09:57:24', '2018-11-27 09:57:24', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Item 6', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'branded-sunglasses', '', '', '2020-03-01 07:12:37', '2020-03-01 07:12:37', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=80', 0, 'product', '', 0),
(81, 1, '2018-11-27 09:57:17', '2018-11-27 09:57:17', '', 'sunglasses-2632259_960_720', '', 'inherit', 'open', 'closed', '', 'sunglasses-2632259_960_720', '', '', '2018-11-27 09:57:17', '2018-11-27 09:57:17', '', 80, 'http://cosmetic.test/wp-content/uploads/2018/11/sunglasses-2632259_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(82, 1, '2018-11-27 09:58:21', '2018-11-27 09:58:21', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Item 5', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'hero-bicycle', '', '', '2020-03-01 06:59:48', '2020-03-01 06:59:48', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=82', 0, 'product', '', 0),
(83, 1, '2018-11-27 09:58:10', '2018-11-27 09:58:10', '', 'bicycle-1209682_960_720', '', 'inherit', 'open', 'closed', '', 'bicycle-1209682_960_720', '', '', '2018-11-27 09:58:10', '2018-11-27 09:58:10', '', 82, 'http://cosmetic.test/wp-content/uploads/2018/11/bicycle-1209682_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(84, 1, '2018-11-27 09:59:10', '2018-11-27 09:59:10', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Item 4', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'iphone-4-64gb', '', '', '2020-03-01 07:12:01', '2020-03-01 07:12:01', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=84', 0, 'product', '', 0),
(85, 1, '2018-11-27 09:59:00', '2018-11-27 09:59:00', '', 'iphone-410324_960_720', '', 'inherit', 'open', 'closed', '', 'iphone-410324_960_720', '', '', '2018-11-27 09:59:00', '2018-11-27 09:59:00', '', 84, 'http://cosmetic.test/wp-content/uploads/2018/11/iphone-410324_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(86, 1, '2018-11-27 09:59:55', '2018-11-27 09:59:55', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Make Up 3', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'canon-5300d', '', '', '2020-03-01 06:58:33', '2020-03-01 06:58:33', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=86', 0, 'product', '', 0),
(87, 1, '2018-11-27 09:59:47', '2018-11-27 09:59:47', '', 'photo-431119_960_720', '', 'inherit', 'open', 'closed', '', 'photo-431119_960_720', '', '', '2018-11-27 09:59:47', '2018-11-27 09:59:47', '', 86, 'http://cosmetic.test/wp-content/uploads/2018/11/photo-431119_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(88, 1, '2018-11-27 10:01:30', '2018-11-27 10:01:30', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Make Up 1', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'brown-shoes', '', '', '2020-03-01 06:57:41', '2020-03-01 06:57:41', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=88', 0, 'product', '', 0),
(89, 1, '2018-11-27 10:01:21', '2018-11-27 10:01:21', '', 'brown-shoes-1150071_960_720 (1)', '', 'inherit', 'open', 'closed', '', 'brown-shoes-1150071_960_720-1', '', '', '2018-11-27 10:01:21', '2018-11-27 10:01:21', '', 88, 'http://cosmetic.test/wp-content/uploads/2018/11/brown-shoes-1150071_960_720-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(90, 1, '2018-11-27 10:02:15', '2018-11-27 10:02:15', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Make Up 2', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'publish', 'open', 'closed', '', 'leather-wallet', '', '', '2020-03-01 07:11:12', '2020-03-01 07:11:12', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=90', 0, 'product', '', 0),
(91, 1, '2018-11-27 10:02:06', '2018-11-27 10:02:06', '', 'purse-494169_960_720', '', 'inherit', 'open', 'closed', '', 'purse-494169_960_720', '', '', '2018-11-27 10:02:06', '2018-11-27 10:02:06', '', 90, 'http://cosmetic.test/wp-content/uploads/2018/11/purse-494169_960_720.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `cm_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(92, 1, '2018-11-27 10:02:55', '2018-11-27 10:02:55', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n\r\nWith ultralight, quality cotton canvas, the JanSport Houston backpack is ideal for a life-on-the-go. This backpack features premium faux leather bottom and trim details, padded 15 in laptop sleeve and tricot lined tablet sleeve\r\n\r\n</div>\r\n</div>', 'Good Bye Item 2', '<div id=\"tab-description\" class=\"woocommerce-Tabs-panel woocommerce-Tabs-panel--description entry-content wc-tab\" role=\"tabpanel\" aria-labelledby=\"tab-title-description\">\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.\r\n\r\nEtiam dictumst congue a non class risus sed a. Diam adipiscing a condimentum in a nisl a maecenas libero pharetra tincidunt phasellus justo molestie bibendum. Vestibulum penatibus vestibulum lobortis vehicula euismod a platea taciti a eget in nec cum eget curabitur justo id enim mi velit at cum. Eu amet ut elit a sociis himenaeos eros nunc at pharetra magna suscipit.\r\n\r\n</div>', 'publish', 'open', 'closed', '', 'current-watch', '', '', '2020-03-01 06:55:32', '2020-03-01 06:55:32', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=92', 0, 'product', '', 0),
(93, 1, '2018-11-27 10:02:48', '2018-11-27 10:02:48', '', 'watch-1245791_960_720', '', 'inherit', 'open', 'closed', '', 'watch-1245791_960_720', '', '', '2018-11-27 10:02:48', '2018-11-27 10:02:48', '', 92, 'http://cosmetic.test/wp-content/uploads/2018/11/watch-1245791_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(94, 1, '2018-11-27 10:03:47', '2018-11-27 10:03:47', '<div id=\"tab-description\" class=\"woocommerce-Tabs-panel woocommerce-Tabs-panel--description entry-content wc-tab\" role=\"tabpanel\" aria-labelledby=\"tab-title-description\">\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.\r\n\r\nEtiam dictumst congue a non class risus sed a. Diam adipiscing a condimentum in a nisl a maecenas libero pharetra tincidunt phasellus justo molestie bibendum. Vestibulum penatibus vestibulum lobortis vehicula euismod a platea taciti a eget in nec cum eget curabitur justo id enim mi velit at cum. Eu amet ut elit a sociis himenaeos eros nunc at pharetra magna suscipit.\r\n\r\nCubilia vestibulum interdum nisl a parturient a auctor vestibulum taciti vel bibendum tempor adipiscing suspendisse posuere libero penatibus lorem at interdum tristique iaculis redosan condimentum a ac rutrum mollis consectetur. Aenean nascetur vehicula egestas a adipiscing a est egestas suspendisse parturient diam adipiscing mattis elementum velit pulvinar suscipit sagittis facilisis facilisi tortor morbi at aliquam.\r\n\r\n</div>', 'Goodby Item 1', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'brown-belt', '', '', '2020-03-01 06:54:47', '2020-03-01 06:54:47', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?post_type=product&#038;p=94', 0, 'product', '', 0),
(95, 1, '2018-11-27 10:03:40', '2018-11-27 10:03:40', '', 'menswear-952836_960_720', '', 'inherit', 'open', 'closed', '', 'menswear-952836_960_720', '', '', '2018-11-27 10:03:40', '2018-11-27 10:03:40', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/menswear-952836_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(117, 1, '2018-11-27 10:54:02', '2018-11-27 10:54:02', '[woocommerce_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart-2-2', '', '', '2018-11-27 10:54:02', '2018-11-27 10:54:02', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?page_id=117', 0, 'page', '', 0),
(119, 1, '2018-11-27 10:54:16', '2018-11-27 10:54:16', '', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout-2-2', '', '', '2018-11-27 10:54:16', '2018-11-27 10:54:16', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?page_id=119', 0, 'page', '', 0),
(121, 1, '2018-11-27 10:54:30', '2018-11-27 10:54:30', '[woocommerce_my_account]', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account-2-2', '', '', '2018-11-27 10:54:30', '2018-11-27 10:54:30', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?page_id=121', 0, 'page', '', 0),
(127, 1, '2018-11-27 11:03:33', '2018-11-27 11:03:33', '', 'payment', '', 'inherit', 'open', 'closed', '', 'payment', '', '', '2018-11-27 11:03:33', '2018-11-27 11:03:33', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/payment.png', 0, 'attachment', 'image/png', 0),
(129, 1, '2018-11-27 11:37:15', '2018-11-27 11:37:15', '[yith_wcwl_wishlist]', 'WishList', '', 'publish', 'closed', 'closed', '', 'wishlist', '', '', '2018-11-27 11:37:15', '2018-11-27 11:37:15', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?page_id=129', 0, 'page', '', 0),
(131, 1, '2018-11-28 05:23:17', '2018-11-28 05:23:17', '', 's1', '', 'inherit', 'open', 'closed', '', 's1', '', '', '2018-11-28 05:23:17', '2018-11-28 05:23:17', '', 377, 'http://cosmetic.test/wp-content/uploads/2018/11/s1.jpg', 0, 'attachment', 'image/jpeg', 0),
(132, 1, '2018-11-28 05:23:21', '2018-11-28 05:23:21', '', 's2', '', 'inherit', 'open', 'closed', '', 's2', '', '', '2018-11-28 05:23:21', '2018-11-28 05:23:21', '', 377, 'http://cosmetic.test/wp-content/uploads/2018/11/s2.jpg', 0, 'attachment', 'image/jpeg', 0),
(133, 1, '2018-11-28 05:23:23', '2018-11-28 05:23:23', '', 's3', '', 'inherit', 'open', 'closed', '', 's3', '', '', '2018-11-28 05:23:23', '2018-11-28 05:23:23', '', 377, 'http://cosmetic.test/wp-content/uploads/2018/11/s3.jpg', 0, 'attachment', 'image/jpeg', 0),
(141, 1, '2018-11-28 05:36:13', '2018-11-28 05:36:13', '', 't1', '', 'inherit', 'open', 'closed', '', 't1-2', '', '', '2018-11-28 05:36:13', '2018-11-28 05:36:13', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/t1-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(143, 1, '2018-11-28 06:00:44', '2018-11-28 06:00:44', '', 's4', '', 'inherit', 'open', 'closed', '', 's4', '', '', '2018-11-28 06:00:44', '2018-11-28 06:00:44', '', 377, 'http://cosmetic.test/wp-content/uploads/2018/11/s4.jpg', 0, 'attachment', 'image/jpeg', 0),
(144, 1, '2018-11-28 06:01:34', '2018-11-28 06:01:34', '', 't4', '', 'inherit', 'open', 'closed', '', 't4', '', '', '2018-11-28 06:01:34', '2018-11-28 06:01:34', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/t4.jpg', 0, 'attachment', 'image/jpeg', 0),
(146, 1, '2018-11-28 06:04:19', '2018-11-28 06:04:19', '', 't6', '', 'inherit', 'open', 'closed', '', 't6', '', '', '2018-11-28 06:04:19', '2018-11-28 06:04:19', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/t6.jpg', 0, 'attachment', 'image/jpeg', 0),
(149, 1, '2018-11-28 06:05:34', '2018-11-28 06:05:34', '', 't7', '', 'inherit', 'open', 'closed', '', 't7', '', '', '2018-11-28 06:05:34', '2018-11-28 06:05:34', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/t7.jpg', 0, 'attachment', 'image/jpeg', 0),
(150, 1, '2018-11-28 06:06:23', '2018-11-28 06:06:23', '', 's1', '', 'inherit', 'open', 'closed', '', 's1-2', '', '', '2018-11-28 06:06:23', '2018-11-28 06:06:23', '', 376, 'http://cosmetic.test/wp-content/uploads/2018/11/s1-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(151, 1, '2018-11-28 06:10:20', '2018-11-28 06:10:20', '', '2018-11-27', '', 'inherit', 'open', 'closed', '', '2018-11-27', '', '', '2018-11-28 06:10:20', '2018-11-28 06:10:20', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/2018-11-27.png', 0, 'attachment', 'image/png', 0),
(153, 1, '2018-11-28 06:17:22', '2018-11-28 06:17:22', '', 'home1-large-banner', '', 'inherit', 'open', 'closed', '', 'home1-large-banner', '', '', '2018-11-28 06:17:22', '2018-11-28 06:17:22', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/home1-large-banner.jpg', 0, 'attachment', 'image/jpeg', 0),
(157, 1, '2018-11-28 09:03:15', '2018-11-28 09:03:15', '', '1', '', 'inherit', 'open', 'closed', '', '1', '', '', '2018-11-28 09:03:15', '2018-11-28 09:03:15', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/1.jpg', 0, 'attachment', 'image/jpeg', 0),
(158, 1, '2018-11-28 09:03:17', '2018-11-28 09:03:17', '', '2', '', 'inherit', 'open', 'closed', '', '2', '', '', '2018-11-28 09:03:17', '2018-11-28 09:03:17', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/2.jpg', 0, 'attachment', 'image/jpeg', 0),
(159, 1, '2018-11-28 09:03:19', '2018-11-28 09:03:19', '', '4', '', 'inherit', 'open', 'closed', '', '4', '', '', '2018-11-28 09:03:19', '2018-11-28 09:03:19', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/4.jpg', 0, 'attachment', 'image/jpeg', 0),
(160, 1, '2018-11-28 09:04:17', '2018-11-28 09:04:17', '', '9', '', 'inherit', 'open', 'closed', '', '9', '', '', '2018-11-28 09:04:17', '2018-11-28 09:04:17', '', 92, 'http://cosmetic.test/wp-content/uploads/2018/11/9.jpg', 0, 'attachment', 'image/jpeg', 0),
(161, 1, '2018-11-28 09:04:19', '2018-11-28 09:04:19', '', '10', '', 'inherit', 'open', 'closed', '', '10', '', '', '2018-11-28 09:04:19', '2018-11-28 09:04:19', '', 92, 'http://cosmetic.test/wp-content/uploads/2018/11/10.jpg', 0, 'attachment', 'image/jpeg', 0),
(162, 1, '2018-11-28 09:04:21', '2018-11-28 09:04:21', '', '11', '', 'inherit', 'open', 'closed', '', '11', '', '', '2018-11-28 09:04:21', '2018-11-28 09:04:21', '', 92, 'http://cosmetic.test/wp-content/uploads/2018/11/11.jpg', 0, 'attachment', 'image/jpeg', 0),
(164, 1, '2018-11-28 09:07:10', '2018-11-28 09:07:10', '', '19', '', 'inherit', 'open', 'closed', '', '19', '', '', '2018-11-28 09:07:10', '2018-11-28 09:07:10', '', 90, 'http://cosmetic.test/wp-content/uploads/2018/11/19.jpg', 0, 'attachment', 'image/jpeg', 0),
(166, 1, '2018-11-28 09:08:20', '2018-11-28 09:08:20', '', '23', '', 'inherit', 'open', 'closed', '', '23', '', '', '2018-11-28 09:08:20', '2018-11-28 09:08:20', '', 65, 'http://cosmetic.test/wp-content/uploads/2018/11/23.jpg', 0, 'attachment', 'image/jpeg', 0),
(167, 1, '2018-11-28 09:10:13', '2018-11-28 09:10:13', '', '27-27-600x600', '', 'inherit', 'open', 'closed', '', '27-27-600x600', '', '', '2018-11-28 09:10:13', '2018-11-28 09:10:13', '', 63, 'http://cosmetic.test/wp-content/uploads/2018/11/27-27-600x600-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(168, 1, '2018-11-28 09:10:15', '2018-11-28 09:10:15', '', '27', '', 'inherit', 'open', 'closed', '', '27', '', '', '2018-11-28 09:10:15', '2018-11-28 09:10:15', '', 63, 'http://cosmetic.test/wp-content/uploads/2018/11/27.jpg', 0, 'attachment', 'image/jpeg', 0),
(170, 1, '2018-11-28 09:13:21', '2018-11-28 09:13:21', '', '25', '', 'inherit', 'open', 'closed', '', '25', '', '', '2018-11-28 09:13:21', '2018-11-28 09:13:21', '', 61, 'http://cosmetic.test/wp-content/uploads/2018/11/25.jpg', 0, 'attachment', 'image/jpeg', 0),
(171, 1, '2018-11-28 09:14:26', '2018-11-28 09:14:26', '', 'prd3', '', 'inherit', 'open', 'closed', '', 'prd3', '', '', '2018-11-28 09:14:26', '2018-11-28 09:14:26', '', 59, 'http://cosmetic.test/wp-content/uploads/2018/11/prd3.jpg', 0, 'attachment', 'image/jpeg', 0),
(172, 1, '2018-11-28 09:14:29', '2018-11-28 09:14:29', '', '26', '', 'inherit', 'open', 'closed', '', '26', '', '', '2018-11-28 09:14:29', '2018-11-28 09:14:29', '', 59, 'http://cosmetic.test/wp-content/uploads/2018/11/26.jpg', 0, 'attachment', 'image/jpeg', 0),
(174, 1, '2018-11-28 09:18:43', '2018-11-28 09:18:43', '', 'prd1', '', 'inherit', 'open', 'closed', '', 'prd1', '', '', '2018-11-28 09:18:43', '2018-11-28 09:18:43', '', 55, 'http://cosmetic.test/wp-content/uploads/2018/11/prd1.jpg', 0, 'attachment', 'image/jpeg', 0),
(175, 1, '2018-11-28 09:20:50', '2018-11-28 09:20:50', '', '21', '', 'inherit', 'open', 'closed', '', '21', '', '', '2018-11-28 09:20:50', '2018-11-28 09:20:50', '', 57, 'http://cosmetic.test/wp-content/uploads/2018/11/21.jpg', 0, 'attachment', 'image/jpeg', 0),
(176, 1, '2018-11-28 09:21:44', '2018-11-28 09:21:44', '', '81', '', 'inherit', 'open', 'closed', '', '81', '', '', '2018-11-28 09:21:44', '2018-11-28 09:21:44', '', 67, 'http://cosmetic.test/wp-content/uploads/2018/11/81.jpg', 0, 'attachment', 'image/jpeg', 0),
(177, 1, '2018-11-28 09:23:03', '2018-11-28 09:23:03', '', '1-1', '', 'inherit', 'open', 'closed', '', '1-1', '', '', '2018-11-28 09:23:03', '2018-11-28 09:23:03', '', 71, 'http://cosmetic.test/wp-content/uploads/2018/11/1-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(178, 1, '2018-11-28 09:24:25', '2018-11-28 09:24:25', '', '15', '', 'inherit', 'open', 'closed', '', '15', '', '', '2018-11-28 09:24:25', '2018-11-28 09:24:25', '', 76, 'http://cosmetic.test/wp-content/uploads/2018/11/15.jpg', 0, 'attachment', 'image/jpeg', 0),
(179, 1, '2018-11-28 09:24:29', '2018-11-28 09:24:29', '', '15h', '', 'inherit', 'open', 'closed', '', '15h', '', '', '2018-11-28 09:24:29', '2018-11-28 09:24:29', '', 76, 'http://cosmetic.test/wp-content/uploads/2018/11/15h.jpg', 0, 'attachment', 'image/jpeg', 0),
(180, 1, '2018-11-28 09:25:45', '2018-11-28 09:25:45', '', '29', '', 'inherit', 'open', 'closed', '', '29', '', '', '2018-11-28 09:25:45', '2018-11-28 09:25:45', '', 78, 'http://cosmetic.test/wp-content/uploads/2018/11/29.jpg', 0, 'attachment', 'image/jpeg', 0),
(181, 1, '2018-11-28 09:25:49', '2018-11-28 09:25:49', '', '29h-700x869', '', 'inherit', 'open', 'closed', '', '29h-700x869', '', '', '2018-11-28 09:25:49', '2018-11-28 09:25:49', '', 78, 'http://cosmetic.test/wp-content/uploads/2018/11/29h-700x869-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(182, 1, '2018-11-28 09:26:57', '2018-11-28 09:26:57', '', '18-1', '', 'inherit', 'open', 'closed', '', '18-1', '', '', '2018-11-28 09:26:57', '2018-11-28 09:26:57', '', 80, 'http://cosmetic.test/wp-content/uploads/2018/11/18-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(183, 1, '2018-11-28 09:26:59', '2018-11-28 09:26:59', '', '18h-700x869', '', 'inherit', 'open', 'closed', '', '18h-700x869', '', '', '2018-11-28 09:26:59', '2018-11-28 09:26:59', '', 80, 'http://cosmetic.test/wp-content/uploads/2018/11/18h-700x869-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(184, 1, '2018-11-28 09:40:04', '2018-11-28 09:40:04', '', 'pebbled-backpack1', '', 'inherit', 'open', 'closed', '', 'pebbled-backpack1', '', '', '2018-11-28 09:40:04', '2018-11-28 09:40:04', '', 82, 'http://cosmetic.test/wp-content/uploads/2018/11/pebbled-backpack1.jpg', 0, 'attachment', 'image/jpeg', 0),
(185, 1, '2018-11-28 09:40:07', '2018-11-28 09:40:07', '', 'pebbled-backpack3-800x778', '', 'inherit', 'open', 'closed', '', 'pebbled-backpack3-800x778', '', '', '2018-11-28 09:40:07', '2018-11-28 09:40:07', '', 82, 'http://cosmetic.test/wp-content/uploads/2018/11/pebbled-backpack3-800x778-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(186, 1, '2018-11-28 09:40:30', '2018-11-28 09:40:30', '', 'asymm1', '', 'inherit', 'open', 'closed', '', 'asymm1', '', '', '2018-11-28 09:40:30', '2018-11-28 09:40:30', '', 82, 'http://cosmetic.test/wp-content/uploads/2018/11/asymm1.jpg', 0, 'attachment', 'image/jpeg', 0),
(187, 1, '2018-11-28 09:40:33', '2018-11-28 09:40:33', '', 'asymm2-800x778', '', 'inherit', 'open', 'closed', '', 'asymm2-800x778', '', '', '2018-11-28 09:40:33', '2018-11-28 09:40:33', '', 82, 'http://cosmetic.test/wp-content/uploads/2018/11/asymm2-800x778-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(189, 1, '2018-11-28 09:42:43', '2018-11-28 09:42:43', '', '3-600x600', '', 'inherit', 'open', 'closed', '', '3-600x600', '', '', '2018-11-28 09:42:43', '2018-11-28 09:42:43', '', 86, 'http://cosmetic.test/wp-content/uploads/2018/11/3-600x600-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(190, 1, '2018-11-28 09:43:40', '2018-11-28 09:43:40', '', 'leatherbelt1', '', 'inherit', 'open', 'closed', '', 'leatherbelt1', '', '', '2018-11-28 09:43:40', '2018-11-28 09:43:40', '', 88, 'http://cosmetic.test/wp-content/uploads/2018/11/leatherbelt1.jpg', 0, 'attachment', 'image/jpeg', 0),
(191, 1, '2018-11-28 09:43:43', '2018-11-28 09:43:43', '', 'leatherbelt2-800x778', '', 'inherit', 'open', 'closed', '', 'leatherbelt2-800x778', '', '', '2018-11-28 09:43:43', '2018-11-28 09:43:43', '', 88, 'http://cosmetic.test/wp-content/uploads/2018/11/leatherbelt2-800x778-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(193, 1, '2018-11-28 09:49:39', '2018-11-28 09:49:39', '', '2018-11-27', '', 'inherit', 'open', 'closed', '', '2018-11-27-2', '', '', '2018-11-28 09:49:39', '2018-11-28 09:49:39', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/2018-11-27-1.png', 0, 'attachment', 'image/png', 0),
(204, 1, '2018-11-29 05:11:08', '2018-11-29 05:11:08', '', 'mid1', '', 'inherit', 'open', 'closed', '', 'mid1', '', '', '2018-11-29 05:11:08', '2018-11-29 05:11:08', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/mid1.png', 0, 'attachment', 'image/png', 0),
(205, 1, '2018-11-29 05:11:10', '2018-11-29 05:11:10', '', 'mid2', '', 'inherit', 'open', 'closed', '', 'mid2', '', '', '2018-11-29 05:11:10', '2018-11-29 05:11:10', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/mid2.png', 0, 'attachment', 'image/png', 0),
(206, 1, '2018-11-29 05:11:12', '2018-11-29 05:11:12', '', 'mid3', '', 'inherit', 'open', 'closed', '', 'mid3', '', '', '2018-11-29 05:11:12', '2018-11-29 05:11:12', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/mid3.png', 0, 'attachment', 'image/png', 0),
(207, 1, '2018-11-29 05:36:08', '2018-11-29 05:36:08', '', 'mid4', '', 'inherit', 'open', 'closed', '', 'mid4', '', '', '2018-11-29 05:36:08', '2018-11-29 05:36:08', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/mid4.png', 0, 'attachment', 'image/png', 0),
(208, 1, '2018-11-29 05:38:29', '2018-11-29 05:38:29', '', 'aaa (1)', '', 'inherit', 'open', 'closed', '', 'aaa-1', '', '', '2018-11-29 05:38:29', '2018-11-29 05:38:29', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/aaa-1.png', 0, 'attachment', 'image/png', 0),
(210, 1, '2018-11-29 05:52:55', '2018-11-29 05:52:55', '', 'slider223', '', 'inherit', 'open', 'closed', '', 'slider223', '', '', '2018-11-29 05:52:55', '2018-11-29 05:52:55', '', 377, 'http://cosmetic.test/wp-content/uploads/2018/11/slider223.png', 0, 'attachment', 'image/png', 0),
(211, 1, '2018-11-30 06:18:53', '2018-11-30 06:18:53', '<div id=\"pl-211\" class=\"panel-layout\">\n<div id=\"pg-211-0\" class=\"panel-grid panel-has-style\" data-style=\"{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;,&quot;row_stretch&quot;:&quot;full-stretched&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;}\" data-ratio=\"1\" data-ratio-direction=\"right\">\n<div class=\"siteorigin-panels-stretch panel-row-style panel-row-style-for-211-0\" data-stretch-type=\"full-stretched\">\n<div id=\"pgc-211-0-0\" class=\"panel-grid-cell\" data-weight=\"1\">\n<div id=\"panel-211-0-0-0\" class=\"so-panel widget widget_sow-google-map panel-first-child panel-last-child\" data-index=\"0\" data-style=\"{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;}\"></div>\n</div>\n</div>\n</div>\n<div id=\"pg-211-1\" class=\"panel-grid panel-no-style\" data-style=\"{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;}\">\n<div id=\"pgc-211-1-0\" class=\"panel-grid-cell\" data-weight=\"0.5\">\n<div id=\"panel-211-1-0-0\" class=\"so-panel widget widget_sow-editor panel-first-child panel-last-child\" data-index=\"1\" data-style=\"{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;}\">\n<div class=\"so-widget-sow-editor so-widget-sow-editor-base\">\n<div class=\"siteorigin-widget-tinymce textwidget\">\n<h2 class=\"sml-ct-get-in-touch\">GET IN TOUCH !</h2>\nAlienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis, hinc partem ei est.\n<h2 class=\"sml-ct-details\">DETAILS</h2>\n3 Wakehurst Street New York, NY 10002\n+1-202-555-0133\nsupport@fluent.com\n<h2 class=\"sml-ct-op-hours\">OPENING HOURS</h2>\nMonday – Friday 09:00 – 23:00\nSaturday 09:00 – 22:00\nSunday 12:00 – 18:00\n\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>', 'Contact Us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2020-02-23 08:14:47', '2020-02-23 08:14:47', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?page_id=211', 0, 'page', '', 0),
(222, 1, '2018-11-30 07:26:57', '2018-11-30 07:26:57', '<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>Chances are unless you are very lucky you will go thru many different relationships before you find your special someone. Then one day I had an idea, it was not an original idea.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Finding your sole mate is like gambling. In poker and blackjack you may have to play dozens of hands until you get a winning hand, and it is the same with relationships. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>Cras sit amet eros lacinia</li><li>In tincidunt gravida lectus, id ornare</li><li>Suspendisse at suscipit nunc</li></ul>\n<!-- /wp:list --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:image {\"id\":649,\"width\":499,\"height\":471} -->\n<figure class=\"wp-block-image is-resized\"><img src=\"https://demo.accesspressthemes.com/zigcy/demo-one/wp-content/uploads/sites/2/2018/11/post-img-4.jpg\" alt=\"\" class=\"wp-image-649\" width=\"499\" height=\"471\"/></figure>\n<!-- /wp:image --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>Myspace Layouts The Missing Element</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Then one day I decided I had enough of trying to go to clubs and bars to meet people. I was sick and tired of trying to find someone in the time it takes to finish a drink. It always seemed the ladies I would meet were all wrong for me, or they seemed great after talking to them for 5 minutes but they seemed to have no interest in me.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Then one day I had an idea, it was not an original idea but I decided to use the internet to try to find the right person for me. So I proceeded to make a myspace profile. On this page I tried to put the real me and not the funny guy trying to be charming that was looking for love at the bar.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":706} -->\n<figure class=\"wp-block-image\"><img src=\"https://demo.accesspressthemes.com/zigcy/demo-one/wp-content/uploads/sites/2/2018/11/blog-ime4-1024x507.jpg\" alt=\"\" class=\"wp-image-706\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Luxury is something everyone deserves from time to time. Such an indulgence can make a vacation a truly rejuvenating experience. One of the best ways to get the luxury of the rich and famous to fit into your budget can be yours through yacht charter companies. These companies specialize in creating custom sailing vacations that redefine travel.&nbsp;</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>With your budget in mind, it is easy to plan a chartered yacht vacation. Companies often have a fleet of sailing vessels that can accommodate parties of various sizes. You may want to make it a more intimate trip with only close family. There are charters that can be rented for as few as two people. These include either a sailboat or motorboat and can come with or without a crew and captain to sail the ship for you. If you choose not to hire a crew, you will have to show that you are knowledgeable of sailing and can handle the ship competently.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p> “Theme is very flexible and easy to use. Perfect for us. Customer support has been excellent and answered every question we’ve thrown at them with 12 hours.”<br></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>The next part of planning is to determine your starting and ending ports. This could be a place close to home and sail in one area or start and finish at two different ports. Generally, starting and stopping in the same port will save you money and is usually more convenient. You can also fly to a destination far from home and then sail another exotic sea. There are luxury yacht charter companies that cruise the Caribbean and Mediterranean seas or around Alaska, the Panama Canal, or anyplace you can imagine.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Determining the type of cruise is another aspect of planning a chartered yachting trip. You can have as little or many crew members as the ship will hold. A captain takes all the worries out of navigating and onboard housekeeping services make it a real vacation that rivals the finest hotel services. You can also choose to have a chef and service crew as part of your vacation package.<br></p>\n<!-- /wp:paragraph -->', 'Best fashion trends for women', '', 'publish', 'open', 'open', '', 'latest-styling', '', '', '2018-11-30 07:26:57', '2018-11-30 07:26:57', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?p=222', 0, 'post', '', 0),
(223, 1, '2018-11-30 07:26:49', '2018-11-30 07:26:49', '', 'portrait-1130391_1920', '', 'inherit', 'open', 'closed', '', 'portrait-1130391_1920', '', '', '2018-11-30 07:26:49', '2018-11-30 07:26:49', '', 222, 'http://cosmetic.test/wp-content/uploads/2018/11/portrait-1130391_1920.jpg', 0, 'attachment', 'image/jpeg', 0),
(225, 1, '2018-11-30 07:28:40', '2018-11-30 07:28:40', '<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>Chances are unless you are very lucky you will go thru many different relationships before you find your special someone. Then one day I had an idea, it was not an original idea.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Finding your sole mate is like gambling. In poker and blackjack you may have to play dozens of hands until you get a winning hand, and it is the same with relationships. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>Cras sit amet eros lacinia</li><li>In tincidunt gravida lectus, id ornare</li><li>Suspendisse at suscipit nunc</li></ul>\n<!-- /wp:list --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:image {\"id\":649,\"width\":499,\"height\":471} -->\n<figure class=\"wp-block-image is-resized\"><img src=\"https://demo.accesspressthemes.com/zigcy/demo-one/wp-content/uploads/sites/2/2018/11/post-img-4.jpg\" alt=\"\" class=\"wp-image-649\" width=\"499\" height=\"471\"/></figure>\n<!-- /wp:image --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>Myspace Layouts The Missing Element</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Then one day I decided I had enough of trying to go to clubs and bars to meet people. I was sick and tired of trying to find someone in the time it takes to finish a drink. It always seemed the ladies I would meet were all wrong for me, or they seemed great after talking to them for 5 minutes but they seemed to have no interest in me.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Then one day I had an idea, it was not an original idea but I decided to use the internet to try to find the right person for me. So I proceeded to make a myspace profile. On this page I tried to put the real me and not the funny guy trying to be charming that was looking for love at the bar.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":706} -->\n<figure class=\"wp-block-image\"><img src=\"https://demo.accesspressthemes.com/zigcy/demo-one/wp-content/uploads/sites/2/2018/11/blog-ime4-1024x507.jpg\" alt=\"\" class=\"wp-image-706\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Luxury is something everyone deserves from time to time. Such an indulgence can make a vacation a truly rejuvenating experience. One of the best ways to get the luxury of the rich and famous to fit into your budget can be yours through yacht charter companies. These companies specialize in creating custom sailing vacations that redefine travel.&nbsp;</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>With your budget in mind, it is easy to plan a chartered yacht vacation. Companies often have a fleet of sailing vessels that can accommodate parties of various sizes. You may want to make it a more intimate trip with only close family. There are charters that can be rented for as few as two people. These include either a sailboat or motorboat and can come with or without a crew and captain to sail the ship for you. If you choose not to hire a crew, you will have to show that you are knowledgeable of sailing and can handle the ship competently.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p> “Theme is very flexible and easy to use. Perfect for us. Customer support has been excellent and answered every question we’ve thrown at them with 12 hours.”<br></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>The next part of planning is to determine your starting and ending ports. This could be a place close to home and sail in one area or start and finish at two different ports. Generally, starting and stopping in the same port will save you money and is usually more convenient. You can also fly to a destination far from home and then sail another exotic sea. There are luxury yacht charter companies that cruise the Caribbean and Mediterranean seas or around Alaska, the Panama Canal, or anyplace you can imagine.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Determining the type of cruise is another aspect of planning a chartered yachting trip. You can have as little or many crew members as the ship will hold. A captain takes all the worries out of navigating and onboard housekeeping services make it a real vacation that rivals the finest hotel services. You can also choose to have a chef and service crew as part of your vacation package.<br></p>\n<!-- /wp:paragraph -->', 'Current Styling Trend', '', 'publish', 'open', 'open', '', 'current-styling-trend', '', '', '2018-11-30 07:28:40', '2018-11-30 07:28:40', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?p=225', 0, 'post', '', 0),
(226, 1, '2018-11-30 07:27:58', '2018-11-30 07:27:58', '', 'slider1 (1)', '', 'inherit', 'open', 'closed', '', 'slider1-1', '', '', '2018-11-30 07:27:58', '2018-11-30 07:27:58', '', 225, 'http://cosmetic.test/wp-content/uploads/2018/11/slider1-1.png', 0, 'attachment', 'image/png', 0),
(231, 1, '2018-12-03 10:42:51', '2018-12-03 10:42:51', '', 'cropped-watch-1245791_960_720.jpg', '', 'inherit', 'open', 'closed', '', 'cropped-watch-1245791_960_720-jpg', '', '', '2018-12-03 10:42:51', '2018-12-03 10:42:51', '', 0, 'http://cosmetic.test/wp-content/uploads/2018/11/cropped-watch-1245791_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(233, 1, '2019-01-17 10:44:26', '2019-01-17 10:44:26', '', '2019-01-17', '', 'inherit', 'open', 'closed', '', '2019-01-17', '', '', '2019-01-17 10:44:26', '2019-01-17 10:44:26', '', 0, 'http://cosmetic.test/wp-content/uploads/2019/01/2019-01-17.png', 0, 'attachment', 'image/png', 0),
(235, 1, '2019-01-17 10:56:39', '2019-01-17 10:56:39', '', '2019-01-17 (1)', '', 'inherit', 'open', 'closed', '', '2019-01-17-1', '', '', '2019-01-17 10:56:39', '2019-01-17 10:56:39', '', 377, 'http://cosmetic.test/wp-content/uploads/2018/11/2019-01-17-1.png', 0, 'attachment', 'image/png', 0),
(237, 1, '2019-01-17 11:56:39', '2019-01-17 11:56:39', '', '2019-01-17 (2)', '', 'inherit', 'open', 'closed', '', '2019-01-17-2', '', '', '2019-01-17 11:56:39', '2019-01-17 11:56:39', '', 0, 'http://cosmetic.test/wp-content/uploads/2019/01/2019-01-17-2.png', 0, 'attachment', 'image/png', 0),
(238, 1, '2019-01-17 11:57:17', '2019-01-17 11:57:17', '', '2019-01-17 (3)', '', 'inherit', 'open', 'closed', '', '2019-01-17-3', '', '', '2019-01-17 11:57:17', '2019-01-17 11:57:17', '', 0, 'http://cosmetic.test/wp-content/uploads/2019/01/2019-01-17-3.png', 0, 'attachment', 'image/png', 0),
(239, 1, '2019-01-17 11:57:32', '2019-01-17 11:57:32', '', '2019-01-17 (4)', '', 'inherit', 'open', 'closed', '', '2019-01-17-4', '', '', '2019-01-17 11:57:32', '2019-01-17 11:57:32', '', 0, 'http://cosmetic.test/wp-content/uploads/2019/01/2019-01-17-4.png', 0, 'attachment', 'image/png', 0),
(243, 1, '2019-01-29 04:48:43', '2019-01-29 04:48:43', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://demo.accesspressthemes.com/zigcy-lite/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page-2', '', '', '2019-01-29 04:48:43', '2019-01-29 04:48:43', '', 0, 'http://demo.accesspressthemes.com/zigcy-lite/?page_id=2', 0, 'page', '', 0),
(244, 1, '2019-01-29 04:48:43', '2019-01-29 04:48:43', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://demo.accesspressthemes.com/zigcy-lite.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2019-01-29 04:48:43', '2019-01-29 04:48:43', '', 0, 'http://demo.accesspressthemes.com/zigcy-lite/?page_id=3', 0, 'page', '', 0),
(245, 1, '2018-11-27 07:07:22', '2018-11-27 07:07:22', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://demo.accesspressthemes.com/store-mart-lite/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page-2-2', '', '', '2018-11-27 07:07:22', '2018-11-27 07:07:22', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?page_id=2', 0, 'page', '', 0),
(246, 1, '2018-11-27 07:07:22', '2018-11-27 07:07:22', '<h2>Who we are</h2><p>Our website address is: http://demo.accesspressthemes.com/store-mart-lite.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2018-11-27 07:07:22', '2018-11-27 07:07:22', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?page_id=3', 0, 'page', '', 0),
(253, 1, '2019-01-29 04:48:43', '2019-01-29 04:48:43', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world-2__trashed', '', '', '2020-02-23 07:52:54', '2020-02-23 07:52:54', '', 0, 'http://demo.accesspressthemes.com/zigcy-lite/?p=1', 0, 'post', '', 0),
(254, 1, '2018-11-27 07:07:22', '2018-11-27 07:07:22', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world-2-2__trashed', '', '', '2020-02-23 07:53:01', '2020-02-23 07:53:01', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?p=1', 0, 'post', '', 0),
(272, 1, '2019-03-08 12:43:57', '2019-03-08 12:43:57', '', 'logo-1', '', 'publish', 'open', 'open', '', 'logo-1', '', '', '2019-03-08 12:43:57', '2019-03-08 12:43:57', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?p=272', 0, 'post', '', 0),
(273, 1, '2019-03-08 12:43:45', '2019-03-08 12:43:45', '', 'clients-logo-1', '', 'inherit', 'open', 'closed', '', 'clients-logo-1', '', '', '2019-03-08 12:43:45', '2019-03-08 12:43:45', '', 272, 'http://cosmetic.test/wp-content/uploads/2019/03/clients-logo-1.png', 0, 'attachment', 'image/png', 0),
(274, 1, '2019-03-08 12:43:47', '2019-03-08 12:43:47', '', 'clients-logo-2', '', 'inherit', 'open', 'closed', '', 'clients-logo-2', '', '', '2019-03-08 12:43:47', '2019-03-08 12:43:47', '', 272, 'http://cosmetic.test/wp-content/uploads/2019/03/clients-logo-2.png', 0, 'attachment', 'image/png', 0),
(275, 1, '2019-03-08 12:43:49', '2019-03-08 12:43:49', '', 'clients-logo-3', '', 'inherit', 'open', 'closed', '', 'clients-logo-3', '', '', '2019-03-08 12:43:49', '2019-03-08 12:43:49', '', 272, 'http://cosmetic.test/wp-content/uploads/2019/03/clients-logo-3.png', 0, 'attachment', 'image/png', 0),
(276, 1, '2019-03-08 12:43:51', '2019-03-08 12:43:51', '', 'clients-logo-4', '', 'inherit', 'open', 'closed', '', 'clients-logo-4', '', '', '2019-03-08 12:43:51', '2019-03-08 12:43:51', '', 272, 'http://cosmetic.test/wp-content/uploads/2019/03/clients-logo-4.png', 0, 'attachment', 'image/png', 0),
(277, 1, '2019-03-08 12:43:53', '2019-03-08 12:43:53', '', 'clients-logo-5', '', 'inherit', 'open', 'closed', '', 'clients-logo-5', '', '', '2019-03-08 12:43:53', '2019-03-08 12:43:53', '', 272, 'http://cosmetic.test/wp-content/uploads/2019/03/clients-logo-5.png', 0, 'attachment', 'image/png', 0),
(278, 1, '2019-03-08 12:43:54', '2019-03-08 12:43:54', '', 'clients-logo-6', '', 'inherit', 'open', 'closed', '', 'clients-logo-6', '', '', '2019-03-08 12:43:54', '2019-03-08 12:43:54', '', 272, 'http://cosmetic.test/wp-content/uploads/2019/03/clients-logo-6.png', 0, 'attachment', 'image/png', 0),
(279, 1, '2019-03-08 12:43:56', '2019-03-08 12:43:56', '', 'clients-logo-7', '', 'inherit', 'open', 'closed', '', 'clients-logo-7', '', '', '2019-03-08 12:43:56', '2019-03-08 12:43:56', '', 272, 'http://cosmetic.test/wp-content/uploads/2019/03/clients-logo-7.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `cm_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(281, 1, '2019-03-08 12:51:11', '2019-03-08 12:51:11', '', 'logo-2', '', 'publish', 'open', 'open', '', 'logo-2', '', '', '2019-03-08 12:51:11', '2019-03-08 12:51:11', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?p=281', 0, 'post', '', 0),
(284, 1, '2019-03-08 12:51:41', '2019-03-08 12:51:41', '', 'logo-3', '', 'publish', 'open', 'open', '', 'logo-3', '', '', '2019-03-08 12:51:41', '2019-03-08 12:51:41', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?p=284', 0, 'post', '', 0),
(287, 1, '2019-03-08 12:52:14', '2019-03-08 12:52:14', '', 'logo-4', '', 'publish', 'open', 'open', '', 'logo-4', '', '', '2019-03-08 12:52:14', '2019-03-08 12:52:14', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?p=287', 0, 'post', '', 0),
(289, 1, '2019-03-08 12:52:47', '2019-03-08 12:52:47', '', 'logo-5', '', 'publish', 'open', 'open', '', 'logo-5', '', '', '2019-03-08 12:52:47', '2019-03-08 12:52:47', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?p=289', 0, 'post', '', 0),
(290, 1, '2019-03-08 12:53:06', '2019-03-08 12:53:06', '', 'logo-6', '', 'publish', 'open', 'open', '', 'logo-6', '', '', '2019-03-08 12:53:06', '2019-03-08 12:53:06', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?p=290', 0, 'post', '', 0),
(293, 1, '2019-03-08 12:53:29', '2019-03-08 12:53:29', '', 'logo-8', '', 'publish', 'open', 'open', '', 'logo-8', '', '', '2019-03-08 12:53:29', '2019-03-08 12:53:29', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?p=293', 0, 'post', '', 0),
(302, 1, '2019-03-10 09:32:30', '2019-03-10 09:32:30', '<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>Chances are unless you are very lucky you will go thru many different relationships before you find your special someone. Then one day I had an idea, it was not an original idea.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Finding your sole mate is like gambling. In poker and blackjack you may have to play dozens of hands until you get a winning hand, and it is the same with relationships. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>Cras sit amet eros lacinia</li><li>In tincidunt gravida lectus, id ornare</li><li>Suspendisse at suscipit nunc</li></ul>\n<!-- /wp:list --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:image {\"id\":649,\"width\":499,\"height\":471} -->\n<figure class=\"wp-block-image is-resized\"><img src=\"https://demo.accesspressthemes.com/zigcy/demo-one/wp-content/uploads/sites/2/2018/11/post-img-4.jpg\" alt=\"\" class=\"wp-image-649\" width=\"499\" height=\"471\"/></figure>\n<!-- /wp:image --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>Myspace Layouts The Missing Element</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Then one day I decided I had enough of trying to go to clubs and bars to meet people. I was sick and tired of trying to find someone in the time it takes to finish a drink. It always seemed the ladies I would meet were all wrong for me, or they seemed great after talking to them for 5 minutes but they seemed to have no interest in me.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Then one day I had an idea, it was not an original idea but I decided to use the internet to try to find the right person for me. So I proceeded to make a myspace profile. On this page I tried to put the real me and not the funny guy trying to be charming that was looking for love at the bar.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":706} -->\n<figure class=\"wp-block-image\"><img src=\"https://demo.accesspressthemes.com/zigcy/demo-one/wp-content/uploads/sites/2/2018/11/blog-ime4-1024x507.jpg\" alt=\"\" class=\"wp-image-706\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Luxury is something everyone deserves from time to time. Such an indulgence can make a vacation a truly rejuvenating experience. One of the best ways to get the luxury of the rich and famous to fit into your budget can be yours through yacht charter companies. These companies specialize in creating custom sailing vacations that redefine travel.&nbsp;</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>With your budget in mind, it is easy to plan a chartered yacht vacation. Companies often have a fleet of sailing vessels that can accommodate parties of various sizes. You may want to make it a more intimate trip with only close family. There are charters that can be rented for as few as two people. These include either a sailboat or motorboat and can come with or without a crew and captain to sail the ship for you. If you choose not to hire a crew, you will have to show that you are knowledgeable of sailing and can handle the ship competently.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p> “Theme is very flexible and easy to use. Perfect for us. Customer support has been excellent and answered every question we’ve thrown at them with 12 hours.”<br></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>The next part of planning is to determine your starting and ending ports. This could be a place close to home and sail in one area or start and finish at two different ports. Generally, starting and stopping in the same port will save you money and is usually more convenient. You can also fly to a destination far from home and then sail another exotic sea. There are luxury yacht charter companies that cruise the Caribbean and Mediterranean seas or around Alaska, the Panama Canal, or anyplace you can imagine.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Determining the type of cruise is another aspect of planning a chartered yachting trip. You can have as little or many crew members as the ship will hold. A captain takes all the worries out of navigating and onboard housekeeping services make it a real vacation that rivals the finest hotel services. You can also choose to have a chef and service crew as part of your vacation package.<br></p>\n<!-- /wp:paragraph -->', 'Spring summer fashion trends', '', 'publish', 'open', 'open', '', 'spring-summer-fashion-trends', '', '', '2019-03-10 09:32:30', '2019-03-10 09:32:30', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?p=302', 0, 'post', '', 0),
(303, 1, '2019-03-10 09:32:17', '2019-03-10 09:32:17', '', 'amanda-vick-1404811-unsplash (1)', '', 'inherit', 'open', 'closed', '', 'amanda-vick-1404811-unsplash-1', '', '', '2019-03-10 09:32:17', '2019-03-10 09:32:17', '', 302, 'http://cosmetic.test/wp-content/uploads/2019/03/amanda-vick-1404811-unsplash-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(313, 1, '2019-03-11 07:22:50', '2019-03-11 07:22:50', '<!-- wp:paragraph -->\n<p><strong>JM Cosmetics are licensed.and approved in the Philippines by <a rel=\"noreferrer noopener\" href=\"https://en.m.wikipedia.org/wiki/Food_and_Drug_Administration\" target=\"_blank\">Food and Drug Administration</a> (FDA), which regulates cosmetics, they defined our product as \"intended to be applied to the human body for cleansing, beautifying, promoting attractiveness, or altering the appearance without affecting the body\'s structure or functions\".It includes material intended for use as an ingredients of a cosmetic products </strong></p>\n<!-- /wp:paragraph -->', 'About My Cosmetics', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2020-02-29 13:37:48', '2020-02-29 13:37:48', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?page_id=313', 0, 'page', '', 0),
(315, 1, '2019-03-11 07:24:23', '2019-03-11 07:24:23', '', 'Shopping Guide', '', 'publish', 'closed', 'closed', '', 'shopping-guide', '', '', '2019-03-11 07:24:23', '2019-03-11 07:24:23', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?page_id=315', 0, 'page', '', 0),
(317, 1, '2019-03-11 07:24:44', '2019-03-11 07:24:44', '', 'Delivery Information', '', 'publish', 'closed', 'closed', '', 'delivery-information', '', '', '2019-03-11 07:24:44', '2019-03-11 07:24:44', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?page_id=317', 0, 'page', '', 0),
(328, 1, '2019-03-11 07:38:51', '2019-03-11 07:38:51', '', 'gajaltae-kte', '', 'inherit', 'open', 'closed', '', 'gajaltae-kte', '', '', '2019-03-11 07:38:51', '2019-03-11 07:38:51', '', 0, 'http://cosmetic.test/wp-content/uploads/2019/03/gajaltae-kte.png', 0, 'attachment', 'image/png', 0),
(329, 1, '2019-03-11 07:39:30', '2019-03-11 07:39:30', '', 'abag', '', 'inherit', 'open', 'closed', '', 'abag', '', '', '2019-03-11 07:39:30', '2019-03-11 07:39:30', '', 0, 'http://cosmetic.test/wp-content/uploads/2019/03/abag.png', 0, 'attachment', 'image/png', 0),
(330, 1, '2019-03-11 07:40:06', '2019-03-11 07:40:06', '', 'goraman', '', 'inherit', 'open', 'closed', '', 'goraman', '', '', '2019-03-11 07:40:06', '2019-03-11 07:40:06', '', 0, 'http://cosmetic.test/wp-content/uploads/2019/03/goraman.png', 0, 'attachment', 'image/png', 0),
(337, 1, '2019-03-11 12:27:29', '2019-03-11 12:27:29', '', '2019-03-11 (2)', '', 'inherit', 'open', 'closed', '', '2019-03-11-2', '', '', '2019-03-11 12:27:29', '2019-03-11 12:27:29', '', 0, 'http://cosmetic.test/wp-content/uploads/2019/03/2019-03-11-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(338, 1, '2019-03-11 12:27:40', '2019-03-11 12:27:40', '', '2019-03-11 (1)', '', 'inherit', 'open', 'closed', '', '2019-03-11-1', '', '', '2019-03-11 12:27:40', '2019-03-11 12:27:40', '', 0, 'http://cosmetic.test/wp-content/uploads/2019/03/2019-03-11-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(339, 1, '2019-03-11 12:28:00', '2019-03-11 12:28:00', '', '2019-03-11', '', 'inherit', 'open', 'closed', '', '2019-03-11', '', '', '2019-03-11 12:28:00', '2019-03-11 12:28:00', '', 0, 'http://cosmetic.test/wp-content/uploads/2019/03/2019-03-11.jpg', 0, 'attachment', 'image/jpeg', 0),
(344, 1, '2019-03-12 09:48:31', '2019-03-12 09:48:31', '', '2019-03-12 (2)', '', 'inherit', 'open', 'closed', '', '2019-03-12-2', '', '', '2019-03-12 09:48:31', '2019-03-12 09:48:31', '', 0, 'http://cosmetic.test/wp-content/uploads/2019/03/2019-03-12-2.png', 0, 'attachment', 'image/png', 0),
(350, 1, '2019-03-22 12:31:58', '2019-03-22 12:31:58', '', '2019-03-22', '', 'inherit', 'open', 'closed', '', '2019-03-22', '', '', '2019-03-22 12:31:58', '2019-03-22 12:31:58', '', 376, 'http://cosmetic.test/wp-content/uploads/2018/11/2019-03-22.jpg', 0, 'attachment', 'image/jpeg', 0),
(351, 1, '2019-03-22 12:32:06', '2019-03-22 12:32:06', '', '2019-03-22 (1)', '', 'inherit', 'open', 'closed', '', '2019-03-22-1', '', '', '2019-03-22 12:32:06', '2019-03-22 12:32:06', '', 376, 'http://cosmetic.test/wp-content/uploads/2018/11/2019-03-22-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(366, 1, '2019-05-19 10:50:24', '2019-05-19 10:50:24', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder-2', '', '', '2019-05-19 10:50:24', '2019-05-19 10:50:24', '', 0, 'http://cosmetic.test/wp-content/uploads/2019/05/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(367, 1, '2020-02-23 07:47:23', '2020-02-23 07:47:23', '', 'Furniture', '', 'publish', 'closed', 'closed', '', 'furniture', '', '', '2020-02-23 07:47:23', '2020-02-23 07:47:23', '', 0, 'http://cosmetic.test/2020/02/23/furniture/', 2, 'nav_menu_item', '', 0),
(368, 1, '2020-02-23 07:47:23', '2020-02-23 07:47:23', '', 'Electronics', '', 'publish', 'closed', 'closed', '', 'electronics', '', '', '2020-02-23 07:47:23', '2020-02-23 07:47:23', '', 0, 'http://cosmetic.test/2020/02/23/electronics/', 3, 'nav_menu_item', '', 0),
(369, 1, '2020-02-23 07:47:23', '2020-02-23 07:47:23', '', 'About us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2020-02-23 07:47:23', '2020-02-23 07:47:23', '', 0, 'http://cosmetic.test/2020/02/23/about-us/', 1, 'nav_menu_item', '', 0),
(370, 1, '2020-02-23 07:47:23', '2020-02-23 07:47:23', '', 'Shipping Guide', '', 'publish', 'closed', 'closed', '', 'shipping-guide', '', '', '2020-02-23 07:47:23', '2020-02-23 07:47:23', '', 0, 'http://cosmetic.test/2020/02/23/shipping-guide/', 2, 'nav_menu_item', '', 0),
(371, 1, '2020-02-23 07:47:23', '2020-02-23 07:47:23', '', 'Delivery Information', '', 'publish', 'closed', 'closed', '', 'delivery-information', '', '', '2020-02-23 07:47:23', '2020-02-23 07:47:23', '', 0, 'http://cosmetic.test/2020/02/23/delivery-information/', 3, 'nav_menu_item', '', 0),
(372, 1, '2020-02-23 07:47:23', '2020-02-23 07:47:23', '', 'Privacy Policy', '', 'publish', 'closed', 'closed', '', 'privacy-policy', '', '', '2020-02-23 07:47:23', '2020-02-23 07:47:23', '', 0, 'http://cosmetic.test/2020/02/23/privacy-policy/', 4, 'nav_menu_item', '', 0),
(373, 1, '2019-01-29 05:12:46', '2019-01-29 05:12:46', '[woocommerce_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart-2', '', '', '2019-01-29 05:12:46', '2019-01-29 05:12:46', '', 0, 'http://demo.accesspressthemes.com/zigcy-lite/cart/', 0, 'page', '', 0),
(374, 1, '2019-01-29 05:12:46', '2019-01-29 05:12:46', '[woocommerce_checkout]', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout-2', '', '', '2019-01-29 05:12:46', '2019-01-29 05:12:46', '', 0, 'http://demo.accesspressthemes.com/zigcy-lite/checkout/', 0, 'page', '', 0),
(375, 1, '2019-01-29 05:12:46', '2019-01-29 05:12:46', '[woocommerce_my_account]', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account-2', '', '', '2019-01-29 05:12:46', '2019-01-29 05:12:46', '', 0, 'http://demo.accesspressthemes.com/zigcy-lite/my-account/', 0, 'page', '', 0),
(376, 1, '2018-11-27 07:24:17', '2018-11-27 07:24:17', 'Lookbook Cosmetics 2019', 'Street Life Cosmetics', '', 'publish', 'open', 'open', '', 'street-life-fashion', '', '', '2020-03-01 06:32:41', '2020-03-01 06:32:41', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?p=10', 0, 'post', '', 0),
(377, 1, '2018-11-27 07:25:03', '2018-11-27 07:25:03', '<div class=\"subtitle\">Clothing Trend 2018</div>', 'Sale Offer 20% Off This Week', '', 'publish', 'open', 'open', '', 'currnet-styling-trend', '', '', '2020-03-01 06:37:17', '2020-03-01 06:37:17', '', 0, 'http://demo.accesspressthemes.com/store-mart-lite/?p=13', 0, 'post', '', 0),
(378, 1, '2019-05-31 08:37:18', '2019-05-31 08:37:18', '', 'shop', '', 'publish', 'closed', 'closed', '', 'shop-2-2', '', '', '2019-05-31 08:37:18', '2019-05-31 08:37:18', '', 0, 'https://demo.accesspressthemes.com/zigcy-lite/demo-one/?page_id=370', 0, 'page', '', 0),
(379, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '379', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/379/', 1, 'nav_menu_item', '', 0),
(380, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '380', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/380/', 7, 'nav_menu_item', '', 0),
(381, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '381', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/381/', 6, 'nav_menu_item', '', 0),
(382, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '382', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/382/', 5, 'nav_menu_item', '', 0),
(383, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '383', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/383/', 9, 'nav_menu_item', '', 0),
(385, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '385', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/385/', 4, 'nav_menu_item', '', 0),
(386, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '386', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/386/', 1, 'nav_menu_item', '', 0),
(387, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '387', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/387/', 2, 'nav_menu_item', '', 0),
(388, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '388', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/388/', 3, 'nav_menu_item', '', 0),
(389, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '389', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/389/', 4, 'nav_menu_item', '', 0),
(390, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '390', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/390/', 4, 'nav_menu_item', '', 0),
(391, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '391', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/391/', 3, 'nav_menu_item', '', 0),
(392, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '392', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/392/', 2, 'nav_menu_item', '', 0),
(393, 1, '2020-02-23 07:47:34', '2020-02-23 07:47:34', ' ', '', '', 'publish', 'closed', 'closed', '', '393', '', '', '2020-02-23 07:47:34', '2020-02-23 07:47:34', '', 0, 'http://cosmetic.test/2020/02/23/393/', 1, 'nav_menu_item', '', 0),
(394, 1, '2020-02-23 07:52:54', '2020-02-23 07:52:54', 'Welcome to <a href=\"http://demo.accesspressthemes.com/zigcy-lite/\">Zigcy Lite </a>. This is your first post. Edit or delete it, then start blogging!', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-02-23 07:52:54', '2020-02-23 07:52:54', '', 1, 'http://cosmetic.test/2020/02/23/1-revision-v1/', 0, 'revision', '', 0),
(395, 1, '2020-02-23 07:52:54', '2020-02-23 07:52:54', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '253-revision-v1', '', '', '2020-02-23 07:52:54', '2020-02-23 07:52:54', '', 253, 'http://cosmetic.test/2020/02/23/253-revision-v1/', 0, 'revision', '', 0),
(396, 1, '2020-02-23 07:53:01', '2020-02-23 07:53:01', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '254-revision-v1', '', '', '2020-02-23 07:53:01', '2020-02-23 07:53:01', '', 254, 'http://cosmetic.test/2020/02/23/254-revision-v1/', 0, 'revision', '', 0),
(397, 1, '2020-02-23 08:14:47', '2020-02-23 08:14:47', '<div id=\"pl-211\" class=\"panel-layout\">\n<div id=\"pg-211-0\" class=\"panel-grid panel-has-style\" data-style=\"{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;,&quot;row_stretch&quot;:&quot;full-stretched&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;}\" data-ratio=\"1\" data-ratio-direction=\"right\">\n<div class=\"siteorigin-panels-stretch panel-row-style panel-row-style-for-211-0\" data-stretch-type=\"full-stretched\">\n<div id=\"pgc-211-0-0\" class=\"panel-grid-cell\" data-weight=\"1\">\n<div id=\"panel-211-0-0-0\" class=\"so-panel widget widget_sow-google-map panel-first-child panel-last-child\" data-index=\"0\" data-style=\"{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;}\"></div>\n</div>\n</div>\n</div>\n<div id=\"pg-211-1\" class=\"panel-grid panel-no-style\" data-style=\"{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;}\">\n<div id=\"pgc-211-1-0\" class=\"panel-grid-cell\" data-weight=\"0.5\">\n<div id=\"panel-211-1-0-0\" class=\"so-panel widget widget_sow-editor panel-first-child panel-last-child\" data-index=\"1\" data-style=\"{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;}\">\n<div class=\"so-widget-sow-editor so-widget-sow-editor-base\">\n<div class=\"siteorigin-widget-tinymce textwidget\">\n<h2 class=\"sml-ct-get-in-touch\">GET IN TOUCH !</h2>\nAlienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis, hinc partem ei est.\n<h2 class=\"sml-ct-details\">DETAILS</h2>\n3 Wakehurst Street New York, NY 10002\n+1-202-555-0133\nsupport@fluent.com\n<h2 class=\"sml-ct-op-hours\">OPENING HOURS</h2>\nMonday – Friday 09:00 – 23:00\nSaturday 09:00 – 22:00\nSunday 12:00 – 18:00\n\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '211-revision-v1', '', '', '2020-02-23 08:14:47', '2020-02-23 08:14:47', '', 211, 'http://cosmetic.test/2020/02/23/211-revision-v1/', 0, 'revision', '', 0),
(398, 1, '2020-02-29 13:33:14', '2020-02-29 13:33:14', '{\n    \"nav_menu_item[384]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 5,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://cosmetic.test/shop-2/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"/shop/\",\n            \"nav_menu_term_id\": 31,\n            \"position\": 8,\n            \"status\": \"publish\",\n            \"original_title\": \"Shop\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:33:07\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '6604d80c-ae6d-4191-a207-29b21a4ac0f6', '', '', '2020-02-29 13:33:14', '2020-02-29 13:33:14', '', 0, 'http://cosmetic.test/?p=398', 0, 'customize_changeset', '', 0),
(399, 1, '2020-02-29 13:34:19', '2020-02-29 13:34:19', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2020-02-29 13:34:19', '2020-02-29 13:34:19', '', 0, 'http://cosmetic.test/wp-content/uploads/2020/02/logo.jpg', 0, 'attachment', 'image/jpeg', 0),
(400, 1, '2020-02-29 13:36:15', '2020-02-29 13:36:15', '{\n    \"zigcy-cosmetics::custom_logo\": {\n        \"value\": 401,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:36:15\"\n    },\n    \"nav_menu_item[384]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 5,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://cosmetic.test/shop-2/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 31,\n            \"position\": 8,\n            \"status\": \"publish\",\n            \"original_title\": \"Shop\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:35:37\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'd5762208-381e-4d3c-b027-9731dd017f75', '', '', '2020-02-29 13:36:15', '2020-02-29 13:36:15', '', 0, 'http://cosmetic.test/?p=400', 0, 'customize_changeset', '', 0),
(401, 1, '2020-02-29 13:36:11', '2020-02-29 13:36:11', 'http://cosmetic.test/wp-content/uploads/2020/02/cropped-logo.jpg', 'cropped-logo.jpg', '', 'inherit', 'open', 'closed', '', 'cropped-logo-jpg', '', '', '2020-02-29 13:36:11', '2020-02-29 13:36:11', '', 0, 'http://cosmetic.test/wp-content/uploads/2020/02/cropped-logo.jpg', 0, 'attachment', 'image/jpeg', 0),
(402, 1, '2020-02-29 13:37:48', '2020-02-29 13:37:48', '<!-- wp:paragraph -->\n<p><strong>JM Cosmetics are licensed.and approved in the Philippines by <a rel=\"noreferrer noopener\" href=\"https://en.m.wikipedia.org/wiki/Food_and_Drug_Administration\" target=\"_blank\">Food and Drug Administration</a> (FDA), which regulates cosmetics, they defined our product as \"intended to be applied to the human body for cleansing, beautifying, promoting attractiveness, or altering the appearance without affecting the body\'s structure or functions\".It includes material intended for use as an ingredients of a cosmetic products </strong></p>\n<!-- /wp:paragraph -->', 'About My Cosmetics', '', 'inherit', 'closed', 'closed', '', '313-revision-v1', '', '', '2020-02-29 13:37:48', '2020-02-29 13:37:48', '', 313, 'http://cosmetic.test/2020/02/29/313-revision-v1/', 0, 'revision', '', 0),
(403, 1, '2020-02-29 13:41:19', '2020-02-29 13:41:19', '{\n    \"widget_media_image[1]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YToxNTp7czoxMzoiYXR0YWNobWVudF9pZCI7aTozOTk7czozOiJ1cmwiO3M6NTY6Imh0dHA6Ly9jb3NtZXRpYy50ZXN0L3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDIwLzAyL2xvZ28uanBnIjtzOjU6InRpdGxlIjtzOjA6IiI7czo0OiJzaXplIjtzOjQ6ImZ1bGwiO3M6NToid2lkdGgiO2k6MTEyNTtzOjY6ImhlaWdodCI7aToxMTAzO3M6NzoiY2FwdGlvbiI7czowOiIiO3M6MzoiYWx0IjtzOjA6IiI7czo5OiJsaW5rX3R5cGUiO3M6NjoiY3VzdG9tIjtzOjg6ImxpbmtfdXJsIjtzOjA6IiI7czoxMzoiaW1hZ2VfY2xhc3NlcyI7czowOiIiO3M6MTI6ImxpbmtfY2xhc3NlcyI7czowOiIiO3M6ODoibGlua19yZWwiO3M6MDoiIjtzOjE3OiJsaW5rX3RhcmdldF9ibGFuayI7YjowO3M6MTE6ImltYWdlX3RpdGxlIjtzOjA6IiI7fQ==\",\n            \"title\": \"\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"5db3ec11ac5b9e4e7d76fb11305cf3b9\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:40:52\"\n    },\n    \"widget_text[2]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YTo0OntzOjU6InRpdGxlIjtzOjA6IiI7czo0OiJ0ZXh0IjtzOjEwNTM6IjxiIGlkPSJtXzMyMzU1MDM4NTYwMzc4MTEwNDZ5TWFpbF9jdXJzb3JFbGVtZW50VHJhY2tlcl8xNTgyNDE3MjIxMzUyIj48c3BhbiBpZD0ibV8zMjM1NTAzODU2MDM3ODExMDQ2eU1haWxfY3Vyc29yRWxlbWVudFRyYWNrZXJfMTU4MjQxNzIyNTI0MiI+Sk0gQ29zbWV0aWNzIGFyZSBsaWNlbnNlZC5hbmQgYXBwcm92ZWQgaW4gdGhlIFBoaWxpcHBpbmVzIGJ5wqA8L3NwYW4+PGEgaWQ9Im1fMzIzNTUwMzg1NjAzNzgxMTA0NnlNYWlsX2N1cnNvckVsZW1lbnRUcmFja2VyXzE1ODI0MTcyOTAwNTIiIHRpdGxlPSJGb29kIGFuZCBEcnVnIEFkbWluaXN0cmF0aW9uIiBocmVmPSJodHRwczovL2VuLm0ud2lraXBlZGlhLm9yZy93aWtpL0Zvb2RfYW5kX0RydWdfQWRtaW5pc3RyYXRpb24iIHRhcmdldD0iX2JsYW5rIiByZWw9Im5vb3BlbmVyIiBkYXRhLXNhZmVyZWRpcmVjdHVybD0iaHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS91cmw/cT1odHRwczovL2VuLm0ud2lraXBlZGlhLm9yZy93aWtpL0Zvb2RfYW5kX0RydWdfQWRtaW5pc3RyYXRpb24mYW1wO3NvdXJjZT1nbWFpbCZhbXA7dXN0PTE1ODMwNjkwNDEzNTYwMDAmYW1wO3VzZz1BRlFqQ05FRG1BMk1wMWt1RUl1YkVpRXpCc1d5aVo5XzRnIj5Gb29kIGFuZCBEcnVnIEFkbWluaXN0cmF0aW9uPC9hPjxzcGFuIGlkPSJtXzMyMzU1MDM4NTYwMzc4MTEwNDZ5TWFpbF9jdXJzb3JFbGVtZW50VHJhY2tlcl8xNTgyNDE3MjIzOTc1Ij7CoChGREEpLCB3aGljaCByZWd1bGF0ZXMgY29zbWV0aWNzLCB0aGV5IGRlZmluZWQgb3VyIHByb2R1Y3QgYXMgImludGVuZGVkIHRvIGJlIGFwcGxpZWQgdG8gdGhlIGh1bWFuIGJvZHkgZm9yIGNsZWFuc2luZywgYmVhdXRpZnlpbmcsIHByb21vdGluZyBhdHRyYWN0aXZlbmVzcywgb3IgYWx0ZXJpbmcgdGhlIGFwcGVhcmFuY2Ugd2l0aG91dCBhZmZlY3RpbmcgdGhlIGJvZHkncyBzdHJ1Y3R1cmUgb3IgZnVuY3Rpb25zIi5JdCBpbmNsdWRlcyBtYXRlcmlhbCBpbnRlbmRlZCBmb3IgdXNlIGFzIGFuIGluZ3JlZGllbnRzwqBvZiBhIGNvc21ldGljIHByb2R1Y3RzwqA8L3NwYW4+PC9iPiI7czo2OiJmaWx0ZXIiO2I6MTtzOjY6InZpc3VhbCI7YjoxO30=\",\n            \"title\": \"\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"aec7cc165aa52d19a224258739cf63de\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:41:19\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '447f4c3a-b7ef-4d5f-8e85-7678c28377f3', '', '', '2020-02-29 13:41:19', '2020-02-29 13:41:19', '', 0, 'http://cosmetic.test/?p=403', 0, 'customize_changeset', '', 0),
(404, 1, '2020-02-29 13:42:16', '2020-02-29 13:42:16', '{\n    \"nav_menu_item[384]\": {\n        \"value\": false,\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:42:16\"\n    },\n    \"nav_menu_item[-2648194092406862000]\": {\n        \"value\": {\n            \"object_id\": 7,\n            \"object\": \"page\",\n            \"menu_item_parent\": 0,\n            \"position\": 8,\n            \"type\": \"post_type\",\n            \"title\": \"Shop\",\n            \"url\": \"http://cosmetic.test/shop/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Shop\",\n            \"nav_menu_term_id\": 31,\n            \"_invalid\": false,\n            \"type_label\": \"Page\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:42:16\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'be529ab7-2cea-4203-9d3a-4978e17f82ed', '', '', '2020-02-29 13:42:16', '2020-02-29 13:42:16', '', 0, 'http://cosmetic.test/2020/02/29/be529ab7-2cea-4203-9d3a-4978e17f82ed/', 0, 'customize_changeset', '', 0),
(405, 1, '2020-02-29 13:42:16', '2020-02-29 13:42:16', ' ', '', '', 'publish', 'closed', 'closed', '', '405', '', '', '2020-02-29 13:42:16', '2020-02-29 13:42:16', '', 0, 'http://cosmetic.test/2020/02/29/405/', 8, 'nav_menu_item', '', 0),
(406, 1, '2020-02-29 13:44:33', '2020-02-29 13:44:33', '{\n    \"sidebars_widgets[wp_inactive_widgets]\": {\n        \"value\": [\n            \"meta-2\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:44:33\"\n    },\n    \"sidebars_widgets[sidebar-1]\": {\n        \"value\": [\n            \"search-2\",\n            \"recent-posts-2\",\n            \"recent-comments-2\",\n            \"archives-2\",\n            \"categories-2\",\n            \"woocommerce_product_search-1\",\n            \"woocommerce_products-1\",\n            \"woocommerce_product_categories-1\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:44:33\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'cdf236b6-b977-4c4d-ba42-c3fa1c33180a', '', '', '2020-02-29 13:44:33', '2020-02-29 13:44:33', '', 0, 'http://cosmetic.test/2020/02/29/cdf236b6-b977-4c4d-ba42-c3fa1c33180a/', 0, 'customize_changeset', '', 0),
(407, 1, '2020-02-29 13:46:50', '2020-02-29 13:46:50', '', 'p1', '', 'inherit', 'open', 'closed', '', 'p1', '', '', '2020-02-29 13:46:50', '2020-02-29 13:46:50', '', 0, 'http://cosmetic.test/wp-content/uploads/2020/02/p1.jpg', 0, 'attachment', 'image/jpeg', 0),
(409, 1, '2020-02-29 13:50:22', '2020-02-29 13:50:22', '', 'front1', '', 'inherit', 'open', 'closed', '', 'front1', '', '', '2020-02-29 13:50:22', '2020-02-29 13:50:22', '', 0, 'http://cosmetic.test/wp-content/uploads/2020/02/front1.jpg', 0, 'attachment', 'image/jpeg', 0),
(410, 1, '2020-02-29 13:51:17', '2020-02-29 13:51:17', '', 'cropped-front1.jpg', '', 'inherit', 'open', 'closed', '', 'cropped-front1-jpg', '', '', '2020-02-29 13:51:17', '2020-02-29 13:51:17', '', 0, 'http://cosmetic.test/wp-content/uploads/2020/02/cropped-front1.jpg', 0, 'attachment', 'image/jpeg', 0),
(411, 1, '2020-02-29 13:55:52', '2020-02-29 13:55:52', '{\n    \"zigcy-cosmetics::header_image\": {\n        \"value\": \"remove-header\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:52:24\"\n    },\n    \"zigcy-cosmetics::header_image_data\": {\n        \"value\": \"remove-header\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:52:24\"\n    },\n    \"zigcy-cosmetics::background_image\": {\n        \"value\": \"\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:51:25\"\n    },\n    \"zigcy-cosmetics::background_preset\": {\n        \"value\": \"fill\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:51:25\"\n    },\n    \"zigcy-cosmetics::background_position_x\": {\n        \"value\": \"left\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:51:25\"\n    },\n    \"zigcy-cosmetics::background_position_y\": {\n        \"value\": \"top\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:51:25\"\n    },\n    \"zigcy-cosmetics::background_size\": {\n        \"value\": \"cover\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:51:25\"\n    },\n    \"zigcy-cosmetics::background_repeat\": {\n        \"value\": \"no-repeat\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:51:25\"\n    },\n    \"zigcy-cosmetics::background_attachment\": {\n        \"value\": \"fixed\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:51:25\"\n    },\n    \"zigcy-cosmetics::zigcy_lite_header_type\": {\n        \"value\": \"layout1\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:55:39\"\n    },\n    \"zigcy-cosmetics::zigcy_lite_cta_bg_image\": {\n        \"value\": \"http://cosmetic.test/wp-content/uploads/2020/02/front1.jpg\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-29 13:55:52\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'bc6fead4-6ea4-4397-be7a-d6df0b4474bb', '', '', '2020-02-29 13:55:52', '2020-02-29 13:55:52', '', 0, 'http://cosmetic.test/?p=411', 0, 'customize_changeset', '', 0),
(413, 1, '2020-02-29 13:59:41', '2020-02-29 13:59:41', '', 'h2', '', 'inherit', 'open', 'closed', '', 'h2', '', '', '2020-02-29 13:59:41', '2020-02-29 13:59:41', '', 0, 'http://cosmetic.test/wp-content/uploads/2020/02/h2.jpg', 0, 'attachment', 'image/jpeg', 0),
(414, 1, '2020-03-01 06:28:28', '2020-03-01 06:28:28', '{\n    \"zigcy-cosmetics::zigcy_lite_area_one_image\": {\n        \"value\": \"http://cosmetic.test/wp-content/uploads/2020/03/h4.jpg\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-01 06:28:07\"\n    },\n    \"zigcy-cosmetics::zigcy_lite_area_two_image\": {\n        \"value\": \"http://cosmetic.test/wp-content/uploads/2020/03/h3.jpg\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-01 06:23:59\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '5a5a793a-742b-411a-a622-cf83cc28831c', '', '', '2020-03-01 06:28:28', '2020-03-01 06:28:28', '', 0, 'http://cosmetic.test/?p=414', 0, 'customize_changeset', '', 0),
(415, 1, '2020-03-01 06:23:47', '2020-03-01 06:23:47', '', 'h3', '', 'inherit', 'open', 'closed', '', 'h3', '', '', '2020-03-01 06:23:47', '2020-03-01 06:23:47', '', 0, 'http://cosmetic.test/wp-content/uploads/2020/03/h3.jpg', 0, 'attachment', 'image/jpeg', 0),
(416, 1, '2020-03-01 06:27:27', '2020-03-01 06:27:27', '', 'h4', '', 'inherit', 'open', 'closed', '', 'h4', '', '', '2020-03-01 06:27:27', '2020-03-01 06:27:27', '', 0, 'http://cosmetic.test/wp-content/uploads/2020/03/h4.jpg', 0, 'attachment', 'image/jpeg', 0),
(417, 1, '2020-03-01 06:32:16', '2020-03-01 06:32:16', 'Lookbook Cosmetics 2019', 'Street Life Cosmetics', '', 'inherit', 'closed', 'closed', '', '376-autosave-v1', '', '', '2020-03-01 06:32:16', '2020-03-01 06:32:16', '', 376, 'http://cosmetic.test/2020/03/01/376-autosave-v1/', 0, 'revision', '', 0),
(418, 1, '2020-03-01 06:32:32', '2020-03-01 06:32:32', '', 's1', '', 'inherit', 'open', 'closed', '', 's1-3', '', '', '2020-03-01 06:32:32', '2020-03-01 06:32:32', '', 376, 'http://cosmetic.test/wp-content/uploads/2018/11/s1-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(419, 1, '2020-03-01 06:32:41', '2020-03-01 06:32:41', 'Lookbook Cosmetics 2019', 'Street Life Cosmetics', '', 'inherit', 'closed', 'closed', '', '376-revision-v1', '', '', '2020-03-01 06:32:41', '2020-03-01 06:32:41', '', 376, 'http://cosmetic.test/2020/03/01/376-revision-v1/', 0, 'revision', '', 0),
(420, 1, '2020-03-01 06:34:42', '2020-03-01 06:34:42', '', 's2', '', 'inherit', 'open', 'closed', '', 's2-2', '', '', '2020-03-01 06:34:42', '2020-03-01 06:34:42', '', 377, 'http://cosmetic.test/wp-content/uploads/2018/11/s2.jpeg', 0, 'attachment', 'image/jpeg', 0),
(421, 1, '2020-03-01 06:34:48', '2020-03-01 06:34:48', '<div class=\"subtitle\">Clothing Trend 2018</div>', 'Sale Offer 20% Off This Week', '', 'inherit', 'closed', 'closed', '', '377-revision-v1', '', '', '2020-03-01 06:34:48', '2020-03-01 06:34:48', '', 377, 'http://cosmetic.test/2020/03/01/377-revision-v1/', 0, 'revision', '', 0),
(422, 1, '2020-03-01 06:37:05', '2020-03-01 06:37:05', '', 's3', '', 'inherit', 'open', 'closed', '', 's3-2', '', '', '2020-03-01 06:37:05', '2020-03-01 06:37:05', '', 377, 'http://cosmetic.test/wp-content/uploads/2018/11/s3-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(423, 1, '2020-03-01 06:39:00', '2020-03-01 06:39:00', '{\n    \"blogdescription\": {\n        \"value\": \"Branded Cosmetic Products\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-01 06:39:00\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '5222db2e-c2e8-475a-b831-d1ecde1321c9', '', '', '2020-03-01 06:39:00', '2020-03-01 06:39:00', '', 0, 'http://cosmetic.test/2020/03/01/5222db2e-c2e8-475a-b831-d1ecde1321c9/', 0, 'customize_changeset', '', 0),
(424, 1, '2020-03-01 06:39:48', '2020-03-01 06:39:48', '{\n    \"zigcy-cosmetics::zigcy_lite_area_two_subtitle\": {\n        \"value\": \"NEW ARRIVAL FALL 2019\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-01 06:39:48\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '9dc96798-dac9-42cd-b7be-30b8f9bf99f8', '', '', '2020-03-01 06:39:48', '2020-03-01 06:39:48', '', 0, 'http://cosmetic.test/2020/03/01/9dc96798-dac9-42cd-b7be-30b8f9bf99f8/', 0, 'customize_changeset', '', 0),
(425, 1, '2020-03-01 06:41:18', '2020-03-01 06:41:18', '{\n    \"zigcy-cosmetics::zigcy_lite_pro_cat_slider_title\": {\n        \"value\": \"Latest Cosmetics\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-01 06:41:18\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'cd1a2085-a13e-4699-adde-ab25d85c5598', '', '', '2020-03-01 06:41:18', '2020-03-01 06:41:18', '', 0, 'http://cosmetic.test/2020/03/01/cd1a2085-a13e-4699-adde-ab25d85c5598/', 0, 'customize_changeset', '', 0),
(426, 1, '2020-03-01 06:43:26', '2020-03-01 06:43:26', '{\n    \"zigcy-cosmetics::zigcy_lite_feature_product_title\": {\n        \"value\": \"Hot Items\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-01 06:42:24\"\n    },\n    \"zigcy-cosmetics::zigcy_lite_feature_product_subtitle\": {\n        \"value\": \"GET SALE ALERTS ON KIT YOU LOVE\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-01 06:42:24\"\n    },\n    \"zigcy-cosmetics::zigcy_lite_cta_subtitle\": {\n        \"value\": \"GRAB YOUR COSMETICS TREND TODAY\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-01 06:42:24\"\n    },\n    \"zigcy-cosmetics::zigcy_lite_blog_title\": {\n        \"value\": \"MY Blog\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-01 06:43:24\"\n    },\n    \"zigcy-cosmetics::zigcy_lite_latest_cat_prod_title\": {\n        \"value\": \"OTHER ITEMS ON SALE\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-01 06:43:24\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'd0089996-23b7-4529-ae3b-de9d8b6a7606', '', '', '2020-03-01 06:43:26', '2020-03-01 06:43:26', '', 0, 'http://cosmetic.test/?p=426', 0, 'customize_changeset', '', 0),
(427, 1, '2020-03-01 06:53:41', '2020-03-01 06:53:41', '', 'front1', '', 'inherit', 'open', 'closed', '', 'front1-2', '', '', '2020-03-01 06:53:41', '2020-03-01 06:53:41', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/front1.jpg', 0, 'attachment', 'image/jpeg', 0),
(428, 1, '2020-03-01 06:53:43', '2020-03-01 06:53:43', '', 'h2', '', 'inherit', 'open', 'closed', '', 'h2-2', '', '', '2020-03-01 06:53:43', '2020-03-01 06:53:43', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/h2.jpg', 0, 'attachment', 'image/jpeg', 0),
(429, 1, '2020-03-01 06:53:44', '2020-03-01 06:53:44', '', 'h3', '', 'inherit', 'open', 'closed', '', 'h3-2', '', '', '2020-03-01 06:53:44', '2020-03-01 06:53:44', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/h3.jpg', 0, 'attachment', 'image/jpeg', 0),
(430, 1, '2020-03-01 06:53:45', '2020-03-01 06:53:45', '', 'h4', '', 'inherit', 'open', 'closed', '', 'h4-2', '', '', '2020-03-01 06:53:45', '2020-03-01 06:53:45', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/h4.jpg', 0, 'attachment', 'image/jpeg', 0),
(431, 1, '2020-03-01 06:53:46', '2020-03-01 06:53:46', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo-7', '', '', '2020-03-01 06:53:46', '2020-03-01 06:53:46', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/logo.jpg', 0, 'attachment', 'image/jpeg', 0),
(432, 1, '2020-03-01 06:53:47', '2020-03-01 06:53:47', '', 'p1', '', 'inherit', 'open', 'closed', '', 'p1-2', '', '', '2020-03-01 06:53:47', '2020-03-01 06:53:47', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p1.jpg', 0, 'attachment', 'image/jpeg', 0),
(433, 1, '2020-03-01 06:53:48', '2020-03-01 06:53:48', '', 'p2', '', 'inherit', 'open', 'closed', '', 'p2', '', '', '2020-03-01 06:53:48', '2020-03-01 06:53:48', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p2.jpg', 0, 'attachment', 'image/jpeg', 0),
(434, 1, '2020-03-01 06:53:50', '2020-03-01 06:53:50', '', 'p3', '', 'inherit', 'open', 'closed', '', 'p3', '', '', '2020-03-01 06:53:50', '2020-03-01 06:53:50', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p3.jpg', 0, 'attachment', 'image/jpeg', 0),
(435, 1, '2020-03-01 06:53:51', '2020-03-01 06:53:51', '', 'p4', '', 'inherit', 'open', 'closed', '', 'p4', '', '', '2020-03-01 06:53:51', '2020-03-01 06:53:51', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p4.jpg', 0, 'attachment', 'image/jpeg', 0),
(436, 1, '2020-03-01 06:53:52', '2020-03-01 06:53:52', '', 'p5', '', 'inherit', 'open', 'closed', '', 'p5', '', '', '2020-03-01 06:53:52', '2020-03-01 06:53:52', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p5.jpg', 0, 'attachment', 'image/jpeg', 0),
(437, 1, '2020-03-01 06:53:53', '2020-03-01 06:53:53', '', 'p6', '', 'inherit', 'open', 'closed', '', 'p6', '', '', '2020-03-01 06:53:53', '2020-03-01 06:53:53', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p6.jpg', 0, 'attachment', 'image/jpeg', 0),
(438, 1, '2020-03-01 06:53:54', '2020-03-01 06:53:54', '', 'p7', '', 'inherit', 'open', 'closed', '', 'p7', '', '', '2020-03-01 06:53:54', '2020-03-01 06:53:54', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p7.jpg', 0, 'attachment', 'image/jpeg', 0),
(439, 1, '2020-03-01 06:53:55', '2020-03-01 06:53:55', '', 'p8', '', 'inherit', 'open', 'closed', '', 'p8', '', '', '2020-03-01 06:53:55', '2020-03-01 06:53:55', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p8.jpg', 0, 'attachment', 'image/jpeg', 0),
(440, 1, '2020-03-01 06:53:56', '2020-03-01 06:53:56', '', 'p9', '', 'inherit', 'open', 'closed', '', 'p9', '', '', '2020-03-01 06:53:56', '2020-03-01 06:53:56', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p9.jpg', 0, 'attachment', 'image/jpeg', 0),
(441, 1, '2020-03-01 06:53:57', '2020-03-01 06:53:57', '', 'p10', '', 'inherit', 'open', 'closed', '', 'p10', '', '', '2020-03-01 06:53:57', '2020-03-01 06:53:57', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p10.jpg', 0, 'attachment', 'image/jpeg', 0),
(442, 1, '2020-03-01 06:53:58', '2020-03-01 06:53:58', '', 'p11', '', 'inherit', 'open', 'closed', '', 'p11', '', '', '2020-03-01 06:53:58', '2020-03-01 06:53:58', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p11.jpg', 0, 'attachment', 'image/jpeg', 0),
(443, 1, '2020-03-01 06:53:59', '2020-03-01 06:53:59', '', 'p12', '', 'inherit', 'open', 'closed', '', 'p12', '', '', '2020-03-01 06:53:59', '2020-03-01 06:53:59', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p12.jpg', 0, 'attachment', 'image/jpeg', 0),
(444, 1, '2020-03-01 06:53:59', '2020-03-01 06:53:59', '', 'p13', '', 'inherit', 'open', 'closed', '', 'p13', '', '', '2020-03-01 06:53:59', '2020-03-01 06:53:59', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p13.jpg', 0, 'attachment', 'image/jpeg', 0),
(445, 1, '2020-03-01 06:54:00', '2020-03-01 06:54:00', '', 'p14', '', 'inherit', 'open', 'closed', '', 'p14', '', '', '2020-03-01 06:54:00', '2020-03-01 06:54:00', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p14.jpg', 0, 'attachment', 'image/jpeg', 0),
(446, 1, '2020-03-01 06:54:01', '2020-03-01 06:54:01', '', 'p15', '', 'inherit', 'open', 'closed', '', 'p15', '', '', '2020-03-01 06:54:01', '2020-03-01 06:54:01', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p15.jpg', 0, 'attachment', 'image/jpeg', 0),
(447, 1, '2020-03-01 06:54:02', '2020-03-01 06:54:02', '', 'p16', '', 'inherit', 'open', 'closed', '', 'p16', '', '', '2020-03-01 06:54:02', '2020-03-01 06:54:02', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p16.jpg', 0, 'attachment', 'image/jpeg', 0),
(448, 1, '2020-03-01 06:54:02', '2020-03-01 06:54:02', '', 'p17', '', 'inherit', 'open', 'closed', '', 'p17', '', '', '2020-03-01 06:54:02', '2020-03-01 06:54:02', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p17.jpg', 0, 'attachment', 'image/jpeg', 0),
(449, 1, '2020-03-01 06:54:03', '2020-03-01 06:54:03', '', 'p18', '', 'inherit', 'open', 'closed', '', 'p18', '', '', '2020-03-01 06:54:03', '2020-03-01 06:54:03', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/p18.jpg', 0, 'attachment', 'image/jpeg', 0),
(450, 1, '2020-03-01 06:54:05', '2020-03-01 06:54:05', '', 's1', '', 'inherit', 'open', 'closed', '', 's1-4', '', '', '2020-03-01 06:54:05', '2020-03-01 06:54:05', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/s1-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(451, 1, '2020-03-01 06:54:05', '2020-03-01 06:54:05', '', 's2', '', 'inherit', 'open', 'closed', '', 's2-3', '', '', '2020-03-01 06:54:05', '2020-03-01 06:54:05', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/s2-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(452, 1, '2020-03-01 06:54:06', '2020-03-01 06:54:06', '', 's3', '', 'inherit', 'open', 'closed', '', 's3-3', '', '', '2020-03-01 06:54:06', '2020-03-01 06:54:06', '', 94, 'http://cosmetic.test/wp-content/uploads/2018/11/s3-2.jpg', 0, 'attachment', 'image/jpeg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cm_termmeta`
--

CREATE TABLE `cm_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_termmeta`
--

INSERT INTO `cm_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 19, 'order', '0'),
(2, 19, 'order', '0'),
(3, 19, 'order', '0'),
(4, 19, 'order', '0'),
(5, 19, 'product_count_product_cat', '8'),
(6, 19, 'display_type', ''),
(7, 19, 'thumbnail_id', '206'),
(8, 20, 'order', '0'),
(9, 20, 'order', '0'),
(10, 20, 'order', '0'),
(11, 20, 'order', '0'),
(12, 20, 'product_count_product_cat', '11'),
(13, 20, 'display_type', ''),
(14, 20, 'thumbnail_id', '239'),
(15, 21, 'order', '0'),
(16, 21, 'order', '0'),
(17, 21, 'display_type', ''),
(18, 21, 'thumbnail_id', '329'),
(19, 22, 'order', '0'),
(20, 22, 'order', '0'),
(21, 22, 'display_type', ''),
(22, 22, 'thumbnail_id', '328'),
(23, 23, 'order', '0'),
(24, 23, 'order', '0'),
(25, 23, 'order', '0'),
(26, 23, 'order', '0'),
(27, 23, 'display_type', ''),
(28, 23, 'thumbnail_id', '238'),
(29, 24, 'order', '0'),
(30, 24, 'order', '0'),
(31, 24, 'display_type', ''),
(32, 24, 'thumbnail_id', '330'),
(33, 25, 'order', '0'),
(34, 25, 'order', '0'),
(35, 25, 'order', '0'),
(36, 25, 'order', '0'),
(37, 25, 'display_type', ''),
(38, 25, 'thumbnail_id', '0'),
(39, 25, 'product_count_product_cat', '4'),
(40, 26, 'order', '0'),
(41, 26, 'order', '0'),
(42, 26, 'order', '0'),
(43, 26, 'order', '0'),
(44, 26, 'display_type', ''),
(45, 26, 'thumbnail_id', '237'),
(46, 26, 'product_count_product_cat', '4'),
(47, 27, 'order', '0'),
(48, 27, 'order', '0'),
(49, 27, 'order', '0'),
(50, 27, 'order', '0'),
(51, 27, 'display_type', ''),
(52, 27, 'thumbnail_id', '0'),
(53, 27, 'product_count_product_cat', '4'),
(54, 28, 'order', '0'),
(55, 28, 'order', '0'),
(56, 28, 'order', '0'),
(57, 28, 'order', '0'),
(58, 28, 'display_type', ''),
(59, 28, 'thumbnail_id', '239');

-- --------------------------------------------------------

--
-- Table structure for table `cm_terms`
--

CREATE TABLE `cm_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_terms`
--

INSERT INTO `cm_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0),
(16, 'Blog', 'blog', 0),
(17, 'logo', 'logo', 0),
(18, 'Slider', 'slider', 0),
(19, 'Eyes', 'eyes', 0),
(20, 'Lips', 'lips', 0),
(21, 'Jaccquard Print', 'jaccquard-print', 0),
(22, 'Mini Studded', 'mini-studded', 0),
(23, 'SALE 2018', 'street-styles', 0),
(24, 'Special Price', 'special-price', 0),
(25, 'Summer', 'summer', 0),
(26, 'SUMMER FASHION', 'summer-fashion', 0),
(27, 'Winter', 'winter', 0),
(28, 'Goodbye Items', 'goodbye-items', 0),
(29, 'About Us', 'about-us', 0),
(30, 'Footer Menu', 'footer-menu', 0),
(31, 'Menu 1', 'menu-1', 0),
(32, 'My Account', 'my-account', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cm_term_relationships`
--

CREATE TABLE `cm_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_term_relationships`
--

INSERT INTO `cm_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(55, 2, 0),
(55, 19, 0),
(55, 26, 0),
(57, 2, 0),
(57, 19, 0),
(57, 26, 0),
(59, 2, 0),
(59, 19, 0),
(59, 27, 0),
(61, 2, 0),
(61, 19, 0),
(61, 27, 0),
(63, 2, 0),
(63, 19, 0),
(63, 27, 0),
(65, 2, 0),
(65, 19, 0),
(65, 25, 0),
(67, 2, 0),
(67, 19, 0),
(67, 20, 0),
(71, 2, 0),
(71, 19, 0),
(76, 2, 0),
(76, 20, 0),
(76, 25, 0),
(78, 2, 0),
(78, 20, 0),
(80, 2, 0),
(80, 20, 0),
(80, 26, 0),
(82, 2, 0),
(82, 20, 0),
(82, 26, 0),
(84, 2, 0),
(84, 20, 0),
(84, 25, 0),
(86, 2, 0),
(86, 20, 0),
(88, 2, 0),
(88, 20, 0),
(90, 2, 0),
(90, 20, 0),
(90, 27, 0),
(92, 2, 0),
(92, 20, 0),
(94, 2, 0),
(94, 20, 0),
(94, 25, 0),
(222, 16, 0),
(225, 16, 0),
(253, 1, 0),
(254, 1, 0),
(272, 17, 0),
(281, 17, 0),
(284, 17, 0),
(287, 17, 0),
(289, 17, 0),
(290, 17, 0),
(293, 17, 0),
(302, 16, 0),
(367, 31, 0),
(368, 31, 0),
(369, 29, 0),
(370, 29, 0),
(371, 29, 0),
(372, 29, 0),
(376, 18, 0),
(377, 18, 0),
(379, 31, 0),
(380, 31, 0),
(381, 31, 0),
(382, 31, 0),
(383, 31, 0),
(385, 31, 0),
(386, 32, 0),
(387, 32, 0),
(388, 32, 0),
(389, 32, 0),
(390, 30, 0),
(391, 30, 0),
(392, 30, 0),
(393, 30, 0),
(405, 31, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cm_term_taxonomy`
--

CREATE TABLE `cm_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_term_taxonomy`
--

INSERT INTO `cm_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'product_type', '', 0, 18),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0),
(16, 16, 'category', '', 0, 3),
(17, 17, 'category', '', 0, 7),
(18, 18, 'category', '', 0, 2),
(19, 19, 'product_cat', '', 0, 8),
(20, 20, 'product_cat', '', 0, 11),
(21, 21, 'product_cat', 'Hairband Collection', 0, 0),
(22, 22, 'product_cat', 'Ducebag Collection', 0, 0),
(23, 23, 'product_cat', 'Get 20% Discount on Everything\n', 0, 0),
(24, 24, 'product_cat', 'Corduroy Culottes', 0, 0),
(25, 25, 'product_cat', '', 0, 4),
(26, 26, 'product_cat', 'Stay Ahead of The Fashion Trends\n', 0, 4),
(27, 27, 'product_cat', '', 0, 4),
(28, 28, 'product_cat', 'Get Ahead of Everyone\r\n', 0, 0),
(29, 29, 'nav_menu', '', 0, 4),
(30, 30, 'nav_menu', '', 0, 4),
(31, 31, 'nav_menu', '', 0, 9),
(32, 32, 'nav_menu', '', 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `cm_usermeta`
--

CREATE TABLE `cm_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_usermeta`
--

INSERT INTO `cm_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'superadmin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'cm_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'cm_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'cm_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, '_woocommerce_tracks_anon_id', 'woo:DLA+etz1vcStQDplfzDuFFcs'),
(20, 1, 'jetpack_tracks_anon_id', 'jetpack:YkQzlwM0OaLlow8UM6B0Y/8F'),
(21, 1, 'wc_last_active', '1583020800'),
(22, 1, '_order_count', '0'),
(23, 1, 'cm_user-settings', 'libraryContent=browse'),
(24, 1, 'cm_user-settings-time', '1583043152'),
(25, 1, 'nav_menu_recently_edited', '29'),
(26, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(27, 1, 'metaboxhidden_nav-menus', 'a:4:{i:0;s:21:\"add-post-type-product\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-product_cat\";i:3;s:15:\"add-product_tag\";}'),
(28, 2, 'nickname', 'admin'),
(29, 2, 'first_name', 'Me'),
(30, 2, 'last_name', 'Admin'),
(31, 2, 'description', ''),
(32, 2, 'rich_editing', 'true'),
(33, 2, 'syntax_highlighting', 'true'),
(34, 2, 'comment_shortcuts', 'false'),
(35, 2, 'admin_color', 'fresh'),
(36, 2, 'use_ssl', '0'),
(37, 2, 'show_admin_bar_front', 'true'),
(38, 2, 'locale', ''),
(39, 2, 'cm_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(40, 2, 'cm_user_level', '10'),
(41, 2, 'dismissed_wp_pointers', '');

-- --------------------------------------------------------

--
-- Table structure for table `cm_users`
--

CREATE TABLE `cm_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_users`
--

INSERT INTO `cm_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'superadmin', '$P$BF0M9fLwteu1ClX8XOKf2QWvWfwbq61', 'superadmin', 'ryandingle09@gmail.com', '', '2020-02-23 07:28:55', '', 0, 'superadmin'),
(2, 'admin', '$P$B8D1b3070398JkwRwBkiGfyeMUqJuX0', 'admin', 'admin@gmail.com', '', '2020-03-01 07:06:27', '', 0, 'Me Admin');

-- --------------------------------------------------------

--
-- Table structure for table `cm_wc_admin_notes`
--

CREATE TABLE `cm_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `icon` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content_data` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_wc_admin_notes`
--

INSERT INTO `cm_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `icon`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`) VALUES
(1, 'wc-admin-welcome-note', 'info', 'en_US', 'New feature(s)', 'Welcome to the new WooCommerce experience! In this new release you\'ll be able to have a glimpse of how your store is doing in the Dashboard, manage important aspects of your business (such as managing orders, stock, reviews) from anywhere in the interface, dive into your store data with a completely new Analytics section and more!', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-02-23 07:32:44', NULL, 0),
(2, 'wc-admin-add-first-product', 'info', 'en_US', 'Add your first product', 'Grow your revenue by adding products to your store. Add products manually, import from a sheet, or migrate from another platform.', 'product', '{}', 'unactioned', 'woocommerce-admin', '2020-02-23 07:32:44', NULL, 0),
(3, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-02-23 07:32:47', NULL, 0),
(4, 'wc-admin-store-notice-giving-feedback', 'info', 'en_US', 'Review your experience', 'If you like WooCommerce Admin please leave us a 5 star rating. A huge thanks in advance!', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-02-29 13:26:11', NULL, 0),
(5, 'wc-admin-mobile-app', 'info', 'en_US', 'Install Woo mobile app', 'Install the WooCommerce mobile app to manage orders, receive sales notifications, and view key metrics — wherever you are.', 'phone', '{}', 'unactioned', 'woocommerce-admin', '2020-02-29 13:26:11', NULL, 0),
(6, 'wc-admin-facebook-extension', 'info', 'en_US', 'Market on Facebook', 'Grow your business by targeting the right people and driving sales with Facebook. You can install this free extension now.', 'thumbs-up', '{}', 'actioned', 'woocommerce-admin', '2020-02-29 13:26:11', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cm_wc_admin_note_actions`
--

CREATE TABLE `cm_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_wc_admin_note_actions`
--

INSERT INTO `cm_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`) VALUES
(1, 1, 'learn-more', 'Learn more', 'https://woocommerce.wordpress.com/', 'actioned', 0),
(2, 2, 'add-a-product', 'Add a product', 'http://cosmetic.test/wp-admin/post-new.php?post_type=product', 'actioned', 1),
(3, 3, 'connect', 'Connect', '?page=wc-addons&section=helper', 'actioned', 0),
(4, 4, 'share-feedback', 'Review', 'https://wordpress.org/support/plugin/woocommerce-admin/reviews/?rate=5#new-post', 'actioned', 0),
(5, 5, 'learn-more', 'Learn more', 'https://woocommerce.com/mobile/', 'actioned', 0),
(6, 6, 'learn-more', 'Learn more', 'https://woocommerce.com/products/facebook/', 'unactioned', 0),
(7, 6, 'install-now', 'Install now', '', 'unactioned', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cm_wc_category_lookup`
--

CREATE TABLE `cm_wc_category_lookup` (
  `category_tree_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_wc_category_lookup`
--

INSERT INTO `cm_wc_category_lookup` (`category_tree_id`, `category_id`) VALUES
(15, 15);

-- --------------------------------------------------------

--
-- Table structure for table `cm_wc_customer_lookup`
--

CREATE TABLE `cm_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_wc_download_log`
--

CREATE TABLE `cm_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_wc_order_coupon_lookup`
--

CREATE TABLE `cm_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_wc_order_product_lookup`
--

CREATE TABLE `cm_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_wc_order_stats`
--

CREATE TABLE `cm_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `total_sales` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_wc_order_tax_lookup`
--

CREATE TABLE `cm_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_wc_product_meta_lookup`
--

CREATE TABLE `cm_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(10,2) DEFAULT NULL,
  `max_price` decimal(10,2) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_wc_product_meta_lookup`
--

INSERT INTO `cm_wc_product_meta_lookup` (`product_id`, `sku`, `virtual`, `downloadable`, `min_price`, `max_price`, `onsale`, `stock_quantity`, `stock_status`, `rating_count`, `average_rating`, `total_sales`) VALUES
(55, '', 0, 0, '40.00', '40.00', 0, NULL, 'instock', 0, '0.00', 0),
(57, '', 0, 0, '45.00', '45.00', 0, NULL, 'instock', 0, '0.00', 0),
(59, '', 0, 0, '55.00', '55.00', 1, NULL, 'instock', 0, '0.00', 0),
(61, '', 0, 0, '90.00', '90.00', 0, NULL, 'instock', 0, '0.00', 0),
(63, '', 0, 0, '85.00', '85.00', 0, NULL, 'instock', 0, '0.00', 0),
(65, '', 0, 0, '90.00', '90.00', 0, NULL, 'instock', 0, '0.00', 0),
(67, '', 0, 0, '125.00', '125.00', 1, NULL, 'instock', 0, '0.00', 0),
(71, '', 0, 0, '95.00', '95.00', 0, NULL, 'instock', 0, '0.00', 0),
(76, '', 0, 0, '20.00', '20.00', 1, NULL, 'instock', 0, '0.00', 0),
(78, '', 0, 0, '115.00', '115.00', 0, NULL, 'instock', 0, '0.00', 0),
(80, '', 0, 0, '70.00', '70.00', 0, NULL, 'instock', 0, '0.00', 0),
(82, '', 0, 0, '155.00', '155.00', 1, NULL, 'instock', 0, '0.00', 0),
(84, '', 0, 0, '45.00', '45.00', 0, NULL, 'instock', 0, '0.00', 0),
(86, '', 0, 0, '700.00', '700.00', 1, NULL, 'instock', 0, '0.00', 0),
(88, '', 0, 0, '125.00', '125.00', 0, NULL, 'instock', 0, '0.00', 0),
(90, '', 0, 0, '100.00', '100.00', 1, NULL, 'instock', 0, '0.00', 0),
(92, '', 0, 0, '90.00', '90.00', 0, NULL, 'instock', 0, '0.00', 0),
(94, '', 0, 0, '150.00', '150.00', 0, NULL, 'instock', 0, '0.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cm_wc_tax_rate_classes`
--

CREATE TABLE `cm_wc_tax_rate_classes` (
  `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_wc_tax_rate_classes`
--

INSERT INTO `cm_wc_tax_rate_classes` (`tax_rate_class_id`, `name`, `slug`) VALUES
(1, 'Reduced rate', 'reduced-rate'),
(2, 'Zero rate', 'zero-rate');

-- --------------------------------------------------------

--
-- Table structure for table `cm_wc_webhooks`
--

CREATE TABLE `cm_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_api_keys`
--

CREATE TABLE `cm_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_attribute_taxonomies`
--

CREATE TABLE `cm_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `cm_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_log`
--

CREATE TABLE `cm_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_order_itemmeta`
--

CREATE TABLE `cm_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_order_items`
--

CREATE TABLE `cm_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_payment_tokenmeta`
--

CREATE TABLE `cm_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_payment_tokens`
--

CREATE TABLE `cm_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_sessions`
--

CREATE TABLE `cm_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_shipping_zones`
--

CREATE TABLE `cm_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_woocommerce_shipping_zones`
--

INSERT INTO `cm_woocommerce_shipping_zones` (`zone_id`, `zone_name`, `zone_order`) VALUES
(1, 'Philippines', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_shipping_zone_locations`
--

CREATE TABLE `cm_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_woocommerce_shipping_zone_locations`
--

INSERT INTO `cm_woocommerce_shipping_zone_locations` (`location_id`, `zone_id`, `location_code`, `location_type`) VALUES
(1, 1, 'PH', 'country');

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_shipping_zone_methods`
--

CREATE TABLE `cm_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cm_woocommerce_shipping_zone_methods`
--

INSERT INTO `cm_woocommerce_shipping_zone_methods` (`zone_id`, `instance_id`, `method_id`, `method_order`, `is_enabled`) VALUES
(1, 1, 'flat_rate', 1, 1),
(0, 2, 'flat_rate', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_tax_rates`
--

CREATE TABLE `cm_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_woocommerce_tax_rate_locations`
--

CREATE TABLE `cm_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cm_yith_wcwl`
--

CREATE TABLE `cm_yith_wcwl` (
  `ID` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `wishlist_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `original_price` decimal(9,3) DEFAULT NULL,
  `original_currency` char(3) DEFAULT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `on_sale` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cm_yith_wcwl_lists`
--

CREATE TABLE `cm_yith_wcwl_lists` (
  `ID` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `wishlist_slug` varchar(200) NOT NULL,
  `wishlist_name` text,
  `wishlist_token` varchar(64) NOT NULL,
  `wishlist_privacy` tinyint(1) NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiration` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cm_actionscheduler_actions`
--
ALTER TABLE `cm_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`);

--
-- Indexes for table `cm_actionscheduler_claims`
--
ALTER TABLE `cm_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `cm_actionscheduler_groups`
--
ALTER TABLE `cm_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `cm_actionscheduler_logs`
--
ALTER TABLE `cm_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `cm_commentmeta`
--
ALTER TABLE `cm_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cm_comments`
--
ALTER TABLE `cm_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `cm_links`
--
ALTER TABLE `cm_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `cm_mailchimp_carts`
--
ALTER TABLE `cm_mailchimp_carts`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `cm_mailchimp_jobs`
--
ALTER TABLE `cm_mailchimp_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cm_options`
--
ALTER TABLE `cm_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `cm_postmeta`
--
ALTER TABLE `cm_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cm_posts`
--
ALTER TABLE `cm_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `cm_termmeta`
--
ALTER TABLE `cm_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cm_terms`
--
ALTER TABLE `cm_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `cm_term_relationships`
--
ALTER TABLE `cm_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `cm_term_taxonomy`
--
ALTER TABLE `cm_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `cm_usermeta`
--
ALTER TABLE `cm_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cm_users`
--
ALTER TABLE `cm_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `cm_wc_admin_notes`
--
ALTER TABLE `cm_wc_admin_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `cm_wc_admin_note_actions`
--
ALTER TABLE `cm_wc_admin_note_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `note_id` (`note_id`);

--
-- Indexes for table `cm_wc_category_lookup`
--
ALTER TABLE `cm_wc_category_lookup`
  ADD PRIMARY KEY (`category_tree_id`,`category_id`);

--
-- Indexes for table `cm_wc_customer_lookup`
--
ALTER TABLE `cm_wc_customer_lookup`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `cm_wc_download_log`
--
ALTER TABLE `cm_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `cm_wc_order_coupon_lookup`
--
ALTER TABLE `cm_wc_order_coupon_lookup`
  ADD PRIMARY KEY (`order_id`,`coupon_id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `cm_wc_order_product_lookup`
--
ALTER TABLE `cm_wc_order_product_lookup`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `cm_wc_order_stats`
--
ALTER TABLE `cm_wc_order_stats`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `status` (`status`(191));

--
-- Indexes for table `cm_wc_order_tax_lookup`
--
ALTER TABLE `cm_wc_order_tax_lookup`
  ADD PRIMARY KEY (`order_id`,`tax_rate_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `cm_wc_product_meta_lookup`
--
ALTER TABLE `cm_wc_product_meta_lookup`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `virtual` (`virtual`),
  ADD KEY `downloadable` (`downloadable`),
  ADD KEY `stock_status` (`stock_status`),
  ADD KEY `stock_quantity` (`stock_quantity`),
  ADD KEY `onsale` (`onsale`),
  ADD KEY `min_max_price` (`min_price`,`max_price`);

--
-- Indexes for table `cm_wc_tax_rate_classes`
--
ALTER TABLE `cm_wc_tax_rate_classes`
  ADD PRIMARY KEY (`tax_rate_class_id`),
  ADD UNIQUE KEY `slug` (`slug`(191));

--
-- Indexes for table `cm_wc_webhooks`
--
ALTER TABLE `cm_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cm_woocommerce_api_keys`
--
ALTER TABLE `cm_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `cm_woocommerce_attribute_taxonomies`
--
ALTER TABLE `cm_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `cm_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `cm_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`);

--
-- Indexes for table `cm_woocommerce_log`
--
ALTER TABLE `cm_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `cm_woocommerce_order_itemmeta`
--
ALTER TABLE `cm_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `cm_woocommerce_order_items`
--
ALTER TABLE `cm_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `cm_woocommerce_payment_tokenmeta`
--
ALTER TABLE `cm_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `cm_woocommerce_payment_tokens`
--
ALTER TABLE `cm_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cm_woocommerce_sessions`
--
ALTER TABLE `cm_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `cm_woocommerce_shipping_zones`
--
ALTER TABLE `cm_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `cm_woocommerce_shipping_zone_locations`
--
ALTER TABLE `cm_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `cm_woocommerce_shipping_zone_methods`
--
ALTER TABLE `cm_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `cm_woocommerce_tax_rates`
--
ALTER TABLE `cm_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `cm_woocommerce_tax_rate_locations`
--
ALTER TABLE `cm_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `cm_yith_wcwl`
--
ALTER TABLE `cm_yith_wcwl`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `prod_id` (`prod_id`);

--
-- Indexes for table `cm_yith_wcwl_lists`
--
ALTER TABLE `cm_yith_wcwl_lists`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `wishlist_token` (`wishlist_token`),
  ADD KEY `wishlist_slug` (`wishlist_slug`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cm_actionscheduler_actions`
--
ALTER TABLE `cm_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `cm_actionscheduler_claims`
--
ALTER TABLE `cm_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_actionscheduler_groups`
--
ALTER TABLE `cm_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cm_actionscheduler_logs`
--
ALTER TABLE `cm_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `cm_commentmeta`
--
ALTER TABLE `cm_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_comments`
--
ALTER TABLE `cm_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_links`
--
ALTER TABLE `cm_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_mailchimp_jobs`
--
ALTER TABLE `cm_mailchimp_jobs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_options`
--
ALTER TABLE `cm_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1041;

--
-- AUTO_INCREMENT for table `cm_postmeta`
--
ALTER TABLE `cm_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1205;

--
-- AUTO_INCREMENT for table `cm_posts`
--
ALTER TABLE `cm_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=453;

--
-- AUTO_INCREMENT for table `cm_termmeta`
--
ALTER TABLE `cm_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `cm_terms`
--
ALTER TABLE `cm_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `cm_term_taxonomy`
--
ALTER TABLE `cm_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `cm_usermeta`
--
ALTER TABLE `cm_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `cm_users`
--
ALTER TABLE `cm_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cm_wc_admin_notes`
--
ALTER TABLE `cm_wc_admin_notes`
  MODIFY `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cm_wc_admin_note_actions`
--
ALTER TABLE `cm_wc_admin_note_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cm_wc_customer_lookup`
--
ALTER TABLE `cm_wc_customer_lookup`
  MODIFY `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_wc_download_log`
--
ALTER TABLE `cm_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_wc_tax_rate_classes`
--
ALTER TABLE `cm_wc_tax_rate_classes`
  MODIFY `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cm_wc_webhooks`
--
ALTER TABLE `cm_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_woocommerce_api_keys`
--
ALTER TABLE `cm_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_woocommerce_attribute_taxonomies`
--
ALTER TABLE `cm_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `cm_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_woocommerce_log`
--
ALTER TABLE `cm_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_woocommerce_order_itemmeta`
--
ALTER TABLE `cm_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_woocommerce_order_items`
--
ALTER TABLE `cm_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_woocommerce_payment_tokenmeta`
--
ALTER TABLE `cm_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_woocommerce_payment_tokens`
--
ALTER TABLE `cm_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_woocommerce_sessions`
--
ALTER TABLE `cm_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_woocommerce_shipping_zones`
--
ALTER TABLE `cm_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cm_woocommerce_shipping_zone_locations`
--
ALTER TABLE `cm_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cm_woocommerce_shipping_zone_methods`
--
ALTER TABLE `cm_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cm_woocommerce_tax_rates`
--
ALTER TABLE `cm_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_woocommerce_tax_rate_locations`
--
ALTER TABLE `cm_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_yith_wcwl`
--
ALTER TABLE `cm_yith_wcwl`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cm_yith_wcwl_lists`
--
ALTER TABLE `cm_yith_wcwl_lists`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cm_wc_download_log`
--
ALTER TABLE `cm_wc_download_log`
  ADD CONSTRAINT `fk_cm_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `cm_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;
--
-- Database: `dialmedinventory`
--
CREATE DATABASE IF NOT EXISTS `dialmedinventory` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dialmedinventory`;

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `name`, `prefix`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'create', 'create', 'Create Access Level', NULL, NULL, NULL, NULL),
(2, 'read', 'read', 'Read Access Level', NULL, NULL, NULL, NULL),
(3, 'update', 'update', 'Update Access Level', NULL, NULL, NULL, NULL),
(4, 'delete', 'delete', 'Delete Access Level', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `module_id` bigint(20) NOT NULL,
  `action` bigint(20) NOT NULL,
  `new_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_data` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2019_08_19_000000_create_failed_jobs_table', 1),
(14, '2020_02_17_120329_create_roles_table', 1),
(15, '2020_02_17_120405_create_types_table', 1),
(16, '2020_02_17_120417_create_user_access_table', 1),
(17, '2020_02_17_120430_create_modules_table', 1),
(18, '2020_02_17_121831_create_role_modules_table', 1),
(19, '2020_02_17_135340_create_access_table', 1),
(20, '2020_02_29_071132_create_logs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` smallint(6) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `prefix`, `icon`, `description`, `order`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Users', 'user', 'fas fa-users', 'User Module', 1, NULL, 1, NULL, '2020-02-29 23:59:09'),
(2, 'User Types', 'type', 'fas fa-users-cog', 'User Type Module', 2, NULL, 1, NULL, '2020-03-01 00:00:19'),
(3, 'Roles', 'role', 'fas fa-user-shield', 'Roles Module', 3, NULL, 1, NULL, '2020-03-01 00:01:50'),
(4, 'Access Rights', 'access', 'fas fa-shield-alt', 'Access Rights Module', 4, NULL, 1, NULL, '2020-03-01 00:00:38'),
(5, 'Modules', 'module', 'fas fa-folder', 'Modules Module', 5, NULL, 1, NULL, '2020-03-01 00:02:18'),
(6, 'Logs', 'log', 'far fa-eye', 'Log/Audit Trail Module', 6, NULL, 1, NULL, '2020-03-01 00:02:53');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `prefix`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Admin Role', 'admin', 'Admin user Type', NULL, NULL, NULL, NULL),
(2, 'Encoder', 'encoder', 'Encoder user Type', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_modules`
--

CREATE TABLE `role_modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `module_id` bigint(20) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_modules`
--

INSERT INTO `role_modules` (`id`, `role_id`, `module_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL, NULL, NULL),
(2, 1, 2, NULL, NULL, NULL, NULL),
(3, 1, 3, NULL, NULL, NULL, NULL),
(4, 1, 4, NULL, NULL, NULL, NULL),
(5, 1, 5, NULL, NULL, NULL, NULL),
(6, 1, 6, NULL, NULL, NULL, NULL),
(7, 2, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`, `prefix`, `description`, `role_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'CEO', 'ceo', 'CEO user Type', 1, NULL, NULL, NULL, NULL),
(2, 'Manager', 'manager', 'Manager user Type', 2, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `is_super_admin` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `email_verified_at`, `password`, `type_id`, `is_super_admin`, `status`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'John', 'Doe', 'superadmin@gmail.com', NULL, '$2y$10$CbS62YMVe2M9xyr6hSyPAut67KhQPaswA3xfsbrbNmLIINJC/t6Z.', NULL, '1', '1', NULL, 1, 1, NULL, '2020-02-29 02:02:49'),
(3, 'Admin 1', 'Admin', 'ryandingle09@gmail.com', NULL, '$2y$10$ZpkTpsCwlw4tMLgmR/OGGO9L0QCXZa2d2zZU5FZDbzaod/UPpPfm2', 2, '0', '1', NULL, NULL, 3, '2020-03-01 00:08:40', '2020-03-06 22:04:21');

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE `user_access` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `access_id` bigint(20) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_access`
--

INSERT INTO `user_access` (`id`, `user_id`, `access_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(9, 2, 1, 1, NULL, '2020-03-01 00:03:22', '2020-03-01 00:03:22'),
(10, 2, 2, 1, NULL, '2020-03-01 00:03:22', '2020-03-01 00:03:22'),
(11, 2, 3, 1, NULL, '2020-03-01 00:03:22', '2020-03-01 00:03:22'),
(12, 2, 4, 1, NULL, '2020-03-01 00:03:22', '2020-03-01 00:03:22'),
(17, 3, 1, 3, NULL, '2020-03-06 22:04:21', '2020-03-06 22:04:21'),
(18, 3, 2, 3, NULL, '2020-03-06 22:04:21', '2020-03-06 22:04:21'),
(19, 3, 3, 3, NULL, '2020-03-06 22:04:21', '2020-03-06 22:04:21'),
(20, 3, 4, 3, NULL, '2020-03-06 22:04:21', '2020-03-06 22:04:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `access_prefix_unique` (`prefix`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `modules_prefix_unique` (`prefix`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_prefix_unique` (`prefix`);

--
-- Indexes for table `role_modules`
--
ALTER TABLE `role_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `types_prefix_unique` (`prefix`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_access`
--
ALTER TABLE `user_access`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_modules`
--
ALTER TABLE `role_modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_access`
--
ALTER TABLE `user_access`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Database: `sabongpinoy`
--
CREATE DATABASE IF NOT EXISTS `sabongpinoy` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sabongpinoy`;

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `name`, `prefix`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'create', 'create', 'Create Access Level', NULL, NULL, NULL, NULL),
(2, 'read', 'read', 'Read Access Level', NULL, NULL, NULL, NULL),
(3, 'update', 'update', 'Update Access Level', NULL, NULL, NULL, NULL),
(4, 'delete', 'delete', 'Delete Access Level', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `arenas`
--

CREATE TABLE `arenas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `arenas`
--

INSERT INTO `arenas` (`id`, `name`, `location`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'BULACAN COCK DERBY', 'STA. MARIA BULACAN', '2020-03-20 04:02:20', '2020-03-20 04:59:24', 1, 1),
(2, 'BACCARAT COCK DERBY', 'BACCARAT, MANILA', '2020-03-20 04:03:37', '2020-03-20 04:59:32', 1, 1),
(3, 'GEN X CUP 4 COCK DERBY', 'STA. ANA MANILA', '2020-03-20 04:06:38', '2020-03-20 04:59:16', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `room_id` bigint(20) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `room_id`, `message`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 2, 'fwfewef', '2020-03-21 06:44:32', '2020-03-21 06:44:32', 1, NULL),
(2, 2, 'qwdq', '2020-03-21 06:46:24', '2020-03-21 06:46:24', 1, NULL),
(3, 2, 'khhkj', '2020-03-21 06:47:50', '2020-03-21 06:47:50', 1, NULL),
(4, 1, 'test', '2020-03-21 06:48:10', '2020-03-21 06:48:10', 1, NULL),
(5, 1, 'test2', '2020-03-21 06:49:48', '2020-03-21 06:49:48', 1, NULL),
(6, 2, 'test hrer', '2020-03-21 06:50:08', '2020-03-21 06:50:08', 1, NULL),
(7, 1, 'hjkjh', '2020-03-21 06:53:44', '2020-03-21 06:53:44', 1, NULL),
(8, 1, 'kjhhk', '2020-03-21 06:53:45', '2020-03-21 06:53:45', 1, NULL),
(9, 1, 'kjkhhkj', '2020-03-21 06:53:46', '2020-03-21 06:53:46', 1, NULL),
(10, 1, 'qwd', '2020-03-21 06:58:20', '2020-03-21 06:58:20', 1, NULL),
(11, 1, 'a', '2020-03-21 06:58:24', '2020-03-21 06:58:24', 1, NULL),
(12, 1, 'qwdqd', '2020-03-21 06:59:51', '2020-03-21 06:59:51', 1, NULL),
(13, 1, 'a', '2020-03-21 07:03:21', '2020-03-21 07:03:21', 1, NULL),
(14, 1, 'b', '2020-03-21 07:04:07', '2020-03-21 07:04:07', 1, NULL),
(15, 1, 'fwef', '2020-03-21 07:08:33', '2020-03-21 07:08:33', 1, NULL),
(16, 1, 'qw', '2020-03-21 07:10:48', '2020-03-21 07:10:48', 1, NULL),
(17, 1, 'qwfwef', '2020-03-21 07:11:39', '2020-03-21 07:11:39', 1, NULL),
(18, 2, 'hei', '2020-03-21 07:21:23', '2020-03-21 07:21:23', 3, NULL),
(19, 1, 'kamusta', '2020-03-21 07:21:49', '2020-03-21 07:21:49', 3, NULL),
(20, 1, 'wdwad', '2020-03-21 07:22:39', '2020-03-21 07:22:39', 3, NULL),
(21, 1, 'hmm', '2020-03-21 07:23:31', '2020-03-21 07:23:31', 3, NULL),
(22, 1, 'sige', '2020-03-21 07:23:51', '2020-03-21 07:23:51', 1, NULL),
(23, 1, 'im test message', '2020-03-21 07:28:26', '2020-03-21 07:28:26', 3, NULL),
(24, 1, 'this is also a test from admin', '2020-03-21 07:28:51', '2020-03-21 07:28:51', 1, NULL),
(25, 1, 'nice one', '2020-03-21 07:34:15', '2020-03-21 07:34:15', 1, NULL),
(26, 1, 'nice 2', '2020-03-21 07:34:25', '2020-03-21 07:34:25', 3, NULL),
(27, 1, 'fwfwf wefwefew wefewfewfewffwfwf wefwefew wefewfewfewffwfwf wefwefew wefewfewfewf fwfwf wefwefew wefewfewfewf', '2020-03-21 07:41:34', '2020-03-21 07:41:34', 1, NULL),
(28, 1, 'wdwd', '2020-03-21 07:52:18', '2020-03-21 07:52:18', 1, NULL),
(29, 1, 'hi', '2020-03-21 07:57:21', '2020-03-21 07:57:21', 1, NULL),
(30, 1, 'this is a test', '2020-03-21 07:58:23', '2020-03-21 07:58:23', 1, NULL),
(31, 1, 'wefwef', '2020-03-21 08:00:00', '2020-03-21 08:00:00', 3, NULL),
(32, 1, 'to test again', '2020-03-21 08:00:22', '2020-03-21 08:00:22', 1, NULL),
(33, 1, 'hello', '2020-03-21 08:37:05', '2020-03-21 08:37:05', 3, NULL),
(34, 1, 'test', '2020-03-21 08:39:13', '2020-03-21 08:39:13', 1, NULL),
(35, 1, 'qdqdwqd', '2020-03-21 08:41:46', '2020-03-21 08:41:46', 1, NULL),
(36, 1, 'wefwef', '2020-03-21 08:45:16', '2020-03-21 08:45:16', 1, NULL),
(37, 1, 'this a test for time', '2020-03-21 08:48:42', '2020-03-21 08:48:42', 3, NULL),
(38, 1, 'qwdwqdqwdqwd', '2020-03-21 09:26:24', '2020-03-21 09:26:24', 1, NULL),
(39, 1, 'my message should see updated', '2020-03-21 09:41:45', '2020-03-21 09:41:45', 3, NULL),
(40, 1, 'cahf;elwkf', '2020-03-21 09:51:36', '2020-03-21 09:51:36', 3, NULL),
(41, 1, 'wehfwef', '2020-03-21 09:51:39', '2020-03-21 09:51:39', 3, NULL),
(42, 1, 'ewfwf', '2020-03-21 09:51:40', '2020-03-21 09:51:40', 3, NULL),
(43, 1, 'meron wins', '2020-03-21 10:04:40', '2020-03-21 10:04:40', 3, NULL),
(44, 1, 'wefwf', '2020-03-21 10:04:47', '2020-03-21 10:04:47', 3, NULL),
(45, 1, 'this is a test', '2020-03-21 10:05:27', '2020-03-21 10:05:27', 3, NULL),
(46, 1, 'ok lets go', '2020-03-21 10:05:40', '2020-03-21 10:05:40', 1, NULL),
(47, 1, 'ger', '2020-03-21 10:08:27', '2020-03-21 10:08:27', 1, NULL),
(48, 1, 'this is a test flight', '2020-03-21 10:16:07', '2020-03-21 10:16:07', 1, NULL),
(49, 1, ':D', '2020-03-21 10:16:14', '2020-03-21 10:16:14', 1, NULL),
(50, 1, 'hi', '2020-03-21 10:30:17', '2020-03-21 10:30:17', 1, NULL),
(51, 1, 'kamusta mga parekoy', '2020-03-21 10:31:52', '2020-03-21 10:31:52', 3, NULL),
(52, 1, 'pauwi nako', '2020-03-21 10:32:04', '2020-03-21 10:32:04', 1, NULL),
(53, 1, 'haha ok sige ingat', '2020-03-21 10:32:14', '2020-03-21 10:32:14', 3, NULL),
(54, 2, 'kamusta ?', '2020-03-21 10:34:39', '2020-03-21 10:34:39', 3, NULL),
(55, 2, 'dito ka rin ?', '2020-03-21 10:34:53', '2020-03-21 10:34:53', 1, NULL),
(56, 2, 'hahaha', '2020-03-21 10:34:55', '2020-03-21 10:34:55', 1, NULL),
(57, 2, 'ou dalwa taya ako', '2020-03-21 10:35:02', '2020-03-21 10:35:02', 3, NULL),
(58, 2, 'ryanlwf', '2020-03-21 10:38:17', '2020-03-21 10:38:17', 3, NULL),
(59, 2, 'panalo na pula', '2020-03-21 10:38:30', '2020-03-21 10:38:30', 1, NULL),
(60, 2, 'panalo meron', '2020-03-21 10:39:36', '2020-03-21 10:39:36', 2, NULL),
(61, 2, 'dpa boss', '2020-03-21 10:39:56', '2020-03-21 10:39:56', 3, NULL),
(62, 2, 'https://youtube.com', '2020-03-21 10:42:10', '2020-03-21 10:42:10', 2, NULL),
(63, 2, 'fwf', '2020-03-21 10:46:58', '2020-03-21 10:46:58', 1, NULL),
(64, 2, 'wfwef', '2020-03-21 10:46:59', '2020-03-21 10:46:59', 1, NULL),
(65, 2, 'wefwef', '2020-03-21 10:46:59', '2020-03-21 10:46:59', 1, NULL),
(66, 2, 'wefwe', '2020-03-21 10:46:59', '2020-03-21 10:46:59', 1, NULL),
(67, 2, 'wefwef', '2020-03-21 10:47:00', '2020-03-21 10:47:00', 1, NULL),
(68, 2, 'wefwef', '2020-03-21 10:47:00', '2020-03-21 10:47:00', 1, NULL),
(69, 2, 'wefwef', '2020-03-21 10:47:01', '2020-03-21 10:47:01', 1, NULL),
(70, 2, 'wefwef', '2020-03-21 10:47:01', '2020-03-21 10:47:01', 1, NULL),
(71, 2, 'wefwef', '2020-03-21 10:47:02', '2020-03-21 10:47:02', 1, NULL),
(72, 2, 'wefwf', '2020-03-21 10:47:02', '2020-03-21 10:47:02', 1, NULL),
(73, 1, 'wfwef', '2020-03-21 11:41:48', '2020-03-21 11:41:48', 1, NULL),
(74, 1, 'wefwfef', '2020-03-21 11:42:23', '2020-03-21 11:42:23', 3, NULL),
(75, 1, 'teest eto', '2020-03-21 11:42:50', '2020-03-21 11:42:50', 1, NULL),
(76, 1, 'admin ako', '2020-03-21 11:46:45', '2020-03-21 11:46:45', 1, NULL),
(77, 1, 'test', '2020-03-21 11:48:35', '2020-03-21 11:48:35', 3, NULL),
(78, 1, 'akin to', '2020-03-21 11:53:53', '2020-03-21 11:53:53', 1, NULL),
(79, 1, 'last dapat to', '2020-03-21 12:01:17', '2020-03-21 12:01:17', 1, NULL),
(80, 1, 'last for this morning as chat by test user', '2020-03-21 12:20:23', '2020-03-21 12:20:23', 1, NULL),
(81, 1, 'edi wowow week haha', '2020-03-21 12:20:38', '2020-03-21 12:20:38', 3, NULL),
(82, 1, 'ok', '2020-03-21 12:20:42', '2020-03-21 12:20:42', 1, NULL),
(83, 1, 'game', '2020-03-22 07:26:50', '2020-03-22 07:26:50', 1, NULL),
(84, 1, 'klj', '2020-03-22 07:47:58', '2020-03-22 07:47:58', 1, NULL),
(85, 3, 'erg', '2020-03-22 07:53:14', '2020-03-22 07:53:14', 1, NULL),
(86, 3, 'this ia test', '2020-03-22 07:53:22', '2020-03-22 07:53:22', 1, NULL),
(87, 1, 'load kona sau boss dhaks', '2020-03-22 08:26:34', '2020-03-22 08:26:34', 1, NULL),
(88, 1, 'este boss ivana alawi', '2020-03-22 08:26:55', '2020-03-22 08:26:55', 1, NULL),
(89, 1, 'sige po .. G load mona para makataya na', '2020-03-22 08:27:54', '2020-03-22 08:27:54', 3, NULL),
(90, 1, 'ok load coming..', '2020-03-22 08:28:15', '2020-03-22 08:28:15', 1, NULL),
(91, 1, 'ok sana e', '2020-03-22 08:36:23', '2020-03-22 08:36:23', 3, NULL),
(92, 1, 'let go', '2020-03-22 08:41:55', '2020-03-22 08:41:55', 1, NULL),
(93, 1, 'talo pula', '2020-03-22 08:42:01', '2020-03-22 08:42:01', 1, NULL),
(94, 1, 'awti', '2020-03-23 05:53:33', '2020-03-23 05:53:33', 3, NULL),
(95, 1, 'sali boss amo', '2020-03-23 06:01:47', '2020-03-23 06:01:47', 2, NULL),
(96, 1, 'wefwef', '2020-03-23 06:01:53', '2020-03-23 06:01:53', 2, NULL),
(97, 1, 'tst', '2020-03-23 22:37:22', '2020-03-23 22:37:22', 3, NULL),
(98, 1, 'wefe', '2020-03-23 22:40:50', '2020-03-23 22:40:50', 2, NULL),
(99, 1, 'test again', '2020-03-23 22:41:32', '2020-03-23 22:41:32', 2, NULL),
(100, 1, 'taya na', '2020-03-23 22:53:23', '2020-03-23 22:53:23', 2, NULL),
(101, 1, 'copy boss amo', '2020-03-23 22:53:32', '2020-03-23 22:53:32', 3, NULL),
(102, 1, 'ok start napo tayo', '2020-03-23 22:54:24', '2020-03-23 22:54:24', 5, NULL),
(103, 1, 'kamusta mga boss amo', '2020-03-24 01:17:27', '2020-03-24 01:17:27', 3, NULL),
(104, 2, 'hello', '2020-03-25 20:18:51', '2020-03-25 20:18:51', 3, NULL),
(105, 1, 'THIS IS JUST A TEST', '2020-03-26 05:03:04', '2020-03-26 05:03:04', 3, NULL),
(106, 1, 'let get ready to rumble', '2020-03-28 06:40:13', '2020-03-28 06:40:13', 3, NULL),
(107, 2, 'hi', '2020-03-29 04:53:01', '2020-03-29 04:53:01', 3, NULL),
(108, 1, 'hello 2 with token', '2020-03-29 04:57:18', '2020-03-29 04:57:18', 3, NULL),
(109, 1, 'test', '2020-04-05 05:30:04', '2020-04-05 05:30:04', 4, NULL),
(110, 1, 'start napo tayo', '2020-04-05 05:46:26', '2020-04-05 05:46:26', 2, NULL),
(111, 1, 'new player habol', '2020-04-05 06:06:16', '2020-04-05 06:06:16', 6, NULL),
(112, 1, 'welcome test 3', '2020-04-05 06:06:29', '2020-04-05 06:06:29', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` bigint(20) DEFAULT NULL,
  `expected_deal` int(11) DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  `is_broadcast` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover_image` text COLLATE utf8mb4_unicode_ci,
  `cover_image_full_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `location`, `expected_deal`, `event_date`, `is_broadcast`, `description`, `url`, `cover_image`, `cover_image_full_path`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'ARENA | GENX CUP 4 COCK DERBY', 1, 20, '2020-04-06 00:47:49', 1, 'Live at BACCARAT LIVE with 550 Expected Deals Watch Now!! <br>\r\nLive at ARENA DE NCOV with 80 Expected Fights Watch Now!!<br>\r\n\r\nExpected Deals : 550', 'http://hello.coms', '/storage/events/bdxrPtdVsMPcar76yCiOffMxdfwpAWonfjpGUHwb.jpeg', 'C:\\Users\\Ryan Dingle\\workspace\\sabongpinoy\\storage\\app/public/events/bdxrPtdVsMPcar76yCiOffMxdfwpAWonfjpGUHwb.jpeg', '2020-03-14 08:48:14', '2020-03-19 09:01:56', 1, 2),
(2, 'BULACAN LIVE | CARD DEAL', 2, 200, '2020-04-07 00:48:01', 1, 'this is just a test', 'https://youtube.com/v=fwefwef', '/storage/events/s7Cs03YhPudLnuGCRKkydmsBrrqTWcusJxN6Py2e.jpeg', 'C:\\Users\\Ryan Dingle\\workspace\\sabongpinoy\\storage\\app/public/events/s7Cs03YhPudLnuGCRKkydmsBrrqTWcusJxN6Py2e.jpeg', '2020-03-17 09:23:14', '2020-03-19 08:45:58', 5, 2),
(3, 'BACCARAT LIVE | CARD DEAL', 3, 34, '2020-04-08 00:35:51', 1, 'qwddwqdqdqwd', 'http://hello.com', '/storage/events/gaWvI3skI3Z0kmgX36klZ9Z2pM70AlW454qgcDIi.jpeg', 'C:\\Users\\Ryan Dingle\\workspace\\sabongpinoy\\storage\\app/public/events/gaWvI3skI3Z0kmgX36klZ9Z2pM70AlW454qgcDIi.jpeg', '2020-03-19 02:45:58', '2020-03-19 08:56:49', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `game_bettings`
--

CREATE TABLE `game_bettings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_id` bigint(20) DEFAULT NULL,
  `game_count` int(11) DEFAULT NULL,
  `odds` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `game_bettings`
--

INSERT INTO `game_bettings` (`id`, `user_id`, `amount`, `action`, `event_id`, `game_count`, `odds`, `type`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(2, 3, 2000.00, 'add-bet', 1, 17, '10-10', 'meron', '2020-04-06 17:10:00', '2020-04-06 17:10:00', 3, NULL),
(3, 4, 2000.00, 'add-bet', 1, 17, '10-10', 'meron', '2020-04-06 17:10:11', '2020-04-06 17:10:11', 4, NULL),
(4, 6, 3800.00, 'add-bet', 1, 17, '10-10', 'wala', '2020-04-06 17:10:39', '2020-04-06 17:10:39', 6, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `game_logs`
--

CREATE TABLE `game_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `game_count` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `winner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'close',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `game_logs`
--

INSERT INTO `game_logs` (`id`, `game_count`, `event_id`, `winner`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(20, '1', '1', 'WD', 'close', '2020-04-04 07:38:51', '2020-04-04 08:37:50', 2, 2),
(26, '2', '1', 'C', 'close', '2020-04-04 08:55:22', '2020-04-04 09:19:49', 2, 2),
(27, '3', '1', 'D', 'close', '2020-04-04 09:19:54', '2020-04-04 09:22:08', 2, 2),
(28, '4', '1', 'D', 'close', '2020-04-04 09:22:11', '2020-04-04 10:21:39', 2, NULL),
(29, '5', '1', 'MD', 'close', '2020-04-04 10:22:14', '2020-04-04 10:23:12', 2, 2),
(30, '6', '1', 'MD', 'close', '2020-04-04 10:23:24', '2020-04-04 10:38:23', 2, NULL),
(31, '7', '1', 'C', 'close', '2020-04-04 10:42:51', '2020-04-04 10:43:04', 2, NULL),
(32, '8', '1', 'WL', 'close', '2020-04-04 10:43:48', '2020-04-04 10:43:57', 2, NULL),
(33, '9', '1', 'C', 'close', '2020-04-05 00:43:02', '2020-04-05 00:47:51', 2, NULL),
(34, '10', '1', 'D', 'close', '2020-04-05 00:48:02', '2020-04-05 00:48:37', 2, NULL),
(35, '11', '1', 'MD', 'close', '2020-04-05 00:48:47', '2020-04-05 00:48:50', 2, NULL),
(36, '12', '1', 'MD', 'close', '2020-04-05 00:49:09', '2020-04-05 00:49:13', 2, NULL),
(37, '13', '1', 'WL', 'close', '2020-04-05 00:49:16', '2020-04-05 00:49:19', 2, NULL),
(38, '14', '1', 'WD', 'close', '2020-04-05 00:49:21', '2020-04-05 00:49:32', 2, NULL),
(39, '15', '1', 'ML', 'close', '2020-04-05 00:50:55', '2020-04-05 00:51:16', 2, NULL),
(40, '16', '1', 'MD', 'close', '2020-04-05 00:51:26', '2020-04-05 01:03:58', 2, 2),
(41, '17', '1', NULL, 'open', '2020-04-05 01:04:01', '2020-04-05 05:40:13', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `module_id` bigint(20) NOT NULL,
  `action` bigint(20) NOT NULL,
  `new_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_data` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2014_10_12_000000_create_users_table', 1),
(14, '2014_10_12_100000_create_password_resets_table', 1),
(15, '2019_08_19_000000_create_failed_jobs_table', 1),
(16, '2020_02_17_120329_create_roles_table', 1),
(17, '2020_02_17_120405_create_types_table', 1),
(18, '2020_02_17_120417_create_user_access_table', 1),
(19, '2020_02_17_120430_create_modules_table', 1),
(20, '2020_02_17_121831_create_role_modules_table', 1),
(21, '2020_02_17_135340_create_access_table', 1),
(22, '2020_02_29_071132_create_logs_table', 1),
(24, '2020_03_14_153134_create_events_table', 2),
(25, '2020_03_14_185645_create_user_money_logs', 3),
(26, '2020_03_13_175456_create_user_details_table', 4),
(28, '2020_03_20_113815_create_arenas_table', 5),
(29, '2020_03_21_115037_create_chats_table', 6),
(30, '2020_03_28_171414_add_api_token_on_users', 7),
(31, '2020_03_28_180235_add_game_action_on_user_money_logs', 8),
(32, '2020_03_28_180747_create_game_logs', 8),
(33, '2020_03_29_155943_create_user_hits', 9),
(34, '2016_06_01_000001_create_oauth_auth_codes_table', 10),
(35, '2016_06_01_000002_create_oauth_access_tokens_table', 10),
(36, '2016_06_01_000003_create_oauth_refresh_tokens_table', 10),
(37, '2016_06_01_000004_create_oauth_clients_table', 10),
(38, '2016_06_01_000005_create_oauth_personal_access_clients_table', 10),
(39, '2020_04_07_005622_create_game_bettings', 11);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` smallint(6) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `prefix`, `icon`, `description`, `order`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Users', 'user', 'fas fa-users', 'User Module', 1, NULL, NULL, NULL, NULL),
(2, 'User Types', 'type', 'fas fa-users-cog', 'User Type Module', 2, NULL, NULL, NULL, NULL),
(3, 'Roles', 'role', 'fas fa-user-shield', 'Roles Module', 3, NULL, NULL, NULL, NULL),
(4, 'Access Rights', 'access', 'fas fa-shield-alt', 'Access Rights Module', 4, NULL, NULL, NULL, NULL),
(5, 'Modules', 'module', 'fas fa-folder', 'Modules Module', 5, NULL, NULL, NULL, NULL),
(6, 'Logs', 'system-log', 'far fa-eye', 'Log/Audit Trail Module', 6, NULL, NULL, NULL, NULL),
(7, 'Event Setup', 'game-event', 'fas fa-video', 'Event Setup Module', 7, 1, 2, '2020-03-06 15:10:31', '2020-03-17 10:41:12'),
(9, 'Accounting', 'accounting', 'fas fa-money-check', 'Accounting Module', 9, 1, NULL, '2020-03-06 15:17:33', '2020-03-06 15:17:33'),
(10, 'Activation', 'activation', 'fas fa-screwdriver', 'Activation Module', 10, 1, NULL, '2020-03-06 15:19:56', '2020-03-06 15:19:56'),
(13, 'Betting history', 'betting', 'fas fa-history', 'Betting History Module', 13, 1, NULL, '2020-03-06 15:26:37', '2020-03-06 15:26:37'),
(17, 'Cashout/Loading/Transfer', 'user-management', 'far fa-user-circle', 'User Management Module for Cashout, Pasaload/Transfer and Loading Points', 13, 1, 1, '2020-03-15 03:16:50', '2020-03-15 08:14:06'),
(18, 'Hits Checker', 'hits', 'fas fa-list', 'Hits Checker History', 12, 1, NULL, '2020-03-16 03:00:29', '2020-03-16 03:00:29'),
(19, 'Transaction History', 'history', 'fas fa-list', 'Transaction history Module', 13, 1, NULL, '2020-03-16 03:02:35', '2020-03-16 03:02:35'),
(20, 'Arena', 'arena', 'fas fa-location-arrow', 'Arena Module', 14, 2, NULL, '2020-03-20 03:48:23', '2020-03-20 03:48:23');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `prefix`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Admin Role', 'admin', 'Admin user Type', NULL, 1, NULL, '2020-03-15 05:02:20'),
(2, 'CSR', 'encoder', 'Encoder user Type', NULL, 1, NULL, '2020-03-20 07:52:42'),
(3, 'Player', 'player', 'Player Modules List', 1, 1, '2020-03-16 02:57:49', '2020-03-16 03:03:56');

-- --------------------------------------------------------

--
-- Table structure for table `role_modules`
--

CREATE TABLE `role_modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `module_id` bigint(20) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_modules`
--

INSERT INTO `role_modules` (`id`, `role_id`, `module_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(24, 1, 1, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(25, 1, 2, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(26, 1, 3, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(27, 1, 4, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(28, 1, 5, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(29, 1, 6, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(30, 1, 7, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(31, 1, 8, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(32, 1, 9, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(33, 1, 10, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(34, 1, 12, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(35, 1, 13, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(36, 1, 17, 1, NULL, '2020-03-15 05:02:20', '2020-03-15 05:02:20'),
(41, 3, 13, 1, NULL, '2020-03-16 03:03:56', '2020-03-16 03:03:56'),
(42, 3, 18, 1, NULL, '2020-03-16 03:03:56', '2020-03-16 03:03:56'),
(43, 3, 19, 1, NULL, '2020-03-16 03:03:56', '2020-03-16 03:03:56'),
(44, 2, 7, 1, NULL, '2020-03-20 07:52:42', '2020-03-20 07:52:42'),
(45, 2, 10, 1, NULL, '2020-03-20 07:52:42', '2020-03-20 07:52:42'),
(46, 2, 17, 1, NULL, '2020-03-20 07:52:42', '2020-03-20 07:52:42'),
(47, 2, 20, 1, NULL, '2020-03-20 07:52:42', '2020-03-20 07:52:42');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`, `prefix`, `description`, `role_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'Admin user Type', 1, NULL, 2, NULL, '2020-03-18 09:21:54'),
(2, 'CSR', 'csr', 'CSR user Type', 2, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `points` float NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `is_super_admin` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_admin` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `points`, `email`, `email_verified_at`, `password`, `api_token`, `type_id`, `is_super_admin`, `is_admin`, `status`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'John', 'Doe', 0, 'superadmin@gmail.com', NULL, '$2y$10$gqwHXnNMBKwoyE7J1BViyOF9igHq.dyfkAkpgHUSQlnEO7Q1SZTje', NULL, NULL, '1', '1', '1', NULL, 1, 1, NULL, '2020-03-15 10:23:28'),
(2, 'Ryan', 'Dingle', 0, 'ryandingle09@gmail.com', NULL, '$2y$10$gBygSckItRCCwML1gAxPuenm7KZpmyBoYpjNL/2pgqw5.TMgq4V3q', '70224ec73cb9870bb141102a28a46e8fa0e598bd16532d451e4d66c516f88c10', 1, '0', '1', '1', 'Vo3N8t5g0a7SarZOQEIelkdpOY9eo7u2DHEn8402zK5EOkUKqLkZmGhGUIdJ', NULL, 2, '2020-03-14 05:59:45', '2020-04-06 04:08:52'),
(3, 'Test', 'Test', 98020, 'test@gmail.com', NULL, '$2y$10$JyfYFbPq6C36G0gIHSoBH.dwQrHbxLVARF4QGybLj.l8bzp3qAHdi', 'bae03f5a739cdf24a7ce6802d19938e51680b86dc870b9c65b81d31b8deb2564', NULL, '0', '0', '1', NULL, NULL, 3, '2020-03-14 06:01:13', '2020-04-06 17:10:00'),
(4, 'Test', 'Test 2', 98060, 'test2@gmail.com', NULL, '$2y$10$VDg16GiLzV3UmiUM9VjsseVt4JyEk/W3wGxaoJMUK0LvQxS.t5dDW', '6fa5e8493072b5b2869b3ec0e1dc85c909c4634be69d6f432b12f15341142863', NULL, '0', '0', '1', NULL, NULL, 4, '2020-03-14 06:58:20', '2020-04-06 17:10:11'),
(5, 'Ivana', 'Alawi', 0, 'ivana@gmail.com', NULL, '$2y$10$OhasUYDD585c5ax4E04hde7EhpXpitIwJiqk0NQSBqICKLJpmXoHq', 'f0219c16366f82da807cd2c8787c86187c5f5b756ee64d7584415c85c986aca7', 2, '0', '1', '1', NULL, NULL, 5, '2020-03-15 10:13:10', '2020-04-06 15:25:28'),
(6, 'test', 'test3', 96200, 'test3@gmail.com', NULL, '$2y$10$Ydf/fBwBb7/XOhwQPj8ESuPKbflnJXVweuIbs40W706k2wuciQz9e', '499b49caaf24fdb77aa0aa35f2428788f78060b061b5ac1c29b099d3f7cf6589', NULL, '0', '0', '1', NULL, NULL, NULL, '2020-04-05 05:50:17', '2020-04-06 17:10:39');

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE `user_access` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `access_id` bigint(20) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_access`
--

INSERT INTO `user_access` (`id`, `user_id`, `access_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(9, 2, 1, 2, NULL, '2020-03-15 09:56:40', '2020-03-15 09:56:40'),
(10, 2, 2, 2, NULL, '2020-03-15 09:56:40', '2020-03-15 09:56:40'),
(11, 2, 3, 2, NULL, '2020-03-15 09:56:40', '2020-03-15 09:56:40'),
(12, 2, 4, 2, NULL, '2020-03-15 09:56:40', '2020-03-15 09:56:40'),
(13, 5, 1, 2, NULL, '2020-03-15 10:13:10', '2020-03-15 10:13:10'),
(14, 5, 2, 2, NULL, '2020-03-15 10:13:10', '2020-03-15 10:13:10'),
(15, 5, 3, 2, NULL, '2020-03-15 10:13:10', '2020-03-15 10:13:10'),
(16, 5, 4, 2, NULL, '2020-03-15 10:13:10', '2020-03-15 10:13:10');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `messenger_url` text COLLATE utf8mb4_unicode_ci,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `photo_full_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `gender`, `age`, `birth_date`, `religion`, `address`, `mobile`, `messenger_url`, `photo`, `photo_full_path`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(7, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '/storage/photo/5YnCVCRRKkYbDe1UcM1IQz5etA7VBZIuhhvlpFae.jpeg', 'C:\\Users\\Ryan Dingle\\workspace\\sabongpinoy\\storage\\app/public/photo/5YnCVCRRKkYbDe1UcM1IQz5etA7VBZIuhhvlpFae.jpeg', '2020-03-15 09:51:11', '2020-03-15 10:23:28', NULL, 1),
(8, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '/storage/photo/clp6Pjhf3RH7Ap0BElS1XToXRZDbxZrI3Dg7FnfT.jpeg', 'C:\\Users\\Ryan Dingle\\workspace\\sabongpinoy\\storage\\app/public/photo/clp6Pjhf3RH7Ap0BElS1XToXRZDbxZrI3Dg7FnfT.jpeg', '2020-03-15 09:56:40', '2020-03-15 09:56:40', NULL, 2),
(9, 5, NULL, NULL, NULL, NULL, NULL, NULL, 'https://hello.coms', '/storage/photo/NI6rYBbQYCCgzd5NKDWf9idWDBHt4GYIPQkCHfP6.jpeg', 'C:\\Users\\Ryan Dingle\\workspace\\sabongpinoy\\storage\\app/public/photo/NI6rYBbQYCCgzd5NKDWf9idWDBHt4GYIPQkCHfP6.jpeg', '2020-03-15 10:30:51', '2020-04-05 18:04:51', NULL, 5),
(10, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '/storage/photo/no0pG4WKx0YxaPiZmS9lIaJWkurX6jUF6VtSO1zs.jpeg', 'C:\\Users\\Ryan Dingle\\workspace\\sabongpinoy\\storage\\app/public/photo/no0pG4WKx0YxaPiZmS9lIaJWkurX6jUF6VtSO1zs.jpeg', '2020-03-16 00:56:22', '2020-03-16 00:56:22', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_hits`
--

CREATE TABLE `user_hits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `event_id` bigint(20) NOT NULL,
  `game_count` int(11) NOT NULL,
  `odds` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bet_amount` double(8,2) NOT NULL DEFAULT '0.00',
  `earned` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_money_logs`
--

CREATE TABLE `user_money_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `module` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'for pasaload, loading or cashout',
  `operation` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'pasaload(+,-), loading(+) or cashout(-)',
  `to` bigint(20) DEFAULT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_id` bigint(20) DEFAULT NULL,
  `game_count` int(11) DEFAULT NULL,
  `odds` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `earned` float DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'if win=hit or loose=miss',
  `from` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_money_logs`
--

INSERT INTO `user_money_logs` (`id`, `user_id`, `amount`, `module`, `operation`, `to`, `action`, `event_id`, `game_count`, `odds`, `type`, `earned`, `status`, `from`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 4, 1.00, 'loading', '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 05:48:49', '2020-03-15 05:48:49', 1, NULL),
(2, 4, 10.00, 'loading', '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 05:50:39', '2020-03-15 05:50:39', 1, NULL),
(3, 4, 11.00, 'cashout', '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 06:57:19', '2020-03-15 06:57:19', 1, NULL),
(4, 4, 100.00, 'loading', '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 06:57:51', '2020-03-15 06:57:51', 1, NULL),
(5, 4, 0.00, 'loading', '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 06:58:22', '2020-03-15 06:58:22', 1, NULL),
(6, 4, 0.00, 'loading', '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 06:59:03', '2020-03-15 06:59:03', 1, NULL),
(7, 4, 10.00, 'cashout', '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 07:58:21', '2020-03-15 07:58:21', 1, NULL),
(8, 4, 10.00, 'cashout', '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 07:59:57', '2020-03-15 07:59:57', 1, NULL),
(9, 4, 10.00, 'transfer/pasaload', '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 08:04:30', '2020-03-15 08:04:30', 1, NULL),
(10, 3, 10.00, 'transfer/pasaload', '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 08:04:30', '2020-03-15 08:04:30', 1, NULL),
(11, 4, 5.00, 'transfer/pasaload', '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 08:08:07', '2020-03-15 08:08:07', 1, NULL),
(12, 3, 5.00, 'transfer/pasaload', '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 08:08:07', '2020-03-15 08:08:07', 1, NULL),
(13, 4, 5.00, 'transfer/pasaload', '-', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-15 08:09:12', '2020-03-15 08:09:12', 1, NULL),
(14, 3, 5.00, 'transfer/pasaload', '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2020-03-15 08:09:12', '2020-03-15 08:09:12', 1, NULL),
(52, 4, 100000.00, 'loading', '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-04-05 05:41:03', '2020-04-05 05:41:03', 2, NULL),
(53, 3, 100000.00, 'loading', '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-04-05 05:41:11', '2020-04-05 05:41:11', 2, NULL),
(54, 6, 100000.00, 'loading', '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-04-05 05:59:24', '2020-04-05 05:59:24', 2, NULL),
(59, 3, 2000.00, 'betting', '-', NULL, 'add-bet', 1, 17, '10-10', 'meron', NULL, NULL, NULL, '2020-04-06 17:10:00', '2020-04-06 17:10:00', 3, NULL),
(60, 4, 2000.00, 'betting', '-', NULL, 'add-bet', 1, 17, '10-10', 'meron', NULL, NULL, NULL, '2020-04-06 17:10:11', '2020-04-06 17:10:11', 4, NULL),
(61, 6, 3800.00, 'betting', '-', NULL, 'add-bet', 1, 17, '10-10', 'wala', NULL, NULL, NULL, '2020-04-06 17:10:39', '2020-04-06 17:10:39', 6, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `access_prefix_unique` (`prefix`);

--
-- Indexes for table `arenas`
--
ALTER TABLE `arenas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_bettings`
--
ALTER TABLE `game_bettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_logs`
--
ALTER TABLE `game_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `modules_prefix_unique` (`prefix`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_prefix_unique` (`prefix`);

--
-- Indexes for table `role_modules`
--
ALTER TABLE `role_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `types_prefix_unique` (`prefix`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- Indexes for table `user_access`
--
ALTER TABLE `user_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_hits`
--
ALTER TABLE `user_hits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_money_logs`
--
ALTER TABLE `user_money_logs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `arenas`
--
ALTER TABLE `arenas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `game_bettings`
--
ALTER TABLE `game_bettings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `game_logs`
--
ALTER TABLE `game_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_modules`
--
ALTER TABLE `role_modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_access`
--
ALTER TABLE `user_access`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_hits`
--
ALTER TABLE `user_hits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_money_logs`
--
ALTER TABLE `user_money_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
