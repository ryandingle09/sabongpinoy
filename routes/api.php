<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function(Request $request) {
//     return $request->user();
// });

// Route::group(['middleware' => 'auth:api'], function(){

//     Route::get('generate/token','ApiTokenController@update')->name('get-token');
//     Route::get('/user/show/{id}/points','UserController@showPoints')->name('get-points');
//     // Route::get('/game/add-bet','UserController@showPoints')->name('add-bet');
//     // Route::get('/game/cancel-bet','UserController@showPoints')->name('cancel-bet');

//     # Realtime Chat
//     Route::get('/chat', 'ChatController@index');
//     Route::post('/chat', 'ChatController@store')->name('chat-store');
//     Route::get('/chat/next', 'ChatController@nextPage')->name('next-page');

//     # Realtime Betting
//     Route::post('/game/add-bet', 'GameController@addBet')->name('add-bet');
//     Route::post('/game/cancel-bet', 'GameController@cancelBet')->name('cancel-bet');
//     Route::get('/game/get-total-per-type', 'GameController@getTotalPerType')->name('get-total-per-type');

//     # Admin Announce Control
//     Route::post('/announce/status', 'GameController@status')->name('announce');

// });
