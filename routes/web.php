<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Not authenticated routes

if(Auth::check()){
    return redirect('dashboard');
}else{
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/contact', 'ContactController@index')->name('contact');
    Route::get('/disclaimer', 'DisclaimerController@index')->name('disclaimer');
    Route::get('/about', 'AboutController@index')->name('about');
    Route::get('/terms', 'TermsConditionController@index')->name('terms');
    Route::get('/events', 'EventController@index')->name('event-web');
    Auth::routes();
}

# Auth::routes(['register' => false]);
// end --

// Manual Auth
Route::post('/login-check', 'LoginController@authenticate')->name('login-active');
Route::post('/register-check', 'RegisterController@store')->name('register-user');

// Route::controller('datatables', 'UserController', [
//     'anyData'  => 'datatables.data',
//     'getIndex' => 'datatables',
// ]);

Route::get('/users-list', 'UserController@anyData')->name('datatables.data');

// Authenticated routes
Route::group(['middleware' => 'auth'], function(){

    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    Route::get('/profile', 'AccountController@index')->name('profile');
    Route::post('/profile', 'AccountController@update')->name('profile-update');

    Route::get('/event/{id}', 'EventController@show')->name('event-web-show');
    // Route::get('/event/test/{id}', 'EventController@show_test')->name('event-web-show-test');

    // settings module (users, user roles, user types, user access level, modules) --
    Route::group(['middleware' => 'role'], function(){

        # Settings
        Route::get('/setting', 'SettingController@index')->name('setting');
        Route::post('/setting', 'SettingController@update')->name('setting-update');

        # Transaction History
        Route::get('/history', 'TransactionHistoryController@index')->name('history');
        Route::get('/history/{id}/show', 'TransactionHistoryController@show')->name('history-show');

        # Hits Checker checker
        Route::get('/hits', 'HitCheckerController@index')->name('hits');
        Route::get('/hits/{id}/show', 'HitCheckerController@show')->name('hits-show');

        # Users
        Route::get('/users', 'UserController@index')->name('user');
        Route::get('/users/create', 'UserController@create')->name('user-create');
        Route::post('/users/store', 'UserController@store')->name('user-store');
        Route::get('/users/{id}/show', 'UserController@show')->name('user-show');
        Route::get('/users/{id}/edit', 'UserController@edit')->name('user-edit');
        Route::post('/users/{id}/update', 'UserController@update')->name('user-update');
        Route::post('/users/{id}/destroy', 'UserController@destroy')->name('user-destroy');

        # Users Roles
        Route::get('/roles', 'RoleController@index')->name('role');
        Route::get('/roles/create', 'RoleController@create')->name('role-create');
        Route::post('/roles/store', 'RoleController@store')->name('role-store');
        Route::get('/roles/{id}/show', 'RoleController@show')->name('role-show');
        Route::get('/roles/{id}/edit', 'RoleController@edit')->name('role-edit');
        Route::post('/roles/{id}//update', 'RoleController@update')->name('role-update');
        Route::post('/roles/{id}/destroy', 'RoleController@destroy')->name('role-destroy');

        # Users Types
        Route::get('/types', 'TypeController@index')->name('type');
        Route::get('/types/create', 'TypeController@create')->name('type-create');
        Route::post('/types/store', 'TypeController@store')->name('type-store');
        Route::get('/types/{id}/show', 'TypeController@show')->name('type-show');
        Route::get('/types/{id}/edit', 'TypeController@edit')->name('type-edit');
        Route::post('/types/{id}/update', 'TypeController@update')->name('type-update');
        Route::post('/types/{id}/destroy', 'TypeController@destroy')->name('type-destroy');

        # Users Access
        Route::get('/access', 'AccessController@index')->name('access');
        Route::get('/access/create', 'AccessController@create')->name('access-create');
        Route::post('/access/store', 'AccessController@store')->name('access-store');
        Route::get('/access/{id}/show', 'AccessController@show')->name('access-show');
        Route::get('/access/{id}/edit', 'AccessController@edit')->name('access-edit');
        Route::post('/access/{id}/update', 'AccessController@update')->name('access-update');
        Route::post('/access/{id}/destroy', 'AccessController@destroy')->name('access-destroy');

        # Modules
        Route::get('/modules', 'ModuleController@index')->name('module');
        Route::get('/modules/create', 'ModuleController@create')->name('module-create');
        Route::post('/modules/store', 'ModuleController@store')->name('module-store');
        Route::get('/modules/{id}/show', 'ModuleController@show')->name('module-show');
        Route::get('/modules/{id}/edit', 'ModuleController@edit')->name('module-edit');
        Route::post('/modules/{id}/update', 'ModuleController@update')->name('module-update');
        Route::post('/modules/{id}/destroy', 'ModuleController@destroy')->name('module-destroy');

        # Logging/Audit Trail
        Route::get('/logs', 'LogController@index')->name('system-log');
        Route::get('/logs/{id}/show', 'LogController@show')->name('system-log-show');

        # Loading
        Route::get('/loadings', 'LoadingController@index')->name('loading');
        Route::get('/loading/create', 'LoadingController@create')->name('loading-create');
        Route::post('/loading/store', 'LoadingController@store')->name('loading-store');
        Route::get('/loading/{id}/show', 'LoadingController@show')->name('loading-show');
        Route::get('/loading/{id}/edit', 'LoadingController@edit')->name('loading-edit');
        Route::post('/loading/{id}/update', 'LoadingController@update')->name('loading-update');
        Route::post('/loading/{id}/destroy', 'LoadingController@destroy')->name('loading-destroy');

        # Cashout
        Route::get('/cashouts', 'CashoutController@index')->name('cashout');
        Route::get('/cashout/create', 'CashoutController@create')->name('cashout-create');
        Route::post('/cashout/store', 'CashoutController@store')->name('cashout-store');
        Route::get('/cashout/{id}/show', 'CashoutController@show')->name('cashout-show');
        Route::get('/cashout/{id}/edit', 'CashoutController@edit')->name('cashout-edit');
        Route::post('/cashout/{id}/update', 'CashoutController@update')->name('cashout-update');
        Route::post('/cashout/{id}/destroy', 'CashoutController@destroy')->name('cashout-destroy');

        # Transfer Pasaload
        Route::get('/transfer-pasaloads', 'TransferPasaloadController@index')->name('transfer-pasaload');
        Route::get('/transfer-pasaload/create', 'TransferPasaloadController@create')->name('transfer-pasaload-create');
        Route::post('/transfer-pasaload/store', 'TransferPasaloadController@store')->name('transfer-pasaload-store');
        Route::get('/transfer-pasaload/{id}/show', 'TransferPasaloadController@show')->name('transfer-pasaload-show');
        Route::get('/transfer-pasaload/{id}/edit', 'TransferPasaloadController@edit')->name('transfer-pasaload-edit');
        Route::post('/transfer-pasaload/{id}/update', 'TransferPasaloadController@update')->name('transfer-pasaload-update');
        Route::post('/transfer-pasaload/{id}/destroy', 'TransferPasaloadController@destroy')->name('transfer-pasaload-destroy');

        # Betting 
        Route::get('/bettings', 'BettingController@index')->name('betting');
        Route::get('/betting/create', 'BettingController@create')->name('betting-create');
        Route::post('/betting/store', 'BettingController@store')->name('betting-store');
        Route::get('/betting/{id}/show', 'BettingController@show')->name('betting-show');
        Route::get('/betting/{id}/edit', 'BettingController@edit')->name('betting-edit');
        Route::post('/betting/{id}/update', 'BettingController@update')->name('betting-update');
        Route::post('/betting/{id}/destroy', 'BettingController@destroy')->name('betting-destroy');

        # User Management
        Route::get('/player-management', 'UserManagementController@index')->name('player-management');
        Route::get('/player-management/{id}/loading', 'UserManagementController@loading_create')->name('player-management-loading');
        Route::post('/player-management/{id}/loading', 'UserManagementController@loading')->name('player-management-loading');
        Route::get('/player-management/{id}/pasaload', 'UserManagementController@pasaload_create')->name('player-management-pasaload');
        Route::post('/player-management/{id}/pasaload', 'UserManagementController@pasaload')->name('player-management-pasaload');
        Route::get('/player-management/{id}/cashout', 'UserManagementController@cashout_create')->name('player-management-cashout');
        Route::post('/player-management/{id}/cashout', 'UserManagementController@cashout')->name('player-management-cashout');
        Route::get('/player-management/find', 'UserManagementController@search_user')->name('player-management-get-user');

        # Activation
        Route::get('/activations', 'ActivationController@index')->name('activation');
        Route::post('/activation/activate/{id}', 'ActivationController@activate')->name('activation-activate');

        # Accounting
        Route::get('/accountings', 'AccountingController@index')->name('accounting');
        Route::get('/accounting/create', 'AccountingController@create')->name('accounting-create');
        Route::post('/accounting/store', 'AccountingController@store')->name('accounting-store');
        Route::get('/accounting/{id}/show', 'AccountingController@show')->name('accounting-show');
        Route::get('/accounting/{id}/edit', 'AccountingController@edit')->name('accounting-edit');
        Route::post('/accounting/{id}/update', 'AccountingController@update')->name('accounting-update');
        Route::post('/accounting/{id}/destroy', 'AccountingController@destroy')->name('accounting-destroy');

        # Arena
        Route::get('/arenas', 'ArenaController@index')->name('arena');
        Route::get('/arena/create', 'ArenaController@create')->name('arena-create');
        Route::post('/arena/store', 'ArenaController@store')->name('arena-store');
        Route::get('/arena/{id}/show', 'ArenaController@show')->name('arena-show');
        Route::get('/arena/show/list', 'ArenaController@show_list')->name('arena-show-list');
        Route::get('/arena/{id}/edit', 'ArenaController@edit')->name('arena-edit');
        Route::post('/arena/{id}/update', 'ArenaController@update')->name('arena-update');
        Route::post('/arena/{id}/destroy', 'ArenaController@destroy')->name('arena-destroy');

        # Admin Event Setup
        Route::get('/game-events', 'GameEventController@index')->name('game-event');
        Route::get('/game-event/create', 'GameEventController@create')->name('game-event-create');
        Route::post('/game-event/store', 'GameEventController@store')->name('game-event-store');
        Route::get('/game-event/{id}/show', 'GameEventController@show')->name('game-event-show');
        Route::get('/game-event/{id}/edit', 'GameEventController@edit')->name('game-event-edit');
        Route::post('/game-event/{id}/update', 'GameEventController@update')->name('game-event-update');
        Route::post('/game-event/{id}/destroy', 'GameEventController@destroy')->name('game-event-destroy');

    });

    // end settings

    // Temp api route
    Route::group(['prefix' => 'api/v1', 'middleware' => 'token'], function(){

        Route::get('generate/token','ApiTokenController@update')->name('get-token');
        Route::get('/user/show/{id}/points','UserController@showPoints')->name('get-points');
    
        # Realtime Chat
        Route::get('/chat', 'ChatController@index');
        Route::post('/chat', 'ChatController@store')->name('chat-store');
        Route::get('/chat/next', 'ChatController@nextPage')->name('next-page');
    
        # Realtime Betting
        Route::post('/game/add-bet', 'GameController@addBet')->name('add-bet');
        Route::post('/game/cancel-bet', 'GameController@cancelBet')->name('cancel-bet');
        Route::get('/game/get-total-per-type', 'GameController@getTotalPerType')->name('get-total-per-type');
    
        # Admin Announce Control
        Route::post('/announce/status', 'GameController@status')->name('announce');
    
    });
});
// end --
