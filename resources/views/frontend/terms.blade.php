@extends('layouts.app')

@section('title') Terms and Conditions | {{config('app.name', 'Laravel')}} @endsection
@section('content')
<header id="head" class="secondary"></header>

<!-- container -->
<div class="container">

    <div class="row">
        
        <!-- Article main content -->
        <article class="col-sm-8 maincontent">
            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">Terms and Conditions</h1>
                </header>
                <p>
                "PinoySabong Account" means a unique Account that has been created by you through registration;
                <br>This "Agreement" means these Terms and Conditions;
                <br>The "Service" means the availability and provision of software that enables you to bet in the Arena using the Web and the Internet;
                <br>The "Player/Customer/You" means the person who utilizes his own money to place wagers at the ONLINE BETTING CONSOLE;
                <br>The "ARENA" means the place where the coverage is taking place;
                <br>We/Us/Our" means, PinoySabong, which is the owner and operator of the website;
                <br>"Your Jurisdiction" means the territory over which authority is exercised;
                <br>"PinoySabong Points" is the digital virtual point that is used as wager in the game.
                </p>
            </div>

            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">Participation</h1>
                </header>
                <p>
                <br>1.It is your sole option, discretion and risk to participate in the online betting.
                <br>2.You are solely responsible for ascertaining whether it is allowed in your jurisdiction to participate in this online betting.
                <br>3.You may only participate in the online betting if it is legal for you to do so within your jurisdiction.
                <br>4.You may only participate in the online betting if you have at least reached the legal age of 18.
                <br>5.We do not warrant the legality of your participation in the online betting in terms of the laws of your jurisdiction.

                </p>
            </div>

            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">YOUR WARRANTIES</h1>
                </header>
                <p>
                <br>1.We have the right to request with proof of your identity and age as a pre-requisite of your participation in the online betting and at any time throughout your participation in this website.
                <br>2.You agree that we may use personal information provided by you in order to conduct appropriate anti-fraud check. Personal Information that you provided may be disclosed to a credit reference or fraud prevention agency. The agency may, at its discretion and according to its policies, keep appropriate records of the collected information.
                <br>1.You warrant and represent. We enter into this Agreement on the basis of such representations and warranties, all of which are material at the time you enter into this Agreement and throughout the currency hereof, that you:
                <br>a.are legally able to participate in the online betting within your jurisdiction;
                <br>b.is an “adult”, having attained the age of majority within your jurisdiction;
                <br>c.have furnished us with personal details that are valid, accurate, true and complete in each and every respect and that you shall advise us immediately should your details changed;
                <br>d.are true and lawful owner of the virtual points that you wager in the online betting and /or that you are duly and properly authorized to utilize your PinoySabong Points for the aforementioned purpose.
                <br>e.shall not deposit nor wager any monies that are derived in any manner whatsoever from illegal activities.
                <br>f.shall pay all monies owed to the online betting directly to Us and, in respect of payment, shall not charge-back, deny, reverse and/or countermand any such payments;
                <br>g.shall ensure that any monies owed by you to the ONLINE BETTING are paid to Us and a payment to any third party (irrespective of whether the third party is acting as an agent for you or Us) shall not constitute a discharge thereof until the said monies have actually been received by Us. A breach hereof may result in a refusal by Us to pay any cash-in ;.have read and understood these Terms and Conditions;

                </p>
            </div>

            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">THE SERVICES</h1>
                </header>
                <p>
                <br>1.The Service is provided "as is". We make no warranty or representation, whether express or implied, in relation to the satisfactory quality, fitness for purpose, completeness or accuracy of the Service.
                <br>1.We shall not be liable for computer malfunctions, failure of telecommunications service or Internet connections nor attempts by you to participate in games by methods, means or ways not intended by us.
                <br>2.We may temporarily suspend the whole or any part of the Service for any reason at our sole discretion. We may, but shall not be obliged to, give you as much notice as is reasonably practicable of such suspension. We will restore the Service, as soon as it is reasonably practicable, after temporary suspension.
                <br>3.In the event of technical difficulties and BETTING is still OPEN, we will declare the fight as CANCELLED and all matched/unmatched bets will be voided and returned to the player's; accounts. HOWEVER, if the BETTING has been CLOSED, we will declare whatever the winner is in the arena.
                <br>4.We assert that there may be a delay of a few seconds of the video, although details about the precise length of the delay cannot be provided. We refuse to incur any liability for any loss that is caused by these delays.
                <br>5.PinoySabong Management strictly prohibits the duplication, modification, distribution, and public exhibition of its video contents without its express written authorization.
                <br>6.PinoySabong Management reserves its right to permanently ban any parties that are proven guilty of violating the rule expressed on item 3.6.
                <br>7.All transactions are processed through our Customer Service Representatives.

                </p>
            </div>

            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">RULES OF PLAY</h1>
                </header>
                <p>
                <br>1.4.1. In addition to these Terms and Conditions, certain Rules of Play shall apply to you and be binding upon you in respect to your participation in the Arena.
                <br>2.You hereby agree to be bound by the aforementioned Rules of Play as if they were specifically incorporated into these Terms and Conditions.
                <br>3.All bets are assumed final by the Player. Minimum wager is 100 PinoySabong points. All winnings are subject to 8% house commission or "plasada".
                <br>4.Betting can only be done through the Game/Betting Console. Betting done through the chat-room or any other means besides the Game/Betting Console is VOID.
                <br>5.We reserve the right to exclude players from participating in the betting without prior notice and/or providing reasons for the exclusion.
                <br>6.If the bets have not been matched properly on a fight and BETTING has been closed, the betting will be cancelled and all bets will be credited back to the players’ accounts.
                <br>7.CANCELLATION of a bet is only possible while betting is still OPEN and the bet concerned is still UNMATCHED.
                <br>8.BETTING is open as soon as a cock/stag is already available in the ARENA and still WITHOUT any assigned SIDE. The player has the option to place a bet on his favored cock/stag at this point BUT if the sides have been assigned and are not the same with their original places, we will NOT CANCEL the fight.
                <br>9.In the event, the sides(MERON or WALA) are switched/reversed with players'; bets already matched, the fight will be declared CANCELLED and bets are credited back to their respective accounts
                <br>1.In the event that any erroneous information is shown on the video graphics (e.g. incorrect entry name, entry name placed on the wrong side MERON or WALA), and error is noticed by admin while betting is still open, betting will be cancelled; and if time permits, betting on the same fights will be re-opened. If error is noticed by admin after betting has been closed, the betting will resume and the result of the fight will be declared.
                <br>2.In the event of buffered video or video hang:
                <br>a.If betting is closed before video buffer or video hang, the betting will resume and the result of the fight will be declared.
                <br>b.If betting is open before video buffer or video hang, the betting will be cancelled and the wagers will be returned and credited back to the players’ accounts.
                <br>3.In the event that the betting is open and the cocks/stags have already engaged in battle, the betting will be cancelled and the wagers will be credited back to the players’ accounts.
                <br>4.The Declared Winner by the “Sentensyador” at the end of a match is FINAL; unless the Cockpit Management requests for a review of the fight because of a protest on the actual declaration.
                <br>a.Only the Cockpit Management may request a review of the fight in question.
                <br>If the Cockpit Management decides to change the initial declaration, we will honor the decision and make the necessary adjustments to the players PinoySabong points. The actual adjustments will be done manually and the players concerned will be notified to the adjustments made.
                <br>1.The fight review, and the change in the declaration, if there is any, must be done before the next fight resumes. Otherwise, the initial declaration will be considered FINAL and IRREVOCABLE.
                <br>2.The ODDS are FIXED and payouts are computed AUTOMATICALLY after the RESULT has been DECLARED.
                <br>3.In the event of a "Draw", bets are returned to the player's; and no house commission is deducted except those who bet in the “Draw”.
                <br>4.if you need clarification on the computation, you can contact or message our Customer Service Representatives through messenger.

                </p>
            </div>

            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">{{config('app.name', 'Laravel')}}   ACCOUNT</h1>
                </header>
                <p>
                <br>1.By clicking on the ‘REGISTER’ button in our website, you are required to enter a valid email address, your username and password for your access to the service. A valid email address and facebook account is required for the activation.
                <br>1.Players shall remain responsible for protecting the secrecy of their accounts and shall not allow third parties to access or use their account. The Player accepts full responsibility for the consequences of allowing such activities.
                <br>2.In the event of obvious errors in drawing up players account balances, PinoySabong has the right to make the necessary amendments. Claims regarding account statements and the balances in these statements may only be made within 15 days of the betting event. Failure to claim shall be deemed an acceptance of the notified account balance.

                </p>
            </div>

            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">REFUSAL TO REGISTER, DEREGISTRATION, EXCLUSION & SUSPENSION</h1>
                </header>
                <p>
                <br>1.	We may refuse to register you as a Player or elect to unregister and exclude you or suspend you as a Player from the website at any time if we deem that your participation on the website is, shall be or has been previously, in any way not for personal entertainment i.e. business], fraudulent, illegal or that your participation is or has been abusive, collusive or irregular in any way.
                <br>2.	You acknowledge hereby that PinoySabong Management is not obliged to give you prior notice of its decision to refuse, deregister or exclude or suspend you, nor to furnish you with any reasons for such decision.
                <br>3.	If PinoySabong Management becomes aware that a player is underage, we will, except where there are grounds to believe that a fraud has been perpetrated:
                <br>a.	Suspend the account immediately;
                <br>b.	Void all wagers that have taken place
                <br>c.	Refund the value of all deposits net of withdrawals; and
                <br>d.	Close the account.
                <br>4.	Consequences of Deregistration, Exclusion or Suspension:
                <br>a.	If we deregister or exclude or suspend you from the Console we shall have the right to:
                <br>i.	withhold payment to you of any contested funds whether such contested funds are deposits, refunds, bonuses, payouts or the like; and/or
                <br>ii.	exclude you from all or any other sites of the PinoySabong Management and/or
                <br>iii.	solely determine what criteria you shall have to meet in order to establish a New Account at the Console; and/or
                <br>iv.	in the case of fraudulent, illegal or similar misconduct by you or failure by you to pay any sums due to us:
                <br>	iv.i   furnish any relevant information about you to an intra-group database recording such mischief and, if necessary, hand over your account details to a collections agency for the recovery of any sums that you owe us. You hereby irrevocably authorize us to do so in our absolute discretion, and/or
                <br>	iv.ii   have forfeited to us any contested funds that may be derived by you from fraudulent, illegal or similar misconduct.
                </p>
            </div>

            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">CHATROOM ETIQUETTE</h1>
                </header>
                <p>
                <br>1.	We value respect in dealing with our Players. Cockfighting is a gentlemen's game and it is expected that players will be gentle at all times, even in the chatbox. Any form of disrespect to the PinoySabong Management and to the Staff, or to other players is strictly prohibited.
                <br>2.	Comments, suggestions, and constructive criticisms are welcome in the chatbox.
                <br>3.	The use of foul and offensive words is strictly prohibited in the chatbox.
                <br>4.	Any form of baseless accusations or direct defamation of PinoySabong Management is strictly prohibited.
                <br>5.	Violation of these rules may cause your PinoySabong Account to be temporarily or permanently discontinued.
                </p>
            </div>

            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">CONNECTIVITY</h1>
                </header>
                <p>
                <br>1.	Our Game Console is optimized for Google Chrome. Use of other browsers such as Mozilla Firefox, Internet Explorer or Safari may require you to install the latest update of Flash Player.
                <br>2.	Mobile Phones may require you to install the Puffin Browser for you to be able to access the video.
                <br>3.	You are solely responsible for any telecommunications devices, networks, GPRS, Internet access services and other consents and permissions required in connection with your use of the website.
                </p>
            </div>

            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">LOADING METHOD</h1>
                </header>
                <p>
                <br>1.	Our loading method are through bank deposits thru BDO and BPI; Remittance Center: MLhuillier, Cebuana Lhuillier, LBC and Palawan Express; or through Online Payment Gateway via Smart Padala, Globe GCASH, and PayMaya.
                <br>2.	All remittances must be claimed first before being credited to players’ accounts.
                <br>3.	Remittances deposited to our bank accounts must reflect or be posted to our accounts first before the equal amount can be loaded to the player's account.
                <br>4.	Verification and/or claiming of remittances will depend on the operating hours of the Remittance Centers.
                <br>5.	PinoySabong  Group is not liable for any loss of remittance due to the players’ negligence.
                <br>6.	All loadings should be made via any of our Official Receivers found in the About Us of the website. Requests made via other channels besides the above mentioned will not be processed, except for cash loads that are directly handed to our loading staff.
                <br>7.	Receipts or proof of payment must be send via messenger and CSR Messenger’s Account), viber or whatsapp.
                <br>2.	CASHOUT/WITHDRAWAL METHOD
                <br>1.	You need to fill-up completely the Request Payout form with the correct details. The form can be found in the lobby of the website and you must inform the Customer Service Representative about it for processing.
                <br>2.	Any fees charged by the remittance channels used to send the player's withdrawal will be charged to the player.
                <br>3.	Processing of your Cash-out request is within 24 hours during the weekdays. Requests made on weekends or legal holidays (bank or public holidays) will be processed on the following working day.
                <br>4.	All cash-out requests must be requested via messenger. Requests made via other channels beside messenger will not be processed.
                <br>5.	Cashout Gateway may be processed through any of the following: Bank Deposits thru BDO and BPI; Remittance Center: MLhuillier, Cebuana Lhuillier, LBC and Palawan Express; or through Online Payment Gateway via Smart Padala, Globe GCASH, and PayMaya.
                <br>6.	Only the owner of the account may request for a withdrawal of the credits from his/her account.
                <br>7.	Receipts are provided as proof of payment and will be provided by the Customer Services Representative.
                <br>8.	Cut-off time for Cashout is 12nn (Ph, GMT 8).
                </p>
            </div>

            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">TRANSFER LOAD METHOD</h1>
                </header>
                <p>
                <br>1.	Players may request to transfer their PinoySabong points to other accounts. Such request should be made through chatbox or through messenger with the following code: “PASA Username” or “TRANSFER Username”.
                <br>2.	Transfer of points may be confirmed through chatbox or messenger.
                <br>1.	Only the owner of the Account may request for a transfer of loads from his/her account.
                </p>
            </div>

            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">OUR PRIVACY POLICY</h1>
                </header>
                <p>
                <br>Your online privacy is of the utmost importance to us, therefore we have formulated the below guarantees to ensure that you receive a safe and secure player experience. Should you have any questions please feel free to contact our Customer Service Representatives. PinoySabong guarantees:
                <br>o	We will keep the confidentiality of your personal or corporate information except as necessary to provide you with the best players’ satisfaction.
                <br>o	To only use your personal details to provide you with information or materials that we feel necessary in order to provide you with quality of service.
                <br>o	We are committed to give the best to our valued players by maintaining the following values: Integrity, Commitment, Respect, Team Work, Innovation and Customer Service.
                </p>
            </div>
        </article>
        <!-- /Article -->
        
        <!-- Sidebar -->
        <aside class="col-sm-4 sidebar sidebar-right">

            <div class="widget">
                <h4>Upcoming Events</h4>
                <ul class="list-unstyled list-spaces">
                    @if(count($data) == 0)
                    <li>
                        <span class="small text-muted">No events yet.</span>
                    </li>
                    @endif
                    @foreach($data as $event)
                    <li>
                        <a href="{{ route('event-web-show', [$event->id]) }}">
                            {{ $event->name }}
                            <br>
                            <span class="small text-muted"><b>Location</b>: {{$event->location}}</span>
                            <br>
                            <span class="small text-muted"><b>Expected Deal</b>: {{$event->expected_deal}}</span>
                            <br>
                            <span class="small text-muted"><b>Event Date</b>: {{date('Y-m-d', strtotime($event->event_date))}}</span>
                            <br>
                            <!-- <img src="{{$event->cover_image}}" class="image-responsive" /> -->
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>

        </aside>
        <!-- /Sidebar -->

    </div>
</div>	<!-- /container -->
@endsection