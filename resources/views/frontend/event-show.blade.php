<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="{{ asset('images/ico.png') }}" type="image/x-icon"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('app.name', 'Laravel'))</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

	<!-- CSS Files -->
	
    <link rel="stylesheet" media="screen" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('front/css/font-awesome.min.css') }}">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="{{ asset('front/css/main.css') }}">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="{{ asset('front/js/html5shiv.js') }}"></script>
	<script src="{{ asset('front/js/respond.min.js') }}"></script>
	<![endif]-->

    <style>
        .ps-container::-webkit-scrollbar {
            width: 0.3em;
        }
        
        .ps-container::-webkit-scrollbar-track {
        box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        }
        
        .ps-container::-webkit-scrollbar-thumb {
        background-color: darkgrey;
        outline: 1px solid slategrey;
        }

        .statuses::-webkit-scrollbar {
            width: 0.3em;
        }
        
        .statuses::-webkit-scrollbar-track {
        box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        }
        
        .statuses::-webkit-scrollbar-thumb {
        background-color: darkgrey;
        outline: 1px solid slategrey;
        }

        .ps-container {
            position: relative;
        }

        .ps-container {
            -ms-touch-action: auto;
            touch-action: auto;
            overflow: hidden !important;
            -ms-overflow-style: none
        }

        .media-chat {
            padding-right: 0px;
            margin-bottom: 0
        }

        .media {
            /* padding: 16px 12px; */
            -webkit-transition: background-color .2s linear;
            transition: background-color .2s linear
        }

        .media .avatar {
            flex-shrink: 0
        }

        .avatar {
            position: relative;
            display: inline-block;
            width: 36px;
            height: 36px;
            line-height: 36px;
            text-align: center;
            border-radius: 100%;
            background-color: #f5f6f7;
            color: #8b95a5;
            text-transform: uppercase
        }

        .media-chat .media-body {
            -webkit-box-flex: initial;
            flex: initial;
            display: table
        }

        .media-body {
            /* min-width: 0 */
            width: 100%;
        }

        .media-chat .media-body p {
            position: relative;
            /* padding: 6px 8px; */
            margin: 0px 0;
            /* background-color: #f5f6f7; */
            background-color: #eee;
            border-radius: 3px;
            font-weight: 100;
            color: #909090;
        }

        .media>* {
            /* margin: 0 8px */
        }

        .media-chat .media-body p.meta {
            background-color: transparent !important;
            padding: 0;
            opacity: .8
        }

        .media-meta-day {
            -webkit-box-pack: justify;
            justify-content: space-between;
            -webkit-box-align: center;
            align-items: center;
            margin-bottom: 0;
            color: #8b95a5;
            opacity: .8;
            font-weight: 400
        }

        .media {
            /* padding: 0px 12px; */
            -webkit-transition: background-color .2s linear;
            transition: background-color .2s linear
        }

        .media-meta-day::before {
            margin-right: 16px
        }

        .media-meta-day::before,
        .media-meta-day::after {
            content: '';
            -webkit-box-flex: 1;
            flex: 1 1;
            border-top: 1px solid #ebebeb
        }

        .media-meta-day::after {
            content: '';
            -webkit-box-flex: 1;
            flex: 1 1;
            border-top: 1px solid #ebebeb
        }

        .media-meta-day::after {
            /* margin-left: 16px */
        }

        .media-chat-other {
            background: #e5ebee;
            margin-top: 5px
        }

        .media-chat.media-chat-reverse {
            /* padding-right: 12px;
            padding-left: 64px; */
            -webkit-box-orient: horizontal;
            -webkit-box-direction: reverse;
            flex-direction: row-reverse
        }

        .media-chat {
            /* padding-right: 64px; */
            padding-right: 5px;
            margin-bottom: 0
        }

        .media {
            /* padding: 16px 12px; */
            -webkit-transition: background-color .2s linear;
            transition: background-color .2s linear
        }

        .media-chat.media-chat-reverse .media-body p {
            float: right;
            clear: right;
            /* background-color: #48b0f7; */
            color: #fff;
            border: 1px solid #eee;
        }

        .media-chat .media-body p {
            position: relative;
            padding: 6px 8px;
            margin: 5px;
            background-color: #222;
            border-radius: 3px;
            border: 1px solid #48b0f7;
            float: left;
            clear: left;
        }

        .border-light {
            /* border-color: #f1f2f3 !important */
            /* border: 1px solid #f3f5ee; */
        }

        .bt-1 {
            /* border-top: 1px solid #ebebeb !important */
            /* border-top: 1px solid #ccc !important; */
        }

        .publisher {
            position: relative;
            display: -webkit-box;
            display: flex;
            -webkit-box-align: center;
            align-items: center;
            padding: 12px 0px;
            /* background-color: #f9fafb; */
            /* border-top: 1px solid #ccc; */
        }

        .publisher>*:first-child {
            margin-left: 0
        }

        .publisher>* {
            margin: 0 8px
        }

        .chat-input{
            border-radius: 5px;
            background: #222;
            border: 1px solid #ccc;
            color: #eee;
            width: 90%;
        }

        .specinput{
            border-radius: 5px;
            background: #222;
            border: 1px solid #ccc;
            color: #eee;
            text-align: center;
        }

        .border-green{
            border: 1px solid #31CE36;
        }

        .text-green{
            color: #31CE36;   
        }

        .publisher-input {
            -webkit-box-flex: 1;
            flex-grow: 1;
            border: none;
            outline: none !important;
            background-color: transparent
        }

        button,
        input,
        optgroup,
        select,
        textarea {
            font-family: Roboto, sans-serif;
            font-weight: 300
        }

        .publisher-btn {
            background-color: transparent;
            border: none;
            color: #8b95a5;
            font-size: 16px;
            cursor: pointer;
            overflow: -moz-hidden-unscrollable;
            -webkit-transition: .2s linear;
            transition: .2s linear
        }

        .file-group {
            position: relative;
            overflow: hidden
        }

        .publisher-btn {
            background-color: transparent;
            border: none;
            color: #cac7c7;
            font-size: 16px;
            cursor: pointer;
            overflow: -moz-hidden-unscrollable;
            -webkit-transition: .2s linear;
            transition: .2s linear
        }

        .file-group input[type="file"] {
            position: absolute;
            opacity: 0;
            z-index: -1;
            width: 20px
        }

        .text-info {
            color: #48b0f7 !important
        }

        body{
            background: #222;
            color: #fefefe;
        }
        .yellow{
            color: yellow;
        }
        table td{
            padding: 0px;
            margin: 0px;
        }

        .border{
            border: 1px solid #ccc;
        }

        .border-left{
            border-left: 1px solid #ccc;
        }

        .border-right{
            border-right: 1px solid #ccc;
        }

        .border-top{
            border-top: 1px solid #ccc;
        }

        .border-bottom{
            border-bottom: 1px solid #ccc;
        }

        .padding{
            padding: 10px;
        }

        .padding-half{
            padding: 5px;
        }

        .no-padding{
            padding: 0px;
        }

        .no-margin{
            margin: 0px;
        }

        .text-center{
            text-align: center;
        }
        .blue{
            color: blue;
        }
        .pink{
            color: pink;
        }
        .nostyle{
            text-decoration: none;
        }
        .border-blue{
            border:1 px solid blue;
        }
        .round-red{
            border: 1px solid red;
            color: red;
            padding: 3px;
            padding-left: 5px;
            padding-right: 5px;
            border-radius: 5px;
            cursor: pointer;
        }
        .round-info{
            border: 1px solid #48ABF7;
            color: #48ABF7;
            padding: 3px;
            padding-left: 5px;
            padding-right: 5px;
            border-radius: 5px;
            cursor: pointer;
        }
        .sptooltip{
            background: #ccc;
            border: 1px solid #eee;
            color: #222;
            padding: 5px;
            border-radius: 5px 0 5px 0;
        }
        .pointer{
            cursor: pointer;
        }
    </style>
</head>
<body style="padding: 0px;margin:0px">
    <div id="app" class="container-fluid">


        @php

            $total_meron_1010 = 0;
            $total_meron_109 = 0;
            $total_meron_108 = 0;
            $total_meron_86 = 0;
            $total_meron_118 = 0;
            $total_meron_910 = 0;
            $total_meron_810 = 0;
            $total_meron_811 = 0;
            $total_meron_68 = 0;
            $total_meron_18 = 0;

            $total_wala_1010 = 0;
            $total_wala_109 = 0;
            $total_wala_108 = 0;
            $total_wala_86 = 0;
            $total_wala_118 = 0;
            $total_wala_910 = 0;
            $total_wala_810 = 0;
            $total_wala_811 = 0;
            $total_wala_68 = 0;
            $total_wala_18 = 0;

            $bet_meron_1010 = '';
            $bet_meron_109 = '';
            $bet_meron_108 = '';
            $bet_meron_86 = '';
            $bet_meron_118 = '';
            $bet_meron_910 = '';
            $bet_meron_810 = '';
            $bet_meron_811 = '';
            $bet_meron_68 = '';
            $bet_meron_18 = '';

            $bet_wala_1010 = '';
            $bet_wala_109 = '';
            $bet_wala_108 = '';
            $bet_wala_86 = '';
            $bet_wala_118 = '';
            $bet_wala_910 = '';
            $bet_wala_810 = '';
            $bet_wala_811 = '';
            $bet_wala_68 = '';
            $bet_wala_18 = '';

            foreach($oddsbet as $bet)
            {
                if($bet->user_id == Auth::user()->id)
                {
                    if($bet->type == 'meron' && $bet->odds == '10-10')
                        $bet_meron_1010 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'meron' && $bet->odds == '10-9')
                        $bet_meron_109 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'meron' && $bet->odds == '10-8')
                        $bet_meron_108 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'meron' && $bet->odds == '8-6')
                        $bet_meron_86 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'meron' && $bet->odds == '11-8')
                        $bet_meron_118 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'meron' && $bet->odds == '9-10')
                        $bet_meron_910 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'meron' && $bet->odds == '8-10')
                        $bet_meron_810 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'meron' && $bet->odds == '8-11')
                        $bet_meron_811 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'meron' && $bet->odds == '6-8')
                        $bet_meron_68 = round($bet->amount,0 , PHP_ROUND_HALF_UP);

                    if($bet->type == 'wala' && $bet->odds == '10-10')
                        $bet_wala_1010 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'wala' && $bet->odds == '10-9')
                        $bet_wala_109 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'wala' && $bet->odds == '10-8')
                        $bet_wala_108 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'wala' && $bet->odds == '8-6')
                        $bet_wala_86 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'wala' && $bet->odds == '11-8')
                        $bet_wala_118 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'wala' && $bet->odds == '9-10')
                        $bet_wala_910 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'wala' && $bet->odds == '8-10')
                        $bet_wala_810 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'wala' && $bet->odds == '8-11')
                        $bet_wala_811 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'wala' && $bet->odds == '6-8')
                        $bet_wala_68 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                    if($bet->type == 'wala' && $bet->odds == '1-8')
                        $bet_wala_18 = round($bet->amount,0 , PHP_ROUND_HALF_UP);
                }
                
                // for wala
                if($bet->type == 'wala' && $bet->odds == '10-10')
                    $total_wala_1010 = round($total_wala_1010 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'wala' && $bet->odds == '10-9')
                    $total_wala_109 = round($total_wala_109 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'wala' && $bet->odds == '10-8')
                    $total_wala_108 = round($total_wala_108 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'wala' && $bet->odds == '8-6')
                    $total_wala_86 = round($total_wala_86 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'wala' && $bet->odds == '11-8')
                    $total_wala_118 = round($total_wala_118 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'wala' && $bet->odds == '9-10')
                    $total_wala_910 = round($total_wala_910 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'wala' && $bet->odds == '8-10')
                    $total_wala_810 = round($total_wala_810 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'wala' && $bet->odds == '8-11')
                    $total_wala_811 = round($total_wala_811 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'wala' && $bet->odds == '6-8')
                    $total_wala_68 = round($total_wala_68 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'wala' && $bet->odds == '1-8')
                    $total_wala_18 = round($total_wala_18 + $bet->amount,0 , PHP_ROUND_HALF_UP);

                // for meron
                if($bet->type == 'meron' && $bet->odds == '10-10')
                    $total_meron_1010 = round($total_meron_1010 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'meron' && $bet->odds == '10-9')
                    $total_meron_109 = round($total_meron_109 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'meron' && $bet->odds == '10-8')
                    $total_meron_108 = round($total_meron_108 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'meron' && $bet->odds == '8-6')
                    $total_meron_86 = round($total_meron_86 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'meron' && $bet->odds == '11-8')
                    $total_meron_118 = round($total_meron_118 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'meron' && $bet->odds == '9-10')
                    $total_meron_910 = round($total_meron_910 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'meron' && $bet->odds == '8-10')
                    $total_meron_810 = round($total_meron_810 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'meron' && $bet->odds == '8-11')
                    $total_meron_811 = round($total_meron_811 + $bet->amount,0 , PHP_ROUND_HALF_UP);
                if($bet->type == 'meron' && $bet->odds == '6-8')
                    $total_meron_68 = round($total_meron_68 + $bet->amount,0 , PHP_ROUND_HALF_UP);
            }

        @endphp
        
        <input type="hidden" class="game-count-{{$event->id}}" value="{{$game_count}}">
        <input type="hidden" class="event" value="{{$event->id}}">
        <table width="100%" class="" cellpadding="0" cellspacing="0">
            <tr>
                <td class="border-right" width="50%" cellpadding="0" cellspacing="0">
                    <table width="100%" cellpadding="0" cellspacing="0" style="{{ (Auth::user()->is_admin == 1) ? 'margin-top: -1%' : ''}}">

                        <tr>
                            <td class="no-padding no-margin">
                                <iframe width="950" height="470" src="{{$event->url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table width="100%" class="border-top border-bottom" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="padding-half text-center btn-danger">
                                            <b style="margin: 0px">MERON</b>
                                        </td>
                                        <td class="text-center" style="width: 70%">
                                            <b>{{ $event->arena->name }}</b>
                                        </td>
                                        <td class="padding-half text-center btn-info">
                                            <b style="margin: 0px">WALA</b>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        @if(Auth::user()->is_admin == 1)
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="3">
                                    <tr>
                                        <td class="padding">
                                            <button class="control btn btn-success" data-action="status" data-value="open" data-message="You are going to Open Betting Activity on this Current Game"><b>Open Bet <i class="fa fa-check"></i></b></button>
                                            <button class="control btn btn-warning" data-action="status" data-value="close" data-message="You are going to Close Betting Activity on this Current Game"><b>Close Bet <i class="fa fa-times"></i></b></button>
                                        </td>
                                        <td class="padding" width="1%"></td>
                                        <td class="padding text-center">
                                            <button class="control btn btn-danger btn-xs" data-action="declare" data-value="ML" data-message="You are going to Declare MERON Lamado as Winner on this Current Game"><b>Declare M L Win <i class="fa fa-flag"></i></b></button>
                                            <button class="control btn btn-danger btn-xs" data-action="declare" data-value="MD" data-message="You are going to Declare MERON Dehado as Winner on this Current Game"><b>Declare M D Win <i class="fa fa-flag"></i></b></button>
                                            <button class="control btn btn-success btn-xs" data-action="declare" data-value="D" data-message="You are going to Declare that this game is DRAW"><b>Declare Draw <i class="fa fa-times"></i></b></button>
                                            <hr>
                                            <button class="control btn btn-info btn-xs" data-action="declare" data-value="WL" data-message="You are going to Declare WALA Lamado as Winner on this Current Game"><b>Declare W L Win <i class="fa fa-flag"></i></b></button>
                                            <button class="control btn btn-info btn-xs" data-action="declare" data-value="WD" data-message="You are going to Declare WALA Lamada as Winner on this Current Game"><b>Declare W D Win <i class="fa fa-flag"></i></b></button>
                                            <button class="control btn btn-warning btn-xs" data-action="declare" data-value="C" data-message="You are going to Declare that this game is CANCELLED"><b>Declare Cancel <i class="fa fa-times"></i></b></button>
                                        </td>
                                        <td class="padding" width="1%"></td>
                                        <td class="padding">
                                            <button class="control btn btn-warning" data-action="next-game" data-value="{{ $game_count }}" data-message="You about to proceed to the next round of this event."><b>Next Game <i class="fa fa-arrow-right"></i></b></button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-center">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="btn-danger padding-half">
                                            <b>TOTAL MERON</b>
                                        </td>
                                        <td class="btn-warning padding-half">
                                            <b>O D S</b>
                                        </td>
                                        <td class="btn-info padding-half">
                                            <b>TOTAL WALA</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-bottom padding-half">
                                            <b class="yellow total-meron-10-10-{{$event->id}}">{{ $total_meron_1010 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>10 - 10</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-10-10-{{$event->id}}">{{ $total_wala_1010 }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-bottom padding-half">
                                            <b class="yellow total-meron-10-9-{{$event->id}}">{{ $total_meron_109 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>10 - 9</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-10-9-{{$event->id}}">{{ $total_wala_109 }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-bottom padding-half">
                                            <b class="yellow total-meron-10-8-{{$event->id}}">{{ $total_meron_108 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>10 - 8</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-10-8-{{$event->id}}">{{ $total_wala_108 }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-bottom padding-half">
                                            <b class="yellow total-meron-8-6-{{$event->id}}">{{ $total_meron_86 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>8 - 6</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-8-6-{{$event->id}}">{{ $total_wala_86 }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-bottom padding-half">
                                            <b class="yellow total-meron-11-8-{{$event->id}}">{{ $total_meron_118 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>11 - 8</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-11-8-{{$event->id}}">{{ $total_wala_118 }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-bottom padding-half">
                                            <b class="yellow total-meron-9-10-{{$event->id}}">{{ $total_meron_910 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>9 - 10</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-9-10-{{$event->id}}">{{ $total_wala_910 }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-bottom padding-half">
                                            <b class="yellow total-meron-8-10-{{$event->id}}">{{ $total_meron_810 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>8 - 10</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-8-10-{{$event->id}}">{{ $total_wala_810 }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-bottom padding-half">
                                            <b class="yellow total-meron-6-8-{{$event->id}}">{{ $total_meron_68 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>6 - 8</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-6-8-{{$event->id}}">{{ $total_wala_68 }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-bottom padding-half">
                                            <b class="yellow total-meron-8-11-{{$event->id}}">{{ $total_meron_811 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>8 - 11</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-8-11-{{$event->id}}">{{ $total_wala_811 }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-bottom padding-half">
                                            <b>DRAW</b>
                                        </td>

                                        <td class="border-bottom"><b>1 - 8</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-1-8-{{$event->id}}">{{ $total_wala_18 }}</b>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        @else
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="3">
                                    <tr>
                                        <td width="25%" class="text-center">
                                            <h3 class="yellow">POINTS: <span class="points-{{Auth::user()->id}}">{{Auth::user()->points}}</span></h3>
                                        </td>
                                        <td width="10%">
                                            <input name="bet_input" type="text" class="specinput form-control" placeholder="BET HERE">
                                        </td>
                                        <td></td>
                                        <td>
                                            <p class="border-green text-green text-center pointer bet-amount">100</p>
                                        </td>
                                        <td>
                                            <p class="border-green text-green text-center pointer bet-amount">200</p>
                                        </td>
                                        <td>
                                            <p class="border-green text-green text-center pointer bet-amount">500</p>
                                        </td>
                                        <td>
                                            <p class="border-green text-green text-center pointer bet-amount">1000</p>
                                        </td>
                                        <td>
                                            <p class="border-green text-green text-center pointer bet-amount">2000</p>
                                        </td>
                                        <td>
                                            <p class="border-green text-green text-center pointer bet-amount">5000</p>
                                        </td>
                                        <td>
                                            <p class="border-green text-green text-center pointer bet-amount">10000</p>
                                        </td>
                                        <td>
                                            <p class="border-green text-green text-center pointer bet-amount">ALL IN</p>
                                        </td>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-center">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="btn-danger padding-half" width="5%">
                                            <b></b>
                                        </td>
                                        <td class="btn-danger padding-half" width="1%"></td>
                                        <td class="btn-danger padding-half" width="1%"></td>
                                        <td class="btn-danger padding-half">
                                            <b>YOUR BET</b>
                                        </td>
                                        <td class="btn-danger padding-half">
                                            <b>TOTAL MERON</b>
                                        </td>
                                        <td class="btn-warning padding-half">
                                            <b>O D S</b>
                                        </td>
                                        <td class="btn-info padding-half">
                                            <b>TOTAL WALA</b>
                                        </td>
                                        <td class="btn-info padding-half">
                                            <b>YOUR BET</b>
                                        </td>
                                        <td class="btn-info padding-half" width="1%"></td>
                                        <td class="btn-info padding-half" width="1%"></td>
                                        <td class="btn-info padding-half" width="5%">
                                            <b></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding-half border-bottom">
                                        
                                        </td>

                                        <td class="border-bottom">
                                            <i class="round-red cancel-bet fa fa-times" data-bet="meron" data-odds="10-10" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>

                                        <td class="padding-half border-bottom">
                                            <i class="round-red add-bet fa fa-plus" data-bet="meron" data-odds="10-10" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                            <b class="bet-meron-10-10-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_meron_1010 }}</b>
                                        </td>

                                        <td class="border-bottom">
                                            <b class="yellow total-meron-10-10-{{$event->id}}">{{ $total_meron_1010 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>10 - 10</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-10-10-{{$event->id}}">{{ $total_wala_1010 }}</b>
                                        </td>

                                        <td class="border-bottom">
                                            <b class="bet-wala-10-10-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_wala_1010 }}</b>
                                        </td>

                                        <td class="padding-half border-bottom">
                                            <i class="round-info add-bet fa fa-plus" data-bet="wala" data-odds="10-10" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                            <i class="round-info cancel-bet fa fa-times" data-bet="wala" data-odds="10-10" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding-half border-bottom">
                                        
                                        </td>
                                        
                                        <td class="border-bottom">
                                            <i class="round-red cancel-bet fa fa-times" data-odds="10-9" data-bet="meron" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>
                                        <td class="padding-half border-bottom">
                                            <i class="round-red add-bet fa fa-plus" data-odds="10-9" data-bet="meron" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="bet-meron-10-9-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_meron_109 }}</b>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="yellow total-meron-10-9-{{$event->id}}">{{ $total_meron_109 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>10 - 9</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-10-9-{{$event->id}}">{{ $total_wala_109 }}</b>
                                        </td>

                                        <td class="border-bottom">
                                            <b class="bet-wala-10-9-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_wala_109 }}</b>
                                        </td>

                                        <td class="padding-half border-bottom">
                                            <i class="round-info add-bet fa fa-plus" data-odds="10-9" data-bet="wala" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                            <i class="round-info cancel-bet fa fa-times" data-odds="10-9" data-bet="wala" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding-half border-bottom">
                                        
                                        </td>
                                        
                                        <td class="border-bottom">
                                            <i class="round-red cancel-bet fa fa-times" data-odds="10-8" data-bet="meron" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>
                                        <td class="padding-half border-bottom">
                                            <i class="round-red add-bet fa fa-plus" data-odds="10-8" data-bet="meron" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="bet-meron-10-8-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_meron_108 }}</b>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="yellow total-meron-10-8-{{$event->id}}">{{ $total_meron_108 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>10 - 8</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-10-8-{{$event->id}}">{{ $total_wala_108 }}</b>
                                        </td>

                                        <td class="border-bottom">
                                            <b class="bet-wala-10-8-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_wala_108 }}</b>
                                        </td>

                                        <td class="padding-half border-bottom">
                                            <i class="round-info add-bet fa fa-plus" data-odds="10-8" data-bet="wala" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                            <i class="round-info cancel-bet fa fa-times" data-odds="10-8" data-bet="wala" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding-half border-bottom">
                                        
                                        </td>
                                        
                                        <td class="border-bottom">
                                            <i class="round-red cancel-bet fa fa-times" data-odds="8-6" data-bet="meron" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>
                                        <td class="padding-half border-bottom">
                                            <i class="round-red add-bet fa fa-plus" data-odds="8-6" data-bet="meron" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="bet-meron-8-6-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_meron_86 }}</b>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="yellow total-meron-8-6-{{$event->id}}">{{ $total_meron_86 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>8 - 6</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-8-6-{{$event->id}}">{{ $total_wala_86 }}</b>
                                        </td>

                                        <td class="border-bottom">
                                            <b class="bet-wala-8-6-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_wala_86 }}</b>
                                        </td>

                                        <td class="padding-half border-bottom">
                                            <i class="round-info add-bet fa fa-plus" data-odds="8-6" data-bet="wala" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                            <i class="round-info cancel-bet fa fa-times" data-odds="8-6" data-bet="wala" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding-half border-bottom">
                                        
                                        </td>
                                        
                                        <td class="border-bottom">
                                            <i class="round-red cancel-bet fa fa-times" data-odds="11-8" data-bet="meron" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>
                                        <td class="padding-half border-bottom">
                                            <i class="round-red add-bet fa fa-plus" data-odds="11-8" data-bet="meron" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="bet-meron-11-8-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_meron_118 }}</b>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="yellow total-meron-11-8-{{$event->id}}">{{ $total_meron_118 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>11 - 8</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-11-8-{{$event->id}}">{{ $total_wala_118 }}</b>
                                        </td>

                                        <td class="border-bottom">
                                            <b class="bet-wala-11-8-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_wala_118 }}</b>
                                        </td>

                                        <td class="padding-half border-bottom">
                                            <i class="round-info add-bet fa fa-plus" data-odds="11-8" data-bet="wala" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                            <i class="round-info cancel-bet fa fa-times" data-odds="11-8" data-bet="wala" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding-half border-bottom">
                                        
                                        </td>
                                        
                                        <td class="border-bottom">
                                            <i class="round-red cancel-bet fa fa-times" data-odds="9-10" data-bet="meron" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>
                                        <td class="padding-half border-bottom">
                                            <i class="round-red add-bet fa fa-plus" data-odds="9-10" data-bet="meron" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="bet-meron-9-10-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_meron_910 }}</b>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="yellow total-meron-9-10-{{$event->id}}">{{ $total_meron_910 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>9 - 10</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-9-10-{{$event->id}}">{{ $total_wala_910 }}</b>
                                        </td>

                                        <td class="border-bottom">
                                            <b class="bet-wala-9-10-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_wala_910 }}</b>
                                        </td>

                                        <td class="padding-half border-bottom">
                                            <i class="round-info add-bet fa fa-plus" data-odds="9-10" data-bet="wala" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                            <i class="round-info cancel-bet fa fa-times" data-odds="9-10" data-bet="wala" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding-half border-bottom">
                                        
                                        </td>
                                        
                                        <td class="border-bottom">
                                            <i class="round-red cancel-bet fa fa-times" data-odds="8-10" data-bet="meron" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>
                                        <td class="padding-half border-bottom">
                                            <i class="round-red add-bet fa fa-plus" data-odds="8-10" data-bet="meron" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="bet-meron-8-10-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_meron_810 }}</b>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="yellow total-meron-8-10-{{$event->id}}">{{ $total_meron_810 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>8 - 10</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-8-10-{{$event->id}}">{{ $total_wala_810 }}</b>
                                        </td>

                                        <td class="border-bottom">
                                            <b class="bet-wala-8-10-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_wala_810 }}</b>
                                        </td>

                                        <td class="padding-half border-bottom">
                                            <i class="round-info add-bet fa fa-plus" data-odds="8-10" data-bet="wala" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                            <i class="round-info cancel-bet fa fa-times" data-odds="8-10" data-bet="wala" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding-half border-bottom">
                                        
                                        </td>
                                        
                                        <td class="border-bottom">
                                            <i class="round-red cancel-bet fa fa-times" data-odds="6-8" data-bet="meron" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>
                                        <td class="padding-half border-bottom">
                                            <i class="round-red add-bet fa fa-plus" data-odds="6-8" data-bet="meron" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="bet-meron-6-8-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_meron_68 }}</b>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="yellow total-meron-6-8-{{$event->id}}">{{ $total_meron_68 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>6 - 8</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-6-8-{{$event->id}}">{{ $total_wala_68 }}</b>
                                        </td>

                                        <td class="border-bottom">
                                            <b class="bet-wala-6-8-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_wala_68 }}</b>
                                        </td>

                                        <td class="padding-half border-bottom">
                                            <i class="round-info add-bet fa fa-plus" data-odds="6-8" data-bet="wala" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                            <i class="round-info cancel-bet fa fa-times" data-odds="6-8" data-bet="wala" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding-half border-bottom">
                                        
                                        </td>
                                        
                                        <td class="border-bottom">
                                            <i class="round-red cancel-bet fa fa-times" data-odds="8-11" data-bet="meron" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>
                                        <td class="padding-half border-bottom">
                                            <i class="round-red add-bet fa fa-plus" data-odds="8-11" data-bet="meron" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="bet-meron-8-11-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_meron_811 }}</b>
                                        </td>
                                        <td class="border-bottom">
                                            <b class="yellow total-meron-8-11-{{$event->id}}">{{ $total_meron_811 }}</b>
                                        </td>

                                        <td class="border-bottom"><b>8 - 11</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-8-11-{{$event->id}}">{{ $total_wala_811 }}</b>
                                        </td>

                                        <td class="border-bottom">
                                            <b class="bet-wala-8-11-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_wala_811 }}</b>
                                        </td>

                                        <td class="padding-half border-bottom">
                                            <i class="round-info add-bet fa fa-plus" data-odds="8-11" data-bet="wala" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                            <i class="round-info cancel-bet fa fa-times" data-odds="8-11" data-bet="wala" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding-half border-bottom">
                                        
                                        </td>
                                        
                                        <td class="border-bottom">
                                            <!-- <i class="round-red cancel-bet fa fa-times" data-bet="meron" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/> -->
                                        </td>
                                        <td class="padding-half border-bottom">
                                            <!-- <i class="round-red add-bet fa fa-plus" data-bet="meron" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/> -->
                                        </td>
                                        <td class="border-bottom">
                                        
                                        </td>
                                        <td class="border-bottom">
                                            <b>DRAW</b>
                                        </td>

                                        <td class="border-bottom"><b>1 - 8</b></td>

                                        <td class="yellow border-bottom">
                                            <b class="yellow total-wala-1-8-{{$event->id}}">{{ $total_wala_18 }}</b>
                                        </td>

                                        <td class="border-bottom">
                                            <b class="bet-wala-1-8-{{Auth::user()->id}}-{{$event->id}}">{{ $bet_wala_18 }}</b>
                                        </td>

                                        <td class="padding-half border-bottom">
                                            <i class="round-info add-bet fa fa-plus" data-bet="wala" data-odds="1-8" data-toggle="tooltip" data-placement="top" data-html="true" title="<b class='sptooltip'>Add Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                            <i class="round-info cancel-bet fa fa-times" data-bet="wala" data-odds="1-8" data-toggle="tooltip" data-placement="top"  data-html="true" title="<b class='sptooltip'>Cancel your Bet</b>"><i/>
                                        </td>

                                        <td class="border-bottom">
                                        
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        @endif
                    </table>
                    
                </td>

                <td width="41%">
                    <table class="border-bottom" width="100%" cellpadding="0" cellspacing="5" style="{{ (Auth::user()->is_admin == 1) ? '' : 'margin-top: -6%'}}">
                        <tr>
                            <td colspan="10" style="width: 500px;text-align: center">
                                <p><b class="realtime"></b></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="border text-center padding-half">
                                <a class="nostyle" href="{{ route('home') }}">Home</a>
                            </td>
                            <td class="border text-center">
                                <a class="nostyle" href="{{ route('dashboard') }}">Dashboard</a>
                            </td>
                            <td class="border text-center">
                                <a class="nostyle" href="{{ route('terms') }}">Rules</a>
                            </td>
                            <td class="border text-center">
                                <a class="nostyle" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" class="text-center">
                                <img src="{{ asset('images/logo3.png') }}" class="image-responsive" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" class="padding border-top border-bottom text-center">
                            @php
                                $type = '';
                                $color = '';
                                $greeting = '';
                                if(Auth::user()->is_super_admin == 1){
                                    $type = 'Super Admin';
                                    $color = '#48b0f7';
                                    $greeting = 'Welcome Super Admin ';
                                }elseif(Auth::user()->is_admin == 1){
                                    $type = Auth::user()->type->name;
                                    
                                    $greeting = 'Welcome '.$type.' ';
                                    if(Auth::user()->type->prefix == 'csr')
                                        $color = 'pink';
                                    else
                                        $color = 'yellow';
                                }else{
                                    $greeting = 'GOODLUCK Player';
                                    $color = 'yellow';
                                }
                            @endphp
                                {{$greeting}} <b class="{{$color}}">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</b> !
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10">
                                <table style="width: 100%" class="" width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="padding-half text-center">FIGHT NO.</td>
                                        <td class="padding-half text-center">STATUS</td>
                                        <td class="padding-half text-center">WINNER</td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="padding-half text-center yellow"><b class="game-no-{{ $event->id }}">{{$game_count}}</b></td>
                                        <td class="padding-half text-center yellow"><b class="game-status-{{ $event->id }}">{{strtoupper($game_status)}}</b></td>
                                        <td class="padding-half text-center yellow"><b class="game-winner-{{ $event->id }}">{{strtoupper($game_winner)}}</b></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" class="border-top padding-half text-center"><b>LIVE CHAT</b></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" class="border-top">
                                            <div class="messages ps-container ps-theme-default ps-active-y border-bottom" id="chat-content" style="overflow-y: scroll !important; height:350px !important;">
                                                @if(count($messages) == 0)
                                                <p class="text-center no-data">No Messages ..</p>
                                                @endif			
                                                @foreach($messages as $chat)
                                                    @php
                                                        $type = '';
                                                        $color = '';
                                                        if($chat->from->is_super_admin == 1){
                                                            $type = 'Super Admin';
                                                            $color = '#48b0f7';
                                                        }elseif($chat->from->is_admin == 1){
                                                            $type = $chat->from->type->name;
                                                            
                                                            if($chat->from->type->prefix == 'csr')
                                                                $color = 'pink';
                                                            else
                                                                $color = '#48b0f7';
                                                        }
                                                    @endphp
                                                    <div class="media media-chat {{ ($chat->created_by == Auth::user()->id) ? 'media-chat-reverse' : '' }}">
                                                        @if($chat->created_by !== Auth::user()->id)
                                                        <img class="avatar" src="{{ (isset($chat->from->detail->photo)) ? $chat->from->detail->photo : asset('images/default.png') }}"> &nbsp; <b style="color: {{ $color }}">{{ $type }} {{ $chat->from->first_name.' '.$chat->from->last_name }}</b>
                                                        @endif
                                                        <div class="media-body">
                                                            <p class="{{ ($chat->created_by !== Auth::user()->id) ? 'media-chat-other' : '' }}">{{ $chat->message }}</p>
                                                            <!-- <p class="meta"><time class="chat_id_{{$chat->id}}_{{ $event->id }}_reverse" data-id="{{$chat->id}}_{{ $event->id }}" data-datetime="{{ $chat->created_at }}">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans() }}</time></p> -->
                                                        </div>
                                                    </div>
                                                @endforeach

                                            </div>

                                            <div class="publisher bt-1 border-light"> 
                                                <img class="avatar avatar-xs" src="{{ (isset(Auth::user()->detail->photo)) ? Auth::user()->detail->photo : asset('images/default.png') }}" alt=""> 
                                                <form method="POST" action="{{ route('chat-store') }}?api_token={{Auth::user()->api_token}}" id="chat-form" style="width: 100%">
                                                    @csrf
                                                    <input name="room_id" class="room_id" value="{{ $event->id }}" type="hidden">
                                                    <input class="photo" value="{{ (isset(Auth::user()->detail->photo)) ? Auth::user()->detail->photo : asset('images/default.png') }}" type="hidden">
                                                    <input name="created_by" class="created_by" value="{{ Auth::user()->id }}" type="hidden">
                                                    <input name="message" class="chat-input form-control" type="text" placeholder="Write something" autocomplete="off">
                                                    <!-- <a class="publisher-btn text-info send" href="#" data-abc="true"><i class="fas fa-paper-plane"></i></a>  -->
                                                </form>
                                            </div>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                
                <td width="9%">
                                    
                    <table width="100%" class="border-left border-bottom" cellpadding="0" cellspacing="0" style="{{ (Auth::user()->is_admin == 1) ? '' : 'margin-top: -25%'}}">
                        <tr>
                            <td class="radius">
                                <div class="statuses" style="overflow-y: scroll !important; height:890px !important;">

                                @php

                                    $total_m = 0;
                                    $total_w = 0;
                                    $total_d = 0;
                                    $total_c = 0;

                                    foreach($game_logs as $logs)
                                    {
                                        if($logs->winner == 'ML')
                                            $total_m = $total_m + 1;
                                        if($logs->winner == 'MD')
                                            $total_m = $total_m + 1;
                                        if($logs->winner == 'WL')
                                            $total_w = $total_w + 1;
                                        if($logs->winner == 'WD')
                                            $total_w = $total_w + 1;
                                        if($logs->winner == 'D')
                                            $total_d = $total_d + 1;
                                        if($logs->winner == 'C')
                                            $total_c = $total_c + 1;
                                    }

                                @endphp

                                <button type="button" class="btn btn-danger" style="margin: 2px;width: 95%">
                                    <b>M : <span class="total_win_meron_{{$event->id}}">{{$total_m}}</span></b>
                                </button>
                                
                                <button type="button" class="btn btn-info" style="margin: 2px;width: 95%">
                                    <b >W : <span class="total_win_wala_{{$event->id}}">{{$total_m}}</span></b>
                                </button>
                                
                                <button type="button" class="btn btn-success" style="margin: 2px;width: 95%">
                                    <b>D : <span class="total_win_draw_{{$event->id}}">{{$total_d}}</span></b>
                                </button>
                                
                                <button type="button" class="btn btn-warning" style="margin: 2px;margin-bottom: 20px;width: 95%">
                                    <b>C : <span class="total_win_cancel_{{$event->id}}">{{$total_c}}</span></b>
                                </button>
                                
                                    <div class="game_logs_{{$event->id}}">
                                    @foreach($game_logs as $logs)
                                        @if(is_null($logs->winner))
                                        <button type="button" class="btn btn-default log-current-{{$event->id}}-{{$logs->game_count}}" style="margin: 2px;width: 95%">
                                            <b class="current_{{$event->id}}_{{$logs->game_count}}">{{$logs->game_count}} : </b>
                                        </button>
                                        @endif

                                        @if($logs->winner == 'ML' || $logs->winner == 'MD')
                                        <button type="button" class="btn btn-danger winner-{{$event->id}}-{{$logs->game_count}}" style="margin: 2px;width: 95%">
                                            <b>{{$logs->game_count}} : {{$logs->winner}}</b>
                                        </button>
                                        @endif

                                        @if($logs->winner == 'WL' || $logs->winner == 'WD')
                                        <button type="button" class="btn btn-info winner-{{$event->id}}-{{$logs->game_count}}" style="margin: 2px;width: 95%">
                                            <b>{{$logs->game_count}} : {{$logs->winner}}</b>
                                        </button>
                                        @endif

                                        @if($logs->winner == 'D')
                                        <button type="button" class="btn btn-success winner-{{$event->id}}-{{$logs->game_count}}" style="margin: 2px;width: 95%">
                                            <b>{{$logs->game_count}} : {{$logs->winner}}</b>
                                        </button>
                                        @endif

                                        @if($logs->winner == 'C')
                                        <button type="button" class="btn btn-warning winner-{{$event->id}}-{{$logs->game_count}}" style="margin: 2px;width: 95%">
                                            <b>{{$logs->game_count}} : {{$logs->winner}}</b>
                                        </button>
                                        @endif
                                    @endforeach
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tale>
                </td>
            </tr>
        </table>

    </div>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ env('CHAT_SERVER', 'localhost') }}/socket.io/socket.io.js"></script>
    <script type="text/javascript">
    
    // var socket = io("{{ url('/') }}:3000", {transports: ['websocket'], path: "/socket.io"});
    var socket = io('{{ env('CHAT_SERVER', 'localhost') }}', {transports: ['websocket'], path: "/socket.io"});
    var token = 'api_token={{ Auth::user()->api_token }}';
    var user_id = '{{Auth::user()->id}}'; 
    var event_id = '{{$event->id}}';
    var game_count = $('.game-count-'+event_id+'').val();

    // betting broadcast
    socket.on('announce', function(data){
        if(data['event_id'] == event_id)
        {
            if(data['action'] == "status")
            {
                if(data['value'] == "open")
                {
                    // add new game count
                    $('.game-count-'+data['event_id']+'').val(data['game_count']);
                    $('.game-no-'+data['event_id']+'').html(data['game_count']);
                }
                else
                {
                    $('.game-count-'+data['event_id']+'').val(data['game_count']);
                    $('.game-no-'+data['event_id']+'').html(data['game_count']);

                    // update user bet per odds live --------------------

                    var total_meron_1010 = 0;
                    var total_meron_109 = 0;
                    var total_meron_108 = 0;
                    var total_meron_86 = 0;
                    var total_meron_118 = 0;
                    var total_meron_910 = 0;
                    var total_meron_810 = 0;
                    var total_meron_811 = 0;
                    var total_meron_68 = 0;
                    var total_meron_18 = 0;

                    var total_wala_1010 = 0;
                    var total_wala_109 = 0;
                    var total_wala_108 = 0;
                    var total_wala_86 = 0;
                    var total_wala_118 = 0;
                    var total_wala_910 = 0;
                    var total_wala_810 = 0;
                    var total_wala_811 = 0;
                    var total_wala_68 = 0;
                    var total_wala_18 = 0;

                    data['game_betting'].forEach(function(bet){

                        // for specific user dom
                        if(user_id == bet.user_id)
                            $('.bet-'+bet.type+'-'+bet.odds+'-'+bet.user_id+'-'+bet.event_id+'').html(bet.amount);

                        // for all in the dom same event id
                        if(bet.type == 'wala' && bet.odds == '10-10')
                            total_wala_1010 = parseInt(total_wala_1010) + parseInt(bet.amount);
                        if(bet.type == 'wala' && bet.odds == '10-9')
                            total_wala_109 = parseInt(total_wala_109) + parseInt(bet.amount);
                        if(bet.type == 'wala' && bet.odds == '10-8')
                            total_wala_108 = parseInt(total_wala_108) + parseInt(bet.amount);
                        if(bet.type == 'wala' && bet.odds == '8-6')
                            total_wala_86 = parseInt(total_wala_86) + parseInt(bet.amount);
                        if(bet.type == 'wala' && bet.odds == '11-8')
                            total_wala_118 = parseInt(total_wala_118) + parseInt(bet.amount);
                        if(bet.type == 'wala' && bet.odds == '9-10')
                            total_wala_910 = parseInt(total_wala_910) + parseInt(bet.amount);
                        if(bet.type == 'wala' && bet.odds == '8-10')
                            total_wala_810 = parseInt(total_wala_810) + parseInt(bet.amount);
                        if(bet.type == 'wala' && bet.odds == '8-11')
                            total_wala_811 = parseInt(total_wala_811) + parseInt(bet.amount);
                        if(bet.type == 'wala' && bet.odds == '6-8')
                            total_wala_68 = parseInt(total_wala_68) + parseInt(bet.amount);
                        if(bet.type == 'wala' && bet.odds == '1-8')
                            total_wala_18 = parseInt(total_wala_18) + parseInt(bet.amount);

                        if(bet.type == 'meron' && bet.odds == '10-10')
                            total_meron_1010 = parseInt(total_meron_1010) + parseInt(bet.amount);
                        if(bet.type == 'meron' && bet.odds == '10-9')
                            total_meron_109 = parseInt(total_meron_109) + parseInt(bet.amount);
                        if(bet.type == 'meron' && bet.odds == '10-8')
                            total_meron_108 = parseInt(total_meron_108) + parseInt(bet.amount);
                        if(bet.type == 'meron' && bet.odds == '8-6')
                            total_meron_86 = parseInt(total_meron_86) + parseInt(bet.amount);
                        if(bet.type == 'meron' && bet.odds == '11-8')
                            total_meron_118 = parseInt(total_meron_118) + parseInt(bet.amount);
                        if(bet.type == 'meron' && bet.odds == '9-10')
                            total_meron_910 = parseInt(total_meron_910) + parseInt(bet.amount);
                        if(bet.type == 'meron' && bet.odds == '8-10')
                            total_meron_810 = parseInt(total_meron_810) + parseInt(bet.amount);
                        if(bet.type == 'meron' && bet.odds == '8-11')
                            total_meron_811 = parseInt(total_meron_811) + parseInt(bet.amount);
                        if(bet.type == 'meron' && bet.odds == '6-8')
                            total_meron_68 = parseInt(total_meron_68) + parseInt(bet.amount);
                    });
                    
                    $('.total-meron-10-10-'+event_id+'').html(total_meron_1010);
                    $('.total-meron-10-9-'+event_id+'').html(total_meron_109);
                    $('.total-meron-10-8-'+event_id+'').html(total_meron_108);
                    $('.total-meron-8-6-'+event_id+'').html(total_meron_86);
                    $('.total-meron-11-8-'+event_id+'').html(total_meron_118);
                    $('.total-meron-9-10-'+event_id+'').html(total_meron_910);
                    $('.total-meron-8-10-'+event_id+'').html(total_meron_810);
                    $('.total-meron-8-11-'+event_id+'').html(total_meron_811);
                    $('.total-meron-6-8-'+event_id+'').html(total_meron_68);

                    $('.total-wala-10-10-'+event_id+'').html(total_wala_1010);
                    $('.total-wala-10-9-'+event_id+'').html(total_wala_109);
                    $('.total-wala-10-8-'+event_id+'').html(total_wala_108);
                    $('.total-wala-8-6-'+event_id+'').html(total_wala_86);
                    $('.total-wala-11-8-'+event_id+'').html(total_wala_118);
                    $('.total-wala-9-10-'+event_id+'').html(total_wala_910);
                    $('.total-wala-8-10-'+event_id+'').html(total_wala_810);
                    $('.total-wala-8-11-'+event_id+'').html(total_wala_811);
                    $('.total-wala-6-8-'+event_id+'').html(total_wala_68);
                    $('.total-wala-1-8-'+event_id+'').html(total_wala_18);

                    // end update user bet and total odds -----------------------------


                    // notify users ------------------------
                    
                    data['notify_users'].forEach(function(bet){
                        if(user_id == bet.user_id)
                        {
                            var content = {};
                            var style = "withicon";
                            content.message = bet.message;
                            content.title = 'Notification';
                            if (style == "withicon") {
                                content.icon = 'fa fa-bell';
                            } else {
                                content.icon = 'none';
                            }

                            $.notify(content,{
                                type: 'info',
                                placement: {
                                    from: 'top',
                                    align: 'right'
                                },
                                time: 1000,
                                delay: 0,
                            });

                            $('.points-'+user_id+'').html(bet.points);

                            if(bet.is_cancel == '1')
                                $('.bet-'+bet.type+'-'+bet.odds+'-'+bet.user_id+'-'+bet.event_id+'').html('');
                        }
                    });

                    // notify users end --------------------
                }

                

                $('.game-count-'+data['event_id']+'').val(data['game_count']);
                $('.game-no-'+data['event_id']+'').html(data['game_count']);
                $('.game-status-'+data['event_id']+'').html(data['value'].toUpperCase());
            }
            else if(data['action'] == "next-game")
            {
                $('button[data-action="next-game"]').data('value', data['game_count']);
                $('.game-count-'+event_id+'').val(data['game_count']);
                $('.game-no-'+event_id+'').html(data['game_count']);
                $('.game-winner-'+data['event_id']+'').html('NONE');

                var but = '<button type="button" class="btn btn-default log-current-'+data['event_id']+'-'+data['game_count']+'" style="margin: 2px;width: 95%">';
                but += '    <b class="current_'+data['event_id']+'_'+data['game_count']+'">'+data['game_count']+' : </b>';
                but += '</button>';

                $('.game_logs_'+data['event_id']+'').prepend(but);
            }
            else
            {
                $('.game-winner-'+data['event_id']+'').html(data['winner'].toUpperCase());
                $('.log-current-'+data['event_id']+'-'+data['game_count']+'').remove();

                var but = '';

                if(data['winner'] == 'ML' || data['winner'] == 'MD'){
                    but += '<button type="button" class="btn btn-danger winner-'+data['event_id']+'-'+data['game_count']+'" style="margin: 2px;width: 95%">';
                    but += '    <b>'+data['game_count']+' : '+data['winner']+'</b>';
                    but += '</button>';
                }

                if(data['winner'] == 'WL' || data['winner'] == 'WD'){
                    but += '<button type="button" class="btn btn-info winner-'+data['event_id']+'-'+data['game_count']+'" style="margin: 2px;width: 95%">';
                    but += '    <b>'+data['game_count']+' : '+data['winner']+'</b>';
                    but += '</button>';
                }

                if(data['winner'] == 'D'){
                    but += '<button type="button" class="btn btn-success winner-'+data['event_id']+'-'+data['game_count']+'" style="margin: 2px;width: 95%">';
                    but += '    <b>'+data['game_count']+' : '+data['winner']+'</b>';
                    but += '</button>';
                }

                if(data['winner'] == 'C'){
                    but += '<button type="button" class="btn btn-warning winner-'+data['event_id']+'-'+data['game_count']+'" style="margin: 2px;width: 95%">';
                    but += '    <b>'+data['game_count']+' : '+data['winner']+'</b>';
                    but += '</button>';
                }

                $('.winner-'+data['event_id']+'-'+data['game_count']+'').remove();

                $('.total_win_meron_'+data['event_id']+'').html(data['total_win_meron']);
                $('.total_win_wala_'+data['event_id']+'').html(data['total_win_wala']);
                $('.total_win_draw_'+data['event_id']+'').html(data['total_win_draw']);
                $('.total_win_cancel_'+data['event_id']+'').html(data['total_win_cancel']);

                $('.game_logs_'+data['event_id']+'').prepend(but);

                // notify users ------------------------
                    
                data['notify_users'].forEach(function(bet){
                    if(user_id == bet.user_id)
                    {
                        var content = {};
                        var style = "withicon";
                        content.message = bet.message;
                        content.title = 'Notification';
                        if (style == "withicon") {
                            content.icon = 'fa fa-bell';
                        } else {
                            content.icon = 'none';
                        }

                        $.notify(content,{
                            type: 'success',
                            placement: {
                                from: 'top',
                                align: 'right'
                            },
                            time: 1000,
                            delay: 0,
                        });

                        $('.points-'+user_id+'').html(bet.points);

                        // if(bet.is_cancel == '1')
                        //     $('.bet-'+bet.type+'-'+bet.odds+'-'+bet.user_id+'-'+bet.event_id+'').html('');
                    }
                });

                // notify users end --------------------
            }
        }
    });

    // betting broadcast
    socket.on('betting', function(data){
        $.ajax({
            url: "{{ route('get-total-per-type') }}?"+token+"",
            type: 'GET',
            data: data,
            success: function(response){
                $('.total-'+data['type']+'-'+data['odds']+'-'+data['event_id']+'').html(response['total']);
            },
            error: function(){
                custom_alert('warning', 'Unknown Error: Please contact system admin or Just Refresh this page');
            }
        });

    });

    // live chat
    socket.on('live-chat', function(msg){

        var chat_message = '';

        if(msg['room_id'] == event_id)
        {
            if(user_id == msg['user_id'])
            {
                chat_message += '<div class="media media-chat media-chat-reverse">';
                chat_message += '    <div class="media-body">';
                chat_message += '        <p>'+msg['message']+'</p>';
                //chat_message += '        <p class="meta"><time class="chat_id__'+$('time').length+'_{{ $event->id }}_reverse" data-id="_'+$('time').length+'_{{ $event->id }}_reverse" data-datetime="'+today+'">'+msg['created_at']+'</time></p>';
                chat_message += '    </div>';
                chat_message += '</div>';
            }
            else
            {
                chat_message += '<div class="media media-chat">';
                chat_message += '    <img class="avatar" src="'+msg['photo']+'" > '+msg['name']+'';
                chat_message += '    <div class="media-body">';
                chat_message += '        <p>'+msg['message']+'</p>';
                //chat_message += '        <p class="meta"><time class="chat_id__'+$('time').length+'_{{ $event->id }}" data-id="_'+$('time').length+'_{{ $event->id }}" data-datetime="'+today+'">'+msg['created_at']+'</time></p>';
                chat_message += '    </div>';
                chat_message += '</div>';
            }

            $('.messages').append(chat_message);
            $("#chat-content").animate({ 
                scrollTop: $( 
                    '#chat-content').get(0).scrollHeight 
            }, 1000); 
        }
    });

    $(document).on('click', '.control', function(){
        var dis = $(this)
        var action = dis.data('action');
        var value = dis.data('value');
        var message = dis.data('message');

        var data = {
            'action': action,
            'value': value,
            'event_id': event_id
        }

        var buttons = {
            confirm: {
                text : 'Yes, Do it.',
                className : 'btn btn-success',
                value: '1'
            },
            cancel: {
                visible: true,
                className: 'btn'
            }
        }

        // if(action == 'status' && value == 'close')
        // {
        //     buttons = {
        //         confirm: {
        //             text : 'MERON as LAMADO',
        //             className : 'btn btn-danger',
        //             value: 'meron'
        //         },
        //         confirm2: {
        //             text : 'WALA as LAMADO',
        //             className : 'btn btn-info',
        //             value: 'wala'
        //         },
        //         cancel: {
        //             visible: true,
        //             className: 'btn'
        //         }
        //     } 
        // }

        swal({
            title: 'Are you sure ?',
            text: message,
            icon: 'warning',
            buttons: buttons
        })
        .then((value) => {
            if (value) 
            {
                // set lamado in the game
                // data['lamado'] = value;
                
                // action
                if(action == "status")
                {
                    if(value == "open")
                    {
                        $.ajax({
                            url: "{{ route('announce') }}?"+token+"",
                            type: 'POST',
                            data: data,
                            success: function(response){
                                // add new game count
                                if(response['status'] != '0')
                                {
                                    data['game_count'] = response['game_count']
                                    data['status'] = response['status']
                                    data['winner'] = response['winner']
                                    data['total_win_meron'] = response['total_m']
                                    data['total_win_wala'] = response['total_w']
                                    data['total_win_draw'] = response['total_d']
                                    data['total_win_cancel'] = response['total_c']
                                    socket.emit('announce', data);
                                }
                                else
                                {
                                    custom_alert('warning', response['message']);
                                }
                            },
                            error: function(){
                                custom_alert('warning', 'Unknown Error: Please contact system admin or Just Refresh this page');
                            }
                        });
                    }
                    else
                    {
                        $.ajax({
                            url: "{{ route('announce') }}?"+token+"",
                            type: 'POST',
                            data: data,
                            success: function(response){
                                if(response['status'] != '0')
                                {
                                    data['game_count'] = response['game_count']
                                    data['status'] = response['status']
                                    data['winner'] = response['winner']
                                    data['total_win_meron'] = response['total_m']
                                    data['total_win_wala'] = response['total_w']
                                    data['total_win_draw'] = response['total_d']
                                    data['total_win_cancel'] = response['total_c']
                                    data['notify_users'] = response['notify_users']
                                    data['game_betting'] = response['game_betting']
                                    socket.emit('announce', data);
                                }
                                else
                                {
                                    custom_alert('warning', response['message']);
                                }
                            },
                            error: function(){
                                custom_alert('warning', 'Unknown Error: Please contact system admin or Just Refresh this page');
                            }
                        });
                    }
                }
                else if(action == "next-game")
                {
                    $.ajax({
                        url: "{{ route('announce') }}?"+token+"",
                        type: 'POST',
                        data: data,
                        success: function(response){
                            if(response['status'] != '0')
                            {
                                data['game_count'] = response['game_count']
                                data['status'] = response['status']
                                data['winner'] = response['winner']
                                data['total_win_meron'] = response['total_m']
                                data['total_win_wala'] = response['total_w']
                                data['total_win_draw'] = response['total_d']
                                data['total_win_cancel'] = response['total_c']
                                socket.emit('announce', data);
                            }
                            else
                            {
                                custom_alert('warning', response['message']);
                            }
                        },
                        error: function(){
                            custom_alert('warning', 'Unknown Error: Please contact system admin or Just Refresh this page');
                        }
                    });
                }
                else
                {
                    $.ajax({
                        url: "{{ route('announce') }}?"+token+"",
                        type: 'POST',
                        data: data,
                        success: function(response){
                            if(response['status'] != '0')
                            {
                                data['game_count'] = response['game_count']
                                data['status'] = response['status']
                                data['winner'] = response['winner']
                                data['total_win_meron'] = response['total_m']
                                data['total_win_wala'] = response['total_w']
                                data['total_win_draw'] = response['total_d']
                                data['total_win_cancel'] = response['total_c']
                                data['notify_users'] = response['notify_users']
                                socket.emit('announce', data);
                            }
                            else
                            {
                                custom_alert('warning', response['message']);
                            }
                        },
                        error: function(){
                            custom_alert('warning', 'Unknown Error: Please contact system admin or Just Refresh this page');
                        }
                    });
                }
            } 
            else 
            {
                swal.close();
            }
        });
    });
    
    $(document).on('click', '.add-bet', function(){
        var dis = $(this)
        var type = dis.data('bet');
        var odds = dis.data('odds');
        var amount = $('input[name="bet_input"]').val();
        var game_count = $('.game-count-'+event_id+'').val();
        var current_bet = $('.bet-'+type+'-'+odds+'-'+user_id+'-'+event_id+'').html();

        if(amount == '')
            amount = 0;

        if(current_bet == '')
            current_bet = 0;

        current_bet = parseInt(current_bet);
        amount = parseInt(amount)// + current_bet;

        // get current points
        var points = 0;
        $.get('{{ route('get-points', [Auth::user()->id]) }}?'+token+'', function(data){
            points = parseInt(data['points']);
            
            if(current_bet == 0)
            {
                if(amount == 0)
                {
                    custom_alert('info', 'Please specify bet amount.');
                    $('input[name="bet_input"]').focus();
                }
                else
                {
                    if(points >= amount)
                    {

                        var bet_data = {
                            'action': 'add-bet',
                            'event_id': event_id,
                            'user_id': user_id,
                            'game_count': game_count,
                            'odds': odds,
                            'type': type,
                            'current_bet': current_bet,
                            'amount': amount,
                        }

                        $.ajax({
                            url: "{{ route('add-bet') }}?"+token+"",
                            type: 'POST',
                            data: bet_data,
                            success: function(response){
                                if(response['status'] == '1')
                                {
                                    socket.emit('betting', bet_data);
                                    $('.bet-'+type+'-'+odds+'-'+user_id+'-'+event_id+'').html(amount);
                                    $('.points-'+user_id+'').html(response['points']);
                                }
                                else
                                {
                                    custom_alert('warning', response['message']);
                                }
                            },
                            error: function(){
                                custom_alert('warning', 'Unknown Error: Please contact system admin or Just Refresh this page');
                            }
                        });
                    }
                    else
                    {
                        custom_alert('info', 'Not enough Points!');
                        $('input[name="bet_input"]').focus();
                    }
                }
            }
        });
    });

    $(document).on('click', '.cancel-bet', function(){
        var dis = $(this)
        var type = dis.data('bet');
        var odds = dis.data('odds');
        var amount = $('input[name="bet_input"]').val();
        var game_count = $('.game-count-'+event_id+'').val();
        var current_bet = $('.bet-'+type+'-'+odds+'-'+user_id+'-'+event_id+'').html();

        if(amount == '')
            amount = 0;

        if(current_bet == '')
            current_bet = 0;

        current_bet = parseInt(current_bet);
        amount = parseInt(amount);

        var bet_data = {
            'action': 'cancel-bet',
            'event_id': event_id,
            'user_id': user_id,
            'game_count': game_count,
            'odds': odds,
            'type': type,
            'current_bet': current_bet,
            'amount': amount,
        }

        if(current_bet !== 0) 
        {
            $.ajax({
                url: "{{ route('cancel-bet') }}?"+token+"",
                type: 'POST',
                data: bet_data,
                success: function(response){
                    if(response['status'] == '1')
                    {
                        socket.emit('betting', bet_data);
                        $('.bet-'+type+'-'+odds+'-'+user_id+'-'+event_id+'').html('');
                        $('.points-'+user_id+'').html(response['points']);
                    }
                    else
                    {
                        custom_alert('warning', response['message']);
                    }
                },
                error: function(){
                    custom_alert('warning', 'Unknown Error: Please contact system admin or Just Refresh this page');
                }
            });
        }
    });

    $(document).on('click', '.bet-amount', function(){
        var dis = $(this)
        var amount = dis.html();

        if(amount == 'ALL IN')
        {
            $.get('{{ route('get-points', [Auth::user()->id]) }}?'+token+'', function(data){
                $('input[name="bet_input"]').val(data['points']);
            });
        }
        else
        {
            $('input[name="bet_input"]').val(amount);
        }
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(document).ready(function () {
        $("#chat-content").animate({ 
            scrollTop: $( 
                '#chat-content').get(0).scrollHeight 
        }, 100); 

        @php
            $type = '';
            $color = '';
            if(Auth::user()->is_super_admin == 1){
                $type = 'Super Admin';
                $color = '#48b0f7';
            }elseif(Auth::user()->is_admin == 1){
                $type = Auth::user()->type->name;
                
                if(Auth::user()->type->prefix == 'csr')
                    $color = 'pink';
                else
                    $color = '#48b0f7';
            }
        @endphp

        var message = $('#chat-form .chat-input').val();
        var room = $('#chat-form .room_id').val();
        var photo = $('#chat-form .photo').val();
        var user_id = $('#chat-form .created_by').val();
        var name = '<b style="color: {{$color}}">{{ $type }} {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</b>';
        var today = new Date();
        var created_at = moment(today, 'YYYY.MM.DD').fromNow();

        $(".send").click(function() {
            $("#chat-form").submit();
        });

        $('#chat-form').submit(function(e){

            e.preventDefault();

            var dis = $(this)
            var form = new FormData(dis[0]);

            message = dis.find('.chat-input').val();

            if(message != '')
            {
                socket.emit('live-chat', {
                    'message': message,
                    'name': name,
                    'created_at': created_at,
                    'photo': photo,
                    'room_id': room,
                    'user_id': user_id
                });

                $.ajax({
                    type: 'POST',
                    url: dis.attr('action'),
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function(response){
                        console.log('message-sent');
                    }
                });

                $('.no-data').remove();
            }

            $('.chat-input').val('');

            return false;
            
        });

        var $win = $('#chat-content');
        var current_page = 1;

        $win.scroll(function () {
            if ($win.scrollTop() == 0){
                current_page = current_page + 1;

                $.get('{{ route("next-page") }}?room_id={{ $event->id }}&page='+current_page+'&'+token+'', function(response){
                    // console.log(response);

                    if(response.length !== 0)
                    {
                        $.each(response, function(data, val){

                            if(val.room_id == event_id)
                            {
                                var chat_message = '';

                                if(user_id == val.created_by)
                                {
                                    chat_message += '<div class="media media-chat media-chat-reverse">';
                                    chat_message += '    <div class="media-body">';
                                    chat_message += '        <p>'+val.message+'</p>';
                                    //chat_message += '        <p class="meta"><time class="chat_id__'+$('time').length+'_{{ $event->id }}_reverse" data-id="_'+$('time').length+'_{{ $event->id }}_reverse" data-datetime="'+today+'">'+msg['created_at']+'</time></p>';
                                    chat_message += '    </div>';
                                    chat_message += '</div>';
                                }
                                else
                                {
                                    var type = '';
                                    var color = '';

                                    var photo = '{{asset("images/default.png")}}';

                                    if(val.from.detail !== null && val.from.detail.photo !== null)
                                    {
                                        photo = val.from.detail.photo;
                                    }

                                    if(val.from.is_super_admin == 1){
                                        type = 'Super Admin';
                                        color = '#48b0f7';
                                    }else if(val.from.is_admin == 1){
                                        type = val.from.type.name;
                                            
                                        if(val.from.type.prefix == 'csr')
                                            color = 'pink';
                                        else
                                            color = '#48b0f7';
                                    }

                                    chat_message += '<div class="media media-chat">';
                                    chat_message += '    <img class="avatar" src="'+photo+'" > <b style="color: '+color+'">'+type+' '+val.from.first_name+' '+val.from.last_name+'</b>';
                                    chat_message += '    <div class="media-body">';
                                    chat_message += '        <p>'+val.message+'</p>';
                                    //chat_message += '        <p class="meta"><time class="chat_id__'+$('time').length+'_{{ $event->id }}" data-id="_'+$('time').length+'_{{ $event->id }}" data-datetime="'+today+'">'+msg['created_at']+'</time></p>';
                                    chat_message += '    </div>';
                                    chat_message += '</div>';
                                }

                                $('.messages').prepend(chat_message);
                            }

                        });
                    }
                    else
                        current_page = current_page - 1;

                });

                // console.log(current_page);
            }
        });

    });

    tday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
    tmonth=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
    
    function GetClock(){
        var d=new Date();
        var nday=d.getDay(),nmonth=d.getMonth(),ndate=d.getDate(),nyear=d.getYear();
        if(nyear<1000) nyear+=1900;
        var nhour=d.getHours(),nmin=d.getMinutes(),nsec=d.getSeconds(),ap;
        
        if(nhour==0){ap=" AM";nhour=12;}
        else if(nhour<12){ap=" AM";}
        else if(nhour==12){ap=" PM";}
        else if(nhour>12){ap=" PM";nhour-=12;}
        
        if(nmin<=9) nmin="0"+nmin;
        if(nsec<=9) nsec="0"+nsec;
        
        $('.realtime').html(""+tday[nday]+", "+tmonth[nmonth]+" "+ndate+", "+nyear+" "+nhour+":"+nmin+":"+nsec+ap+"");
    }

    window.onload=function(){
        GetClock();
        setInterval(GetClock,1000);
    }
    </script>
    
    <script src="{{ asset('app/js/plugin/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('app/js/plugin/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Accept': 'application/json',
                'Authorization': 'Bearer {{Auth::user()->api_token}}'
            }
        }); 
        $( document ).ajaxComplete(function() {
            // Required for Bootstrap tooltips in DataTables
            $('[data-toggle="tooltip"]').tooltip({
                "html": true,
                //"delay": {"show": 1000, "hide": 0},
            });
        });

        function custom_alert(status, message)
        {
            swal({
                title: 'Opps..',
                text: message,
                icon: status,
                buttons:{
                    close: {
                        visible: true,
                        className: 'btn btn-info'
                    }
                }
            });
        }
    </script>
</body>
</html>

