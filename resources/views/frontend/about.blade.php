@extends('layouts.app')

@section('title') About | {{config('app.name', 'Laravel')}} @endsection
@section('content')
<header id="head" class="secondary"></header>

<!-- container -->
<div class="container">

    <div class="row">
        
        <!-- Article main content -->
        <article class="col-sm-8 maincontent">
            <header class="page-header">
                <h1 class="page-title">About us</h1>
            </header>
            <p>
            Here at {{config('app.name', 'Laravel')}}, We offer free registration and free viewing services. 
            <br>Once registered, contact any our CSRs on duty for the activation of your Account ({{config('app.name', 'Laravel')}} Account). 
            <br>Don’t forget your unique username and password. Username is required by the CSR to activate your {{config('app.name', 'Laravel')}} Account. A valid facebook account is necessary for validation purposes. 
            <br>To join the game, buy points through our Official Receivers found below.
            To learn about our latest promotions and events, like our facebook page or go to the homepage of this site.
            </p>
            
        </article>
        <!-- /Article -->
        
        <!-- Sidebar -->
        <aside class="col-sm-4 sidebar sidebar-right">

            <div class="widget">
                <h4>Upcoming Events</h4>
                <ul class="list-unstyled list-spaces">
                    @if(count($data) == 0)
                    <li>
                        <span class="small text-muted">No events yet.</span>
                    </li>
                    @endif
                    @foreach($data as $event)
                    <li>
                        <a href="{{ route('event-web-show', [$event->id]) }}">
                            {{ $event->name }}
                            <br>
                            <span class="small text-muted"><b>Location</b>: {{$event->location}}</span>
                            <br>
                            <span class="small text-muted"><b>Expected Deal</b>: {{$event->expected_deal}}</span>
                            <br>
                            <span class="small text-muted"><b>Event Date</b>: {{date('Y-m-d', strtotime($event->event_date))}}</span>
                            <br>
                            <!-- <img src="{{$event->cover_image}}" class="image-responsive" /> -->
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>

        </aside>
        <!-- /Sidebar -->

    </div>
</div>	<!-- /container -->
@endsection