@extends('layouts.app')

@section('title') Welcome to {{config('app.name', 'Laravel')}} @endsection
@section('content')

<div>

	@if(count($data) == 0)
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#carousel-example-generic" data-slide-to="1" class="active"></li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<div class="item active" style="background:#181015 url({{ asset('images/bg.jpg') }}) no-repeat; background-size: cover">
				<img src="" alt="FIGHT OF THE DECADE" style="height: 100vh">
				<div class="carousel-caption" style="margin-bottom: 300px;text-align: center">
					<h1 class="page-title"><b>BULACAN LIVE | CARD DEAL</b></h1>
					<h4 class="page-title">Expected Deals: <b>30</b></h4>
					<h4 class="page-title">Location: <b>Bulacan | Derby Cockpit</b></h4>
					<h4 class="page-title">Event Date: <b>TODAY {{date('F j, Y')}}</b>
					</h4>
					<a target="_blank" href="#" class="btn btn-primary btn-lg">Watch Now &nbsp;<i class="fa fa-arrow-right"></i></a>
				</div>
			</div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>

	</div>
		
	@else
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
		<?php $count = 0; ?>
			@foreach($data as $event)
			<?php $count = $count + 1;?>
			<li data-target="#carousel-example-generic" data-slide-to="{{ $count - 1}}" class="{{ ($count - 1 == 0) ? 'active' : '' }}"></li>
			@endforeach
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<?php $count = 0; ?>
			@foreach($data as $event)
			<?php $count = $count + 1;?>
			<div class="item {{ ($count == 1) ? 'active' : '' }}" style="background:#181015 url({{$event->cover_image}}) no-repeat; background-size: cover">
				<img src="" alt="{{$event->name}}" style="height: 100vh">
				<?php
					$position = '';

					if($count == 1){
						$position = 'center';
					}else{
						$position = 'right';

						if(($count % 2) == 0){
							$position = 'left';
						}
					}
				?>
				<div class="carousel-caption" style="margin-bottom: 200px;text-align: {{ $position }}">
					<h1 class="page-title"><b>{{$event->name}}</b></h1>
					<h4 class="page-title">Expected Deals: <b>{{$event->expected_deal}}</b></h4>
					<h4 class="page-title">Location: <b>{{$event->arena->name}} in {{$event->arena->location}}</b></h4>
					<h4 class="page-title">Event Date: 
						<b>
						@if(date('Y-m-d', strtotime($event->event_date)) == date('Y-m-d'))
						Today
						@endif
						{{date('F j, Y', strtotime($event->event_date))}}
						</b>
					</h4>
					@if(date('Y-m-d', strtotime($event->event_date)) == date('Y-m-d'))
					<a target="_blank" href="{{ route('event-web-show', [$event->id]) }}" class="btn btn-primary btn-lg">Watch Now &nbsp;<i class="fa fa-arrow-right"></i></a>
					@endif
				</div>
			</div>
			@endforeach
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>

	</div>
	@endif

	<!-- Intro -->
	<div class="container text-center">
		<br> <br>
		<h2 class="thin">{{config('app.name', 'Laravel')}}  </h2>
		<p class="text-muted">
			The best online Cock fighting in the Philippines.
		</p>
		<h3 class="text-center thin">Presented to you our Beautiful CSR's</h3>
	</div>
	<!-- /Intro-->
		
	<!-- Highlights - jumbotron -->
	<div class="container">
		
		<div class="row">
			@if(count($csr) == 0)
				@for($a=1;$a<=9;$a++)
				<div class="col-xs-6 col-md-4 highlight">
					<a href="https://fb.com/profile.php" target="_blank" class="thumbnail">
						@php
							$text = 'front/images/csr/'.$a.'.jpg';
							$img = asset($text);
						@endphp
						
						<img class="img-responsive" src="{{ $img }}" alt="{{ $img }}" style="width: 350px; height: 350px">
					</a>
					<div class="h-caption"><h4>CSR Jenny {{$a}}</h4></div>
				</div>
				@endfor
			@endif
			
			@foreach($csr as $cs)
				@php
					$text = (isset($cs->detail->photo)) ? $cs->detail->photo : asset('images/default.png');
					$img = asset($text);
					$messenger = (isset($cs->detail) && !is_null($cs->detail->messenger_url)) ? $cs->detail->messenger_url : '';
				@endphp
			<div class="col-xs-6 col-md-4 highlight">
				<a href="{{$messenger}}" target="_blank" class="thumbnail">
					<img class="img-responsive" src="{{ $img }}" alt="{{ $img }}" style="width: 350px; height: 350px">
				</a>
				<div class="h-caption"><h4>CSR {{ucfirst($cs->first_name)}}</h4></div>
			</div>
			@endforeach
		</div> <!-- /row  -->
	
	</div>
	<!-- /Highlights -->
</div>
	
@endsection

@push('scripts')
<script>
$('.carousel').carousel({
  interval: 3000
})
</script>
@endpush