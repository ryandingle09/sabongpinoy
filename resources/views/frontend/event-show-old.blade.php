@extends('layouts.app')

@section('title') {{$event->name}} @endsection

@section('content')
<header id="head" class="secondary"></header>

<div class="container" style="margin-top: 10px">
	<div class="row">
		<div class="col-md-8">
			<iframe width="750" height="470" src="https://www.youtube.com/embed/TJBhlrr-lhk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>

		<div class="col-md-4" style="border: 1px solid #ccc;border-radius: 5px;padding: 5px;background: #eee">
			<div class="text-center" style="border-bottom: 1px solid #ccc;">
				<h4>Live Chat</h4>
			</div>
			<div class="messages ps-container ps-theme-default ps-active-y" id="chat-content" style="overflow-y: scroll !important; height:350px !important;">
				@if(count($messages) == 0)
				<p class="text-center no-data">No Messages ..</p>
				@endif			
				@foreach($messages as $chat)
					<div class="media media-chat {{ ($chat->created_by == Auth::user()->id) ? 'media-chat-reverse' : '' }}">
						@if($chat->created_by !== Auth::user()->id)
						<img class="avatar" src="{{ $chat->from->detail->photo }}"> &nbsp; {{ $chat->from->first_name.' '.$chat->from->last_name }}
						@endif
						<div class="media-body">
							<p class="{{ ($chat->created_by !== Auth::user()->id) ? 'media-chat-other' : '' }}">{{ $chat->message }}</p>
							<!-- <p class="meta"><time class="chat_id_{{$chat->id}}_{{ $event->id }}_reverse" data-id="{{$chat->id}}_{{ $event->id }}" data-datetime="{{ $chat->created_at }}">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans() }}</time></p> -->
						</div>
					</div>
				@endforeach

			</div>

			<div class="publisher bt-1 border-light"> 
				<img class="avatar avatar-xs" src="{{ Auth::user()->detail->photo }}" alt=""> 
				<form method="POST" action="{{ route('chat-store') }}?api_token={{Auth::user()->api_token}}" id="chat-form" style="width: 100%">
					@csrf
					<input name="room_id" class="room_id" value="{{ $event->id }}" type="hidden">
					<input class="photo" value="{{ (isset(Auth::user()->detail->photo)) ? Auth::user()->detail->photo : asset('images/default.png') }}" type="hidden">
					<input name="created_by" class="created_by" value="{{ Auth::user()->id }}" type="hidden">
					<input name="message" class="chat-input form-control" type="text" placeholder="Write something" autocomplete="off">
					<!-- <a class="publisher-btn text-info send" href="#" data-abc="true"><i class="fas fa-paper-plane"></i></a>  -->
				</form>
			</div>
		</div>
		
		<div class="col-md-12" style="border: 1px solid #ccc">
				this is a test
		</div>
	</div>
</div>
@endsection

@push('styles')
<style>
	.ps-container::-webkit-scrollbar {
		width: 0.5em;
	}
	
	.ps-container::-webkit-scrollbar-track {
	box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
	}
	
	.ps-container::-webkit-scrollbar-thumb {
	background-color: darkgrey;
	outline: 1px solid slategrey;
	}

	.ps-container {
		position: relative;
	}

	.ps-container {
		-ms-touch-action: auto;
		touch-action: auto;
		overflow: hidden !important;
		-ms-overflow-style: none
	}

	.media-chat {
		padding-right: 0px;
		margin-bottom: 0
	}

	.media {
		/* padding: 16px 12px; */
		-webkit-transition: background-color .2s linear;
		transition: background-color .2s linear
	}

	.media .avatar {
		flex-shrink: 0
	}

	.avatar {
		position: relative;
		display: inline-block;
		width: 36px;
		height: 36px;
		line-height: 36px;
		text-align: center;
		border-radius: 100%;
		background-color: #f5f6f7;
		color: #8b95a5;
		text-transform: uppercase
	}

	.media-chat .media-body {
		-webkit-box-flex: initial;
		flex: initial;
		display: table
	}

	.media-body {
		/* min-width: 0 */
		width: 100%;
	}

	.media-chat .media-body p {
		position: relative;
		/* padding: 6px 8px; */
		margin: 0px 0;
		/* background-color: #f5f6f7; */
		background-color: #eee;
		border-radius: 3px;
		font-weight: 100;
		color: #909090;
	}

	.media>* {
		/* margin: 0 8px */
	}

	.media-chat .media-body p.meta {
		background-color: transparent !important;
		padding: 0;
		opacity: .8
	}

	.media-meta-day {
		-webkit-box-pack: justify;
		justify-content: space-between;
		-webkit-box-align: center;
		align-items: center;
		margin-bottom: 0;
		color: #8b95a5;
		opacity: .8;
		font-weight: 400
	}

	.media {
		/* padding: 0px 12px; */
		-webkit-transition: background-color .2s linear;
		transition: background-color .2s linear
	}

	.media-meta-day::before {
		margin-right: 16px
	}

	.media-meta-day::before,
	.media-meta-day::after {
		content: '';
		-webkit-box-flex: 1;
		flex: 1 1;
		border-top: 1px solid #ebebeb
	}

	.media-meta-day::after {
		content: '';
		-webkit-box-flex: 1;
		flex: 1 1;
		border-top: 1px solid #ebebeb
	}

	.media-meta-day::after {
		/* margin-left: 16px */
	}

	.media-chat-other {
		background: #e5ebee;
		margin-top: 5px
	}

	.media-chat.media-chat-reverse {
		/* padding-right: 12px;
		padding-left: 64px; */
		-webkit-box-orient: horizontal;
		-webkit-box-direction: reverse;
		flex-direction: row-reverse
	}

	.media-chat {
		/* padding-right: 64px; */
		padding-right: 5px;
		margin-bottom: 0
	}

	.media {
		/* padding: 16px 12px; */
		-webkit-transition: background-color .2s linear;
		transition: background-color .2s linear
	}

	.media-chat.media-chat-reverse .media-body p {
		float: right;
		clear: right;
		background-color: #48b0f7;
		color: #fff
	}

	.media-chat .media-body p {
		position: relative;
		padding: 6px 8px;
		/* margin: 4px 0; */
		background-color: #eeeeee;
		border-radius: 3px
	}

	.border-light {
		/* border-color: #f1f2f3 !important */
		/* border: 1px solid #f3f5ee; */
	}

	.bt-1 {
		/* border-top: 1px solid #ebebeb !important */
		/* border-top: 1px solid #ccc !important; */
	}

	.publisher {
		position: relative;
		display: -webkit-box;
		display: flex;
		-webkit-box-align: center;
		align-items: center;
		padding: 12px 0px;
		/* background-color: #f9fafb; */
		/* border-top: 1px solid #ccc; */
	}

	.publisher>*:first-child {
		margin-left: 0
	}

	.publisher>* {
		margin: 0 8px
	}

	.publisher-input {
		-webkit-box-flex: 1;
		flex-grow: 1;
		border: none;
		outline: none !important;
		background-color: transparent
	}

	button,
	input,
	optgroup,
	select,
	textarea {
		font-family: Roboto, sans-serif;
		font-weight: 300
	}

	.publisher-btn {
		background-color: transparent;
		border: none;
		color: #8b95a5;
		font-size: 16px;
		cursor: pointer;
		overflow: -moz-hidden-unscrollable;
		-webkit-transition: .2s linear;
		transition: .2s linear
	}

	.file-group {
		position: relative;
		overflow: hidden
	}

	.publisher-btn {
		background-color: transparent;
		border: none;
		color: #cac7c7;
		font-size: 16px;
		cursor: pointer;
		overflow: -moz-hidden-unscrollable;
		-webkit-transition: .2s linear;
		transition: .2s linear
	}

	.file-group input[type="file"] {
		position: absolute;
		opacity: 0;
		z-index: -1;
		width: 20px
	}

	.text-info {
		color: #48b0f7 !important
	}
</style>
@endpush

@push('scripts')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ url('/') }}:3000/socket.io/socket.io.js"></script>
<script>

// function update(old_date, class_name) {
// 	date = moment('Sun Mar 21 2020 16:45:33 GMT+0800 (China Standard Time)').fromNow();
// 	console.log(date);
//     // $('.'+class_name+'').html(date);
// };

// $('time').each( function(){
// 	var dis = $(this)
// 	var old_date = dis.data('datetime');
// 	var class_name = 'chat_id_'+dis.data('id');
// 	setInterval(update(old_date, class_name), 1000);
// });

$(function () {
	$("#chat-content").animate({ 
		scrollTop: $( 
			'#chat-content').get(0).scrollHeight 
	}, 100); 

	var socket = io("{{ url('/') }}:3000", {path: "/socket.io"});
	// var moment = require('moment');

	var message = $('#chat-form .chat-input').val();
	var room = $('#chat-form .room_id').val();
	var photo = $('#chat-form .photo').val();
	var user_id = $('#chat-form .created_by').val();
	var name = '{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}';
	var today = new Date();
	var created_at = moment(today, 'YYYY.MM.DD').fromNow();

	$(".send").click(function() {
		$("#chat-form").submit();
	});

	$('#chat-form').submit(function(e){

		e.preventDefault();

		var dis = $(this)
		var form = new FormData(dis[0]);

		message = dis.find('.chat-input').val();

		if(message != '')
		{
			socket.emit('live-chat', {
				'message': message,
				'name': name,
				'created_at': created_at,
				'photo': photo,
				'room_id': room,
				'user_id': user_id
			});

			$.ajax({
				type: 'POST',
				url: dis.attr('action'),
				data: form,
				processData: false,
				contentType: false,
				success: function(response){
					console.log('message-sent-to-server');
				}
			});

			$('.no-data').remove();
		}

		$('.chat-input').val('');

		return false;
		
	});

	socket.on('live-chat', function(msg){

		var chat_message = '';

		if(msg['room_id'] == '{{ $event->id }}')
		{
			if('{{ Auth::user()->id }}' == msg['user_id'])
			{
				chat_message += '<div class="media media-chat media-chat-reverse">';
				chat_message += '    <div class="media-body">';
				chat_message += '        <p>'+msg['message']+'</p>';
				//chat_message += '        <p class="meta"><time class="chat_id__'+$('time').length+'_{{ $event->id }}_reverse" data-id="_'+$('time').length+'_{{ $event->id }}_reverse" data-datetime="'+today+'">'+msg['created_at']+'</time></p>';
				chat_message += '    </div>';
				chat_message += '</div>';
			}
			else
			{
				chat_message += '<div class="media media-chat">';
				chat_message += '    <img class="avatar" src="'+msg['photo']+'" > '+msg['name']+'';
				chat_message += '    <div class="media-body">';
				chat_message += '        <p>'+msg['message']+'</p>';
				//chat_message += '        <p class="meta"><time class="chat_id__'+$('time').length+'_{{ $event->id }}" data-id="_'+$('time').length+'_{{ $event->id }}" data-datetime="'+today+'">'+msg['created_at']+'</time></p>';
				chat_message += '    </div>';
				chat_message += '</div>';
			}

			$('.messages').append(chat_message);
			$("#chat-content").animate({ 
				scrollTop: $( 
					'#chat-content').get(0).scrollHeight 
			}, 1000); 
		}
	});

	var $win = $('#chat-content');
	var current_page = 1;

	$win.scroll(function () {
		if ($win.scrollTop() == 0){
			current_page = current_page + 1;

			$.get('{{ route("next-page") }}?room_id={{ $event->id }}&page='+current_page+'&api_token={{Auth::user()->api_token}}', function(response){
				console.log(response);

				if(response.length !== 0)
				{
					$.each(response, function(data, val){

						if(''+val.room_id+'' == '{{ $event->id }}')
						{
							var chat_message = '';

							if('{{ Auth::user()->id }}' == val.created_by)
							{
								chat_message += '<div class="media media-chat media-chat-reverse">';
								chat_message += '    <div class="media-body">';
								chat_message += '        <p>'+val.message+'</p>';
								//chat_message += '        <p class="meta"><time class="chat_id__'+$('time').length+'_{{ $event->id }}_reverse" data-id="_'+$('time').length+'_{{ $event->id }}_reverse" data-datetime="'+today+'">'+msg['created_at']+'</time></p>';
								chat_message += '    </div>';
								chat_message += '</div>';
							}
							else
							{
								chat_message += '<div class="media media-chat">';
								chat_message += '    <img class="avatar" src="'+val.from.detail.photo+'" > '+val.from.first_name+' '+val.from.last_name+'';
								chat_message += '    <div class="media-body">';
								chat_message += '        <p>'+val.message+'</p>';
								//chat_message += '        <p class="meta"><time class="chat_id__'+$('time').length+'_{{ $event->id }}" data-id="_'+$('time').length+'_{{ $event->id }}" data-datetime="'+today+'">'+msg['created_at']+'</time></p>';
								chat_message += '    </div>';
								chat_message += '</div>';
							}

							$('.messages').prepend(chat_message);
						}

					});
				}
				else
				{
					current_page = current_page - 1;
				}

			});

			console.log(current_page);
		}
	});

});
</script>

@endpush