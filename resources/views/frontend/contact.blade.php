@extends('layouts.app')

@section('title') Contact Us | {{config('app.name', 'Laravel')}} @endsection
@section('content')

<header id="head" class="secondary"></header>

<!-- container -->
<div class="container">

    <div class="row">
        
        <!-- Article main content -->
        <article class="col-sm-9 maincontent" style="margin-bottom: 150px">
            <header class="page-header">
                <h1 class="page-title">Contact Us</h1>
            </header>
            
            <p>
                We’d love to hear from you. Any concern that you feel we may help? send us a message via email of Phone/mobile with some info about you and we will get back to you as soon as we can. Please allow a couple days for me to respond.
            </p>
            <br>
                <!-- <form>
                    <div class="row">
                        <div class="col-sm-4">
                            <input class="form-control" type="text" placeholder="Name">
                        </div>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" placeholder="Email">
                        </div>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" placeholder="Phone">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <textarea placeholder="Type your message here..." class="form-control" rows="9"></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="checkbox"><input type="checkbox"> Sign up for newsletter</label>
                        </div>
                        <div class="col-sm-6 text-right">
                            <input class="btn btn-action" type="submit" value="Send message">
                        </div>
                    </div>
                </form> -->

        </article>
        <!-- /Article -->
        
        <!-- Sidebar -->
        <aside class="col-sm-3 sidebar sidebar-right">

            <div class="widget">
                <h4>Email</h4>
                <address>
                    <a href="mailto:#">Pinoy.sabong31820@gmail.com</a>
                </address>
                <h4>Phone:</h4>
                <address>
                    +63 975-256-3921
                </address>
            </div>

        </aside>
        <!-- /Sidebar -->

    </div>
</div>	<!-- /container -->
@endsection