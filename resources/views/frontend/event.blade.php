@extends('layouts.app')

@section('title') Events | {{config('app.name', 'Laravel')}} @endsection
@section('content')
<header id="head" class="secondary"></header>

<!-- container -->
<div class="container">

	<div class="row" style="margin-bottom: 250px">
		@if(count($data) == 0)
		<article class="col-sm-12 maincontent text-center">
			<header class="page-header">
				<h1 class="page-title">No Schedule Event at this moment!</h1>
			</header>
        </article>
		<!-- /Article -->
		@endif
		<!-- Article main content -->
		@foreach($data as $event)
		@if(date('Y-m-d', strtotime($event->event_date)) == date('Y-m-d'))
        <article class="col-sm-12 maincontent text-center">
			<a href="{{ route('event-web-show', [$event->id]) }}">
				<header class="page-header">
					<h1 class="page-title">{{$event->name}}</h1>
					<br>
					<span>Expected Deals: <b>{{$event->expected_deal}}</b>&nbsp;| Location: <b>{{$event->arena->name}} in {{$event->arena->location}}</b>&nbsp;| Schedule: <b>Today {{date('F j, Y', strtotime($event->event_date))}}</b></span>
				</header>
				
				<img src="{{$event->cover_image}}" class="image-responsive" />
			</a>
        </article>
		<!-- /Article -->
		@else
		<article class="col-sm-12 maincontent text-center">
			<header class="page-header">
				<h1 class="page-title">{{$event->name}}</h1>
				<br>
				<span>Expected Deals: <b>{{$event->expected_deal}}</b>&nbsp;| Location: <b>{{$event->arena->name}} in {{$event->arena->location}}</b>&nbsp;| Schedule: <b>{{date('F j, Y', strtotime($event->event_date))}}</b></span>
			</header>
			
			<img src="{{$event->cover_image}}" class="image-responsive" />
        </article>
		<!-- /Article -->
		@endif
		@endforeach

		<hr />
	</div>
	
</div>	<!-- /container -->
@endsection