@extends('layouts.app')

@section('title') Disclaimer | {{config('app.name', 'Laravel')}} @endsection
@section('content')
<header id="head" class="secondary"></header>

<!-- container -->
<div class="container">

    <div class="row">
        
        <!-- Article main content -->
        <article class="col-sm-8 maincontent">
            <header class="page-header">
                <h1 class="page-title">Disclaimer</h1>
            </header>
            <p>
            If you are within the Philippine government jurisdiction, you are not allowed to avail the services (e.g. live streaming, video recording, previews, etc.) that we provide andthe company has no liability or obligations to any issues while using our website.

            The information provided by PinoySabong  (“we”, “us” or “our” ) on 
            www.Pinoysabong.com (the “site”) is for general information and entertainment purposes only. All information on the Site is provided in good faight, however we make no representation or warranty of any kind, express or implied.
            For sports wagering information contained on this site, please confirm the regulation in your jurisdiction as they vary from  state to state, province to province and country to country. Use of this information is violation of any law is probihited
            By using this site, you acknowledge that you have read and agree to be bound by these terms and it is solely at your own risk. For any problem within the operation, please contact Our Customer Service Representatives (CSR) 
            PinoySabong management may make change and / update the system. Its Terms and Conditions without prior notice.

            </p>
            
        </article>
        <!-- /Article -->
        
        <!-- Sidebar -->
        <aside class="col-sm-4 sidebar sidebar-right">

            <div class="widget">
                <h4>Upcoming Events</h4>
                <ul class="list-unstyled list-spaces">
                    @if(count($data) == 0)
                    <li>
                        <span class="small text-muted">No events yet.</span>
                    </li>
                    @endif
                    @foreach($data as $event)
                    <li>
                        <a href="{{ route('event-web-show', [$event->id]) }}">
                            {{ $event->name }}
                            <br>
                            <span class="small text-muted"><b>Location</b>: {{$event->location}}</span>
                            <br>
                            <span class="small text-muted"><b>Expected Deal</b>: {{$event->expected_deal}}</span>
                            <br>
                            <span class="small text-muted"><b>Event Date</b>: {{date('Y-m-d', strtotime($event->event_date))}}</span>
                            <br>
                            <!-- <img src="{{$event->cover_image}}" class="image-responsive" /> -->
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>

        </aside>
        <!-- /Sidebar -->

    </div>
</div>	<!-- /container -->
@endsection