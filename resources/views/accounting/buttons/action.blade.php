@if(Auth::user()->is_super_admin == 1)
    <a href="{{ route('accounting-show', [$item->id]) }}"><i class="far fa-eye"></i></a>&nbsp;
@else
    @foreach(Auth::user()->user_access as $ua)
        @if($ua->access->prefix == 'read')
        <a href="{{ route('accounting-show', [$item->id]) }}"><i class="far fa-eye"></i></a>&nbsp;
        @endif
    @endforeach
@endif