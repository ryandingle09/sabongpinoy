@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Modules</h2>
                        <h5 class="text-white op-7 mb-2">Showing module <b>{{ ucfirst($data->name) }}</b> details</h5>
                    </div>
                    
                    <div class="ml-md-auto py-2 py-md-0">
                        <a href="{{ route('module') }}" class="btn btn-white btn-border btn-round mr-2"><i class="fas fa-step-backward"></i>&nbsp; Back </a>
                        <a href="{{ route('module-edit', [$data->id]) }}" class="btn btn-white btn-border btn-round mr-2"><i class="fas fa-edit"></i>&nbsp; Edit </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Name:</label>

                                <div class="col-md-6">
                                <h3 style="text-decoration: underline">{{ $data->name }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Prefix:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ $data->prefix }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="order" class="col-md-4 col-form-label text-md-right">Icon:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ (!is_null($data->icon)) ? $data->icon : 'No icon' }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="order" class="col-md-4 col-form-label text-md-right">Order:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ $data->order }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Description:</label>

                                <div class="col-md-6">
                                <h3>{{ $data->description }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Created At:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ date('Y-m-d', strtotime($data->created_at)) }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Created By:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ ($data->created_who) ? $data->created_who->first_name.' '.$data->created_who->last_name : 'System' }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Updated At:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ date('Y-m-d', strtotime($data->updated_at)) }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Updated By:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ ($data->updated_who) ? $data->updated_who->first_name.' '.$data->updated_who->last_name : 'System' }} </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
