@if(Auth::user()->is_super_admin == 1)
    <a href="{{ route('arena-show', [$item->id]) }}"><i class="far fa-eye"></i></a>&nbsp;
    <a href="{{ route('arena-edit', [$item->id]) }}"><i class="fas fa-pencil-alt"></i></a>&nbsp;
    <a href="{{ route('arena-destroy', [$item->id]) }}" class="delete" data-id="{{ $item->id }}"><i class="fas fa-trash-alt"></i></a>

    <form id="form-{{ $item->id }}" action="{{ route('arena-destroy', [$item->id]) }}" method="POST" style="display: none;">
        @csrf
    </form>
@else
    @foreach(Auth::user()->user_access as $ua)
        @if($ua->access->prefix == 'read')
        <a href="{{ route('arena-show', [$item->id]) }}"><i class="far fa-eye"></i></a>&nbsp;
        
        @elseif($ua->access->prefix == 'update')
        <a href="{{ route('arena-edit', [$item->id]) }}"><i class="fas fa-pencil-alt"></i></a>&nbsp;
        

        @elseif($ua->access->prefix == 'delete')
        <a href="{{ route('arena-destroy', [$item->id]) }}" class="delete" data-id="{{ $item->id }}"><i class="fas fa-trash-alt"></i></a>

        <form id="form-{{ $item->id }}" action="{{ route('arena-destroy', [$item->id]) }}" method="POST" style="display: none;">
            @csrf
        </form>
        @endif
    @endforeach
@endif