@php
$list = 'terms event-web event-web-show home about disclaimer contact login register password.request password.reset password.email password.confirm verification.notice verification.verify';
@endphp
@if(strpos($list, Route::currentRouteName()) === false && Auth::check())

<div class="main-header">
    <!-- Logo Header -->
    <div class="logo-header" data-background-color="blue">
        
        <a href="{{ route('home') }}" class="logo" style="color: #fff">
            <img src="{{ asset('images/logo-admin.png') }}" alt="{{ config('app.name', 'Laravel') }}" class="navbar-brand">
        </a>
        
        <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon">
                <i class="icon-menu"></i>
            </span>
        </button>
        <button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
        <div class="nav-toggle">
            <button class="btn btn-toggle toggle-sidebar">
                <i class="icon-menu"></i>
            </button>
        </div>
    </div>
    <!-- End Logo Header -->

    <!-- Navbar Header -->
    <nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
        
        <div class="container-fluid">
            <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                <li class="nav-item dropdown hidden-caret">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                        <div class="avatar-sm">
                            <img src="{{ (isset(Auth::user()->detail->photo)) ? Auth::user()->detail->photo : asset('images/default.png') }}" alt="..." class="avatar-img rounded-circle">
                        </div>
                    </a>
                    <ul class="dropdown-menu dropdown-user animated fadeIn">
                        <div class="dropdown-user-scroll scrollbar-outer">
                            <li>
                                <div class="user-box">
                                    <div class="avatar-lg"><img src="{{ (isset(Auth::user()->detail->photo)) ? Auth::user()->detail->photo : asset('images/default.png') }}" alt="image profile" class="avatar-img rounded"></div>
                                    <div class="u-text">
                                        <h4>{{ ucfirst(Auth::user()->first_name) }} {{ ucfirst(Auth::user()->last_name) }}</h4>
                                        <p class="text-muted">{{ Auth::user()->email }}</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('profile') }}">Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </div>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- End Navbar -->
</div>
@else
<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
    <div class="container">
        <div class="navbar-header">
            <!-- Button for smallest screens -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="{{ asset('images/logo-real.png') }}" class="image-responsive" style="margin-top: -25px" alt="{{ strtoupper(config('app.name', 'Laravel')) }}">
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
                <li class="{{ (Route::currentRouteName() == 'home') ? 'active' : '' }}"><a href="{{ route('home') }}">Home</a></li>
                <li class="{{ (Route::currentRouteName() == 'about') ? 'active' : '' }}"><a href="{{ route('about') }}">About</a></li>
                <li class="{{ (Route::currentRouteName() == 'contact') ? 'active' : '' }}"><a href="{{ route('contact') }}">Contact</a></li>
                <li class="{{ (Route::currentRouteName() == 'disclaimer') ? 'active' : '' }}"><a href="{{ route('disclaimer') }}">Disclaimer</a></li>
                <li class="{{ (Route::currentRouteName() == 'event-web') ? 'active' : '' }}"><a href="{{ route('event-web') }}">Events</a></li>
                @if(Auth::check())
                <li><a class="btn" href="{{ route('dashboard') }}">Dashboard</a></li>
                @else
                <li>
                    <div class="btn-group btn-group" role="group">
                        <a class="btn btn-xs" href="{{ route('login') }}">LOGIN</a>
                        <a class="btn btn-xs" href="{{ route('register') }}">REGISTER</a>
                    </div>
                </li>
                @endif
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div> 
<!-- /.navbar -->
@endif