@php
    $list = 'terms event-web event-web-show home about disclaimer contact login register password.request password.reset password.email password.confirm verification.notice verification.verify';
    $route = Route::currentRouteName();
@endphp

@if(strpos($list, Route::currentRouteName()) === false && Auth::check())
<div class="sidebar sidebar-style-2">			
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="{{ (isset(Auth::user()->detail->photo)) ? Auth::user()->detail->photo : asset('images/default.png') }}" class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            {{ ucfirst(Auth::user()->first_name) }} {{ ucfirst(Auth::user()->last_name) }}
                            <span class="user-level">
                                @if(Auth::user()->is_super_admin == 1)
                                Super Admin
                                @elseif(Auth::user()->is_admin == 1)
                                {{ Auth::user()->type->name }}
                                @else
                                Player
                                @endif
                            </span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="{{ route('profile') }}">
                                    <span class="link-collapse">Account Settings</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav nav-primary">
                <li class="nav-item {{ (Route::currentRouteName() == 'dashboard') ? 'active' : '' }}">
                    <a href="{{ route('dashboard') }}">
                        <i class="fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <?php 
                    
                    $uam = [];

                    if(Auth::user()->is_super_admin == 0):
                        if(Auth::user()->is_admin == 1):

                            foreach(Auth::user()->type->role->role_modules as $um):
                                foreach($um->modules as $m):
                                    $uam[] = $m->prefix;
                                endforeach;
                            endforeach;
                        
                        else:

                            foreach(\App\Role::where('prefix', 'player')->first()->role_modules as $um):
                                foreach($um->modules as $m):
                                    $uam[] = $m->prefix;
                                endforeach;
                            endforeach;
                            
                        endif;
                    endif;
                    
                    
                    foreach(\App\Module::orderBy('order','asc')->get() as $item):
                        $a_routes = ''.$item->prefix.' '.$item->prefix.'-create '.$item->prefix.'-edit '.$item->prefix.'-show '.$item->prefix.'-pasaload '.$item->prefix.'-cashout '.$item->prefix.'-loading';

                        if(Auth::user()->is_super_admin == 0):
                            foreach($uam as $a):
                                if($a == $item->prefix):
                ?>
                                    <li class="nav-item {{ (strpos($a_routes, $route) !== false) ? 'active' : '' }}">
                                        <a href="{{ route($item->prefix) }}">
                                            <i class="{{$item->icon}}"></i>
                                            <p>{{$item->name}}</p>
                                        </a>
                                    </li>
                <?php
                                endif;
                            endforeach;
                        else:
                ?>
                            <li class="nav-item {{ (strpos($a_routes, $route) !== false) ? 'active' : '' }}">
                                <a href="{{ route($item->prefix) }}">
                                    <i class="{{$item->icon}}"></i>
                                    <p>{{$item->name}}</p>
                                </a>
                            </li>
                <?php
                        endif;
                    endforeach;
                ?>

            </ul>
        </div>
    </div>
</div>
@endif