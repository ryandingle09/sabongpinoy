@php
$list = 'terms event-web event-web-show home about disclaimer contact login register password.request password.reset password.email password.confirm verification.notice verification.verify';
@endphp
@if(strpos($list, Route::currentRouteName()) === false && Auth::check())

@else

<footer id="footer" class="top-space">

    <div class="footer1">
        <div class="container">
            <div class="row">
                
                <div class="col-md-3 widget">
                    <h3 class="widget-title">Contact</h3>
                    <div class="widget-body">
                        <p>
                            +63 975-256-3921<br>
                            <a href="mailto:#">Pinoy.sabong31820@gmail.com</a><br>
                        <br>
                        </p>	
                    </div>
                </div>

                <div class="col-md-3 widget">
                    <!-- <h3 class="widget-title">Follow Us</h3>
                    <div class="widget-body">
                        <p class="follow-me-icons">
                            <a href=""><i class="fa fa-twitter fa-2"></i></a>
                            <a href=""><i class="fa fa-facebook fa-2"></i></a>
                            <a href=""><i class="fa fa-instagram fa-2"></i></a>
                        </p>	
                    </div> -->
                </div>

                <div class="col-md-6 widget">
                    <h3 class="widget-title">About {{config('app.name', 'Laravel')}}</h3>
                    <div class="widget-body">
                        <p>
                        Here at {{config('app.name', 'Laravel')}}, We offer free registration and free viewing services. Once registered, contact any our CSRs on duty for the activation of your Account ({{config('app.name', 'Laravel')}}   Account). Don’t forget your unique username and password. Username is required by the CSR to activate your {{config('app.name', 'Laravel')}}   Account. A valid facebook account is necessary for validation purposes. To join the game, buy points through our Official Receivers found below.
                        To learn about our latest promotions and events, like our facebook page or go to the homepage of this site.
                        </p>
                    </div>
                </div>

            </div> <!-- /row of widgets -->
        </div>
    </div>

    <div class="footer2">
        <div class="container">
            <div class="row">
                
                <div class="col-md-6 widget">
                    <div class="widget-body">
                        <p class="simplenav">
                            <a href="{{ route('home') }}">Home</a> | 
                            <a href="{{ route('about') }}">About</a> |
                            <a href="{{ route('contact') }}">Contact</a> |
                            <a href="{{ route('disclaimer') }}">Disclamer</a> |
                            <a href="{{ route('terms') }}">Terms and Conditions</a> |
                            <a href="{{ route('event-web') }}"><b>Events</b></a> |
                            <a href="{{ route('register') }}"><b>Sign Up</b></a>
                        </p>
                    </div>
                </div>

                <div class="col-md-6 widget">
                    <div class="widget-body">
                        <p class="text-right">
                            Copyright &copy; {{date('Y')}}, {{config('app.name', 'Laravel')}}. All Rights Reserved</a> 
                        </p>
                    </div>
                </div>

            </div> <!-- /row of widgets -->
        </div>
    </div>

</footer>

@endif