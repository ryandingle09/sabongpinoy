<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="{{ asset('images/ico.png') }}" type="image/x-icon"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('app.name', 'Laravel'))</title>

    @php
        $list = 'terms event-web event-web-show home about disclaimer contact login register password.request password.reset password.email password.confirm verification.notice verification.verify';
    @endphp

    @if(strpos($list, Route::currentRouteName()) === false)

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Fonts and icons -->
	<script src="{{ asset('app/js/plugin/webfont/webfont.min.js') }}"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ["{{ asset('app/css/fonts.min.css') }}"]},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	
    <link rel="stylesheet" href="{{ asset('app/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('app/css/atlantis.min.css') }}"> 

    @else

    <link rel="stylesheet" media="screen" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('front/css/font-awesome.min.css') }}">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="{{ asset('front/css/bootstrap-theme.css') }}" media="screen" >
	<link rel="stylesheet" href="{{ asset('front/css/main.css') }}">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="{{ asset('front/js/html5shiv.js') }}"></script>
	<script src="{{ asset('front/js/respond.min.js') }}"></script>
	<![endif]-->
    
    @endif

    @stack('styles')
</head>
<body class="{{(strpos($list, Route::currentRouteName()) !== false) ? 'home' : '' }}">
    <div class="wrapper">
        @include('layouts.includes.header')
        @include('layouts.includes.sidebar')
        @yield('content')
        @include('layouts.includes.footer')
    </div>
    
    <!--   Core JS Files   -->

    @if(strpos($list, Route::currentRouteName()) === false)

    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
    <script src="{{ asset('app/js/core/jquery.3.2.1.min.js') }}"></script>
    <script src="{{ asset('app/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('app/js/core/bootstrap.min.js') }}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('app/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('app/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>

    <!-- jQuery Scrollbar -->
    <script src="{{ asset('app/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>


    <!-- Chart JS -->
    <!-- <script src="{{ asset('app/js/plugin/chart.js/chart.min.js') }}"></script> -->

    <!-- jQuery Sparkline -->
    <!-- <script src="{{ asset('app/js/plugin/jquery.sparkline/jquery.sparkline.min.js') }}"></script> -->

    <!-- Chart Circle -->
    <!-- <script src="{{ asset('app/js/plugin/chart-circle/circles.min.js') }}"></script> -->

    <!-- Datatables -->
    <script src="{{ asset('app/js/plugin/datatables/datatables.min.js') }}"></script>

    <!-- Bootstrap Notify -->
    <script src="{{ asset('app/js/plugin/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

    <!-- jQuery Vector Maps -->
    <!-- <script src="{{ asset('app/js/plugin/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('app/js/plugin/jqvmap/maps/jquery.vmap.world.js') }}"></script> -->

    <!-- Sweet Alert -->
    <script src="{{ asset('app/js/plugin/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Atlantis JS -->
    <script src="{{ asset('app/js/atlantis.min.js') }}"></script>
    
    <script type="text/javascript">
        $( document ).ajaxComplete(function() {
            // Required for Bootstrap tooltips in DataTables
            $('[data-toggle="tooltip"]').tooltip({
                "html": true,
                //"delay": {"show": 1000, "hide": 0},
            });
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        @if (session('status'))
            var content = {};
            var style = "withicon";
            content.message = "{{ session('status') }}";
            content.title = 'Notification';
            if (style == "withicon") {
                content.icon = 'fa fa-bell';
            } else {
                content.icon = 'none';
            }

            $.notify(content,{
                type: 'success',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                time: 1000,
                delay: 0,
            });
        @endif

        $(document).on('click', '.delete', function(e){
            e.preventDefault();

            var id = $(this).data('id');

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                buttons:{
                    confirm: {
                        text : 'Yes, delete it!',
                        className : 'btn btn-success'
                    },
                    cancel: {
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {
                    $('#form-'+ id).submit();
                } else {
                    swal.close();
                }
            });
        });
    </script>

    @else

    <!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="{{ asset('front/js/headroom.min.js') }}"></script>
	<script src="{{ asset('front/js/jQuery.headroom.min.js') }}"></script>
	<script src="{{ asset('front/js/template.js') }}"></script>

    @endif

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
    </script>

    @stack('scripts')
</body>
</html>
