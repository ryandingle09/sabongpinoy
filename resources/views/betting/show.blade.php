@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Betting History</h2>
                        <h5 class="text-white op-7 mb-2">Showing Betting History Record #<b>{{ ucfirst($data->id) }}</b> details</h5>
                    </div>
                    
                    <div class="ml-md-auto py-2 py-md-0">
                        <a href="{{ route('betting') }}" class="btn btn-white btn-border btn-round mr-2"><i class="fas fa-step-backward"></i>&nbsp; Back </a>
                        <!-- <a href="{{ route('betting', [$data->id]) }}" class="btn btn-white btn-border btn-round mr-2">Print <i class="fas fa-print"></i></a> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Event:</label>

                                <div class="col-md-6">
                                    <h3>{{ $data->event->name }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Game Count:</label>

                                <div class="col-md-6">
                                    <h3>{{ $data->game_count }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Type:</label>

                                <div class="col-md-6">
                                    <h3>{{ $data->type }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Odds:</label>

                                <div class="col-md-6">
                                    <h3>{{ $data->odds }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Amount:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ $data->amount }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Earned:</label>

                                <div class="col-md-6">
                                    <h3>{{ $data->earned }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Status:</label>

                                <div class="col-md-6">
                                    <h3>{{ $data->status }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Created At:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ date('Y-m-d', strtotime($data->created_at)) }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Created By:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ ($data->created_who) ? $data->created_who->first_name.' '.$data->created_who->last_name : 'System' }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Updated At:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ date('Y-m-d', strtotime($data->updated_at)) }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Updated By:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ ($data->updated_who) ? $data->updated_who->first_name.' '.$data->updated_who->last_name : 'System' }} </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
