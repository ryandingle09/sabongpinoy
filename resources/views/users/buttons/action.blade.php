@if(Auth::user()->is_super_admin == 1)
    <a href="{{ route('user-show', [$item->id]) }}"><i class="far fa-eye"></i></a>&nbsp;
    <a href="{{ route('user-edit', [$item->id]) }}"><i class="fas fa-pencil-alt"></i></a>&nbsp;
    <a href="{{ route('user-destroy', [$item->id]) }}" class="delete" data-id="{{ $item->id }}"><i class="fas fa-trash-alt"></i></a>

    <form id="form-{{ $item->id }}" action="{{ route('user-destroy', [$item->id]) }}" method="POST" style="display: none;">
        @csrf
    </form>
@else
    @if($item->is_super_admin == 0)
        @if($item->is_admin == 0)
            <a href="{{ route('user-show', [$item->id]) }}"><i class="far fa-eye"></i></a>&nbsp;
        @else
            @foreach(Auth::user()->user_access as $ua)
                @if($ua->access->prefix == 'read')
                <a href="{{ route('user-show', [$item->id]) }}"><i class="far fa-eye"></i></a>&nbsp;
                
                @elseif($ua->access->prefix == 'update')
                <a href="{{ route('user-edit', [$item->id]) }}"><i class="fas fa-pencil-alt"></i></a>&nbsp;
                
                @elseif($ua->access->prefix == 'delete')
                <a href="{{ route('user-destroy', [$item->id]) }}" class="delete" data-id="{{ $item->id }}"><i class="fas fa-trash-alt"></i></a>

                <form id="form-{{ $item->id }}" action="{{ route('user-destroy', [$item->id]) }}" method="POST" style="display: none;">
                    @csrf
                </form>
                @endif
            @endforeach
        @endif
    @endif
@endif

   
