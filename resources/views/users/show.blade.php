@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Users</h2>
                        <h5 class="text-white op-7 mb-2">Showing User <b>{{ ucfirst($data->first_name) }} {{ ucfirst($data->last_name) }}</b> details</h5>
                    </div>
                    
                    <div class="ml-md-auto py-2 py-md-0">
                        <a href="{{ route('user') }}" class="btn btn-white btn-border btn-round mr-2"><i class="fas fa-step-backward"></i>&nbsp; Back </a>
                        @if($data->is_admin == 1)
                        <a href="{{ route('user-edit', [$data->id]) }}" class="btn btn-white btn-border btn-round mr-2"><i class="fas fa-edit"></i>&nbsp; Edit </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Name:</label>

                                <div class="col-md-6">
                                <h3 style="text-decoration: underline">{{ ucfirst($data->first_name) }} {{ ucfirst($data->last_name) }}</h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Email:</label>

                                <div class="col-md-6">
                                <h3 style="text-decoration: underline">{{ $data->email }}</h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">User Type:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">
                                    @if($data->is_super_admin == 1)
                                    Super Admin
                                    @elseif($data->is_admin == 1)
                                    {{ $data->type->name }}
                                    @else
                                    Player
                                    @endif
                                    </h3>
                                </div>
                            </div>

                            @if($data->is_admin == 1)

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Access Rights:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">
                                    @foreach($access as $acs)
                                        <b>{{ $acs->access->name }}</b><br>
                                    @endforeach
                                    </h3>
                                </div>
                            </div>

                            @endif

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Status:</label>

                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">
                                        @if($data->status == 1)
                                            <span class="badge badge-success">Active</span>
                                        @else
                                            <span class="badge badge-warning">Not Active</span>
                                        @endif
                                    </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Created At:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ date('Y-m-d', strtotime($data->created_at)) }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Created By:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ ($data->created_who) ? $data->created_who->first_name.' '.$data->created_who->last_name : 'System' }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Updated At:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ date('Y-m-d', strtotime($data->updated_at)) }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Updated By:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ ($data->updated_who) ? $data->updated_who->first_name.' '.$data->updated_who->last_name : 'System' }} </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
