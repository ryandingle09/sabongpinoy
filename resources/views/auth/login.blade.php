@extends('layouts.app')

@section('title') Login | {{config('app.name', 'Laravel')}} @endsection
@section('content')
<header id="head" class="secondary"></header>

<!-- container -->
<div class="container">

    <div class="row">
        
        <!-- Article main content -->
        <article class="col-xs-12 maincontent">
            <header class="page-header">
                <h1 class="page-title">Sign in</h1>
            </header>
            
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="thin text-center">Sign in to your account</h3>
                        <hr>
                        
                        <form method="POST" action="{{ route('login-active') }}">
                            @csrf
                            <div class="top-margin form-group @if(session('notfound')) has-error @endif">
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>

                                @if(session('notfound'))
                                <p class="text-danger">{{ session('notfound') }}</p>
                                @endif
                            </div>
                            <div class="top-margin form-group  @error('password') has-error @enderror">
                                <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control" name="password" autocomplete="current-password">

                                @error('password')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} value=""> {{ __('Remember Me') }}
                                </label>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-lg-8">
                                    <b>
                                        @if (Route::has('password.request'))
                                            <a href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        @endif
                                    </b>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <button class="btn btn-action" type="submit">Sign in</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

            @if(env('APP_DEBUG'))
            
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="thin text-center">Demo Accounts</h3>
                        <hr>
                        
                        <p><b>Admin Account</b></p>
                        <p>email: admin@gmail.com</p>
                        <p>password: p@ssw0rd</p>

                        <br>

                        <p><b>CSR 1 Account</b></p>
                        <p>email: csr1@gmail.com</p>
                        <p>password: p@ssw0rd</p>

                        <br>
                        <p>For the creation of <b>player accounts</b>. You can use the registration and activate the account using admin or csr user role.</p>
                    </div>
                </div>

            </div>

            @endif
        </article>
        <!-- /Article -->

    </div>
</div>	<!-- /container -->
@endsection
