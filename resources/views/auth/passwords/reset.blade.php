@extends('layouts.app')

@section('title') Reset Your Password | {{config('app.name', 'Laravel')}} @endsection
@section('content')
<header id="head" class="secondary"></header>

<!-- container -->
<div class="container">

    <div class="row">
        
        <!-- Article main content -->
        <article class="col-xs-12 maincontent">
            <header class="page-header">
                <h1 class="page-title">{{ __('Reset Password') }}</h1>
            </header>
            
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="thin text-center">{{ __('Reset Your Password') }}</h3>
                        <hr>
                        
                        <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="top-margin form-group @error('password') has-error @enderror">
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>

                                @error('email')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="top-margin form-group  @error('password') has-error @enderror">
                                <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control" name="password" autocomplete="current-password">

                                @error('password')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="top-margin form-group">
                                <label>Confirm Password <span class="text-danger">*</span></label>
                                <input type="password" class="form-control" name="password_confirmation">
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-lg-4">
                                    <button class="btn btn-action" type="submit">{{ __('Reset Password') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            
        </article>
        <!-- /Article -->

    </div>
</div>	<!-- /container -->
@endsection
