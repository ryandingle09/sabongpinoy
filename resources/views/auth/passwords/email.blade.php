@extends('layouts.app')

@section('title') Reset Your Password | {{config('app.name', 'Laravel')}} @endsection
@section('content')
<header id="head" class="secondary"></header>
<div class="container">

    <div class="row">
        
        <!-- Article main content -->
        <article class="col-xs-12 maincontent">
            <header class="page-header">
                <h1 class="page-title">{{ __('Reset Password') }}</h1>
            </header>
            
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="thin text-center">{{ __('Reset Your Account Password') }}</h3>
                        <hr>
                        
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <div class="top-margin form-group @if(session('notfound')) has-error @endif">
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>

                                @error('email')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-lg-4">
                                    <button class="btn btn-action" type="submit">{{ __('Send Password Reset Link') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            
        </article>
        <!-- /Article -->

    </div>
</div>	<!-- /container -->
@endsection
