@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Cashout/Withdraw Points</h2>
                        <h5 class="text-white op-7 mb-2">User Cashout/Withdraw</h5>
                    </div>
                    
                    <div class="ml-md-auto py-2 py-md-0">
                        <a href="{{ route('player-management') }}" class="btn btn-white btn-border btn-round mr-2"><i class="fas fa-step-backward"></i>&nbsp; Back </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('player-management-cashout', [$data->id]) }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Player Name: </label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" value="{{ $data->first_name }} {{ $data->last_name }}" autocomplete="name" disabled>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="points" class="col-md-4 col-form-label text-md-right">Available Points</label>

                                    <div class="col-md-6">
                                        <input id="points" type="text" class="form-control" value="{{ $data->points }}" autocomplete="points" disabled>
                                        <input type="hidden" name="current_points" value="{{ $data->points }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="points" class="col-md-4 col-form-label text-md-right">Amount/Points to be Cashout</label>
                                    
                                    <div class="col-md-6">
                                        <input id="points" type="text" class="form-control @error('points') is-invalid @enderror" name="points" value="{{ old('points') }}" placeholder="0.00" autocomplete="points" autofocus>

                                        @error('points')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Cashout
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
