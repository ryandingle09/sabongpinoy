@if(Auth::user()->is_super_admin == 1)
    <a href="{{ route('log-show', [$item->id]) }}"><i class="far fa-eye"></i></a>
@else
    @if($item->is_super_admin == 0)
        @foreach(Auth::user()->user_access as $ua)
            @if($ua->access->prefix == 'read')
            <a href="{{ route('log-show', [$item->id]) }}"><i class="far fa-eye"></i></a>
            @endif
        @endforeach
    @endif
@endif