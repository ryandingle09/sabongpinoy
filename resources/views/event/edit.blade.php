@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Events</h2>
                        <h5 class="text-white op-7 mb-2"><i class="fas fa-edit"></i>&nbsp; Edit  Event</h5>
                    </div>
                    
                    <div class="ml-md-auto py-2 py-md-0">
                        <a href="{{ route('game-event') }}" class="btn btn-white btn-border btn-round mr-2"><i class="fas fa-step-backward"></i>&nbsp; Back </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('game-event-update', [$data->id]) }}" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ (old('name')) ? old('name') : $data->name }}"  autocomplete="name" autofocus>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="location" class="col-md-4 col-form-label text-md-right">Location/Arena</label>

                                    <div class="col-md-6">
                                        <!-- <input id="location" type="text" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ (old('location')) ? old('location') : $data->location }}"  autocomplete="location" autofocus> -->
                                        <select class="location location2 form-control @error('location') is-invalid @enderror" name="location">
                                            <option value="{{ $data->location }}" selected="selected">{{ $data->arena->name }}</option>
                                        </select>

                                        @error('location')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="event_date" class="col-md-4 col-form-label text-md-right">Event Date</label>

                                    <div class="col-md-6">
                                        <input id="event_date" type="date" class="form-control @error('event_date') is-invalid @enderror" name="event_date" value="{{ (old('event_date')) ? date('Y-m-d', strtotime(old('event_date'))) : date('Y-m-d', strtotime($data->event_date)) }}"  autocomplete="event_date" autofocus>

                                        @error('event_date')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="expected_deal" class="col-md-4 col-form-label text-md-right">Expected Deal</label>

                                    <div class="col-md-6">
                                        <input id="expected_deal" type="text" class="form-control @error('expected_deal') is-invalid @enderror" name="expected_deal" value="{{ (old('expected_deal')) ? old('expected_deal') : $data->expected_deal }}"  autocomplete="expected_deal" autofocus>

                                        @error('expected_deal')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="is_broadcast" class="col-md-4 col-form-label text-md-right">Is Broad Cast</label>

                                    <div class="col-md-6">
                                        <input type="hidden" class="form-control @error('is_broadcast') is-invalid @enderror">
                                            
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="is_broadcast" id="is_broadcast" value="1" {{($data->is_broadcast == 1) ? 'checked="checked"' : ''}}>
                                                <span class="form-check-sign"><small></small></span>
                                            </label>
                                        </div>
                                        @error('is_broadcast')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>

                                    <div class="col-md-6">
                                        <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description"  autocomplete="description">{{ (old('description')) ? old('description') : $data->description }}</textarea>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="url" class="col-md-4 col-form-label text-md-right">Event/Video URL</label>
                                    
                                    <div class="col-md-6">
                                        <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url" value="{{ (old('url')) ? old('url') : $data->url }}"  autocomplete="url" autofocus>

                                        @error('url')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="cover_image" class="col-md-4 col-form-label text-md-right">Cover Image</label>
                                    
                                    <div class="col-md-6">
                                        <input id="cover_image" type="file" class="form-control @error('cover_image') is-invalid @enderror" name="cover_image" autocomplete="cover_image">

                                        @error('cover_image')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        
                                        @if($data->cover_image)
                                            <img src="{{ $data->cover_image }}" alt="{{ $data->name }}" class="image-reponsive" style="width: 350px">
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endpush

@push('scripts')
<script src="{{ asset('js/select2.min.js') }}"></script>

<script>
    $(document).ready(function() {

        $('.location').select2({
            placeholder: "Select Player",
            allowClear: true,
            ajax: {
                url: "{{ route('arena-show-list') }}",
                data: function (params) {
                    var query = {
                        search: params.term
                    }
                    return query;
                },
                processResults: function (data) {

                    var data = $.map(data, function (obj) {
                        obj.id = obj.id;
                        obj.text = obj.name;
                        return obj;
                    });
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: data
                    };
                }
            }
        });
    });
</script>
@endpush
