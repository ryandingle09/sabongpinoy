@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Events</h2>
                        <h5 class="text-white op-7 mb-2">List of Events</h5>
                    </div>
                    <div class="ml-md-auto py-2 py-md-0">
                        <a href="{{ route('game-event-create') }}" class="btn btn-white btn-border btn-round mr-2"><i class="fas fa-plus-circle"></i>&nbsp; Add Event</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                        {!! $dataTable->table() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('datatables/datatables.min.css') }}"/>
@endpush
@push('scripts')
<script src="{{ asset('datatables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/Buttons-1.6.1/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('datatables/buttons.server-side.js') }}"></script>
{!! $dataTable->scripts() !!}
@endpush
