<!doctype html>
<html>
  <head>
    <title>Socket.IO chat</title>
    <style>
      .card-bordered {
            border: 1px solid #ebebeb
        }

        .card {
            border: 0;
            border-radius: 0px;
            margin-bottom: 30px;
            -webkit-box-shadow: 0 2px 3px rgba(0, 0, 0, 0.03);
            box-shadow: 0 2px 3px rgba(0, 0, 0, 0.03);
            -webkit-transition: .5s;
            transition: .5s
        }

        .padding {
            padding: 3rem !important
        }

        body {
            background-color: #f9f9fa
        }

        .card-header:first-child {
            border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0
        }

        .card-header {
            display: -webkit-box;
            display: flex;
            -webkit-box-pack: justify;
            justify-content: space-between;
            -webkit-box-align: center;
            align-items: center;
            padding: 15px 20px;
            background-color: transparent;
            border-bottom: 1px solid rgba(77, 82, 89, 0.07)
        }

        .card-header .card-title {
            padding: 0;
            border: none
        }

        h4.card-title {
            font-size: 17px
        }

        .card-header>*:last-child {
            margin-right: 0
        }

        .card-header>* {
            margin-left: 8px;
            margin-right: 8px
        }

        .btn-secondary {
            color: #4d5259 !important;
            background-color: #e4e7ea;
            border-color: #e4e7ea;
            color: #fff
        }

        .btn-xs {
            font-size: 11px;
            padding: 2px 8px;
            line-height: 18px
        }

        .btn-xs:hover {
            color: #fff !important
        }

        .card-title {
            font-family: Roboto, sans-serif;
            font-weight: 300;
            line-height: 1.5;
            margin-bottom: 0;
            padding: 15px 20px;
            border-bottom: 1px solid rgba(77, 82, 89, 0.07)
        }

        .ps-container {
            position: relative
        }

        .ps-container {
            -ms-touch-action: auto;
            touch-action: auto;
            overflow: hidden !important;
            -ms-overflow-style: none
        }

        .media-chat {
            padding-right: 64px;
            margin-bottom: 0
        }

        .media {
            padding: 16px 12px;
            -webkit-transition: background-color .2s linear;
            transition: background-color .2s linear
        }

        .media .avatar {
            flex-shrink: 0
        }

        .avatar {
            position: relative;
            display: inline-block;
            width: 36px;
            height: 36px;
            line-height: 36px;
            text-align: center;
            border-radius: 100%;
            background-color: #f5f6f7;
            color: #8b95a5;
            text-transform: uppercase
        }

        .media-chat .media-body {
            -webkit-box-flex: initial;
            flex: initial;
            display: table
        }

        .media-body {
            min-width: 0
        }

        .media-chat .media-body p {
            position: relative;
            padding: 6px 8px;
            margin: 4px 0;
            background-color: #f5f6f7;
            border-radius: 3px;
            font-weight: 100;
            color: #9b9b9b
        }

        .media>* {
            margin: 0 8px
        }

        .media-chat .media-body p.meta {
            background-color: transparent !important;
            padding: 0;
            opacity: .8
        }

        .media-meta-day {
            -webkit-box-pack: justify;
            justify-content: space-between;
            -webkit-box-align: center;
            align-items: center;
            margin-bottom: 0;
            color: #8b95a5;
            opacity: .8;
            font-weight: 400
        }

        .media {
            padding: 16px 12px;
            -webkit-transition: background-color .2s linear;
            transition: background-color .2s linear
        }

        .media-meta-day::before {
            margin-right: 16px
        }

        .media-meta-day::before,
        .media-meta-day::after {
            content: '';
            -webkit-box-flex: 1;
            flex: 1 1;
            border-top: 1px solid #ebebeb
        }

        .media-meta-day::after {
            content: '';
            -webkit-box-flex: 1;
            flex: 1 1;
            border-top: 1px solid #ebebeb
        }

        .media-meta-day::after {
            margin-left: 16px
        }

        .media-chat.media-chat-reverse {
            padding-right: 12px;
            padding-left: 64px;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: reverse;
            flex-direction: row-reverse
        }

        .media-chat {
            padding-right: 64px;
            margin-bottom: 0
        }

        .media {
            padding: 16px 12px;
            -webkit-transition: background-color .2s linear;
            transition: background-color .2s linear
        }

        .media-chat.media-chat-reverse .media-body p {
            float: right;
            clear: right;
            background-color: #48b0f7;
            color: #fff
        }

        .media-chat .media-body p {
            position: relative;
            padding: 6px 8px;
            margin: 4px 0;
            background-color: #f5f6f7;
            border-radius: 3px
        }

        .border-light {
            border-color: #f1f2f3 !important
        }

        .bt-1 {
            border-top: 1px solid #ebebeb !important
        }

        .publisher {
            position: relative;
            display: -webkit-box;
            display: flex;
            -webkit-box-align: center;
            align-items: center;
            padding: 12px 20px;
            background-color: #f9fafb
        }

        .publisher>*:first-child {
            margin-left: 0
        }

        .publisher>* {
            margin: 0 8px
        }

        .publisher-input {
            -webkit-box-flex: 1;
            flex-grow: 1;
            border: none;
            outline: none !important;
            background-color: transparent
        }

        button,
        input,
        optgroup,
        select,
        textarea {
            font-family: Roboto, sans-serif;
            font-weight: 300
        }

        .publisher-btn {
            background-color: transparent;
            border: none;
            color: #8b95a5;
            font-size: 16px;
            cursor: pointer;
            overflow: -moz-hidden-unscrollable;
            -webkit-transition: .2s linear;
            transition: .2s linear
        }

        .file-group {
            position: relative;
            overflow: hidden
        }

        .publisher-btn {
            background-color: transparent;
            border: none;
            color: #cac7c7;
            font-size: 16px;
            cursor: pointer;
            overflow: -moz-hidden-unscrollable;
            -webkit-transition: .2s linear;
            transition: .2s linear
        }

        .file-group input[type="file"] {
            position: absolute;
            opacity: 0;
            z-index: -1;
            width: 20px
        }

        .text-info {
            color: #48b0f7 !important
        }
    </style>
  </head>
  <body>
    <!-- <ul id="messages"></ul> -->
    <!-- <form action="">
      <input id="m" autocomplete="off" /><button>Send</button>
    </form> -->
    <div class="page-content page-container" id="page-content">
        <div class="messages ps-container ps-theme-default ps-active-y" id="chat-content" style="overflow-y: scroll !important; height:400px !important;">
                            
                @foreach($messages as $chat)
                    @if($chat->created_by == Auth::user()->id)
                        <div class="media media-chat media-chat-reverse">
                            <div class="media-body">
                                <p>{{ $chat->message }}</p>
                                <p class="meta"><time>{{ $chat->created_at }}</time></p>
                            </div>
                        </div>
                    @else
                        <div class="media media-chat"> 
                            <img class="avatar" src="{{ Auth::user()->detail->photo }}"> &nbsp; {{ Auth::user()->first_name.' '.Auth::user()->last_name }} }}
                            <div class="media-body">
                                <p>{{ $chat->message }}</p>
                                <p class="meta"><time>{{ $chat->created_at }}</time></p>
                            </div>
                        </div>
                    @endif
                @endforeach

            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
            </div>
            <div class="ps-scrollbar-y-rail" style="top: 0px; height: 0px; right: 2px;">
                <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 2px;"></div>
            </div>
        </div>

        <div class="publisher bt-1 border-light"> 
            <img class="avatar avatar-xs" src="{{ Auth::user()->detail->photo }}" alt=""> 
            <form method="POST" action="{{ route('chat-store') }}" id="chat-form">
                @csrf
                <input name="room_id" class="room_id" value="room_1" type="hidden">
                <input class="photo" value="{{ Auth::user()->detail->photo }}" type="hidden">
                <input name="created_by" class="created_by" value="{{ Auth::user()->id }}" type="hidden">
                <input name="message" class="chat-input" type="text" placeholder="Write something">
                <!-- <a class="publisher-btn text-info" href="#" data-abc="true"><i class="fa fa-paper-plane"></i></a>  -->
            </form>
        </div>
    </div>

    <script src="{{ url('/') }}:3000/socket.io/socket.io.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.js"></script>
    <script>
      $(function () {
        var socket = io("{{ url('/') }}:3000", {path: "/socket.io"});

        var message = $('.chat-input').val();
        var room = $('.room_id').val();
        var photo = $('.photo').val();
        var user_id = $('.created_by').val();

        $('#chat-form').submit(function(e){

            e.preventDefault();

            var dis = $(this)

            if(message != '')
            {
                socket.emit('live-chat', {
                    'message': message,
                    'photo': photo,
                    'room': room,
                    'user_id': user_id
                });

                $('.chat-input').val('');

                $.ajax({
                    type: 'POST',
                    url: dis.attr('action'),
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function(response){
                        console.log('message-sent-to-server');
                    }
                });
            }

            return false;
          
        });

        socket.on('live-chat', function(msg){
            console.log(msg);

            var chat_message = '';

            if(msg['room_id'] == 'room_id')
            {
                if('{{ Auth::user()->id }}' == msg['user_id'])
                {
                    chat_message += '<div class="media media-chat media-chat-reverse">';
                    chat_message += '    <div class="media-body">';
                    chat_message += '        <p>'+msg['message']+'</p>';
                    chat_message += '        <p class="meta"><time>'+msg['created_at']+'</time></p>';
                    chat_message += '    </div>';
                    chat_message += '</div>';
                }
                else
                {
                    chat_message += '<div class="media media-chat">';
                    chat_message += '    <img class="avatar" src="{{ Auth::user()->detail->photo }}"> &nbsp; '+msg['photo']+'';
                    chat_message += '    <div class="media-body">';
                    chat_message += '        <p>'+msg['message']+'</p>';
                    chat_message += '        <p class="meta"><time>'+msg['created_at']+'</time></p>';
                    chat_message += '    </div>';
                    chat_message += '</div>';
                }

                $('.messages').append(chat_message);
                window.scrollTo(0, document.body.scrollHeight);
            }
        });

      });
    </script>
  </body>
</html>