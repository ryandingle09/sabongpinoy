@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Dashboard</h2>
                        <h5 class="text-white op-7 mb-2">Welcome to your Dashboard <b>{{ Auth::user()->first_name }}</b></h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row mt--2">
                @if(Auth::user()->is_admin == 0)

                <div class="col-md-4">
                    <div class="card card-stats card-round card-success">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5">
                                    <div class="icon-big text-center">
                                        <i class="flaticon-coins text-success"></i>
                                    </div>
                                </div>
                                <div class="col-7 col-stats">
                                    <div class="numbers">
                                        <p class="card-category">Total Points</p>
                                        <h4 class="card-title">{{ number_format(Auth::user()->points) }}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-stats card-round card-info">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5">
                                    <div class="icon-big text-center">
                                        <i class="flaticon-coins text-success"></i>
                                    </div>
                                </div>
                                <div class="col-7 col-stats">
                                    <div class="numbers">
                                        <p class="card-category">Total Hits Points</p>
                                        <h4 class="card-title">{{number_format($total_hits)}}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-stats card-round card-danger">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5">
                                    <div class="icon-big text-center">
                                        <i class="flaticon-coins text-success"></i>
                                    </div>
                                </div>
                                <div class="col-7 col-stats">
                                    <div class="numbers">
                                        <p class="card-category">Total Loose Points</p>
                                        <h4 class="card-title">{{number_format($total_loose)}}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  

                <div class="col-md-12">

                    <h1 class="font-weight-light text-lg mt-4 mb-0">Events</h1>
                    <hr class="mt-2 mb-5">

                    <div class="row">
                        @if(count($events) == 0)
                        
                        <div class="col-md-12">
                            <h1 class="font-weight-light text-lg-center mt-4 mb-4 text-center"><b style="text-align:center">No Events</b></h1>
                        </div>

                        @endif
                        @foreach($events as $event)
                        <div class="col-md-4">
                            <div class="card card-annoucement card-round" style="color: #fff;background:#181015 url({{$event->cover_image}}) no-repeat; background-size: cover">
                                <div class="card-body text-center">
                                    <div class="card-opening">{{ $event->name }}</div>
                                    <div class="card-desc">
                                        expected deals: {{$event->expected_deal}}
                                        <br>
                                        Schedule: 
                                        @if(date('Y-m-d', strtotime($event->event_date)) == date('Y-m-d'))
                                        Today
                                        @endif
                                        {{date('F j, Y', strtotime($event->event_date))}}
                                    </div>
                                    <div class="card-detail">
                                        @if(date('Y-m-d', strtotime($event->event_date)) == date('Y-m-d'))

                                        <a target="_blank" href="{{ route('event-web-show', [$event->id]) }}" class="btn btn-light btn-rounded">Watch Now &nbsp;<i class="fas fa-video"></i></a>
                                        
                                        @else

                                        <button class="btn btn-light btn-rounded">Upcoming &nbsp;<i class="fas fa-clock"></i></button>

                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                @else
                @if(count($events) == 0)
                        
                <div class="col-md-12 mt-5 text-center">
                    <h1 class="font-weight-light text-lg-center mt-4 mb-4 text-center"><b style="text-align:center">No Events</b></h1>
                    <a href="{{ route('game-event-create') }}" class="btn btn-info btn-border btn-round">Add Now ?</a>
                </div>
                
                @endif

                @foreach($events as $event)
                <div class="col-md-4">
                    <div class="card card-annoucement card-round" style="color: #fff;background:#181015 url({{$event->cover_image}}) no-repeat; background-size: cover">
                        <div class="card-body text-center">
                            <div class="card-opening">{{ $event->name }}</div>
                            <div class="card-desc">
                                Expected deals: {{$event->expected_deal}}
                                <br>
                                Schedule: 
                                @if(date('Y-m-d', strtotime($event->event_date)) == date('Y-m-d'))
                                Today
                                @endif
                                {{date('F j, Y', strtotime($event->event_date))}}
                            </div>
                            <div class="card-detail">
                                @if(date('Y-m-d', strtotime($event->event_date)) == date('Y-m-d'))

                                <a target="_blank" href="{{ route('event-web-show', [$event->id]) }}" class="btn btn-light btn-rounded">Watch Now &nbsp;<i class="fas fa-video"></i></a>
                                
                                @else

                                <button class="btn btn-light btn-rounded">Upcoming &nbsp;<i class="fas fa-clock"></i></button>

                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

                @endif

                <!-- Page Content -->
                <div class="col-md-12">

                    <h1 class="font-weight-light text-lg mt-4 mb-0">Customer Service Representatives</h1>

                    <hr class="mt-2 mb-5">

                    @if(count($csr) == 0)
                        
                    <div class="col-md-12 mt-5 text-center">
                        <h1 class="font-weight-light text-lg-center mt-4 mb-4 text-center"><b style="text-align:center">No CSR Found!</b></h1>
                    </div>
                    
                    @endif

                    <div class="row text-center text-lg-left">

                        @foreach($csr as $cs)
                            @php
                                $text = (isset($cs->detail->photo)) ? $cs->detail->photo : asset('images/default.png');
                                $img = asset($text);
                                $messenger = (isset($cs->detail) && !is_null($cs->detail->messenger_url)) ? $cs->detail->messenger_url : '';
                            @endphp
                                
                            <div class="col-lg-3 col-md-4 col-6 highlight">
                                <a target="_blank" href="{{$messenger}}" class="d-block mb-4 h-100">
                                    <img class="img-fluid img-thumbnail" src="{{$img}}" alt="{{$img}}" style="width: 278px; height: 278px">
                                    
                                    <div class="h-caption"><h4 class="text-center text-lg-center">CSR {{ucfirst($cs->first_name)}}</h4></div>
                                </a>
                            </div>
                        @endforeach

                    </div>

                </div>
                <!-- /.container -->

            </div>
        </div>
    </div>

    @include('layouts.includes.footer')
</div>
@endsection
