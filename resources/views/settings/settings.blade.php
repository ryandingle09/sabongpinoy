@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Settings</h2>
                        <h5 class="text-white op-7 mb-2">Game Setting</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('setting-update') }}" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row">
                                    <label for="plasada" class="col-md-4 col-form-label text-md-right">Plasada (<small>Default 0.8 %</small>)</label>
                                    @php

                                        $plasada = '';

                                        if(!is_null($data))
                                            $plasada = $data->plasada;

                                    @endphp
                                    <div class="col-md-6">
                                        <input id="plasada" type="text" class="form-control @error('plasada') is-invalid @enderror" name="plasada" value="{{ old('plasada') ? old('plasada') : $plasada }}" placeholder="" autofocus>

                                        @error('plasada')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
