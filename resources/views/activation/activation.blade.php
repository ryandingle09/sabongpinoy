@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Activations</h2>
                        <h5 class="text-white op-7 mb-2">List of User for Account activation</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                        {!! $dataTable->table() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('datatables/datatables.min.css') }}"/>
@endpush
@push('scripts')
<script>
    $(document).on('click', '.delete-custom', function(e) {
        e.preventDefault();

        var id = $(this).data('id');

        swal({
            title: 'Are you sure ?',
            text: "This user account will now be able to login as a player role in this web application.",
            type: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, Activate it!',
                    className : 'btn btn-success'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                $('#form-'+ id).submit();
            } else {
                swal.close();
            }
        });
    });
</script>
<script src="{{ asset('datatables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/Buttons-1.6.1/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('datatables/buttons.server-side.js') }}"></script>
{!! $dataTable->scripts() !!}
@endpush
