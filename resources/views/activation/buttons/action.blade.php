<td>
    <a href="{{ route('activation-activate', [$item->id]) }}" class="delete-custom btn btn-success btn-sm" data-id="{{ $item->id }}" data-toggle="tooltip" data-placement="top" title="Click to Activate this user">Activate&nbsp;<i class="fas fa-check-circle"></i></a>
    <form id="form-{{ $item->id }}" action="{{ route('activation-activate', [$item->id]) }}" method="POST" style="display: none;">
        @csrf
    </form>
</td>