@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Account</h2>
                        <h5 class="text-white op-7 mb-2">My Account</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('profile-update') }}" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row">
                                    <div class="avatar avatar-xxl" style="margin: 0px auto">
                                        <img src="{{ (isset($data->detail->photo)) ? $data->detail->photo : asset('images/default.png') }}" alt="{{$data->first_name}} {{$data->last_name}}" class="avatar-img rounded-circle" draggable="true" data-bukket-ext-bukket-draggable="true">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="photo" class="col-md-4 col-form-label text-md-right">Photo</label>

                                    <div class="col-md-6">
                                        <input id="photo" type="file" class="form-control @error('photo') is-invalid @enderror" name="photo" >

                                        @error('photo')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="first_name" class="col-md-4 col-form-label text-md-right">First Name</label>

                                    <div class="col-md-6">
                                        <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') ? old('first_name') : $data->first_name }}" placeholder="first name" autofocus>

                                        @error('first_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="last_name" class="col-md-4 col-form-label text-md-right">Last Name</label>

                                    <div class="col-md-6">
                                        <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') ? old('last_name') : $data->last_name }}" placeholder="last name" autofocus>

                                        @error('last_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">Email Address</label>
                                    
                                    <div class="col-md-6">
                                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ? old('email') : $data->email }}" placeholder="email address" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                @if(Auth::user()->type && Auth::user()->type->prefix == 'csr')

                                <div class="form-group row">
                                    <label for="messenger_url" class="col-md-4 col-form-label text-md-right">Facebook Messender URL</label>
                                    @php

                                        $messenger = (isset($data->detail) && !is_null($data->detail->messenger_url)) ? $data->detail->messenger_url : '';

                                    @endphp
                                    <div class="col-md-6">
                                        <input id="messenger_url" type="text" class="form-control @error('messenger_url') is-invalid @enderror" name="messenger_url" value="{{ old('messenger_url') ? old('messenger_url') : $messenger }}" placeholder="fb messenger url" autofocus>

                                        @error('messenger_url')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                @endif

                                @if(Auth::user()->is_super_admin == 0)

                                <!-- <div class="form-group row">
                                    <label for="user_type" class="col-md-4 col-form-label text-md-right">User Type</label>
                                    
                                    <div class="col-md-3">
                                        <select type="select" id="user_type" class="form-control @error('user_type') is-invalid @enderror" name="user_type" placeholder="user_type" autofocus>
                                            <option value="">Select User Type</option>
                                            @foreach($user_types as $user_type)
                                                <option value="{{ $user_type->id }}" {{ ($user_type->id == $data->type_id) ? 'selected="selected"' : '' }}>{{ $user_type->name }}</option>
                                            @endforeach
                                        </select>

                                        @error('user_type')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="access" class="col-md-4 col-form-label text-md-right">Access Rights</label>

                                    <div class="col-md-6">
                                        <input type="hidden" class="form-control @error('access') is-invalid @enderror">
                                        @foreach($access as $m)

                                            @php
                                                $checked = '';
                                            @endphp
                                            @foreach($user_access as $rd)
                                                @if($rd->access_id == $m->id)
                                                    @php
                                                        $checked = 'checked="checked"';
                                                    @endphp
                                                @endif
                                            @endforeach
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="access[]" id="access" value="{{ $m->id }}" {{ $checked }}>
                                                <span class="form-check-sign">{{ $m->name }}</span>
                                            </label>
                                        </div>
                                        @endforeach

                                        @error('access')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="status" class="col-md-4 col-form-label text-md-right">Status</label>
                                    
                                    <div class="col-md-3">
                                        <select type="select" id="status" class="form-control @error('status') is-invalid @enderror" name="status" placeholder="status" autofocus>
                                            <option value="1" {{ ($data->status == '1') ? 'selected="selected"' : '' }}>Active</option>
                                            <option value="0" {{ ($data->status == '0') ? 'selected="selected"' : ''  }}>In Active</option>
                                        </select>

                                        @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div> -->

                                @endif

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="new-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="new-password">
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
