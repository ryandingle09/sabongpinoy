@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Transaction</h2>
                        <h5 class="text-white op-7 mb-2">Showing Transaction Record #<b>{{ ucfirst($data->id) }}</b> details</h5>
                    </div>
                    
                    <div class="ml-md-auto py-2 py-md-0">
                        <a href="{{ route('history') }}" class="btn btn-white btn-border btn-round mr-2"><i class="fas fa-step-backward"></i>&nbsp; Back </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Name:</label>

                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ $data->user->first_name }} {{ $data->user->last_name }}</h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Amount:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ $data->amount }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Module:</label>

                                <div class="col-md-6">
                                    <h3>{{ $data->module }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Operation:</label>

                                <div class="col-md-6">
                                    <h3>{{ ($data->operation == '+') ? 'Incoming (+)' : 'Outgoing (-)' }}</h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">From:</label>

                                <div class="col-md-6">
                                    <h3>
                                        @if($data->from)
                                            {{ $data->user_from->first_name.' '.$data->user_from->last_name }}
                                        @else
                                            N/A
                                        @endif
                                    </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">To:</label>

                                <div class="col-md-6">
                                    <h3>
                                        @if($data->to)
                                            {{ $data->user_to->first_name.' '.$data->user_to->last_name }}
                                        @else
                                            N/A
                                        @endif
                                    </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Performed By:</label>

                                <div class="col-md-6">
                                    <h3>{{ $data->performed_by->first_name.' '.$data->performed_by->last_name }}</h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Created At:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ date('Y-m-d', strtotime($data->created_at)) }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Created By:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ ($data->created_who) ? $data->created_who->first_name.' '.$data->created_who->last_name : 'System' }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Updated At:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ date('Y-m-d', strtotime($data->updated_at)) }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Updated By:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ ($data->updated_who) ? $data->updated_who->first_name.' '.$data->updated_who->last_name : 'System' }} </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
