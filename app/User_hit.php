<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_hit extends Model
{
    protected $fillable = ['user_id', 'event_id', 'game_count', 'odds', 'type', 'bet_amount', 'earned', 'created_by', 'updated_by'];
}
