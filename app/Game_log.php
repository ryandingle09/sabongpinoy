<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game_log extends Model
{
    protected $fillable = ['event_id', 'game_count', 'winner', 'status', 'created_by', 'updated_by'];
}
