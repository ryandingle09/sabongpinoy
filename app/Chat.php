<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable = ['message', 'room_id', 'created_by', 'updated_by', 'created_at', 'updated_at'];

    public function from()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
