<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Access extends Model
{
    protected $table = 'access';
    protected $fillable = ['name', 'prefix', 'description', 'created_by', 'updated_by'];

    public function updated_who()
    {
        return $this->hasOne('App\User', 'id', 'updated_by');
    }

    public function created_who()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
