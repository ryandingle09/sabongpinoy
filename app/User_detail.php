<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_detail extends Model
{
    protected $fillable = [
        'user_id', 'gender', 'age', 'birth_date', 'religion', 'address', 'photo', 'photo_full_path', 'messenger_url', 'updated_by', 'created_by'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'user_id', 'id');
    }
}
