<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class GameEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $validate = '';

        if($request->hasFile('cover_image')){
            $validate = 'required|mimes:jpeg,jpg,bmp,png';
        }

        return [
            'name' => 'required|string|max:255',
            # 'description' => 'required|string',
            'location' => 'required',
            'event_date' => 'required|date',
            'expected_deal' => 'required|integer',
            'url' => 'required|url|max:255',
            'cover_image' => $validate
        ];
    }
}
