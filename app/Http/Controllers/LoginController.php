<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) 
        {
            if(Auth::user()->status == 1)
            {
                $token = Str::random(60);

                $request->user()->forceFill([
                    'api_token' => hash('sha256', $token),
                ])->save();

                return redirect()->intended('dashboard');
            }
            else
            {
                Auth::logout();
                return back()->withInput()->with('notfound', 'Oops!! Account not yet activated. Please contact any CSR to request activation of you account.');
            }
        }

        return back()->withInput()->with('notfound', 'Oops!! Credentials not match our records.');
    }
}
