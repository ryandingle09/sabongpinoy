<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User_money_log;
use App\User;
use App\Event;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $csr = User::leftjoin('types', 'types.id', 'users.type_id')
                    ->select(['users.*'])
                    ->where('types.prefix', 'csr')
                    ->with(['type', 'detail'])
                    ->get();

        $events = Event::where('is_broadcast', '1')
                    ->whereDate('event_date','>=', date('Y-m-d'))
                    ->orderBy('event_date', 'ASC')
                    ->get();

        if(Auth::user()->is_admin == 1)
        {
            $events = Event::whereDate('event_date','>=', date('Y-m-d'))
                    ->orderBy('event_date', 'ASC')
                    ->get();
        }

        $bets = User_money_log::where(['user_id' => Auth::user()->id])
                    // ->whereRaw('status is not null')
                    ->get();

        $total_hits = 0;
        $total_loose = 0;

        foreach($bets as $bet)
        {
            if($bet->status == 'hit')
                $total_hits = $total_hits + $bet->earned;
            
            if($bet->status == 'loose')
                $total_loose = $total_loose + $bet->amount;
        }
        
        $data = [
            'events' => $events,
            'csr' => $csr,
            'total_hits' => (int)$total_hits,
            'total_loose' => (int)$total_loose,
        ];

        return view('dashboard.dashboard', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
