<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Module;
use App\Role_module;
use App\Http\Requests\RoleRequest;
use Auth;
use App\DataTables\RolesDataTable;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RolesDataTable $dataTable)
    {
        return $dataTable->render('roles.roles');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create', ['modules' => Module::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $modules = $request['modules'];
        $request_f = $request->except(['_token', 'modules']);
        
        $role = Role::create(array_merge($request_f, ['created_by' => Auth::user()->id]));

        foreach($modules as $m)
        {
            $data = [
                'role_id' => $role->id,
                'module_id' =>  $m,
                'created_by' =>  Auth::user()->id
            ];
            
            Role_module::create($data);
        }
        
        return redirect('roles')->with('status', 'Successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['data' => Role::where('id', $id)->first()];

        return view('roles.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'data' => Role::where('id', $id)->first(),
            'role_data' => Role_module::where('role_id', $id)->get(),
            'modules' => Module::all()
        ];

        return view('roles.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        $request_f = $request->except(['_token', 'modules']);
        
        Role::where('id', $id)->update(array_merge($request_f, ['updated_by' => Auth::user()->id]));

        Role_module::where('role_id', $id)->delete();

        foreach($request['modules'] as $m)
        {
            $data = [
                'role_id' => $id,
                'module_id' =>  $m,
                'created_by' =>  Auth::user()->id
            ];
            

            Role_module::create($data);
        }

        return redirect('roles')->with('status', 'Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::where('id', $id)->delete();

        return redirect('roles')->with('status', 'Successfully Deleted!');
    }
}
