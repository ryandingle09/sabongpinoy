<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ArenaRequest;
use App\Arena;
use Auth;
use App\DataTables\ArenasDataTable;

class ArenaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ArenasDataTable $dataTable)
    {
        return $dataTable->render('arena.arena');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('arena.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArenaRequest $request)
    {
        $request = $request->except(['_token']);
        
        Arena::create(array_merge($request, ['created_by' => Auth::user()->id]));
        
        return redirect('arenas')->with('status', 'Successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['data' => Arena::where('id', $id)->first()];

        return view('arena.show', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_list(Request $request)
    {
        
        $data = Arena::where(function($q) use ($request) {
            $q->where('name', 'like', '%'.$request->search.'%')
            ->orWhere('location', 'like', '%'.$request->search.'%');
        })
        ->orderBy('id', 'desc')
        ->limit(20)
        ->get();

        // if($request->selected)
        // {
        //     $data = Arena::where('id', $request->selected)->get();
        // }

        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ['data' => Arena::where('id', $id)->first()];

        return view('arena.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArenaRequest $request, $id)
    {
        $request = $request->except(['_token']);
        
        Arena::where('id', $id)->update(array_merge($request, ['updated_by' => Auth::user()->id]));

        return redirect('arenas')->with('status', 'Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Arena::where('id', $id)->delete();

        return redirect('arenas')->with('status', 'Successfully Deleted!');
    }
}
