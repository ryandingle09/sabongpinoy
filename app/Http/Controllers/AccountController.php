<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AccountRequest;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Access;
use App\Type;
use App\User_access;
use App\User_detail;
use Auth;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'data' => User::where('id', Auth::user()->id)->first(),
            'user_types' => Type::all(), 
            'access' => Access::all(),
            'user_access' => User_access::where('user_id', Auth::user()->id)->get()
        ];

        return view('account.account', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccountRequest $request)
    {
        # $access = $request['access'];
        # $type = $request->user_type;
        $password = Hash::make($request->password);
        $request_f = $request->except(['_token', 'user_type', 'access', 'password', 'password_confirmation', 'photo', 'messenger_url']);

        # if(Auth::user()->is_super_admin == 1)
        #    $request_f = $request->except(['_token', 'status', 'user_type', 'access', 'password', 'password_confirmation', 'photo']);
        
        # $to_update = ['type_id' => $type, 'updated_by' => Auth::user()->id];
        $to_update = ['updated_by' => Auth::user()->id];

        #if(Auth::user()->is_super_admin == 1)
        #    $to_update = ['updated_by' => Auth::user()->id ];

        if(!empty($request->password))
            $to_update['password'] = $password;

        User::where('id', Auth::user()->id)->update(array_merge($request_f, $to_update));

        // if(Auth::user()->is_super_admin == 0)
        // {
        //     User_access::where('user_id', Auth::user()->id)->delete();

        //     foreach($access as $m)
        //     {
        //         $data = [
        //             'user_id' => Auth::user()->id,
        //             'access_id' =>  $m,
        //             'created_by' =>  Auth::user()->id
        //         ];
                
        //         User_access::create($data);
        //     }
        // }
        if(Auth::user()->type && Auth::user()->type->prefix == 'csr')
        {
            User_detail::where('user_id', Auth::user()->id)->update(['messenger_url' => $request->messenger_url]);
        }

        if($request->hasFile('photo'))
        {
            $path = $request->photo->store('photo', 'public');
            $full_path = storage_path('app/public').'/'.$path;
            $old = User_detail::where('user_id', Auth::user()->id)->first();

            if(isset($old->id))
            {
                if(file_exists($old->photo_full_path))
                    unlink($old->photo_full_path);

                User_detail::where('user_id', Auth::user()->id)->update([
                    'photo' => '/storage/'.$path, 
                    'photo_full_path' => $full_path, 
                    'updated_by' => Auth::user()->id
                ]);
            }
            else
            {
                User_detail::create([
                    'user_id' => Auth::user()->id,
                    'photo' => '/storage/'.$path, 
                    'photo_full_path' => $full_path, 
                    'updated_by' => Auth::user()->id
                ]);
            }
        }

        return redirect('profile')->with('status', 'Successfully Updated!');
    }
}
