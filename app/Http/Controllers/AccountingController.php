<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_money_log;
use App\DataTables\AccountingDataTable;

class AccountingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AccountingDataTable $dataTable)
    {
        return $dataTable->render('accounting.accounting');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['data' => User_money_log::where('id', $id)->first()];

        return view('accounting.show', $data);
    }
}
