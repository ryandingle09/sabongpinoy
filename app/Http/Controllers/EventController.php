<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Chat;
use \App\Event;
use \App\User_money_log;
use \App\Game_log;
use \App\User_hit;
use Auth;
use DB;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::where('is_broadcast', '1')
                    ->whereDate('event_date','>=', date('Y-m-d'))
                    ->orderBy('event_date', 'ASC')
                    ->get();

        return view('frontend.event', [
                'data' => $events
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_old($id)
    {
        $event = Event::where(['is_broadcast' => '1', 'id' => $id])->limit(5)->first();
        $messages = Chat::where('room_id', $id)->orderBy('id', 'DESC')->paginate(15)->sortBy('created_at');

        $data = [
            'event' => $event,
            'messages' => $messages
        ];

        return view('frontend.event-show', $data);
    }

    // test
    public function show($id)
    {
        $event = Event::where(['is_broadcast' => '1', 'id' => $id])->limit(5)->first();
        $messages = Chat::where('room_id', $id)->orderBy('id', 'DESC')->paginate(15)->sortBy('created_at');
        $game_log = Game_log::where('event_id', $id)->orderBy('id', 'DESC')->get();

        $game_status = 'close';
        $game_winner = 'NONE';
        
        if(count($game_log) !== 0)
        {
            $status = Game_log::where('event_id', $id)->get()->last();
            $game_status = $status->status;
            $game_winner = (is_null($status->winner)) ? 'NONE' : $status->winner;
        }

        $bet_where = [
            'game_count' => count($game_log),
            'event_id' => $id,
        ];

        $oddsbet = User_money_log::where($bet_where)->get();

        $data = [
            'event' => $event,
            'messages' => $messages,
            'game_count' => count($game_log),
            'game_status' => $game_status,
            'game_winner' => $game_winner,
            'game_logs' => $game_log,
            'oddsbet' => $oddsbet,
        ];

        return view('frontend.event-show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
