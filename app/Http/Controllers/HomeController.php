<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        # $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $csr = User::leftjoin('types', 'types.id', 'users.type_id')
                    ->select(['users.*'])
                    ->where('types.prefix', 'csr')
                    ->with(['type', 'detail'])
                    ->get();

        $events = Event::where('is_broadcast', '1')
                    ->whereDate('event_date','>=', date('Y-m-d'))
                    ->orderBy('event_date', 'ASC')
                    ->get();

        $data = [
            'data' => $events,
            'csr' => $csr
        ];
        return view('frontend.home', $data);
    }
}
