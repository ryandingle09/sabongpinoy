<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\DataTables\ActivationsDataTable;

class ActivationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ActivationsDataTable $dataTable)
    {
        return $dataTable->render('activation.activation');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        User::where('id', $id)->update(['status' => '1']);

        return redirect('activations')->with('status', 'Successfully Activated!');
    }
}
