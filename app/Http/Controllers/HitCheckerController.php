<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_money_log;
use Auth;
use App\DataTables\HitsDataTable;

class HitCheckerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HitsDataTable $dataTable)
    {
        return $dataTable->render('hits.hits');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['data' => User_money_log::where('id', $id)->first()];

        return view('hits.show', $data);
    }
}
