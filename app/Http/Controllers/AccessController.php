<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AccessRequest;
use App\Access;
use Auth;
use App\DataTables\AccessDataTable;

class AccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AccessDataTable $dataTable)
    {
        return $dataTable->render('access.access');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('access.create', ['order' => Access::count() + 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccessRequest $request)
    {
        $request = $request->except(['_token']);
        
        Access::create(array_merge($request, ['created_by' => Auth::user()->id]));
        
        return redirect('access')->with('status', 'Successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['data' => Access::where('id', $id)->first()];

        return view('access.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ['data' => Access::where('id', $id)->first()];

        return view('access.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccessRequest $request, $id)
    {
        $request = $request->except(['_token']);
        
        Access::where('id', $id)->update(array_merge($request, ['updated_by' => Auth::user()->id]));

        return redirect('access')->with('status', 'Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Access::where('id', $id)->delete();

        return redirect('access')->with('status', 'Successfully Deleted!');
    }
}
