<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Http\Requests\GameEventRequest;
use Auth;
use App\DataTables\EventsDataTable;

class GameEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EventsDataTable $dataTable)
    {
        return $dataTable->render('event.event');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('event.create', ['order' => Event::count() + 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GameEventRequest $request)
    {
        $r = $request;
        $request = $request->except(['_token', 'cover_image']);

        if($r->hasFile('cover_image'))
        {
            $path = $r->cover_image->store('events', 'public');
            $full_path = storage_path('app/public').'/'.$path;

            Event::create(array_merge($request, [
                'cover_image' => '/storage/'.$path, 
                'cover_image_full_path' => $full_path, 
                'created_by' => Auth::user()->id
            ]));
        }
        else
            Event::create(array_merge($request, ['created_by' => Auth::user()->id]));
        
        return redirect('game-events')->with('status', 'Successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['data' => Event::where('id', $id)->first()];

        return view('event.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ['data' => Event::where('id', $id)->first()];

        return view('event.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GameEventRequest $request, $id)
    {
        $r = $request;
        $request = $request->except(['_token', 'cover_image']);
        $old = Event::where('id', $id)->first();

        if($r->hasFile('cover_image'))
        {
            $path = $r->cover_image->store('events', 'public');
            $full_path = storage_path('app/public').'/'.$path;

            if(file_exists($old->cover_image_full_path))
                unlink($old->cover_image_full_path);

            Event::where('id', $id)->update(array_merge($request, [
                'cover_image' => '/storage/'.$path, 
                'cover_image_full_path' => $full_path, 
                'updated_by' => Auth::user()->id
            ]));
        }
        else
            Event::where('id', $id)->update(array_merge($request, ['updated_by' => Auth::user()->id]));

        return redirect('game-events')->with('status', 'Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $old = Event::where('id', $id)->first();

        if(file_exists($old->cover_image_full_path))
                unlink($old->cover_image_full_path);

        Event::where('id', $id)->delete();

        return redirect('game-events')->with('status', 'Successfully Deleted!');
    }
}
