<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_money_log;
use App\User;
use App\Game_log;
use App\Game_betting;
use App\Setting;
use Auth;
use DB;

class GameController extends Controller
{
    public function addBet(Request $request)
    {
        //check if game is still open or game exists
        $game = Game_log::where(['event_id' => $request->event_id])->get()->last();

        if(is_null($game))
        {
            return response()->json(['status' => '0', 'message' => 'Sorry! Bet cannot be added, Game is already closed.'], 200);
        }
        else
        {
            if($game->status == 'close')
            {
                return response()->json(['status' => '0', 'message' => 'Sorry! Bet cannot be added, Game is already closed.'], 200);
            }
            else
            {
                if($request->amount < 100)
                    return response()->json(['status' => '0', 'message' => 'Sorry! Minimum BET is 100 points.'], 200);

                $data = [
                    'user_id' => Auth::user()->id,
                    'amount' => $request->amount,
                    'module' => 'betting',
                    'operation' => '-',
                    'action' => $request->action,
                    'event_id' => $request->event_id,
                    'game_count' => $game->game_count,
                    'odds' => $request->odds,
                    'type' => $request->type,
                    'created_by' => Auth::user()->id
                ];
        
                User_money_log::create($data);

                Game_betting::create([
                    'user_id' => Auth::user()->id,
                    'amount' => $request->amount,
                    'action' => $request->action,
                    'event_id' => $request->event_id,
                    'game_count' => $game->game_count,
                    'odds' => $request->odds,
                    'type' => $request->type,
                    'created_by' => Auth::user()->id
                ]);
                
                // update points
                User::where('id', Auth::user()->id)->decrement('points', $request->amount);
        
                $user = User::where('id', Auth::user()->id)->first();
        
                return response()->json(['status' => '1', 'points' => round($user->points,0 , PHP_ROUND_HALF_UP)], 200);
            }
        }
    }

    public function cancelBet(Request $request)
    {
        //check if game is still open or game exists
        $game = Game_log::where(['event_id' => $request->event_id])->get()->last();

        $where = [
            'user_id' => Auth::user()->id,
            'event_id' => $request->event_id,
            'game_count' => $game->game_count,
            'odds' => $request->odds,
            'type' => $request->type
        ];

        //check if game is still open or game exists
        $game = Game_log::where(['event_id' => $request->event_id, 'game_count' => $game->game_count])->first();

        if(is_null($game))
        {
            return response()->json(['status' => '0', 'message' => 'Sorry! Bet cannot be cancel, Game is already closed.'], 200);
        }
        else
        {
            if($game->status == 'close')
            {
                return response()->json(['status' => '0', 'message' => 'Sorry! Bet cannot be cancel, Game is already closed.'], 200);
            }   
            else
            {

                // get current bet
                $current = User_money_log::where($where)->first();
                
                // update points
                User::where('id', Auth::user()->id)->increment('points', $current->amount);
        
                // delete bet
                User_money_log::where($where)->delete();
                // delete bet in betting finalization
                Game_betting::where($where)->delete();
        
                $user = User::where('id', Auth::user()->id)->first();
        
                return response()->json(['status' => '1', 'points' => round($user->points,0 , PHP_ROUND_HALF_UP)], 200);
            }
        }
    }

    public function status(Request $request)
    {
        $palasada   = 0.8;
        $setting = Setting::first();

        if(!is_null($setting))
            $palasada   = $plasada->plasada;

        $action     = $request->action;
        $value      = $request->value;
        $event_id   = $request->event_id;
        $game_count = $request->game_count;

        $count        = Game_log::select(DB::raw("COUNT(*) as count"))->where('event_id', $event_id)->get()[0]->count;
        $current      = Game_log::where(['event_id' => $event_id])->get()->last();
        $notify_users = [];
        $winners      = [];
        $game_total   = [];

        if($action == 'status')
        {
            // game opening and closing
            if($value == 'open')
            {
                if($count == 0)
                {
                    Game_log::create([
                        'event_id' => $event_id, 
                        'game_count' => 1, 
                        'status' => 'open',
                        'created_by' => Auth::user()->id
                    ]);
                }
                else
                {
                    if(is_null($current->winner))
                    {
                        Game_log::where('id', $current->id)->update([
                            'status' => 'open',
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                    else
                    {
                        return response()->json(['status' => '0', 'message' => 'Sorry! This game has a result already.'], 200);
                    }
                }
            }
            else // close
            {
                if(is_null($current->winner))
                {
                    Game_log::where('id', $current->id)->update([
                        'status' => 'close',
                        'updated_by' => Auth::user()->id
                    ]);

                    // get total per odds to compare
                    $where = [
                        'event_id' => $request->event_id,
                        'game_count' => $current->game_count
                    ];

                    $total_meron_1010 = 0;
                    $total_meron_109 = 0;
                    $total_meron_108 = 0;
                    $total_meron_86 = 0;
                    $total_meron_118 = 0;
                    $total_meron_910 = 0;
                    $total_meron_810 = 0;
                    $total_meron_811 = 0;
                    $total_meron_68 = 0;

                    $total_wala_1010 = 0;
                    $total_wala_109 = 0;
                    $total_wala_108 = 0;
                    $total_wala_86 = 0;
                    $total_wala_118 = 0;
                    $total_wala_910 = 0;
                    $total_wala_810 = 0;
                    $total_wala_811 = 0;
                    $total_wala_68 = 0;
                    $total_wala_18 = 0;

                    $game_bets = Game_betting::where($where)->get();

                    foreach($game_bets as $bet)
                    {
                        // for wala
                        if($bet->type == 'wala' && $bet->odds == '10-10')
                            $total_wala_1010 = (int)$total_wala_1010 + (int)$bet->amount;
                        if($bet->type == 'wala' && $bet->odds == '10-9')
                            $total_wala_109 = (int)$total_wala_109 + (int)$bet->amount;
                        if($bet->type == 'wala' && $bet->odds == '10-8')
                            $total_wala_108 = (int)$total_wala_108 + (int)$bet->amount;
                        if($bet->type == 'wala' && $bet->odds == '8-6')
                            $total_wala_86 = (int)$total_wala_86 + (int)$bet->amount;
                        if($bet->type == 'wala' && $bet->odds == '11-8')
                            $total_wala_118 = (int)$total_wala_118 + (int)$bet->amount;
                        if($bet->type == 'wala' && $bet->odds == '9-10')
                            $total_wala_910 = (int)$total_wala_910 + (int)$bet->amount;
                        if($bet->type == 'wala' && $bet->odds == '8-10')
                            $total_wala_810 = (int)$total_wala_810 + (int)$bet->amount;
                        if($bet->type == 'wala' && $bet->odds == '8-11')
                            $total_wala_811 = (int)$total_wala_811 + (int)$bet->amount;
                        if($bet->type == 'wala' && $bet->odds == '6-8')
                            $total_wala_68 = (int)$total_wala_68 + (int)$bet->amount;
                        if($bet->type == 'wala' && $bet->odds == '1-8')
                            $total_wala_18 = (int)$total_wala_18 + (int)$bet->amount;

                        // for meron
                        if($bet->type == 'meron' && $bet->odds == '10-10')
                            $total_meron_1010 = (int)$total_meron_1010 + (int)$bet->amount;
                        if($bet->type == 'meron' && $bet->odds == '10-9')
                            $total_meron_109 = (int)$total_meron_109 + (int)$bet->amount;
                        if($bet->type == 'meron' && $bet->odds == '10-8')
                            $total_meron_108 = (int)$total_meron_108 + (int)$bet->amount;
                        if($bet->type == 'meron' && $bet->odds == '8-6')
                            $total_meron_86 = (int)$total_meron_86 + (int)$bet->amount;
                        if($bet->type == 'meron' && $bet->odds == '11-8')
                            $total_meron_118 = (int)$total_meron_118 + (int)$bet->amount;
                        if($bet->type == 'meron' && $bet->odds == '9-10')
                            $total_meron_910 = (int)$total_meron_910 + (int)$bet->amount;
                        if($bet->type == 'meron' && $bet->odds == '8-10')
                            $total_meron_810 = (int)$total_meron_810 + (int)$bet->amount;
                        if($bet->type == 'meron' && $bet->odds == '8-11')
                            $total_meron_811 = (int)$total_meron_811 + (int)$bet->amount;
                        if($bet->type == 'meron' && $bet->odds == '6-8')
                            $total_meron_68 = (int)$total_meron_68 + (int)$bet->amount;
                    }
                    
                    # 10-10 finalize
                    if($total_meron_1010 !== 0 || $total_wala_1010 !== 0)
                    {   
                        if($total_wala_1010 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'meron';
                            $where['odds'] = '10-10';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>10-10</b> odds Wala.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '10-10',
                                    'type' => 'meron',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all meron no match
                            $where_remove = ['type' => 'meron', 'odds' => '10-10', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        elseif($total_meron_1010 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'wala';
                            $where['odds'] = '10-10';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>10-10</b> odds Meron.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '10-10',
                                    'type' => 'wala',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all wala no match
                            $where_remove = ['type' => 'wala', 'odds' => '10-10', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        else
                        {
                            if($total_meron_1010 !== $total_wala_1010)
                            {
                                $message = '';

                                if($total_meron_1010 > $total_wala_1010)
                                {
                                    $where['type'] = 'meron';
                                    $where['odds'] = '10-10';

                                    $return = round(abs($total_meron_1010 - $total_wala_1010),0 , PHP_ROUND_HALF_UP);
                                    // $message = '<b>'.$return.'</b> points has been deducted to your bet in <b>10-10</b> odds and has been added back to your total points.';
                                    
                                    // get all users who bet in this odds descending order
                                    $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                    $left = $return;

                                    foreach($users as $ub)
                                    {
                                        if($left > $ub->amount)
                                        {
                                            $left = $left - $ub->amount;

                                            $where['user_id'] = $ub->user_id;

                                            User_money_log::where($where)->delete();
                                            Game_betting::where($where)->delete();

                                            $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>10-10</b> odds and has been added back to your total points.';

                                            // update player points for returned 
                                            User::where('id', $ub->user_id)->increment('points', $ub->amount);
            
                                            // get user points
                                            $points = User::where('id', $ub->user_id)->get()->first();
            
                                            $user = [
                                                'odds' => $ub->odds,
                                                'type' => $ub->type,
                                                'event_id' => $ub->event_id,
                                                'user_id' => $ub->user_id, 
                                                'amount' => $ub->amount,
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'is_cancel' => '1'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }
                                        else
                                        {
                                            $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>10-10</b> odds and has been added back to your total points.';
            
                                            // return the amount subracted to player bet record
                                            $where['user_id'] = $ub->user_id;
                                            User_money_log::where($where)->decrement('amount', $left);
                                            Game_betting::where($where)->decrement('amount', $left);
            
                                            // update player points for returned 
                                            User::where('id', $ub->user_id)->increment('points', $left);
            
                                            // get user points
                                            $points = User::where('id', $ub->user_id)->get()->first();
            
                                            $user = [
                                                'odds' => $ub->odds,
                                                'type' => $ub->type,
                                                'event_id' => $ub->event_id,
                                                'user_id' => $ub->user_id, 
                                                'amount' => $left,
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'is_cancel' => '0'
                                            ];

                                            if($left !== 0)
                                            {
                                                array_push($notify_users, $user);
                                                break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    $where['type'] = 'wala';
                                    $where['odds'] = '10-10';

                                    $return = round(abs($total_wala_1010 - $total_meron_1010),0 , PHP_ROUND_HALF_UP);
                                    $message = '<b>'.$return.'</b> points has been deducted to your bet in <b>10-10</b> odds and has been added back to your total points.';
                                
                                    // get last player who bet in highest total bet
                                    $player = Game_betting::where($where)->get()->last();

                                    // return the amount subracted to player bet record
                                    $where['user_id'] = $player->user_id;

                                    User_money_log::where($where)->decrement('amount', $return);
                                    Game_betting::where($where)->decrement('amount', $return);

                                    // update player points for returned 
                                    User::where('id', $player->user_id)->increment('points', $return);

                                    // get user points
                                    $points = User::find($player->user_id);

                                    $user = [
                                        'user_id' => $player->user_id, 
                                        'amount' => $return,
                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                        'message' => $message,
                                        'is_cancel' => '0'
                                    ];
                                    
                                    array_push($notify_users, $user);
                                }
                            }
                        }
                    }
                    // end 10-10
                    
                    // finalize 10-9 - if 1000/900 else *0.9
                    if($total_meron_109 !== 0 || $total_wala_109 !== 0)
                    { 
                        if($total_wala_109 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'meron';
                            $where['odds'] = '10-9';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);;
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>10-9</b> odds Wala.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '10-9',
                                    'type' => 'meron',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all meron no match
                            $where_remove = ['type' => 'meron', 'odds' => '10-9', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        elseif($total_meron_109 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'wala';
                            $where['odds'] = '10-9';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>10-9</b> odds Meron.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '10-9',
                                    'type' => 'wala',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all wala no match
                            $where_remove = ['type' => 'wala', 'odds' => '10-9', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        else
                        {
                            $where['odds'] = '10-9';

                            if($total_meron_109 !== 1000 || $total_wala_109 !== 900) //default betting 1000
                            {
                                if(array_key_exists('type', $where))
                                    unset($where['type']);

                                if(array_key_exists('user_id', $where))
                                    unset($where['user_id']);

                                $first = Game_betting::where($where)->get()->first();

                                if($first->type == 'meron')
                                {
                                    $total_in_meron_109 = ($total_meron_109 * 0.9); // total in wala should match this variable value amount

                                    $total_in_meron_109 = (int)round($total_in_meron_109,0 , PHP_ROUND_HALF_UP);

                                    if($total_in_meron_109 !== $total_wala_109)
                                    {
                                        $message = '';
            
                                        if($total_in_meron_109 > $total_wala_109)
                                        {
                                            $meron_adjustment = $total_wala_109 / 0.9;

                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_109 - $meron_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>10-9</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>10-9</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $wala_adjustment = $total_wala_109 - $total_in_meron_109;

                                            $where['type'] = 'wala';
            
                                            $return = round(abs($wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>10-9</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>10-9</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else // wala
                                {
                                    $total_in_wala_109 = $total_wala_109 / 0.9;

                                    $total_in_wala_109 = (int)round($total_in_wala_109,0 , PHP_ROUND_HALF_UP);
                                    
                                    if($total_in_wala_109 !== $total_meron_109)
                                    {
                                        $message = '';
            
                                        if($total_meron_109 > $total_in_wala_109)
                                        {
                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_109 - $total_in_wala_109),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>10-9</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>10-9</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $where['type'] = 'wala';

                                            $wala_adjustment = $total_meron_109 * 0.9;
            
                                            $return = round(abs($total_wala_109 - $wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>10-9</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>10-9</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // end 10-9

                    // finalize 10-8 - if 1000/800 else *1.25
                    if($total_meron_108 !== 0 || $total_wala_108 !== 0)
                    { 
                        if($total_wala_108 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'meron';
                            $where['odds'] = '10-8';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);;
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>10-8</b> odds Wala.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '10-8',
                                    'type' => 'meron',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all meron no match
                            $where_remove = ['type' => 'meron', 'odds' => '10-8', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        elseif($total_meron_108 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'wala';
                            $where['odds'] = '10-8';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>10-8</b> odds Meron.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '10-8',
                                    'type' => 'wala',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all wala no match
                            $where_remove = ['type' => 'wala', 'odds' => '10-8', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        else
                        {
                            $where['odds'] = '10-8';

                            if($total_meron_108 !== 1000 || $total_in_wala_108 !== 800) //default betting 1000
                            {
                                if(array_key_exists('type', $where))
                                    unset($where['type']);

                                if(array_key_exists('user_id', $where))
                                    unset($where['user_id']);

                                $first = Game_betting::where($where)->get()->first();

                                if($first->type == 'meron')
                                {
                                    $total_in_meron_108 = ($total_meron_108 * 1.25); // total in wala should match this variable value amount

                                    $total_in_meron_108 = (int)round($total_in_meron_108,0 , PHP_ROUND_HALF_UP);

                                    if($total_in_meron_108 !== $total_wala_108)
                                    {
                                        $message = '';
            
                                        if($total_in_meron_108 > $total_wala_108)
                                        {
                                            $meron_adjustment = $total_wala_108 / 1.25;

                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_108 - $meron_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>10-8</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>10-8</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $wala_adjustment = $total_wala_108 - $total_in_meron_108;

                                            $where['type'] = 'wala';
            
                                            $return = round(abs($wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>10-8</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>10-8</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else // wala
                                {
                                    $total_in_wala_108 = $total_wala_108 / 1.25;

                                    $total_in_wala_108 = (int)round($total_in_wala_108,0 , PHP_ROUND_HALF_UP);
                                    
                                    if($total_in_wala_108!== $total_meron_108)
                                    {
                                        $message = '';
            
                                        if($total_meron_108 > $total_in_wala_108)
                                        {
                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_108 - $total_in_wala_108),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>10-8</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>10-8</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $where['type'] = 'wala';

                                            $wala_adjustment = $total_meron_108 * 1.25;
            
                                            $return = round(abs($total_wala_108 - $wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>10-8</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>10-8</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // end 10-8

                    // finalize 8-6 - if 800/600 else *1.33
                    if($total_meron_86 !== 0 || $total_wala_86 !== 0)
                    { 
                        if($total_wala_86 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'meron';
                            $where['odds'] = '8-6';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);;
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>8-6</b> odds Wala.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '8-6',
                                    'type' => 'meron',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all meron no match
                            $where_remove = ['type' => 'meron', 'odds' => '8-6', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        elseif($total_meron_86 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'wala';
                            $where['odds'] = '8-6';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>8-6</b> odds Meron.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '8-6',
                                    'type' => 'wala',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all wala no match
                            $where_remove = ['type' => 'wala', 'odds' => '8-6', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        else
                        {
                            $where['odds'] = '8-6';

                            if($total_meron_86 !== 800 || $total_wala_86 !== 600) //default betting 800
                            {
                                if(array_key_exists('type', $where))
                                    unset($where['type']);

                                if(array_key_exists('user_id', $where))
                                    unset($where['user_id']);

                                $first = Game_betting::where($where)->get()->first();

                                if($first->type == 'meron')
                                {
                                    $total_in_meron_86 = ($total_meron_86 * 1.33); // total in wala should match this variable value amount

                                    $total_in_meron_86 = (int)round($total_in_meron_86,0 , PHP_ROUND_HALF_UP);

                                    if($total_in_meron_86 !== $total_wala_86)
                                    {
                                        $message = '';
            
                                        if($total_in_meron_86 > $total_wala_86)
                                        {
                                            $meron_adjustment = $total_wala_86 / 1.33;

                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_86 - $meron_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>8-6</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>8-6</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $wala_adjustment = $total_wala_86 - $total_in_meron_86;

                                            $where['type'] = 'wala';
            
                                            $return = round(abs($wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>8-6</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>8-6</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else // wala
                                {
                                    $total_in_wala_86 = $total_wala_86 / 1.33;

                                    $total_in_wala_86 = (int)round($total_in_wala_86,0 , PHP_ROUND_HALF_UP);
                                    
                                    if($total_in_wala_86 !== $total_meron_86)
                                    {
                                        $message = '';
            
                                        if($total_meron_86 > $total_in_wala_86)
                                        {
                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_86 - $total_in_wala_86),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>8-6</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>8-6</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $where['type'] = 'wala';

                                            $wala_adjustment = $total_meron_86 * 1.33;
            
                                            $return = round(abs($total_wala_86 - $wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>8-6</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>8-6</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // end 8-6

                    // finalize 11-8 - if 1100/800 else *1.375
                    if($total_meron_118 !== 0 || $total_wala_118 !== 0)
                    { 
                        if($total_wala_118 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'meron';
                            $where['odds'] = '11-8';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);;
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>11-8</b> odds Wala.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '11-8',
                                    'type' => 'meron',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all meron no match
                            $where_remove = ['type' => 'meron', 'odds' => '11-8', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        elseif($total_meron_118 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'wala';
                            $where['odds'] = '11-8';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>11-8</b> odds Meron.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '11-8',
                                    'type' => 'wala',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all wala no match
                            $where_remove = ['type' => 'wala', 'odds' => '11-8', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        else
                        {
                            $where['odds'] = '11-8';

                            if($total_meron_118 !== 1100 || $total_wala_118 !== 800) //default betting 1100
                            {
                                if(array_key_exists('type', $where))
                                    unset($where['type']);

                                if(array_key_exists('user_id', $where))
                                    unset($where['user_id']);

                                $first = Game_betting::where($where)->get()->first();

                                if($first->type == 'meron')
                                {
                                    $total_in_meron_118 = ($total_meron_118 * 1.375); // total in wala should match this variable value amount

                                    $total_in_meron_118 = (int)round($total_in_meron_118,0 , PHP_ROUND_HALF_UP);

                                    if($total_in_meron_118 !== $total_wala_118)
                                    {
                                        $message = '';
            
                                        if($total_in_meron_118 > $total_wala_118)
                                        {
                                            $meron_adjustment = $total_wala_118 / 1.375;

                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_118 - $meron_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>11-8</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>11-8</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $wala_adjustment = $total_wala_118 - $total_in_meron_118;

                                            $where['type'] = 'wala';
            
                                            $return = round(abs($wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>11-8</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>11-8</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else // wala
                                {
                                    $total_in_wala_118 = $total_wala_118 / 1.375;

                                    $total_in_wala_118 = (int)round($total_in_wala_118,0 , PHP_ROUND_HALF_UP);
                                    
                                    if($total_in_wala_118 !== $total_meron_118)
                                    {
                                        $message = '';
            
                                        if($total_meron_118 > $total_in_wala_118)
                                        {
                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_118 - $total_in_wala_118),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>11-8</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>11-8</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $where['type'] = 'wala';

                                            $wala_adjustment = $total_meron_118 * 1.375;
            
                                            $return = round(abs($total_wala_118 - $wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>11-8</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>11-8</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // end 11-8

                    // finalize 9-10 - if 900/1000 else *0.9
                    if($total_meron_910 !== 0 || $total_wala_910 !== 0)
                    { 
                        if($total_wala_910 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'meron';
                            $where['odds'] = '9-10';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);;
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>9-10</b> odds Wala.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '9-10',
                                    'type' => 'meron',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all meron no match
                            $where_remove = ['type' => 'meron', 'odds' => '9-10', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        elseif($total_meron_910 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'wala';
                            $where['odds'] = '9-10';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>9-10</b> odds Meron.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '9-10',
                                    'type' => 'wala',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all wala no match
                            $where_remove = ['type' => 'wala', 'odds' => '9-10', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        else
                        {
                            $where['odds'] = '9-10';
                            
                            if($total_meron_910 !== 900 || $total_wala_910 !== 1000) //default betting 900
                            {
                                if(array_key_exists('type', $where))
                                    unset($where['type']);

                                if(array_key_exists('user_id', $where))
                                    unset($where['user_id']);

                                $first = Game_betting::where($where)->get()->first();

                                if($first->type == 'meron')
                                {
                                    $total_in_meron_910 = (int)($total_meron_910 / 0.9); // total in wala should match this variable value amount

                                    if($total_in_meron_910 !== $total_wala_910)
                                    {
                                        $message = '';
            
                                        if($total_in_meron_910 > $total_wala_910)
                                        {
                                            // $meron_adjustment = $total_meron_910 - ($total_meron_910 - (($total_wala_910/1) / 0.9) * 1);
                                            $meron_adjustment = $total_wala_910 * 0.9;

                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_910 - $meron_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>9-10</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>9-10</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $wala_adjustment = $total_wala_910 - $total_in_meron_910;

                                            $where['type'] = 'wala';
            
                                            $return = round(abs($wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>9-10</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>9-10</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else // wala
                                {
                                    //$total_in_wala_910 = $total_meron_910 - ($total_meron_910 - (($total_wala_910/1) / 0.9) * 1); // total in meron should match this variable value amount
                                    $total_in_wala_910 = $total_wala_910 * 0.9;

                                    $total_in_wala_910 = (int)$total_in_wala_910;

                                    if($total_in_wala_910 !== $total_meron_910)
                                    {
                                        $message = '';
            
                                        if($total_meron_910 > $total_in_wala_910)
                                        {
                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_910 - $total_in_wala_910),0 , PHP_ROUND_HALF_UP);
                                            
                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>9-10</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>9-10</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $where['type'] = 'wala';

                                            $wala_adjustment = $total_meron_910 / 0.9;
            
                                            $return = round(abs($total_wala_910 - $wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>9-10</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>9-10</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // end 9-10

                    // finalize 8-10 - if 800/1000 else *1.25
                    if($total_meron_810 !== 0 || $total_wala_810 !== 0)
                    { 
                        if($total_wala_810 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'meron';
                            $where['odds'] = '8-10';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);;
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>8-10</b> odds Wala.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '8-10',
                                    'type' => 'meron',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all meron no match
                            $where_remove = ['type' => 'meron', 'odds' => '8-10', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        elseif($total_meron_810 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'wala';
                            $where['odds'] = '8-10';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>8-10</b> odds Meron.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '8-10',
                                    'type' => 'wala',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all wala no match
                            $where_remove = ['type' => 'wala', 'odds' => '8-10', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        else
                        {
                            $where['odds'] = '8-10';

                            if($total_meron_810 !== 800 || $total_wala_810 !== 1000) //default betting 800
                            {
                                if(array_key_exists('type', $where))
                                    unset($where['type']);

                                if(array_key_exists('user_id', $where))
                                    unset($where['user_id']);

                                $first = Game_betting::where($where)->get()->first();

                                if($first->type == 'meron')
                                {
                                    $total_in_meron_810 = (int)($total_meron_810 / 1.25); // total in wala should match this variable value amount

                                    if($total_in_meron_810 !== $total_wala_810)
                                    {
                                        $message = '';
            
                                        if($total_in_meron_810 > $total_wala_810)
                                        {
                                            $meron_adjustment = $total_wala_810 * 1.25;

                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_810 - $meron_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>8-10</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>8-10</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $wala_adjustment = $total_wala_910 - $total_in_meron_910;

                                            $where['type'] = 'wala';
            
                                            $return = round(abs($wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>8-10</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>8-10</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else // wala
                                {
                                    $total_in_wala_810 = $total_wala_810 * 1.25;

                                    $total_in_wala_810 = (int)$total_in_wala_810;

                                    if($total_in_wala_810 !== $total_meron_810)
                                    {
                                        $message = '';
            
                                        if($total_meron_810 > $total_in_wala_810)
                                        {
                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_810 - $total_in_wala_810),0 , PHP_ROUND_HALF_UP);
                                            
                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>8-10</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>8-10</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $where['type'] = 'wala';

                                            $wala_adjustment = $total_meron_810 / 1.25;
            
                                            $return = round(abs($total_wala_810 - $wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>8-10</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>8-10</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // end 8-10

                    // finalize 8-11 - if 800/1100 else *1.375
                    if($total_meron_811 !== 0 || $total_wala_811 !== 0)
                    { 
                        if($total_wala_811 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'meron';
                            $where['odds'] = '8-11';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);;
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>8-11</b> odds Wala.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '8-11',
                                    'type' => 'meron',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all meron no match
                            $where_remove = ['type' => 'meron', 'odds' => '8-11', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        elseif($total_meron_811 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'wala';
                            $where['odds'] = '8-11';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>8-11</b> odds Meron.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '8-11',
                                    'type' => 'wala',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all wala no match
                            $where_remove = ['type' => 'wala', 'odds' => '8-11', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        else
                        {
                            $where['odds'] = '8-11';

                            if($total_meron_811 !== 800 || $ttoal_wala_811 !== 1100) //default betting 800
                            {
                                if(array_key_exists('type', $where))
                                    unset($where['type']);

                                if(array_key_exists('user_id', $where))
                                    unset($where['user_id']);

                                $first = Game_betting::where($where)->get()->first();

                                if($first->type == 'meron')
                                {
                                    $total_in_meron_811 = (int)($total_meron_811 / 1.375); // total in wala should match this variable value amount

                                    if($total_in_meron_811 !== $total_wala_811)
                                    {
                                        $message = '';
            
                                        if($total_in_meron_811 > $total_wala_811)
                                        {
                                            $meron_adjustment = $total_wala_811 * 1.375;

                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_811 - $meron_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>8-11</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>8-11</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $wala_adjustment = $total_wala_811 - $total_in_meron_811;

                                            $where['type'] = 'wala';
            
                                            $return = round(abs($wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>8-11</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>8-11</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else // wala
                                {
                                    $total_in_wala_811 = $total_wala_811 * 1.375;

                                    $total_in_wala_811 = (int)$total_in_wala_811;

                                    if($total_in_wala_811 !== $total_meron_811)
                                    {
                                        $message = '';
            
                                        if($total_meron_811 > $total_in_wala_811)
                                        {
                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_811 - $total_in_wala_811),0 , PHP_ROUND_HALF_UP);
                                            
                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>8-11</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>8-11</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $where['type'] = 'wala';

                                            $wala_adjustment = $total_meron_811 / 1.375;
            
                                            $return = round(abs($total_wala_811 - $wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>8-11</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>8-11</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // end 8-11

                    // finalize 6-8 - if 600/800 else *1.33
                    if($total_meron_68 !== 0 || $total_wala_68 !== 0)
                    { 
                        if($total_wala_68 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'meron';
                            $where['odds'] = '6-8';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);;
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>6-8</b> odds Wala.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '6-8',
                                    'type' => 'meron',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all meron no match
                            $where_remove = ['type' => 'meron', 'odds' => '6-8', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        elseif($total_meron_68 == 0) // cancel all meron no match in wala
                        {
                            $where['type'] = 'wala';
                            $where['odds'] = '6-8';

                            // get all users who has bet in this odds
                            $users_bet = Game_betting::where($where)->get();

                            foreach($users_bet as $user_bet)
                            {
                                $return = round($user_bet->amount,0 , PHP_ROUND_HALF_UP);
                                $message = '<b>'.$return.'</b> points has been added back to your total points. There is no opponent in <b>6-8</b> odds Meron.';
                            
                                // return the amount subracted to player bet record
                                $where['user_id'] = $user_bet->user_id;

                                User_money_log::where($where)->delete();
                                Game_betting::where($where)->delete();

                                // update player points for returned 
                                User::where('id', $user_bet->user_id)->increment('points', $return);

                                // get user points
                                $points = User::find($user_bet->user_id);

                                $user = [
                                    'user_id' => $user_bet->user_id, 
                                    'amount' => $return,
                                    'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                    'message' => $message,
                                    'event_id' => $event_id,
                                    'odds' => '6-8',
                                    'type' => 'wala',
                                    'is_cancel' => '1'
                                ];
                                
                                array_push($notify_users, $user);
                            }

                            // cancel all wala no match
                            $where_remove = ['type' => 'wala', 'odds' => '6-8', 'event_id' => $event_id, 'game_count' => $current->game_count];

                            Game_betting::where($where_remove)->delete();
                            User_money_log::where($where_remove)->delete();
                        }
                        else
                        {
                            $where['odds'] = '6-8';

                            if($total_meron_68 !== 600 || $total_wala_68 !== 800) //default betting 800
                            {
                                if(array_key_exists('type', $where))
                                    unset($where['type']);

                                if(array_key_exists('user_id', $where))
                                    unset($where['user_id']);

                                $first = Game_betting::where($where)->get()->first();

                                if($first->type == 'meron')
                                {
                                    $total_in_meron_68 = (int)($total_meron_68 / 1.33); // total in wala should match this variable value amount

                                    if($total_in_meron_68 !== $total_wala_68)
                                    {
                                        $message = '';
            
                                        if($total_in_meron_68 > $total_wala_68)
                                        {
                                            $meron_adjustment = $total_wala_68 * 1.33;

                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_68 - $meron_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>6-8</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>6-8</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $wala_adjustment = $total_wala_68 - $total_in_meron_68;

                                            $where['type'] = 'wala';
            
                                            $return = round(abs($wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>6-8</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>6-8</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else // wala
                                {
                                    $total_in_wala_68 = $total_wala_68 * 1.33;

                                    $total_in_wala_68 = (int)$total_in_wala_68;

                                    if($total_in_wala_68 !== $total_meron_68)
                                    {
                                        $message = '';
            
                                        if($total_meron_68 > $total_in_wala_68)
                                        {
                                            $where['type'] = 'meron';
            
                                            $return = round(abs($total_meron_68 - $total_in_wala_68),0 , PHP_ROUND_HALF_UP);
                                            
                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>6-8</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>6-8</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $where['type'] = 'wala';

                                            $wala_adjustment = $total_meron_68 / 1.33;
            
                                            $return = round(abs($total_wala_68 - $wala_adjustment),0 , PHP_ROUND_HALF_UP);

                                            // get all users who bet in this odds descending order
                                            $users = Game_betting::where($where)->orderBy('id', 'DESC')->get();

                                            $left = $return;

                                            foreach($users as $ub)
                                            {
                                                if($left > $ub->amount)
                                                {
                                                    $left = $left - $ub->amount;

                                                    $where['user_id'] = $ub->user_id;

                                                    User_money_log::where($where)->delete();
                                                    Game_betting::where($where)->delete();

                                                    $message = 'Your <b>'.$ub->amount.'</b> points bet has been cancelled in <b>6-8</b> odds and has been added back to your total points.';

                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $ub->amount);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $ub->amount,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '1'
                                                    ];
                                                    
                                                    array_push($notify_users, $user);
                                                }
                                                else
                                                {
                                                    $message = '<b>'.$left.'</b> points has been deducted to your bet in <b>6-8</b> odds and has been added back to your total points.';
                    
                                                    // return the amount subracted to player bet record
                                                    $where['user_id'] = $ub->user_id;
                                                    User_money_log::where($where)->decrement('amount', $left);
                                                    Game_betting::where($where)->decrement('amount', $left);
                    
                                                    // update player points for returned 
                                                    User::where('id', $ub->user_id)->increment('points', $left);
                    
                                                    // get user points
                                                    $points = User::where('id', $ub->user_id)->get()->first();
                    
                                                    $user = [
                                                        'odds' => $ub->odds,
                                                        'type' => $ub->type,
                                                        'event_id' => $ub->event_id,
                                                        'user_id' => $ub->user_id, 
                                                        'amount' => $left,
                                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                        'message' => $message,
                                                        'is_cancel' => '0'
                                                    ];

                                                    if($left !== 0)
                                                    {
                                                        array_push($notify_users, $user);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // end 6-8

                }
                else
                {
                    return response()->json(['status' => '0', 'message' => 'Sorry! This game has a result already.'], 200);
                }
            }
        }
        elseif($action == 'next-game')
        {
            // net game
            if(is_null($current))
            {
                return response()->json(['status' => '0', 'message' => 'Sorry! Please Declare game result first before proceeding to the next round of the game.'], 200);
            }
            else
            {
                if(is_null($current->winner))
                {
                    return response()->json(['status' => '0', 'message' => 'Sorry! Please Declare game result first before proceeding to the next round of the game.'], 200);
                }
                else
                {
                    Game_log::create([
                        'event_id' => $event_id, 
                        'game_count' => $count + 1, 
                        'status' => 'close',
                        'created_by' => Auth::user()->id
                    ]);
                }
            }
        }
        else
        {
            // game result declaration
            if(is_null($current))
            {
                return response()->json(['status' => '0', 'message' => 'Sorry! You cannot announce a game result/winner while the game status is not close.'], 200);
            }
            else
            {
                if($current->status == 'open')
                {
                    return response()->json(['status' => '0', 'message' => 'Sorry! You cannot announce a game result/winner while the betting status is open.'], 200);
                }
                else
                {
                    if(is_null($current->winner))
                    {
                        Game_log::where(['id' => $current->id])->update(['winner' => $value]);

                        $where = [
                            'event_id' => $event_id,
                            'game_count' => $current->game_count,
                        ];

                        $users = User_money_log::where($where)->get();

                        $total_meron_1010 = 0;
                        $total_meron_109 = 0;
                        $total_meron_108 = 0;
                        $total_meron_86 = 0;
                        $total_meron_118 = 0;
                        $total_meron_910 = 0;
                        $total_meron_810 = 0;
                        $total_meron_811 = 0;
                        $total_meron_68 = 0;

                        $total_wala_1010 = 0;
                        $total_wala_109 = 0;
                        $total_wala_108 = 0;
                        $total_wala_86 = 0;
                        $total_wala_118 = 0;
                        $total_wala_910 = 0;
                        $total_wala_810 = 0;
                        $total_wala_811 = 0;
                        $total_wala_68 = 0;
                        $total_wala_18 = 0;

                        if(count($users) !== 0)
                        {
                            foreach($users as $bet)
                            {
                                if($bet->type == 'wala' && $bet->odds == '10-10')
                                    $total_wala_1010 = $total_wala_1010 + $bet->amount;
                                if($bet->type == 'wala' && $bet->odds == '10-9')
                                    $total_wala_109 = $total_wala_109 + $bet->amount;
                                if($bet->type == 'wala' && $bet->odds == '10-8')
                                    $total_wala_108 = $total_wala_108 + $bet->amount;
                                if($bet->type == 'wala' && $bet->odds == '8-6')
                                    $total_wala_86 = $total_wala_86 + $bet->amount;
                                if($bet->type == 'wala' && $bet->odds == '11-8')
                                    $total_wala_118 = $total_wala_118 + $bet->amount;
                                if($bet->type == 'wala' && $bet->odds == '9-10')
                                    $total_wala_910 = $total_wala_910 + $bet->amount;
                                if($bet->type == 'wala' && $bet->odds == '8-10')
                                    $total_wala_810 = $total_wala_810 + $bet->amount;
                                if($bet->type == 'wala' && $bet->odds == '8-11')
                                    $total_wala_811 = $total_wala_811 + $bet->amount;
                                if($bet->type == 'wala' && $bet->odds == '6-8')
                                    $total_wala_68 = $total_wala_68 + $bet->amount;
                                if($bet->type == 'wala' && $bet->odds == '1-8')
                                    $total_wala_18 = $total_wala_18 + $bet->amount;

                                // for meron
                                if($bet->type == 'meron' && $bet->odds == '10-10')
                                    $total_meron_1010 = $total_meron_1010 + $bet->amount;
                                if($bet->type == 'meron' && $bet->odds == '10-9')
                                    $total_meron_109 = $total_meron_109 + $bet->amount;
                                if($bet->type == 'meron' && $bet->odds == '10-8')
                                    $total_meron_108 = $total_meron_108 + $bet->amount;
                                if($bet->type == 'meron' && $bet->odds == '8-6')
                                    $total_meron_86 = $total_meron_86 + $bet->amount;
                                if($bet->type == 'meron' && $bet->odds == '11-8')
                                    $total_meron_118 = $total_meron_118 + $bet->amount;
                                if($bet->type == 'meron' && $bet->odds == '9-10')
                                    $total_meron_910 = $total_meron_910 + $bet->amount;
                                if($bet->type == 'meron' && $bet->odds == '8-10')
                                    $total_meron_810 = $total_meron_810 + $bet->amount;
                                if($bet->type == 'meron' && $bet->odds == '8-11')
                                    $total_meron_811 = $total_meron_811 + $bet->amount;
                                if($bet->type == 'meron' && $bet->odds == '6-8')
                                    $total_meron_68 = $total_meron_68 + $bet->amount;
                                    
                                if($value == 'ML')
                                {
                                    // *0.9, *1.25, *1.33, *1.375
                                    if($bet->type == 'meron')
                                    {
                                        if($bet->odds == '10-9')
                                        {
                                            $earned = ($bet->amount * 0.9) - $palasada;

                                            if($total_meron_109 == 1000 && $total_wala_109 == 900)
                                                $earned = ($bet->amount / 0.9) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }

                                        if($bet->odds == '10-8')
                                        {
                                            $earned = ($bet->amount * 1.25) - $palasada;

                                            if($total_meron_108 == 1000 && $total_wala_108 == 800)
                                                $earned = ($bet->amount / 1.25) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }

                                        if($bet->odds == '8-6')
                                        {
                                            $earned = ($bet->amount * 1.33) - $palasada;

                                            if($total_meron_86 == 800 && $total_wala_86 == 600)
                                                $earned = ($bet->amount / 1.33) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }

                                        if($bet->odds == '11-8')
                                        {
                                            $earned = ($bet->amount * 1.375) - $palasada;

                                            if($total_meron_118 == 1100 && $total_wala_118 == 800)
                                                $earned = ($bet->amount / 1.375) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }
                                    }
                                    else
                                    {
                                        $update = ['status' => 'loose'];

                                        $where_to = [
                                            'user_id' => $bet->user_id,
                                            'odds' =>  $bet->odds,
                                            'type' => $bet->type
                                        ];

                                        User_money_log::where(array_merge($where_to, $where))->update($update);
                                    }
                                }
                                elseif($value == 'MD')
                                {
                                    // /0.9, /1.25, /1.33, /1.375
                                    if($bet->type == 'meron')
                                    {
                                        if($bet->odds == '9-10')
                                        {
                                            $earned = ($bet->amount / 0.9) - $palasada;

                                            if($total_meron_910 == 900 && $total_wala_910 == 1000)
                                                $earned = ($bet->amount * 0.9) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }

                                        if($bet->odds == '8-10')
                                        {
                                            $earned = ($bet->amount / 1.25) - $palasada;

                                            if($total_meron_810 == 800 && $total_wala_810 == 1000)
                                                $earned = ($bet->amount * 1.25) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }

                                        if($bet->odds == '6-8')
                                        {
                                            $earned = ($bet->amount / 1.33) - $palasada;

                                            if($total_meron_68 == 600 && $total_wala_68 == 800)
                                                $earned = ($bet->amount * 1.33) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }

                                        if($bet->odds == '8-11')
                                        {
                                            $earned = ($bet->amount / 1.375) - $palasada;

                                            if($total_meron_811 == 800 && $total_wala_811 == 1100)
                                                $earned = ($bet->amount * 1.375) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }
                                    }
                                    else
                                    {
                                        $update = ['status' => 'loose'];

                                        $where_to = [
                                            'user_id' => $bet->user_id,
                                            'odds' =>  $bet->odds,
                                            'type' => $bet->type
                                        ];

                                        User_money_log::where(array_merge($where_to, $where))->update($update);
                                    }
                                }
                                elseif($value == 'WL')
                                {
                                    // *0.9, *1.25, *1.33, *1.375
                                    if($bet->type == 'wala')
                                    {
                                        if($bet->odds == '9-10')
                                        {
                                            $earned = ($bet->amount * 0.9) - $palasada;

                                            if($total_meron_910 == 900 && $total_wala_910 == 1000)
                                                $earned = ($bet->amount / 0.9) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }

                                        if($bet->odds == '8-10')
                                        {
                                            $earned = ($bet->amount * 1.25) - $palasada;

                                            if($total_meron_810 == 800 && $total_wala_810 == 1000)
                                                $earned = ($bet->amount / 1.25) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }

                                        if($bet->odds == '8-6')
                                        {
                                            $earned = ($bet->amount * 1.33) - $palasada;

                                            if($total_meron_86 == 800 && $total_wala_86 == 600)
                                                $earned = ($bet->amount / 1.33) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }

                                        if($bet->odds == '8-11')
                                        {
                                            $earned = ($bet->amount * 1.375) - $palasada;

                                            if($total_meron_811 == 800 && $total_wala_811 == 1100)
                                                $earned = ($bet->amount / 1.375) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }
                                    }
                                    else
                                    {
                                        $update = ['status' => 'loose'];

                                        $where_to = [
                                            'user_id' => $bet->user_id,
                                            'odds' =>  $bet->odds,
                                            'type' => $bet->type
                                        ];

                                        User_money_log::where(array_merge($where_to, $where))->update($update);
                                    }
                                }
                                elseif($value == 'WD')
                                {
                                    // /0.9, /1.25, /1.33, /1.375
                                    if($bet->type == 'meron')
                                    {
                                        if($bet->odds == '10-9')
                                        {
                                            $earned = ($bet->amount / 0.9) - $palasada;

                                            if($total_meron_109 == 1000 && $total_wala_109 == 900)
                                                $earned = ($bet->amount * 0.9) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }

                                        if($bet->odds == '10-8')
                                        {
                                            $earned = ($bet->amount / 1.25) - $palasada;

                                            if($total_meron_108 == 1000 && $total_wala_108 == 800)
                                                $earned = ($bet->amount * 1.25) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }

                                        if($bet->odds == '8-6')
                                        {
                                            $earned = ($bet->amount / 1.33) - $palasada;

                                            if($total_meron_86 == 800 && $total_wala_86 == 600)
                                                $earned = ($bet->amount * 1.33) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }

                                        if($bet->odds == '11-8')
                                        {
                                            $earned = ($bet->amount / 1.375) - $palasada;

                                            if($total_meron_118 == 1100 && $total_wala_118 == 800)
                                                $earned = ($bet->amount * 1.375) - $palasada;

                                            $update = ['status' => 'hit', 'earned' => $earned ];

                                            $where_to = [
                                                'user_id' => $bet->user_id,
                                                'odds' =>  $bet->odds,
                                                'type' => $bet->type
                                            ];

                                            User_money_log::where(array_merge($where_to, $where))->update($update);

                                            $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                            User::where('id', $bet->user_id)->increment('points', $earned);

                                            $points = User::where('id', $bet->user_id)->get()->first();

                                            $user = [
                                                'user_id' => $bet->user_id, 
                                                'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                                'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                                'message' => $message,
                                                'event_id' => $event_id,
                                                'odds' => $bet->odds,
                                                'type' => $bet->type,
                                                'is_cancel' => '0'
                                            ];
                                            
                                            array_push($notify_users, $user);
                                        }
                                    }
                                    else
                                    {
                                        $update = ['status' => 'loose'];

                                        $where_to = [
                                            'user_id' => $bet->user_id,
                                            'odds' =>  $bet->odds,
                                            'type' => $bet->type
                                        ];

                                        User_money_log::where(array_merge($where_to, $where))->update($update);
                                    }
                                }
                                elseif($value == 'D')
                                {
                                    User_money_log::where($where)->update(['status' => 'draw']);

                                    if($user->odds == '1-8')
                                    {
                                        $earned = ($bet->amount * 8) - $palasada;

                                        $update = ['status' => 'hit', 'earned' => $earned ];

                                        $where_to = [
                                            'user_id' => $bet->user_id,
                                            'odds' =>  $bet->odds,
                                            'type' => $bet->type
                                        ];

                                        User_money_log::where(array_merge($where_to, $where))->update($update);

                                        $message = 'You WIN <b>'.round($earned,0 , PHP_ROUND_HALF_UP).'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added to your total points.';

                                        User::where('id', $bet->user_id)->increment('points', $earned);

                                        $points = User::where('id', $bet->user_id)->get()->first();

                                        $user = [
                                            'user_id' => $bet->user_id, 
                                            'amount' => round($earned,0 , PHP_ROUND_HALF_UP),
                                            'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                            'message' => $message,
                                            'event_id' => $event_id,
                                            'odds' => $bet->odds,
                                            'type' => $bet->type,
                                            'is_cancel' => '0'
                                        ];
                                        
                                        array_push($notify_users, $user);
                                    }
                                    else
                                    {
                                        $update = ['status' => 'loose'];

                                        $where_to = [
                                            'user_id' => $bet->user_id,
                                            'odds' =>  $bet->odds,
                                            'type' => $bet->type
                                        ];

                                        User_money_log::where(array_merge($where_to, $where))->update($update);
                                    }
                                }
                                else // cancelled
                                {
                                    User_money_log::where($where)->update(['status' => 'cancel']);

                                    $return = $bet->amount;
                                    $message = '<b>'.$return.'</b> points from your bet in <b>'.$bet->odds.'</b> odds and has been added back to your total points.';

                                    User::where('id', $user->user_id)->increment('points', $return);

                                    $points = User::where('id', $bet->user_id)->get()->first();

                                    $user = [
                                        'user_id' => $bet->user_id, 
                                        'amount' => $return,
                                        'points' => round($points->points,0 , PHP_ROUND_HALF_UP), 
                                        'message' => $message,
                                        'event_id' => $event_id,
                                        'odds' => $bet->odds,
                                        'type' => $bet->type,
                                        'is_cancel' => '0'
                                    ];
                                    
                                    array_push($notify_users, $user);
                                }
                            }
                        }
                    }
                    else
                    {
                        return response()->json(['status' => '0', 'message' => 'Sorry! You already declare '.$current->winner.' as winner on this game.'], 200);
                    }
                }
            }
        }

        $data   = Game_log::where('event_id', $event_id)->get()->last();
        $totals = Game_log::where('event_id', $event_id)->get();

        $total_m = 0;
        $total_w = 0;
        $total_d = 0;
        $total_c = 0;

        foreach($totals as $logs)
        {
            if($logs->winner == 'ML')
                $total_m = $total_m + 1;
            if($logs->winner == 'MD')
                $total_m = $total_m + 1;
            if($logs->winner == 'WL')
                $total_w = $total_w + 1;
            if($logs->winner == 'WD')
                $total_w = $total_w + 1;
            if($logs->winner == 'D')
                $total_d = $total_d + 1;
            if($logs->winner == 'C')
                $total_c = $total_c + 1;
        }

        $data['total_m']      = $total_m;
        $data['total_w']      = $total_w;
        $data['total_d']      = $total_d;
        $data['total_c']      = $total_c;
        $data['notify_users'] = $notify_users;
        $data['game_betting'] = User_money_log::where(['event_id' => $event_id, 'game_count' => $current->game_count])->get();
        $data['winners']      = $winners;

        return response()->json($data, 200);
    }

    public function getTotalPerType(Request $request)
    {
        $where = [
            'event_id' => $request->event_id,
            'game_count' => $request->game_count,
            'odds' => $request->odds,
            'type' => $request->type
        ];

        // get all total amount per odds
        $total = User_money_log::select(DB::raw('SUM(amount) as total'))->where($where)->get();
        
        return response()->json(['total' => (isset($total[0]->total)) ? $total[0]->total : '0'], 200);
    }
}
