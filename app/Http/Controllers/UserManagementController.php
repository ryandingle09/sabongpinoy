<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoadingRequest;
use App\Http\Requests\TransferPasaloadRequest;
use App\Http\Requests\CashoutRequest;
use App\User;
use App\User_money_log;
use Auth;
use App\DataTables\UserManagementsDataTable;

class UserManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UserManagementsDataTable $dataTable)
    {
        return $dataTable->render('user_management.user_management');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function loading_create($id)
    {
        $data = ['data' => User::where('id', $id)->first()];
        return view('user_management.loading', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loading(LoadingRequest $request, $id)
    {
        $user = User::find($id);
        $current_points = $user->points;
        $new_points = $current_points + $request->points;

        User_money_log::create([
            'user_id' => $id,
            'amount' => $request->points,
            'module' => 'loading',
            'operation' => '+',
            'created_by' => Auth::user()->id
        ]);

        User::where('id', $id)->update(['points' => $new_points]);

        return redirect('user-management')->with('status', 'Successfully Loaded Points!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pasaload_create($id)
    {
        $data = ['data' => User::where('id', $id)->first()];
        return view('user_management.pasaload', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function pasaload(TransferPasaloadRequest $request, $id)
    {
        # get to user points data
        $to_user = User::find($request->to_player);
        $current_points_to_player = $to_user->points;
        $new_points_to_player = $current_points_to_player + $request->points;
        
        # get from user points data
        $user = User::find($id);
        $current_points = $user->points;
        $new_points = $current_points - $request->points;

        User_money_log::create([
            'user_id' => $id,
            'amount' => $request->points,
            'module' => 'transfer/pasaload',
            'operation' => '-',
            'to' => $request->to_player,
            'created_by' => Auth::user()->id
        ]);

        User_money_log::create([
            'user_id' => $request->to_player,
            'amount' => $request->points,
            'module' => 'transfer/pasaload',
            'operation' => '+',
            'from' => $id,
            'created_by' => Auth::user()->id
        ]);

        User::where('id', $id)->update(['points' => $new_points]);
        User::where('id', $request->to_player)->update(['points' => $new_points_to_player]);

        return redirect('user-management')->with('status', 'Successfully Pasaload/Transfer Points!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cashout_create($id)
    {
        $data = ['data' => User::where('id', $id)->first()];
        return view('user_management.cashout', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cashout(CashoutRequest $request, $id)
    {
        $user = User::find($id);
        $current_points = $user->points;
        $new_points = $current_points - $request->points;

        User_money_log::create([
            'user_id' => $id,
            'amount' => $request->points,
            'module' => 'cashout',
            'operation' => '-',
            'created_by' => Auth::user()->id
        ]);

        User::where('id', $id)->update(['points' => $new_points]);

        return redirect('user-management')->with('status', 'Successfully Cashout Points!');
    }

    public function search_user(Request $request)
    {
        $data = User::where('status', '1')
            ->where('is_admin', '0')
            ->where('id','!=', $request->except)
            ->where(function($q) use ($request) {
                $q->where('first_name', 'like', '%'.$request->search.'%')
                ->orWhere('last_name', 'like', '%'.$request->search.'%')
                ->orWhere('email', 'like', '%'.$request->search.'%');
            })
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();

        return response()->json($data, 200);
    }
}
