<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_money_log;
use Auth;
use App\DataTables\TransactionsDataTable;

class TransactionHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TransactionsDataTable $dataTable)
    {
        return $dataTable->render('transaction.transaction');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['data' => User_money_log::where('id', $id)->first()];

        return view('transaction.show', $data);
    }
}
