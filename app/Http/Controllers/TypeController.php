<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\Role;
use App\Http\Requests\TypeRequest;
use Auth;
use App\DataTables\TypesDataTable;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TypesDataTable $dataTable)
    {
        return $dataTable->render('types.types');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('types.create', ['roles' => Role::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TypeRequest $request)
    {
        $role = $request->role;
        $request = $request->except(['_token', 'role']);
        
        Type::create(array_merge($request, ['role_id' => $role,'created_by' => Auth::user()->id]));
        
        return redirect('types')->with('status', 'Successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['data' => Type::where('id', $id)->first()];

        return view('types.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'data' => Type::where('id', $id)->first(),
            'roles' => Role::all()
        ];

        return view('types.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TypeRequest $request, $id)
    {
        $role = $request->role;
        $request = $request->except(['_token', 'role']);
        
        Type::where('id', $id)->update(array_merge($request, ['role_id' => $role, 'updated_by' => Auth::user()->id]));

        return redirect('types')->with('status', 'Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Type::where('id', $id)->delete();

        return redirect('types')->with('status', 'Successfully Deleted!');
    }
}
