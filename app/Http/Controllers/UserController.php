<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Type;
use App\Access;
use App\User_access;
use Auth;
use App\DataTables\UsersDataTable;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('users.users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create', ['user_types' => Type::all(), 'access' => Access::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $access = $request['access'];
        $type = $request->user_type;
        $password = Hash::make($request->password);
        $request_f = $request->except(['_token', 'user_type', 'access', 'password', 'password_confirmation']);
        
        $user = User::create(array_merge($request_f, [
            'type_id' => $type,
            'password' => $password,
            'created_by' => Auth::user()->id
        ]));

        foreach($access as $m)
        {
            $data = [
                'user_id' => $user->id,
                'access_id' =>  $m,
                'created_by' =>  Auth::user()->id
            ];
            
            User_access::create($data);
        }
        
        return redirect('users')->with('status', 'Successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'data' => User::where('id', $id)->first(),
            'access' => User_access::where('user_id', $id)->with('access')->get()
        ];

        return view('users.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'data' => User::where('id', $id)->first(), 
            'user_types' => Type::all(), 
            'access' => Access::all(),
            'user_access' => User_access::where('user_id', $id)->get()
        ];

        return view('users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $access = $request['access'];
        $type = $request->user_type;
        $password = Hash::make($request->password);
        $request_f = $request->except(['_token', 'user_type', 'access', 'password', 'password_confirmation']);
        
        $to_update = [
            'type_id' => $type,
            'updated_by' => Auth::user()->id
        ];

        if(!empty($request->password) or !empty($request->password_confirmation))
            $to_update['password'] = $password;

        $user = User::where('id', $id)->update(array_merge($request_f, $to_update));

        User_access::where('user_id', $id)->delete();

        foreach($access as $m)
        {
            $data = [
                'user_id' => $id,
                'access_id' =>  $m,
                'created_by' =>  Auth::user()->id
            ];
            
            User_access::create($data);
        }

        return redirect('users')->with('status', 'Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id', $id)->delete();

        return redirect('users')->with('status', 'Successfully Deleted!');
    }

    // Other functions
    public function showPoints($id)
    {
        $points = User::where('id', $id)->first();

        return response()->json(['points' => $points->points], 200);
    }
}
