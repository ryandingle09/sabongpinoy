<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class BasicToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->api_token;

        $check = User::where('api_token', $token)->get()->first();

        if(is_null($check))
        {
            return response()->json(['message' => 'Api Token not registered.'], 401);
        }

        return $next($request);
    }
}
