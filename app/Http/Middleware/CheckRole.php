<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Role_module;
use App\Module;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $uam = [];
        $sub_uam = [];
        $route = $request->route()->getName();

        if(Auth::user()->is_super_admin == 0):
            if(Auth::user()->is_admin == 1):

                foreach(Auth::user()->type->role->role_modules as $um):
                    foreach($um->modules as $m):
                        $uam[] = $m->prefix;
                        $uam[] = $m->prefix.'-create';
                        $uam[] = $m->prefix.'-store';
                        $uam[] = $m->prefix.'-edit';
                        $uam[] = $m->prefix.'-update';
                        $uam[] = $m->prefix.'-destroy';
                        $uam[] = $m->prefix.'-show';
                        $uam[] = $m->prefix.'-show-list';
                        $uam[] = $m->prefix.'-pasaload';
                        $uam[] = $m->prefix.'-cashout';
                        $uam[] = $m->prefix.'-loading';
                        $uam[] = $m->prefix.'-get-user';
                        $uam[] = $m->prefix.'-activate';
                    endforeach;
                endforeach;
            
            else:

                foreach(\App\Role::where('prefix', 'player')->first()->role_modules as $um):
                    foreach($um->modules as $m):
                        $uam[] = $m->prefix;
                        $uam[] = $m->prefix.'-create';
                        $uam[] = $m->prefix.'-store';
                        $uam[] = $m->prefix.'-edit';
                        $uam[] = $m->prefix.'-update';
                        $uam[] = $m->prefix.'-destroy';
                        $uam[] = $m->prefix.'-show';
                        $uam[] = $m->prefix.'-show-list';
                        $uam[] = $m->prefix.'-pasaload';
                        $uam[] = $m->prefix.'-cashout';
                        $uam[] = $m->prefix.'-loading';
                        $uam[] = $m->prefix.'-get-user';
                        $uam[] = $m->prefix.'-activate';
                    endforeach;
                endforeach;
                
            endif;

            if (!in_array($route, $uam))
                abort(403, 'Unauthorized action.');
            
        endif;

        return $next($request);
    }
}
