<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['name', 'location','expected_deal', 'event_date', 'is_broadcast', 'url', 'description','cover_image', 'cover_image_full_path', 'created_by', 'updated_by'];

    public function arena()
    {
        return $this->hasOne('App\Arena', 'id', 'location');
    }
}
