<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arena extends Model
{
    protected $fillable = ['name', 'location', 'created_by', 'updated_by', 'created_at', 'updated_at'];
}
