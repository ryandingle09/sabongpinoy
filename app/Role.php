<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name', 'prefix', 'description', 'created_by', 'updated_by'];

    public function role_modules()
    {
        return $this->hasMany('App\Role_module', 'role_id');
    }

    public function updated_who()
    {
        return $this->hasOne('App\User', 'id', 'updated_by');
    }

    public function created_who()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
