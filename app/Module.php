<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = ['name', 'prefix', 'icon', 'description', 'order', 'created_by', 'updated_by'];

    public function updated_who()
    {
        return $this->hasOne('App\User', 'id', 'updated_by');
    }

    public function created_who()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
