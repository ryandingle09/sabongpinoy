<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Auth;
use DB;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->filterColumn('user_type', function($query, $keyword) {
                //for mysql server
                $query->whereRaw("LOWER(types.name) like ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', function($item){
                return view('users.buttons.action', ['item' => $item, 'username' => $item->email]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\UsersDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        $data = $model->leftjoin('types', 'types.id', 'users.type_id')
                    ->select([
                        'users.id', 
                        'users.email', 
                        DB::raw("IF(users.is_admin='1' ,'N/A', users.points) AS points"),
                        'users.first_name', 
                        'users.last_name',
                        DB::raw("IF(users.status='1' ,'Active', 'Inactive') AS status"),
                        DB::raw("IF(users.is_super_admin='1' ,'Super Admin', IF(users.is_admin='1', types.name, 'Player')) AS user_type"),
                        'users.created_at', 
                        'users.updated_at'
                    ]);

        if(Auth::user()->is_super_admin == 0)
            $data = $model->leftjoin('types', 'types.id', 'users.type_id')
                    ->select([
                        'users.id', 
                        'users.email', 
                        DB::raw("IF(users.is_admin='1' ,'N/A', users.points) AS points"),
                        'users.first_name', 
                        'users.last_name',
                        DB::raw("IF(users.status='1' ,'Active', 'Inactive') AS status"),
                        DB::raw("IF(users.is_super_admin='1' ,'Super Admin', IF(users.is_admin='1', types.name, 'Player')) AS user_type"),
                        'users.created_at', 
                        'users.updated_at'
                    ])->where('users.is_super_admin', '0');

        return $data;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('usersdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        //Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // Column::make('id'),
            Column::make('email'),
            Column::make('points'),
            Column::make('first_name'),
            Column::make('last_name'),
            Column::make('status'),
            Column::make('user_type'),
            Column::make('created_at'),
            Column::make('updated_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
