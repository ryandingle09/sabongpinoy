<?php

namespace App\DataTables;

use App\User_money_log;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Auth;
use DB;

class TransactionsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->filterColumn('points', function($query, $keyword) {
                //for mysql server
                $query->whereRaw("users.points like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('name', function($query, $keyword) {
                //for mysql server
                $query->whereRaw("CONCAT(users.first_name,' ', users.last_name) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('from', function($query, $keyword) {
                //for mysql server
                $query->whereRaw("CONCAT(users2.first_name,' ', users2.last_name) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('to', function($query, $keyword) {
                //for mysql server
                $query->whereRaw("CONCAT(users3.first_name,' ', users3.last_name) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('performed_by', function($query, $keyword) {
                //for mysql server
                $query->whereRaw("CONCAT(users4.first_name,' ', users4.last_name) like ?", ["%{$keyword}%"]);
            })
            // ->filterColumn('to', function($query, $keyword) {
            //     //for mysql server
            //     $query->whereRaw("LOWER(roles.name) like ?", ["%{$keyword}%"]);
            // })
            ->addColumn('action', function($item){
                return view('transaction.buttons.action', ['item' => $item, 'username' => $item->email]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\TransactionsDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User_money_log $model)
    {
        return $model->leftjoin('users as users', 'users.id', 'user_money_logs.user_id')
            ->leftjoin('users as users2', 'users2.id', 'user_money_logs.from')
            ->leftjoin('users as users3', 'users3.id', 'user_money_logs.to')
            ->leftjoin('users as users4', 'users4.id', 'user_money_logs.created_by')
            ->select([
                'user_money_logs.id', 
                'users.points',
                'user_money_logs.amount', 
                'user_money_logs.operation', 
                'user_money_logs.module', 
                DB::raw("IF(user_money_logs.from is null ,'N/A', CONCAT(users2.first_name,' ', users2.last_name)) AS `from`"),
                DB::raw("IF(user_money_logs.to is null ,'N/A', CONCAT(users3.first_name,' ', users3.last_name)) AS `to`"),
                DB::raw("IF(user_money_logs.created_by is null ,'N/A', CONCAT(users4.first_name,' ', users4.last_name)) AS `performed_by`"),
                'user_money_logs.created_at'
            ])
            ->where('users.id', Auth::user()->id)
            ->where('user_money_logs.module','!=', 'betting');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('transactionsdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        // Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('points'),
            Column::make('amount'),
            Column::make('module'),
            Column::make('operation'),
            Column::make('from'),
            Column::make('to'),
            // Column::make('performed_by'),
            Column::make('created_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Transactions_' . date('YmdHis');
    }
}
