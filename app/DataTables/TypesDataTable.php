<?php

namespace App\DataTables;

use App\Type;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TypesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            // ->filterColumn('role', function($query, $keyword) {
            //     //for mysql server
            //     $query->whereRaw("LOWER(roles.name) like ?", ["%{$keyword}%"]);
            // })
            ->addColumn('action', function($item){
                return view('types.buttons.action', ['item' => $item, 'username' => $item->email]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\TypesDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Type $model)
    {
        return $model->leftjoin('roles', 'roles.id', 'types.role_id')
                ->select([
                    'types.id', 
                    'types.name', 
                    'types.prefix', 
                    'types.description', 
                    'roles.name as role',
                    'types.created_at', 
                    'types.updated_at'
                ]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('typesdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        //Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('name'),
            Column::make('prefix'),
            Column::make('description'),
            Column::make('role'),
            Column::make('created_at'),
            Column::make('updated_at'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Types_' . date('YmdHis');
    }
}
