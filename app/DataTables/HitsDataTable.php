<?php

namespace App\DataTables;

use App\User_money_log;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use DB;
use Auth;

class HitsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->filterColumn('event', function($query, $keyword) {
                //for mysql server
                $query->whereRaw("events.name like ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', function($item){
                return view('hits.buttons.action', ['item' => $item, 'username' => $item->email]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\TransactionsDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User_money_log $model)
    {
        return $model->leftjoin('events as events', 'events.id', 'user_money_logs.event_id')
            ->select([
                'user_money_logs.id', 
                'user_money_logs.amount',
                'user_money_logs.earned', 
                'user_money_logs.type',
                'user_money_logs.game_count',
                'user_money_logs.odds',
                'events.name as event',
                'user_money_logs.status',
                'user_money_logs.created_at'
            ])->where(['user_money_logs.user_id' => Auth::user()->id, 'user_money_logs.status' => 'hit']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('transactionsdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        // Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('event'),
            Column::make('game_count'),
            Column::make('odds'),
            Column::make('type'),
            Column::make('amount'),
            Column::make('earned'),
            Column::make('status'),
            Column::make('created_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Hits_' . date('YmdHis');
    }
}
