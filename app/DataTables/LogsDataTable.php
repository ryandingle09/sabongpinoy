<?php

namespace App\DataTables;

use App\Log;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class LogsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->filterColumn('user', function($query, $keyword) {
                //for mysql server
                $query->whereRaw("CONCAT(users.first_name,' ',users.first_name) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('module', function($query, $keyword) {
                //for mysql server
                $query->whereRaw("modules.name like ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', function($item){
                return view('logs.buttons.action', ['item' => $item, 'username' => $item->email]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\LogsDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Log $model)
    {
        return $model->leftjoin('users', 'users.id', 'logs.user_id')
            ->leftjoin('modules', 'modules.id', 'logs.module_id')
            ->select([
                'logs.id', 
                'logs.user_id', 
                'logs.action', 
                'modules.name as module',
                'logs.new_data',
                'logs.old_data',
                'logs.created_at', 
                'logs.updated_at'
            ]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('logsdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        //Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('user'),
            Column::make('action'),
            Column::make('module'),
            Column::make('new_data'),
            Column::make('old_data'),
            Column::make('created_at'),
            Column::make('updated_at'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Logs_' . date('YmdHis');
    }
}
