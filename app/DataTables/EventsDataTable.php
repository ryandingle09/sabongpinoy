<?php

namespace App\DataTables;

use App\Event;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use DB;

class EventsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->filterColumn('location', function($query, $keyword) {
                //for mysql server
                $query->whereRaw("arenas.name like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('location', function($query, $keyword) {
                //for mysql server
                $query->whereRaw("arenas.location like ?", ["%{$keyword}%"]);
            })
            ->addColumn('cover_image', function($item){
                return view('event.custom.thumb', ['item' => $item]);
            })
            ->addColumn('action', function($item){
                return view('event.buttons.action', ['item' => $item, 'username' => $item->email]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\EventsDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Event $model)
    {
        return $model->leftjoin('arenas', 'arenas.id', 'events.location')
            ->select([
                'events.id', 
                'events.name', 
                DB::raw("arenas.name AS location"),
                'events.expected_deal',
                'events.event_date',
                'events.is_broadcast',
                'events.url',
                'events.cover_image',
                'events.description', 
                'events.created_at', 
                'events.updated_at'
            ]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('eventsdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        //Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('name'),
            Column::make('location'),
            Column::make('expected_deal'),
            Column::make('event_date'),
            Column::make('is_broadcast'),
            Column::make('created_at'),
            Column::make('updated_at'),
            Column::computed('cover_image')
                  ->exportable(true)
                  ->printable(true)
                  ->width(60)
                  ->addClass('text-center'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Events_' . date('YmdHis');
    }
}
