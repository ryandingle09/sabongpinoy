<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game_betting extends Model
{
    protected $fillable = ['user_id', 'amount', 'action', 'event_id', 'game_count', 'odds', 'type', 'created_at', 'created_by', 'updated_by'];
}
