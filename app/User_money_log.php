<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_money_log extends Model
{
    protected $fillable = ['user_id', 'amount', 'module','operation', 'to', 'from', 'action', 'event_id', 'game_count', 'odds', 'type', 'status', 'created_by', 'updated_by'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function user_from()
    {
        return $this->hasOne('App\User', 'id', 'from');
    }

    public function user_to()
    {
        return $this->hasOne('App\User', 'id', 'to');
    }

    public function performed_by()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function event()
    {
        return $this->hasOne('App\Event', 'id', 'event_id');
    }
}
