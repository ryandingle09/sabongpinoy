<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMoneyLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_money_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->float('amount', 8, 2);
            $table->string('module')->comment('action for pasaload, loading , betting, plasada, etc..');
            $table->string('operation', 1)->comment('pasaload(+,-), loading(+) or cashout(-)');
            $table->string('from')->nullable()->comment('from user if pasaload/transfer');
            $table->string('to')->nullable()->comment('to user if pasaload/transfer');
            $table->timestamps();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_money_logs');
    }
}
