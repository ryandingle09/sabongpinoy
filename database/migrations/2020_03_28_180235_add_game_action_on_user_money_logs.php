<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGameActionOnUserMoneyLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_money_logs', function ($table) {
            $table->string('action')->after('to')->nullable()->default(null);
            $table->bigInteger('event_id')->after('action')->nullable()->default(null);
            $table->integer('game_count')->after('event_id')->nullable()->default(null);
            $table->string('odds')->after('game_count')->nullable()->default(null);
            $table->string('type')->after('odds')->nullable()->default(null);
            $table->float('earned', 8, 2)->after('type')->nullable()->default(null);
            $table->string('status')->after('type')->nullable()->default(null)->comment('if win=hit or loose=miss');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
