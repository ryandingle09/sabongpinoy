<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameBettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_bettings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->float('amount', 8, 2);
            $table->string('action')->nullable()->default(null);
            $table->bigInteger('event_id')->nullable()->default(null);
            $table->integer('game_count')->nullable()->default(null);
            $table->string('odds')->nullable()->default(null);
            $table->string('type')->nullable()->default(null);
            $table->timestamps();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_bettings');
    }
}
