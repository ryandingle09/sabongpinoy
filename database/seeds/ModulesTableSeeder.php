<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            [
                'name' => 'Users',
                'prefix' => 'user',
                'description' => 'User Module',
                'icon' => 'fas fa-users',
                'order' => 1
            ],
            [
                'name' => 'User Types',
                'prefix' => 'type',
                'description' => 'User Type Module',
                'icon' => 'fas fa-users-cog',
                'order' => 2
            ],
            [
                'name' => 'Roles',
                'prefix' => 'role',
                'description' => 'Roles Module',
                'icon' => 'fas fa-user-shield',
                'order' => 3
            ],
            [
                'name' => 'Access Rights',
                'prefix' => 'access',
                'description' => 'Access Rights Module',
                'icon' => 'fas fa-shield-alt',
                'order' => 4
            ],
            [
                'name' => 'Modules',
                'prefix' => 'module',
                'description' => 'Modules Module',
                'icon' => 'fas fa-folder',
                'order' => 5
            ],
            [
                'name' => 'Event Setup',
                'prefix' => 'game-event',
                'description' => 'Event Setup Module',
                'icon' => 'fas fa-video',
                'order' => 6
            ],
            [
                'name' => 'Cashout/Loading/Transfer',
                'prefix' => 'player-management',
                'description' => 'Player Management Module for Cashout, Pasaload/Transfer and Loading Points',
                'icon' => 'far fa-user-circle',
                'order' => 7
            ],
            [
                'name' => 'Betting history',
                'prefix' => 'betting',
                'description' => 'Betting History Module',
                'icon' => 'fas fa-history',
                'order' => 8
            ],
            [
                'name' => 'Arena',
                'prefix' => 'arena',
                'description' => 'Arena Module',
                'icon' => 'fas fa-location-arrow',
                'order' => 9
            ],
            [
                'name' => 'Activation',
                'prefix' => 'activation',
                'description' => 'Activation Module',
                'icon' => 'fas fa-screwdriver',
                'order' => 10
            ],
            [
                'name' => 'Accounting',
                'prefix' => 'accounting',
                'description' => 'Accounting Module',
                'icon' => 'fas fa-money-check',
                'order' => 11
            ],
            [
                'name' => 'Hits Checker',
                'prefix' => 'hits',
                'description' => 'Hits Checker History',
                'icon' => 'fas fa-list',
                'order' => 12
            ],
            [
                'name' => 'Transaction History',
                'prefix' => 'history',
                'description' => 'Transaction history Module',
                'icon' => 'fas fa-list',
                'order' => 13
            ]
            // [
            //     'name' => 'Logs',
            //     'prefix' => 'log',
            //     'description' => 'Log/Audit Trail Module',
            //     'icon' => 'far fa-eye',
            //     'order' => 6
            // ]
        ]);
    }
}
