<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            [
                'name' => 'Admin',
                'prefix' => 'admin',
                'description' => 'Admin user Type',
                'role_id' => 1
            ],
            [
                'name' => 'CSR',
                'prefix' => 'csr',
                'description' => 'CSR user Type',
                'role_id' => 2
            ]
        ]);
    }
}
