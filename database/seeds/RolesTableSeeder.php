<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'Admin',
                'prefix' => 'admin',
                'description' => 'Admin user role',
            ],
            [
                'name' => 'CSR',
                'prefix' => 'csr',
                'description' => 'CSR user role',
            ],
            [
                'name' => 'Player',
                'prefix' => 'player',
                'description' => 'player user role, for module view',
            ]
        ]);
    }
}
